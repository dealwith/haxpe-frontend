# Customer order flow

1. We are using lib called [react-steps-builder](https://github.com/sametweb/react-step-builder)
2. We setup initial state into `Context` for other components that are included into `Steps` in [OrderCustomerForm](src/components/Forms/OrderCustomerForm/OrderCustomerForm.tsx) and [OrderCustomerCreated](src/components/Order/Customer/Created/OrderCustomerCreated.tsx) components
3. Changing of the state on the fly is happend with [useSocket](src/hooks/useProvideSocket/useProvideSocket.ts)
4. If the user reload the page, we send him to the right step of the order within
`jumpToCorrectStep` and `API call` on [OrderCustomerForm](src/components/Forms/OrderCustomerForm/OrderCustomerForm.tsx)
