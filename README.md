# Haxpe project

### Installation
1. yarn version `1.22.5`
2. `yarn install`
3. `eslint, prettier` installed in your favourite editor
4. Ask for `.env` file to access external API

### Project start
To start developing project use command

```bash
yarn start
````

Be aware that it is used for developing application on a MacOS

For more info you can go to : https://create-react-app.dev/docs/using-https-in-development/

> **Warning:** Don't turn off HTTPS for development, it's needed for some of the 3rd partie services like google API


### Diagrams for the project
https://drive.google.com/drive/folders/1lgu7D27lXvCyZQ8Y1FC8-CP8HG0GKUSN?usp=sharing
