/* eslint-disable linebreak-style */
const proxy = require('express-http-proxy');
const express = require('express');
const app = express();
const port = 3000;

app.use('/', proxy('94.237.100.88:8081', {
	filter: req => {
		return new Promise(function (resolve) {
			resolve(req.path.indexOf('/api') >= 0);
		});
	},
}));
app.use(express.static('build'));

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`);
});
