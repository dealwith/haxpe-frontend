const EURO = '\u20AC';

type TCurrency = {
	EURO: string;
}

export const CURRENCY: TCurrency = {
	EURO,
};
