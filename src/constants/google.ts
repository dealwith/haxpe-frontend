import { TCoords } from 'interfaces/coords';

const GOOGLE_API_MAP = 'https://maps.googleapis.com';

export const GOOGLE_LINKS = {
	GEOCODE_API: `${GOOGLE_API_MAP}/maps/api/geocode/json`,
	DISTANCE_MATRIX_API: (origins: TCoords, destinations: TCoords): string => (
		`${GOOGLE_API_MAP}/maps/api/distancematrix/json?units=imperial&origins=Washington,DC&destinations=New+York+City,NY&key=${process.env.REACT_APP_GOOGLE_MAP_API_KEY}`
	),
	EMBEDED_MAP_DIRECTION_API: (origins: TCoords, destination: TCoords): string => (
		`https://www.google.com/maps/embed/v1/directions?key=AIzaSyAVU0YoYelBsOesEZTh7JeHV589RGD0fQw&origin=${origins.lat},${origins.lon}&destination=${destination.lat},${destination.lon}&mode=driving`
	),
};
