const signUpBaseRoute = '/sign-up';
const baseOrderRoute = '/order';
const couponsBaseRoute = '/coupons';
const billingBaseRoute = '/billing';
const userSettingsBaseRoute = '/settings';
const paymentBaseRoute = 'payment';
const settingsPaymentMethodBaseRoute = `${userSettingsBaseRoute}/${paymentBaseRoute}`;

export const ROUTES = {
	HOME: '/',
	ABOUT_US: '/about-us',
	SERVICES: '/services',
	PRICING: '/pricing',
	CONTACT: '/contact',
	SIGN_IN: '/sign-in',
	RESTORE: '/restore',
	INVOICES: '/invoices',
	SETTINGS: '/settings',
	TERMS_AND_CONDITIONS: '/terms-and-conditions',
	PRIVACY_POLICY: '/privacy-policy',
	PROFILE: '/profile',
	CHAT: '/chat',
	IMPRINT: '/imprint',
	LOGOUT: '/logout',
	FORBIDDEN: '/forbidden',
	WAITING_LIST: '/waiting-list',
	ADMIN: '/admin',
	GUEST: '/guest',
	USER_SETTINGS: {
		ORIGIN: userSettingsBaseRoute,
		CHANGE_PASSWORD: `${userSettingsBaseRoute}/change-password`,
		CHANGE_ACCOUNT_DETAILS: `${userSettingsBaseRoute}/change-account-details`,
		DELETE_ACCOUNT: `${userSettingsBaseRoute}/delete-account`,
		SET_UP_PAYMENT_METHOD: {
			ORIGIN: settingsPaymentMethodBaseRoute,
		},
	},
	BILLING: {
		INVOICE: `${billingBaseRoute}-invoice`,
		PAYOUT_OVERVIEW_PDF_AS_LINK: (id: string): string => `/payout-overview-pdf/${id}`,
		PAYOUT_OVERVIEW_PDF_AS_ROUTE: `/payout-overview-pdf/:payoutId`,
		SHOW_INVOICE_PDF_AS_LINK: (id: string): string => `/show-invoice-pdf/${id}`,
		SHOW_INVOICE_PDF_AS_ROUTE: `/show-invoice-pdf/:invoiceId`,
	},
	SIGN_UP: {
		URL: signUpBaseRoute,
		CUSTOMER: `${signUpBaseRoute}/customer`,
		PARTNER: `${signUpBaseRoute}/partner`,
	},
	ORDER: {
		ORIGIN: baseOrderRoute,
		CUSTOMER: `${baseOrderRoute}/customer`,
		ACTIVE: `${baseOrderRoute}/active`,
		CURRENT: `${baseOrderRoute}/current`,
	},
	INVOICE: {
		PDF_AS_LINK: (id: string): string => `/invoice-pdf/${id}`,
		PDF_AS_ROUTE: `/invoice-pdf/:orderId`,
		SHOW_PICTURES_AS_LINK: (id: string): string => `/show-pictures/${id}`,
		SHOW_PICTURES_AS_ROUTE: `/show-pictures/:orderId`,
	},
	COUPONS: {
		ORIGIN: couponsBaseRoute,
		CREATE: `${couponsBaseRoute}/create`,
		EDIT: `${couponsBaseRoute}/:id`,
	},
	PAYMENT: {
		ORIGIN: paymentBaseRoute,
		METHODS: {
			VISA: `${paymentBaseRoute}/visa`,
			MASTER_CARD: `${paymentBaseRoute}/master-card`,
			PAY_PAL: `${paymentBaseRoute}/pay-pal`,
			APPLE_PAY: `${paymentBaseRoute}/apple-pay`,
			GOOGLE_PAY: `${paymentBaseRoute}/google-pay`,
			KLARNA: `${paymentBaseRoute}/klarna`,
			CASH: `${paymentBaseRoute}/cash`,
		},
	},
};
