const BASE_URL = '/api/v1';
const BASE_ACCOUNT_URL = `${BASE_URL}/account`;
const BASE_LOGIN_URL = `${BASE_ACCOUNT_URL}/login`;
const BASE_CUSTOMER_URL = `${BASE_URL}/customer`;
const BASE_INDUSTRY_URL = `${BASE_URL}/industry`;
const BASE_ORDER_URL = `${BASE_URL}/order`;
const BASE_COUPON_URL = `${BASE_URL}/coupon`;
const BASE_SERVICE_TYPE_URL = `${BASE_URL}/service-type`;
const BASE_PARTNER_URL = `${BASE_URL}/partner`;
const BASE_USER_URL = `${BASE_URL}/user`;
const BASE_ADDRESS_URL = `${BASE_URL}/address`;
const BASE_WORKER_URL = `${BASE_URL}/worker`;
const BASE_STATISTICS_URL = `${BASE_URL}/statistics`;
const BASE_INVOICE_URL = `${BASE_URL}/invoice`;

export const URLS = {
	BASE_URL,
	ACCOUNT: {
		ORIGIN: BASE_ACCOUNT_URL,
		REGISTER: `${BASE_ACCOUNT_URL}/register`,
		LOGIN: BASE_LOGIN_URL,
		LOGIN_FACEBOOK: `${BASE_LOGIN_URL}-facebook`,
		LOGIN_GOOGLE: `${BASE_LOGIN_URL}-google`,
		PROFILE: `${BASE_ACCOUNT_URL}/profile`,
		LOGOUT: `${BASE_ACCOUNT_URL}/logout`,
		WS_TOKEN: `${BASE_ACCOUNT_URL}/websocket-token`,
		SET_PASSWORD: `${BASE_ACCOUNT_URL}/set-password`,
		ACTIVATE: (id: string): string => `${BASE_ACCOUNT_URL}/${id}/activate`,
		DEACTIVATE: (id: string): string => `${BASE_ACCOUNT_URL}/${id}/deactivate`,
		ID: (id: string): string => `${BASE_ACCOUNT_URL}/${id}`,
		SET_NEW_PASSWORD: (email: string): string => `${BASE_ACCOUNT_URL}/set-new-password?email=${email}`,
	},
	ADDRESS: {
		ORIGIN: BASE_ADDRESS_URL,
		ID: (id: string): string => `${BASE_ADDRESS_URL}/${id}`,
	},
	CUSTOMER: {
		ORIGIN: BASE_CUSTOMER_URL,
		INFO: `${BASE_CUSTOMER_URL}/info`,
		PAGE: `${BASE_CUSTOMER_URL}/page`,
		ID: (id: string | number): string => `${BASE_CUSTOMER_URL}/${id}`,
	},
	SERVICE_TYPE: {
		ORIGIN: BASE_SERVICE_TYPE_URL,
		ALL: `${BASE_SERVICE_TYPE_URL}/get-all`,
		ID: (id: string | number): string => `${BASE_SERVICE_TYPE_URL}/${id}`,
	},
	INDUSTRY: {
		ORIGIN: BASE_INDUSTRY_URL,
		ALL: `${BASE_INDUSTRY_URL}/get-all`,
		ID: (id: number): string => `${BASE_INDUSTRY_URL}/${id}`,
	},
	ORDER: {
		ORIGIN: BASE_ORDER_URL,
		PAGE: `${BASE_ORDER_URL}/page`,
		CREATE: `${BASE_ORDER_URL}/create`,
		APPLY_COUPON: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/apply-coupon`,
		START: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/start`,
		PAUSE: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/pause`,
		COMPLETE: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/complete`,
		RESUME: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/resume`,
		CONFIRM: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/confirm`,
		CANCEL: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/cancel`,
		ID: (id: string): string => `${BASE_ORDER_URL}/${id}`,
		ASSIGN_WORKER: (orderId: string, workerId: string): string => (
			`${BASE_ORDER_URL}/${orderId}/assign-worker/${workerId}`
		),
		ADD_ARRIVAL_TIME: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/set-arrival`,
		ARRIVED_AT_CUSTOMER: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/set-arrived`,
		COMPLETE_WORK: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/complete-work`,
		CUSTOMER_REJECT: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/customer-reject`,
		WORKER_REJECT: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/worker-reject`,
		INVOICE: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/invoice`,
		PAYMENT_START: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/payment-start`,
		RATE: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/rate`,
		SEND_INVOICE: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/send-invoice`,
		REDUCTION_AS_QUERY: (orderId: string, reduction: number): string =>
			`${BASE_ORDER_URL}/${orderId}/reduction?reduction=${reduction}`,
		REPORT_AS_QUERY: (orderId: string, report: string): string =>
			`${BASE_ORDER_URL}/${orderId}/report?report=${report}`,
		EXTRA_SERVICES: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/extra-services`,
		IMAGES: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/image`,
		IMAGES_BY__ORDER_ID: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/image`,
		IMAGE_BY_IMAGE_ID: (
			orderId: string,
			imageId: string,
		): string => `${BASE_ORDER_URL}/${orderId}/image/${imageId}`,
		HOUR_RATE: `${BASE_ORDER_URL}/hour-rate`,
		PAYMENT_CREATE: (orderId: string): string => `${BASE_ORDER_URL}/${orderId}/payment-create`,
		PAYMENT_CAPTURE: (orderId: string, orderPaymentId: string): string => `${BASE_ORDER_URL}/${orderId}/payment-capture?orderPaymentId=${orderPaymentId}`,
	},
	USER: {
		ORIGIN: BASE_USER_URL,
		ID: (id: string): string => `${BASE_USER_URL}/${id}`,
	},
	WORKER: {
		ORIGIN: BASE_WORKER_URL,
		PAGE: `${BASE_WORKER_URL}/page`,
		CREATE: `${BASE_WORKER_URL}/create-full`,
		INFO: `${BASE_WORKER_URL}/info`,
		ID: (id: string): string => `${BASE_WORKER_URL}/${id}`,
		SET_LOCATION: (workerId: string): string => `${BASE_WORKER_URL}/${workerId}/location/set`,
	},
	PARTNER: {
		ORIGIN: BASE_PARTNER_URL,
		PAGE: `${BASE_PARTNER_URL}/page`,
		INFO: `${BASE_PARTNER_URL}/info`,
		ID: (id: string): string => `${BASE_PARTNER_URL}/${id}`,
	},
	STATISTICS: {
		ORDER: `${BASE_STATISTICS_URL}/order`,
		PARTNER: `${BASE_STATISTICS_URL}/partner`,
		CUSTOMER: `${BASE_STATISTICS_URL}/customer`,
		WORKER: `${BASE_STATISTICS_URL}/worker`,
	},
	COUPONS: {
		ORIGIN: BASE_COUPON_URL,
		PAGE: `${BASE_COUPON_URL}/page`,
		ID: (id: string): string => `${BASE_COUPON_URL}/${id}`,
	},
	INVOICE: {
		PAGE: `${BASE_INVOICE_URL}/page`,
		ID_AS_QUERY: (id: string): string => `${BASE_INVOICE_URL}?orderId=${id}`,
	},
};
