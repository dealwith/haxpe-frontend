import { createContext } from 'react';

export const PDFDownloadHandlerContext = createContext(null);
