import { createContext } from 'react';

export const AfterWorkCompletedContext = createContext(null);
