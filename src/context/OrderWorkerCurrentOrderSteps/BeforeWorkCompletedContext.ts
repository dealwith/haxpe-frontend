import { createContext } from 'react';

export const BeforeWorkCompletedContext = createContext(null);
