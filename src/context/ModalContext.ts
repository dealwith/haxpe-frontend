import { createContext } from 'react';

import { useProvideModalContext } from 'hooks';

type ContextType = ReturnType<typeof useProvideModalContext>;

const initialState: ContextType = {
	toggleModal: () => {/* do nothing */},
	modal: {
		isOpen: false,
		text: '',
		isBlocking: false,
	},
	setModal: () => {/* do nothing */},
};

export const ModalContext = createContext<ContextType>(initialState);
