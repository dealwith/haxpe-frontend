import { createContext } from 'react';

export const EditableFieldsContext = createContext(null);
