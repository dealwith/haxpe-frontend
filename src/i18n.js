import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import Backend from 'i18next-locize-backend';

const locizeOptions = {
	projectId: process.env.REACT_APP_LOCIZE_PROJECT_ID,
	apiKey: process.env.REACT_APP_LOCIZE_API_KEY,
	referenceLng: 'en',
};

const isAdmin = window.location.pathname.includes('admin');

i18n
	.use(Backend)
	.use(LanguageDetector)
	.use(initReactI18next)
	.init({
		debug: false,
		fallbackLng: 'en-US',
		saveMissing: true,
		backend: locizeOptions,
		...(isAdmin && {react: {
			useSuspense: false,
		}}),
	});

export default i18n;
