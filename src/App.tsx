import { FunctionComponent, lazy } from 'react';
import { useLocation } from 'react-router-dom';

import {
	AdminProviders,
	AppProviders,
	GlobalProviders,
	Notification,
	UserApp,
} from 'components';

const Admin = lazy(() => import('lazyImports/lazyAdmin'));

export const App: FunctionComponent = () => {
	// useEffect(() => {
	// 	/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
	// 	/*@ts-ignore*/
	// 	window.OneSignal = window.OneSignal || [];

	// 	/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
	// 	/*@ts-ignore*/
	// 	const { OneSignal } = window;
	// 	const oneSignalSDKUpdaterPath = '/push/onesignal/OneSignalSDKWorker.js';
	// 	const isOneSignalRegistered = OneSignal.SERVICE_WORKER_PATH === oneSignalSDKUpdaterPath;

	// 	if (!isOneSignalRegistered) {
	// 		const initConfig = {
	// 			appId: '983a53be-4a32-4622-9418-a6c4187b3641',
	// 			notifyButton: {
	// 				enable: true,
	// 			},
	// 		};

	// 		OneSignal.push(() => {
	// 			OneSignal.SERVICE_WORKER_PARAM = { scope: '/push/onesignal/' };
	// 			OneSignal.SERVICE_WORKER_PATH = '/push/onesignal/OneSignalSDKWorker.js';
	// 			OneSignal.SERVICE_WORKER_UPDATER_PATH = oneSignalSDKUpdaterPath;
	// 			OneSignal.init(initConfig);
	// 		});
	// 	}
	// }, []);

	const { pathname } = useLocation();
	const isAdminPath = pathname.includes('admin');

	return (
		<GlobalProviders>
			{isAdminPath
				? <AdminProviders>
					<Admin/>
				</AdminProviders>
				: <AppProviders>
					<UserApp />
				</AppProviders>
			}
			<Notification/>
		</GlobalProviders>
	);
};
