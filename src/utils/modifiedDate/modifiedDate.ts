import { TDate, TDateFormat } from 'interfaces';

type TCreateDateToShowArgs = {
	dateArg: TDate;
	splitArg?: string;
	format?: TDateFormat;
	exclude?: 'm:s' | 's';
	isFull?: boolean;
	isHours?: boolean;
	isMinutes?: boolean;
	isSeconds?: boolean;
	isFullYear?: boolean;
}

type TCreateTimeToShowArgs = {
	durationSecond: number | string;
	splitArg?: string;
	exclude?: 'm:s' | 's';
}

export class ModifiedDate extends Date {
	year: number;
	month: number;
	dayInMonth: number;
	dayInWeek: number;
	hours: number;
	minutes: number;
	seconds: number;
	fullDate: Date;
	weekStart: number;
	lastDayInMonth: number;
	firstDayInMonth: number;

	constructor(date: TDate = new Date()) {
		super();
		this.setSomeDate(date);
	}

	setSomeDate(date: TDate): void {
		this.fullDate = new Date(date);
		this.year = this.fullDate.getFullYear();
		this.month = this.fullDate.getMonth();
		this.dayInMonth = this.fullDate.getDate();
		this.hours = this.fullDate.getHours();
		this.minutes = this.fullDate.getMinutes();
		this.seconds = this.fullDate.getSeconds();
		this.firstDayInMonth = 1;

		const newDate = new Date(this.fullDate);

		newDate.setMonth(this.month + 1);
		newDate.setDate(0);
		this.lastDayInMonth = newDate.getDate();

		this.dayInWeek = this.fullDate.getDay();

		if (!this.dayInWeek)
			this.dayInWeek = 6;

		this.weekStart = this.dayInMonth - this.dayInWeek;
	}

	createDateToShow = ({
		dateArg,
		splitArg = '/',
		format = 'd:m:y',
		isFull = false,
		exclude,
		isFullYear = true,
	}: TCreateDateToShowArgs): string => {
		if (!dateArg)
			return;

		const date = typeof dateArg === 'string' || typeof dateArg === 'number'
			? new Date(dateArg)
			: dateArg;

		let day: string | number = date.getDate();
		let month: string | number = date.getMonth();
		let year: string | number = date.getFullYear();

		if (!isFullYear)
			year = year.toString().slice(2);

		if (day < 10)
			day = `0${day}`;

		month = month < 9 ? `0${month + 1}` : `${month + 1}`;

		const formatArray = format.split(':');

		const dateForSort = {
			'y': year,
			'm': month,
			'd': day,
		};

		const sortedDate = formatArray.map(format => {
			return dateForSort[format];
		});

		let currentDate = sortedDate.join(splitArg);

		if (isFull) {
			const durationSecond = date.getHours() * 3600
			+ date.getMinutes() * 60
			+ date.getSeconds();

			currentDate = `${currentDate} - ${this.createTimeToShow({durationSecond, exclude})}`;
		}

		return currentDate;
	}

	createTimeToShow ({
		durationSecond,
		splitArg = ':',
		exclude,
	}: TCreateTimeToShowArgs): string {
		if (!durationSecond)
			return `00${splitArg}00${splitArg}00`;

		if (typeof durationSecond === 'string') {
			const splittedTime = durationSecond.match(/\d{2}:\d{2}:\d{2}/g);

			return splittedTime[0];
		}

		let seconds = `${Math.floor(durationSecond % 60)}`;
		let minutes = `${Math.floor((durationSecond / 60) % 60)}`;
		let hours = `${Math.floor(durationSecond / 3600)}`;

		seconds = seconds.length === 1 ? `0${seconds}` : seconds;
		minutes = minutes.length === 1 ? `0${minutes}` : minutes;
		hours = hours.length === 1 ? `0${hours}` : hours;

		let time = [hours, minutes, seconds].join(splitArg);

		if (exclude) {
			switch (exclude) {
				case 'm:s':
					time = time.slice(0, 2);

					break;

				case 's':
					time = time.slice(0, 5);

					break;
				default:
					break;
			}
		}

		return time;
	}
}
