/**
 * Function create items for select
 *
 * @params items {string} - array of items to convert
 */

type TResult = {
	value: string;
	label: string;
}

export const createSelectItems = (
	items: string[],
): TResult[] => items.map(item => ({
	value: item.toLowerCase(),
	label: item,
}));
