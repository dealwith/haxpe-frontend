import { IIndustry, TIndustryResult } from 'interfaces';

export const getIndustry = (
	industries:
	TIndustryResult,
	industryId: number,
): IIndustry => {
	return industries.find(({ id }) => id === industryId);
};
