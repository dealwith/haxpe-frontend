type TObj = { types: string[] };

/**
 * Function help to udentify needed types in array of types inside object
 *
 * @param {{types: []}} - passed object with a bunch of types inside
 * @param type {string} - type to search in types array
 */

export const isObjIncludesType = (obj: TObj, type: string): boolean => obj.types.includes(type);
