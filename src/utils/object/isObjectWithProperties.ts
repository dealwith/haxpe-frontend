export const isObjectWithProperties = (object: Record<any, any>): boolean => {
	return object ?? typeof object === 'object'
		? !Array.isArray(object) && !!Object.keys(object).length
		: false;
};
