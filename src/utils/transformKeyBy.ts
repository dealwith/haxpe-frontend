import { Dictionary } from 'interfaces';

export 	const transformKeyBy = <T>(objectArray: T[], objectKey: string): Dictionary<unknown> => {
	return objectArray.reduce((acc: Record<string, unknown>, object: T) => {
		const key: string = object[objectKey] as string;

		if (key) {
			return {
				...acc,
				[key]: {...object},
			};
		}

		return acc;
	}, {});
};
