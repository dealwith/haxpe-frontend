import { getLocaleStorageItem } from './getLocaleStorageItem';

export const getPreferLang = (): string => getLocaleStorageItem('i18nextLng');
