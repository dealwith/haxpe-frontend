export const getLocaleStorageItem = (key: string): string => localStorage.getItem(key);
