import { TServicesTypeResult, IServiceTypesId } from 'interfaces';

export const getFilteredServices = (
	allServices: TServicesTypeResult,
	workerServiceTypes: IServiceTypesId[],
): TServicesTypeResult => {
	const services: TServicesTypeResult = [];

	workerServiceTypes.forEach(({ serviceTypeId }) => {
		const workerService = allServices.find(({ id }) => id === serviceTypeId);

		services.push(workerService);
	});

	return services;
};
