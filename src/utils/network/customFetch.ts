import { isObjectWithProperties } from 'utils';
import { URLS } from 'constants/urls';
import { TResponse } from 'interfaces';
import { ICustomFetchGetOptions } from './interfaces';

export class CustomFetch {
	static get<T>(
		input: string,
		options?: ICustomFetchGetOptions,
	): Promise<T> {
		const myHeaders = new Headers();
		const queryParams =  options?.queryParams;
		const isExternal =  options?.isExternal;
		const resourceLocation = isExternal
			? input
			: document.location.origin + input;
		const url = new URL(resourceLocation);

		myHeaders.append('Access-Control-Allow-Origin', '*');

		const setUrlParams = (key: string): void => {
			url.searchParams.append(key, options.queryParams[key].toString());
		};

		const setUrlArrayParams = (key: string): void => {
			(queryParams[key] as string[]).forEach(searchParam => {
				url.searchParams.append(key, searchParam);
			});
		};

		if (isObjectWithProperties(queryParams)) {
			Object.keys(queryParams).forEach(key => {
				if (Array.isArray(queryParams[key]))
					setUrlArrayParams(key);
				else
					setUrlParams(key);
			});
		}

		const stringifiedURL = url.toString();

		return fetch(stringifiedURL)
			.then(res => {
				if (res.ok) {
					const getResponse = async () => {
						try {
							return await res.clone().json();
						} catch {
							const result = await res.blob();

							return URL.createObjectURL(result);
						}
					};

					return getResponse();
				}

				throw new Error(res.statusText);
			})
			.then(data => {
				if (data?.errorCode)
					throw new Error(data.errorCode);

				return data;
			});
	}

	static delete<T>(input: RequestInfo): Promise<T> {
		const url = new URL(document.location.origin + input);

		const stringifiedURL = url.toString();

		return fetch(stringifiedURL, {method: 'DELETE'})
			.then(res => {
				if (res.ok)
					return res.json();

				throw new Error(res.statusText);
			})
			.catch(err => console.error(err));
	}

	static put<T, K>(url: string, data: T = {} as T): TResponse<K> {
		const stringifiedData = JSON.stringify(data);
		const myHeaders = new Headers();

		myHeaders.append('Content-Type', 'application/json');

		const requestOptions: RequestInit = {
			method: 'PUT',
			headers: myHeaders,
			body: stringifiedData,
			redirect: 'follow',
			credentials: 'include',
		};

		return fetch(url, requestOptions)
			.then(res => {
				if (res.ok)
					return res.json();

				throw new Error(res.statusText);
			})
			.then(data => {
				if (data?.errorCode)
					throw new Error(data.errorCode);

				return data;
			});
	}

	static post<T, K>(url: string, data: T = {} as T): TResponse<K> {
		const stringifiedData = JSON.stringify(data);
		const myHeaders = new Headers();

		myHeaders.append('Content-Type', 'application/json');

		const requestOptions: RequestInit = {
			method: 'POST',
			headers: myHeaders,
			body: stringifiedData,
			redirect: 'follow',
			credentials: 'include',
		};

		return fetch(url, requestOptions)
			.then(res => {
				if (res.ok) {
					if (url === URLS.ACCOUNT.LOGOUT)
						return Promise.resolve(true);

					return res.json();
				}

				throw new Error(res.statusText);
			})
			.then(data => {
				if (data?.errorCode)
					throw new Error(data.errorCode);

				return data;
			});
	}

	static postFiles<K>(url: string, data: FormData): TResponse<K> {
		const requestOptions: RequestInit = {
			method: 'POST',
			body: data,
			redirect: 'follow',
			credentials: 'include',
		};

		return fetch(url, requestOptions)
			.then(res => {
				if (res.ok) {
					if (url === URLS.ACCOUNT.LOGOUT)
						return Promise.resolve(true);

					return res.json();
				}

				throw new Error(res.statusText);
			})
			.then(data => {
				if (data?.errorCode)
					throw new Error(data.errorCode);

				return data;
			});
	}
}
