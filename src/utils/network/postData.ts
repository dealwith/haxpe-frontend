type TMethod = 'POST' | 'PUT';

export const postData = async (
	url = '',
	data: Record<string, unknown> = {},
	method: TMethod = 'POST',
) => {
	const strigifiedData = JSON.stringify(data);
	const myHeaders = new Headers();

	myHeaders.append('Content-Type', 'application/json');

	const requestOptions = {
		method: method,
		headers: myHeaders,
		body: strigifiedData ?? '',
		redirect: 'follow',
		credentials: 'include',
	};

	// eslint-disable-next-line @typescript-eslint/ban-ts-comment
	//@ts-ignore
	const response = await fetch(url, requestOptions);

	if (response.ok) {
		const result = await response.json();

		return result;
	}

	throw new Error(response.statusText);
};
