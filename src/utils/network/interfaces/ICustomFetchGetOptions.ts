import { Dictionary } from 'interfaces';
import { TQueryParams } from './TQueryParams';

export interface ICustomFetchGetOptions {
	isExternal?: boolean;
	queryParams?: Dictionary<TQueryParams>;
}
