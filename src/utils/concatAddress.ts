type TAddress = {
	country?: string;
	city?: string;
	street?: string;
}

export const concatAddress = ({ country, city, street }: TAddress): string => {
	let address = '';

	if (country)
		address = `${address} ${country}`;

	if (city)
		address = `${address} ${city}`;

	if (street)
		address = `${address} ${street}`;

	return address;
};
