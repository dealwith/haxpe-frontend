import { GOOGLE_LINKS } from 'constants/google';

/**
 * Function create link for geocoding request
 *
 * @param {string} route - street name
 * @param {string} buildingNum - house number
 * @param {string} locality  - city name
 * @param {string} postalCode  - zip code of your location
 * @returns {string} formated link for google request
 */

export const getGeocodingLink = (
	route: string,
	buildingNum: string,
	locality: string,
	postalCode: string,
): string => {
	const language = `language=en`,
		street = `route:${route}`,
		streetNumber = `street_number:${buildingNum}`,
		zipCode = `postal_code:${postalCode}`,
		city = `locality:${locality}`,
		componentsFiltering = `components=${street}|${streetNumber}|${city}|${zipCode}`,
		keyParam = `key=${process.env.REACT_APP_GOOGLE_MAP_API_KEY}`;
	const googleAPILink
		= `${GOOGLE_LINKS.GEOCODE_API}?${language}&${componentsFiltering}&${keyParam}`;

	return googleAPILink;
};
