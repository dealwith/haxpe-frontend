import { IGeocodeResult } from 'interfaces/geolocation/IGeocodingTypes';

enum EGoogleStatusCodes {
	'OK' = 'OK',
	'ZERO_RESULTS' = 'ZERO_RESULTS',
	'OVER_QUERY_LIMIT' = 'OVER_QUERY_LIMIT',
	'REQUEST_DENIED' = 'REQUEST_DENIED',
	'INVALID_REQUEST' = 'INVALID_REQUEST',
	'UNKNOWN_ERROR' = 'UNKNOWN_ERROR',
}

export interface IGeocodeGoogleResponse {
	results: IGeocodeResult[];
	status: EGoogleStatusCodes
}
