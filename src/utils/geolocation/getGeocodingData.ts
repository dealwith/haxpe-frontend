import { IGeocodeResult } from 'interfaces/geolocation/IGeocodingTypes';
import { getGeocodingLink } from 'utils';

// TODO: Fix interface path

import { IGeocodeGoogleResponse } from './IGeocodeGoogleResponse';

interface IDefaultData {
	geometry: {
		location: {
			lat: number;
			lng: number;
		}
	};
	address_components: [
		{
			types: string[];
			long_name: string;
		},
	],
	place_id: string;
}

/**
 * Function converts address into geograthic coordinates
 */

export const getGeocodingData = async (
	street: string,
	buildingNum: string,
	city: string,
	zipCode: string,
): Promise<IGeocodeResult | IDefaultData> => {
	const geocodingLink = getGeocodingLink(street, buildingNum, city, zipCode);

	const geocodingResponse = await fetch(geocodingLink);
	const geocodingData: IGeocodeGoogleResponse = await geocodingResponse.json();

	if (geocodingData.status === 'ZERO_RESULTS') {
		return {
			geometry: {
				location: {
					lat: 0,
					lng: 0,
				},
			},
			address_components: [
				{
					types: ['country'],
					long_name: 'Germany',
				},
			],
			place_id: 'none',
		};
	}

	return geocodingData.results[0];
};
