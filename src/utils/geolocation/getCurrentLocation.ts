
import { getReverseGeocodingLink } from 'utils';
import { IGeocodingResult } from 'components/Forms/OrderCustomerForm/interfaces';
// TODO: Fix interfaces, bring it to the top level

import { isObjIncludesType } from '../object/isObjIncludesType';

/**
 * Function send data about location depending on current GPS coordinates
 */

// TODO: Add interface for options
// type TGetCurrentLocationResult = {
// 	placeId: string,
// 	street: string,
// 	buildingNum: string,
// 	city: string,
// 	lat: number,
// 	lon: number,
// 	zipCode: string,
// 	country: string;
// };

//TODO: ERROR(undefined): Cannot read property 'split' of undefined-- 108 line
export const promisifiedGetCurrentLocation = options => new Promise(
	(res, rej) => navigator.geolocation.getCurrentPosition(
		position => res(position), err => rej(err), options,
	),
);

export const getCurrentLocation = () => {
	const success = (pos: GeolocationPosition) => {
		const { latitude, longitude } = pos.coords;
		const googleAPILink = getReverseGeocodingLink(latitude, longitude);

		return fetch(googleAPILink)
			.then(res => res.json())
			.then((data: IGeocodingResult) => {
				let postalCode: string,
					street: string,
					buildingNum: string,
					city: string,
					placeId: string,
					country: string;

				data.results.forEach(result => {
					placeId = result?.place_id;

					const isPostalCodeObj = isObjIncludesType(result, 'postal_code');
					const isStreetAddress = isObjIncludesType(result, 'street_address');

					if (isPostalCodeObj) {
						postalCode = result
							.address_components
							.find(address => isObjIncludesType(address, 'postal_code'))
							.long_name;
					}

					if (isStreetAddress) {
						result.address_components.forEach(address => {
							if (isObjIncludesType(address, 'street_number'))
								buildingNum = address.long_name;

							if (isObjIncludesType(address, 'route'))
								street = address.long_name;

							if (isObjIncludesType(address, 'locality'))
								city = address.long_name;

							if (isObjIncludesType(address, 'country'))
								country = address.long_name;
						});
					}

				});

				if (!postalCode)
					postalCode = 'Put your zip code here';

				const result = {
					placeId,
					street,
					buildingNum,
					city,
					lat: latitude,
					lon: longitude,
					zipCode: postalCode,
					country,
				};

				return result;
			});
	};

	const errorHandler = err => console.warn(`ERROR(${err.code}): ${err.message}`);

	const options = {
		enableHighAccuracy: true,
		timeout: 5000,
		maximumAge: 0,
	};

	return promisifiedGetCurrentLocation(options)
		.then((position: GeolocationPosition) => success(position))
		.catch(err => errorHandler(err));
};
