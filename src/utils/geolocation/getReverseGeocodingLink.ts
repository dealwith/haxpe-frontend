import { GOOGLE_LINKS } from 'constants/google';

/**
 * Create a link for google reverse geocoding request
 *
 * @param lat {number} - latitude
 * @param lng {number} - longitude
 * @returns {string} formatted google link for request
 */

export const getReverseGeocodingLink = (lat: number, lng: number): string => {
	const latlngParam = `latlng=${lat},${lng}`;
	const language = `language=en`;
	const locationType = '&location_type=ROOFTOP';
	const resultTypeParam = 'result_type=street_address|postal_code';
	const keyParam = `key=${process.env.REACT_APP_GOOGLE_MAP_API_KEY}`;
	const googleAPILink
		= `${GOOGLE_LINKS.GEOCODE_API}?${latlngParam}&${language}&${locationType}&${resultTypeParam}&${keyParam}`;

	return googleAPILink;
};
