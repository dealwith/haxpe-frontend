import { IIndustryId, IIndustry } from 'interfaces';

export const getFilteredIndustries = (
	allIndustries: IIndustry[],
	inputIndustries: IIndustryId[],
): IIndustry[] => {
	const industries: IIndustry[] = [];

	inputIndustries.forEach(({ industryId }) => {
		const industry = allIndustries.find(({ id }) => id === industryId);

		industries.push(industry);
	});

	return industries;
};
