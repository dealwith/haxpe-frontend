export const camelizeFirstLetter = (word: string): string => word[0]
	.toUpperCase() + word.slice(1);
