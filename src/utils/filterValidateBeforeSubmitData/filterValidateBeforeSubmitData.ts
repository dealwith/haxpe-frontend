import { Dictionary } from 'interfaces';

export const filterValidateBeforeSubmitData = (data: Dictionary<any>): Dictionary<any> => {
	return Object.fromEntries(Object.entries(data).filter(([, value]) => value));
};
