type TArgs = {
	creationDate: string;
}

export const sortByCreationDate = (
	{ creationDate: first }: TArgs,
	{ creationDate: second }: TArgs,
): number => +new Date(second) - +new Date(first);
