import i18next from 'i18next';

export const getWithTranslatedKeys = <T extends {key: string}>(namespace: string, input: T[]): T[] =>
	input.map(item => ({
		...item,
		...(item?.key && { key: i18next.t(`${namespace}:${item.key}`)}),
	}));
