import i18next from 'i18next';

export const transformEditedToOriginUserFields = ({ editedFields }) => {
	let currentUser = null;

	const name = i18next.t('user:name');
	const surname = i18next.t('user:surname');
	const tel = i18next.t('user:tel');
	const eMail = i18next.t('user:email');

	const isUser = editedFields?.user || editedFields?.account;

	if (isUser) {
		currentUser = editedFields.user
			? 'user'
			: 'account';
	}

	return isUser && ({
		name: editedFields[currentUser][name],
		surname: editedFields[currentUser][surname],
		phoneNumber: editedFields[currentUser][tel],
		email: editedFields[currentUser][eMail],
	});
};
