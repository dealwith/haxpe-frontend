export enum EResources {
	customer = 'customer',
	worker = 'worker',
	partner = 'partner',
	order = 'order',
	settings = 'settings',
	billing = 'billing',
}
