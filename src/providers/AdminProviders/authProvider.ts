import { AuthProvider } from 'ra-core';

import { AccountService } from 'services';

export const authProvider: AuthProvider = {
	login: params => {
		return AccountService.login(params)
			.then(() => {
				return Promise.resolve();
			})
			.catch(err => Promise.reject());
	},
	logout: () => {
		return AccountService.getAccount()
			.then(() => {
				return AccountService.logout();
			})
			.catch(() => Promise.resolve());
	},
	checkAuth: () => {
		return AccountService.getAccount()
			.then(() => {

				return Promise.resolve();
			})
			.catch(() => Promise.reject());
	},
	checkError: () => {
		return Promise.resolve();
	},
	getPermissions: () => {
		return Promise.resolve();
	},
	getIdentity: () => {
		return AccountService.getAccount()
			.then(({body: { name, surname, id}}) => {
				const fullName = `${name} ${surname}`;

				if (fullName.length > 1)
					return Promise.resolve({fullName, id});
			})
			.catch(() => {
				return Promise.reject();
			});
	},
};
