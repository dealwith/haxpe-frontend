import { GetListParams } from 'react-admin';

import {
	getCustomers,
	getWorkers,
	getPartners,
	getOrders,
	getCoupons,
	getBillings,
} from 'providers';

import { EResources } from 'providers/AdminProviders/interfaces';
import { IAdminDataMany } from '../interfaces';

type TGetListSwitchArgs = {
	resource: string,
	params: GetListParams,
}

export const getListSwitch = ({
	resource,
	params,
}: TGetListSwitchArgs): Promise<IAdminDataMany<unknown>> => {
	switch (resource) {
		case EResources.customer:
			return getCustomers(params);

		case EResources.worker:
			return getWorkers(params);

		case EResources.partner:
			return getPartners(params);

		case EResources.order:
			return getOrders(params);

		case EResources.settings:
			return getCoupons(params);

		case EResources.billing:
			return getBillings(params);

		default:
			return Promise.resolve({data: [], total: 0});
	}
};
