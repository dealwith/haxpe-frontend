import { GetOneParams } from 'react-admin';

import {
	getPartner,
	getOrder,
	getWorker,
	getCoupon,
	getBilling,
	getCustomer,
} from 'providers';

import { EResources } from 'providers/AdminProviders/interfaces';
import { IAdminData } from '../interfaces';

type TGetOneListArgs = {
	resource: string;
	params: GetOneParams;
}

export const getOneSwitch = ({
	resource,
	params,
}: TGetOneListArgs): Promise<IAdminData<unknown>>  => {
	const id = params?.id?.toString();

	switch (resource) {
		case EResources.customer:
			return getCustomer(id);

		case EResources.partner:
			return getPartner(id);

		case EResources.order:
			return getOrder(id);

		case EResources.worker:
			return getWorker(id);

		case EResources.settings:
			return getCoupon(id);

		case EResources.billing:
			return getBilling(id);

		default:
			return Promise.resolve({data: {}});
	}
};
