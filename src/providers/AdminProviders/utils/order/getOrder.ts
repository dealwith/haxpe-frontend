import {
	OrderService,
	IndustriesServiceTypesService,
	CustomerService,
	UserService,
	AddressService,
} from 'services';
import { TOrderPageInfo } from 'interfaces';
import { IAdminData } from '../interfaces';

type TGetOrder = IAdminData<TOrderPageInfo>

export const getOrder = async (orderId: string): Promise<TGetOrder> => {
	try {
		const { industries, services } = await IndustriesServiceTypesService.get();
		const { body: orderRes } = await OrderService.getOrderById(orderId);
		const { industryId, serviceTypeId, customerId } = orderRes;
		const { body: customer } = await CustomerService.getCustomerById(customerId);
		const { body: user } = await UserService.getUserById(customer.userId);
		const { body: address } = await AddressService.getAddressById(customer.addressId);
		const industry = industries.find(({ id }) => id === industryId);
		const service = services.find(({ id }) => id === serviceTypeId);
		const order: TGetOrder = {
			data: {
				...orderRes,
				industry,
				service,
				user,
				address,
			},
		};

		return Promise.resolve(order);
	} catch (error) {
		throw new Error(error);
	}
};
