import { GetListParams } from 'react-admin';

import {
	OrderService,
	IndustriesServiceTypesService,
	CustomerService,
	UserService,
	AddressService,
} from 'services';
import { TOrderPageInfo } from 'interfaces';
import { IAdminDataMany } from '../interfaces';

type TGetOrders = IAdminDataMany<TOrderPageInfo>;

export const getOrders = async (params: GetListParams): Promise<TGetOrders> => {
	try {
		const orderPageInfo: TGetOrders = {
			data: [],
			total: 0,
		};

		const { body: {
			result: ordersRes,
			totalCount,
		} } = await OrderService.getOrderPage(params);

		if (!ordersRes.length)
			return orderPageInfo;

		const customerIds = ordersRes.map(({ customerId }) => customerId);
		const addressIds = ordersRes.map(({ addressId }) => addressId);

		const { body: customersRes } = await CustomerService.getCustomers(customerIds);
		const { body: addressesRes } = await AddressService.getAddresses(addressIds);

		const userIds = customersRes.map(({ userId }) => userId);

		const { body: usersRes } = await UserService.getUsers(userIds);

		const { industries, services } = await IndustriesServiceTypesService.get();

		orderPageInfo.data = ordersRes.map(orderRes => {
			const {
				industryId,
				serviceTypeId,
				customerId,
				addressId,
			} = orderRes;
			const industry = industries.find(({ id }) => id === industryId);
			const service = services.find(({ id }) => id === serviceTypeId);
			const customer = customersRes.find(({ id }) => customerId === id);
			const user = usersRes.find(({ id }) => customer.userId === id);
			const order = {
				...orderRes,
				user: { ...user },
				industry,
				service,
				address: addressesRes.find(({ id }) => id === addressId),
			};

			return order;
		});

		orderPageInfo.total = totalCount;

		return orderPageInfo;
	} catch (error) {
		throw new Error(error);
	}
};
