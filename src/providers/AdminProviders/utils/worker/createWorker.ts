import { CreateParams } from 'react-admin';

import { PartnerService, WorkerService } from 'services';
import { IWorkerCreateData, IWorkerCreateFormParams } from 'services/worker/interfaces';
import { IWorker, EResponseStatus } from 'interfaces';

type TData = CreateParams<IWorkerCreateFormParams>;

export const createWorker = async ( { data }: TData ): Promise<{ data: IWorker }>  => {
	const workerServiceTypes = data.workerServiceTypes.map(id => {
		return {serviceTypeId: id};
	});
	const { firstName, lastName, email, phone }  = data;
	const {
		body: getPartnerInfoRes,
		status: getPartnerInfoStatus,
	} = await PartnerService.getPartnerInfo();

	if (getPartnerInfoStatus === EResponseStatus.success) {
		const requestBody: IWorkerCreateData = {
			partnerId: getPartnerInfoRes?.id,
			firstName,
			lastName,
			email,
			phone,
			serviceTypes: workerServiceTypes,
		};

		const { body: createWorkerRes } = await WorkerService.createWorker(requestBody);

		return { data: createWorkerRes };
	}
};
