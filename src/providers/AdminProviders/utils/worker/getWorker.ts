import {
	WorkerService,
	UserService,
	IndustriesServiceTypesService,
	PartnerService,
} from 'services';
import { getFilteredServices, getIndustry } from 'utils';
import { EAccountStatus, TWorkerPageInfo } from 'interfaces';
import { IAdminData } from '../interfaces';

type TGetWorker = IAdminData<TWorkerPageInfo>;

//TODO: remove userBody.isLocked when into API add statuses
export const getWorker = async (id: string): Promise<TGetWorker> => {
	try {
		const { body: workerRes } = await WorkerService.getWorkerById(id);
		const { userId, serviceTypes } = workerRes;
		const { status: userStatus, body: userRes } = await UserService.getUserById(userId);
		const { body: {
			serviceTypes: partnerServiceTypes,
			industries: partnerIndustries,
		} } = await PartnerService.getPartnerById(workerRes.partnerId);

		const {
			industries: industriesRes,
			services: servicesRes,
		} = await IndustriesServiceTypesService.get();

		let worker: TWorkerPageInfo = {
			...workerRes,
			partnerServiceTypes,
			partnerIndustries,
		};

		if (userStatus === 'success') {
			const user = {
				...userRes,
				accountStatus: userRes?.isLocked
					? EAccountStatus.deactivated
					: EAccountStatus.activated,
			};

			worker = {
				...worker,
				user: { ...user },
			};
		}

		if (serviceTypes?.length) {
			const services = getFilteredServices(servicesRes, serviceTypes);
			const industry = getIndustry(industriesRes, services[0].industryId);

			worker = {
				...worker,
				industry,
				services,
			};
		}

		return Promise.resolve({data: {...worker, id}});
	} catch (error) {
		throw new Error(error);
	}
};
