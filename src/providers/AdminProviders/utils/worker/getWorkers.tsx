import { GetListParams } from 'react-admin';

import {
	WorkerService,
	UserService,
	IndustriesServiceTypesService,
	AccountService,
	PartnerService,
} from 'services';
import { getFilteredServices, getIndustry } from 'utils';
import {
	TWorkerPageInfo,
	IIndustry,
	EAccountStatus,
	ERoles,
} from 'interfaces';
import { IAdminDataMany } from '../interfaces';

type TGetWorkers = IAdminDataMany<TWorkerPageInfo>;

export const getWorkers = async (params: GetListParams): Promise<TGetWorkers> => {
	try {
		const workerPageInfo: TGetWorkers = {
			data: [],
			total: 0,
		};

		let workersRes = [];
		let totalCount = 0;

		const { body: { roles } } = await AccountService.getAccount();

		const isPartner = roles[0] === ERoles.partner;

		if (isPartner) {
			const {
				body: { id },
			} = await PartnerService.getPartnerInfo();
			const { body } = await WorkerService.getWorkersByPartnerId(id);

			workersRes = body;
			totalCount = body.length - 1;
		} else {
			const {
				body: {
					result,
					totalCount: resTotalCount,
				},
			} = await WorkerService.getWorkerPage(params);

			workersRes = result;
			totalCount = resTotalCount;
		}

		if (!workersRes.length)
			return workerPageInfo;

		const {
			services: servicesRes,
			industries: industriesRes,
		} = await IndustriesServiceTypesService.get();

		workerPageInfo.total = totalCount;

		const userIds = [];

		workersRes?.forEach(({ userId }) => userId && userIds.push(userId));

		const { body: users } = await UserService.getUsers(userIds);

		workerPageInfo.data = workersRes?.map(workerRes => {
			const { serviceTypes, userId } = workerRes;
			let user = users.find(({ id }) => userId === id);

			user = {
				...user,
				accountStatus: user?.isLocked
					? EAccountStatus.deactivated
					: EAccountStatus.activated,
			};

			let worker: TWorkerPageInfo = {
				...workerRes,
				user: {...user},
			};

			if (serviceTypes?.length) {
				const services = getFilteredServices(servicesRes, serviceTypes);
				const industry: IIndustry = getIndustry(industriesRes, services[0].industryId);

				worker = {
					...worker,
					industry,
					services,
				};
			}

			return worker;
		});

		return workerPageInfo;
	} catch (error) {
		throw new Error(error);
	}
};
