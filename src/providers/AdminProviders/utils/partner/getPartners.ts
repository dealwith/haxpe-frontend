import { GetListParams } from 'react-admin';

import {
	AddressService,
	IndustriesServiceTypesService,
	PartnerService,
	UserService,
} from 'services';
import {
	getFilteredIndustries,
	getFilteredServices,
	getWithTranslatedKeys,
} from 'utils';
import {
	EAccountStatus,
	IIndustry,
	TPartnerPageInfo,
} from 'interfaces';
import { IAdminDataMany } from '../interfaces';

type TGetPartners = IAdminDataMany<TPartnerPageInfo>;

export const getPartners = async (params: GetListParams): Promise<TGetPartners> => {
	try {
		const partnerPageInfo = {
			data: [],
			total: 0,
		};

		const {
			body: {
				result: partnersRes,
				totalCount,
			},
		} = await PartnerService.getPartnerPage(params);

		if (!partnersRes.length)
			return partnerPageInfo;

		const {
			industries: industriesRes,
			services: servicesRes,
		} = await IndustriesServiceTypesService.get();

		partnerPageInfo.total = totalCount;

		const addressIds = [];
		const ownerUserIds = [];

		partnersRes.forEach(({ addressId, ownerUserId, id }) => {
			if (addressId)
				addressIds.push(addressId);

			if (ownerUserId)
				ownerUserIds.push(ownerUserId);
		});

		const addressesReq = AddressService.getAddresses(addressIds);
		const usersReq = UserService.getUsers(ownerUserIds);

		const [
			{ body: addresses },
			{ body: users },
		] = await Promise.all([addressesReq, usersReq]);

		partnerPageInfo.data = partnersRes.map(partnerRes => {
			const {
				addressId,
				ownerUserId,
				id,
				industries,
				serviceTypes,
			} = partnerRes;
			let user = users.find(({ id }) => ownerUserId === id);
			const address = addresses.find(({ id }) => addressId === id);

			user = {
				...user,
				accountStatus: user.isLocked
					? EAccountStatus.deactivated
					: EAccountStatus.activated,
			};

			let partner: TPartnerPageInfo = {
				id,
				partner: {
					...partnerRes,
				},
				user: {
					...user,
				},
				address: {
					...(addressId && {...address}),
				},
			};

			if (industries?.length) {
				const filteredIndustries = getFilteredIndustries(industriesRes, industries);
				const translatedIndustries = getWithTranslatedKeys<IIndustry>('industries', filteredIndustries);

				const services = getFilteredServices(servicesRes, serviceTypes);

				partner = {
					...partner,
					industries: translatedIndustries,
					services,
					id,
				};
			}

			return partner;
		});

		return partnerPageInfo;
	} catch (error) {
		throw new Error(error);
	}
};
