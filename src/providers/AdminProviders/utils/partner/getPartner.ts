import {
	AddressService,
	PartnerService,
	IndustriesServiceTypesService,
	UserService,
	WorkerService,
} from 'services';
import {
	getFilteredIndustries,
	getFilteredServices,
	getWithTranslatedKeys,
} from 'utils';
import {
	EAccountStatus,
	EResponseStatus,
	IIndustry,
	TPartnerPageInfo,
} from 'interfaces';
import { IAdminData } from '../interfaces';

export const getPartner = async (id: string): Promise<IAdminData<TPartnerPageInfo>> => {
	try {
		const {
			body: partnerRes,
			body: {
				ownerUserId,
				addressId,
				industries,
				serviceTypes,
			},
		} = await PartnerService.getPartnerById(id);

		const { status: userStatus, body: userBody } = await UserService.getUserById(ownerUserId);

		const {
			industries: industriesRes,
			services: servicesRes,
		} = await IndustriesServiceTypesService.get();

		const { body: partnerWorkersRes } = await WorkerService.getWorkersByPartnerId(id);
		const userIds = partnerWorkersRes.map(({ userId }) => userId);

		const services = getFilteredServices(servicesRes, serviceTypes);
		const filteredIndustries = getFilteredIndustries(industriesRes, industries);
		const translatedIndustries = getWithTranslatedKeys<IIndustry>('industries', filteredIndustries);

		let partner: TPartnerPageInfo = {
			id,
			partner: {
				...partnerRes,
			},
			services,
			industries: translatedIndustries,
		};

		if (userIds.length) {
			const { body: partnerWorkers } = await UserService.getUsers(userIds);

			partner = {
				...partner,
				partnerWorkers,
			};
		}

		if (userStatus === EResponseStatus.success) {
			const user = {
				...userBody,
				accountStatus: userBody.isLocked
					? EAccountStatus.deactivated
					: EAccountStatus.activated,
			};

			partner = {
				...partner,
				user: { ...user },
			};
		}

		if (addressId) {
			const {
				status: addressStatus,
				body: addressBody,
			} = await AddressService.getAddressById(addressId);

			if (addressStatus === EResponseStatus.success)
			{partner = {
				...partner,
				address: {...addressBody},
			};}
		}

		return Promise.resolve({data: {...partner, id}});
	} catch (error) {
		throw new Error(error);
	}
};
