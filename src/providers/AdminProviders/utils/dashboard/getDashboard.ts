import { IResponseMany, TRange, IStatistic } from 'interfaces';
import { StatisticService } from 'services';

type TReturnType = [
	IResponseMany<IStatistic>,
	IResponseMany<IStatistic>,
	IResponseMany<IStatistic>,
	IResponseMany<IStatistic>
];

export const getDashboard = (date: TRange): Promise<TReturnType> => {
	return Promise.all([
		StatisticService.getCustomerChart(date),
		StatisticService.getOrderChart(date),
		StatisticService.getPartnerChart(date),
		StatisticService.getWorkerChart(date),
	]);
};
