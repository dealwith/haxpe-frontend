import { AddressService, CustomerService, UserService } from 'services';
import { TCustomerPageInfo, EResponseStatus } from 'interfaces';
import { IAdminData } from '../interfaces';

export const getCustomer = async (id: string): Promise<IAdminData<TCustomerPageInfo>> => {
	try {
		const { body: customerRes } = await CustomerService.getCustomerById(id);
		const { userId, addressId } = customerRes;
		const {
			status: userStatus,
			body: userRes,
		} = await UserService.getUserById(userId);
		let customer: TCustomerPageInfo = {
			...customerRes,
		};

		if (userStatus === EResponseStatus.success) {
			customer = {
				...customer,
				user: { ...userRes },
			};
		}

		if (addressId) {
			const {
				status: addressStatus,
				body: addressRes,
			} = await AddressService.getAddressById(addressId);

			if (addressStatus === EResponseStatus.success) {
				customer = {
					...customer,
					address: { ...addressRes },
				};
			}
		}

		return Promise.resolve({data: {...customer, id}});
	} catch (error) {
		throw new Error(error);
	}
};
