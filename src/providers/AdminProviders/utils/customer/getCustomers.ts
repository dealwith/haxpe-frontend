import { GetListParams } from 'react-admin';

import { isObjectWithProperties } from 'utils';
import { AddressService, CustomerService, UserService } from 'services';
import { TCustomerPageInfo } from 'interfaces';
import { IAdminDataMany } from '../interfaces';

type TGetCustomers = IAdminDataMany<TCustomerPageInfo>;

//TODO: need add latestOrder field
export const getCustomers = async (params : GetListParams): Promise<TGetCustomers> => {
	try {
		const customerPageInfo: TGetCustomers = {
			data: [],
			total: 0,
		};
		const {
			body: {
				result: customersRes,
				totalCount,
			},
		} = await CustomerService.getCustomerPage(params);

		if (!customersRes.length)
			return customerPageInfo;

		customerPageInfo.total = totalCount;

		const addressIds: string[] = [];
		const userIds: string[] = [];

		customersRes.forEach(({ addressId, userId }) => {
			if (addressId)
				addressIds.push(addressId);

			if (userId)
				userIds.push(userId);
		});

		const addressesReq = AddressService.getAddresses(addressIds);
		const usersReq = UserService.getUsers(userIds);

		const [
			{ body: addresses },
			{ body: users },
		] = await Promise.all([addressesReq, usersReq]);

		customerPageInfo.data = customersRes.map(customerRes => {
			const { addressId, userId } = customerRes;
			const user = users.find(({ id }) => userId === id);
			const address = addresses.find(({ id }) => addressId === id);

			const customer = {
				...customerRes,
				user: { ...user },
				address: {...(addressId && {...address})},
			};

			return isObjectWithProperties({...customer})
				? customer
				: null;
		});

		customerPageInfo.data = customerPageInfo.data.filter(e => e !== null);

		return customerPageInfo;
	} catch (error) {
		throw new Error(error);
	}
};
