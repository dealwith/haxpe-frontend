export interface IAdminData<T> {
    data: T;
    total?: number;
}
