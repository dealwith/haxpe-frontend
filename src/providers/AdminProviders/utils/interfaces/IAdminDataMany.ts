export interface IAdminDataMany<T> {
    data: T[];
    total?: number;
}
