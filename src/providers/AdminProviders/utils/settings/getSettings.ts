import {
	AccountService,
	AddressService,
} from 'services';
import { TSettingsPageInfo } from 'interfaces';
import { IAdminData } from '../interfaces';

type TGetSetting = IAdminData<TSettingsPageInfo>;

export const getSettings = async (): Promise<TGetSetting> => {
	try {
		const { body: account } = await AccountService.getAccount();
		const { body: address } = await AddressService.getAddressById(account.id);
		const settings: TSettingsPageInfo = {
			account: {
				...account,
			},
			address: { ...address },
		};

		return Promise.resolve({data: { ...settings }});
	} catch (error) {
		throw new Error(error);
	}
};
