import { DeleteParams } from 'ra-core';

import { EResponseStatus, ICoupon } from 'interfaces';
import { IAdminData } from '../interfaces';
import { CouponService } from 'services';

type TReturnType = IAdminData<ICoupon>

export const deleteCoupon = async (params: DeleteParams): Promise<TReturnType> => {
	try {
		const stringifiedId = params.id.toString();
		const { status, errorCode } = await CouponService.deleteCoupon(stringifiedId);

		if (status === EResponseStatus.success)
			return { data: params.previousData as ICoupon };

		throw new Error(errorCode);
	} catch (err) {
		console.log(err);
	}
};
