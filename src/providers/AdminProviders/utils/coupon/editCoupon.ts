import { UpdateParams } from 'ra-core';

import { CouponService } from 'services';
import { EResponseStatus, ICoupon } from 'interfaces';
import { IAdminData } from '../interfaces';

type TCoupon = IAdminData<ICoupon>;

export const editCoupon = async (params: UpdateParams): Promise<TCoupon> => {
	try {
		const stringifiedId = params.id.toString();
		const {
			status,
			errorCode,
			body,
		} = await CouponService.editCoupon(stringifiedId, params.data);

		if (status === EResponseStatus.success)
			return { data: body };

		throw (errorCode);
	} catch (err) {
		console.log(err);
	}
};
