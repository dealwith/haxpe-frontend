import { GetListParams } from 'react-admin';

import { CouponService } from 'services';
import { ICoupon } from 'interfaces';
import { IAdminDataMany } from '../interfaces';

type TGetCoupons = IAdminDataMany<ICoupon>;

export const getCoupons = async (params: GetListParams): Promise<TGetCoupons> => {
	try {
		const couponsPageInfo = {
			data: [],
			total: 0,
		};

		const { body: { result, totalCount } } = await CouponService.getCouponPage(params);

		couponsPageInfo.total = totalCount;
		couponsPageInfo.data = [...result];

		return Promise.resolve(couponsPageInfo);
	} catch (error) {
		console.log(error);
	}
};
