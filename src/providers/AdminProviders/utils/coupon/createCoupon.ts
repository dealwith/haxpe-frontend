import { ICoupon, ICouponCreateData } from 'interfaces';
import { CouponService } from 'services';
import { IAdminData } from '../interfaces';

type TProps = {
	data: Omit<ICouponCreateData, 'unit'>;
}

export const createCoupon = async({ data }: TProps): Promise<IAdminData<ICoupon>> => {

	const coupon: ICouponCreateData = {
		...data,
		expirationDate: data.expirationDate.replaceAll('-', '/'),
		unit: 'percent',
	};

	try {
		const { body } = await CouponService.createCoupon(coupon);

		return {data: body};
	} catch (err) {
		console.warn(err);
	}
};
