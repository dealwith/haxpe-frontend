import { CouponService } from 'services';
import { Dictionary, ICoupon } from 'interfaces';
import { IAdminData } from '../interfaces';

type TGetCoupon = IAdminData<ICoupon | Dictionary<never>>;

export const getCoupon = async (id: string): Promise<TGetCoupon> => {
	try {
		const { body } = await CouponService.getCouponById(id);

		return ({data: {...body}});
	} catch (error) {
		throw new Error(error);
	}
};
