import { TBillingPageInfo } from 'interfaces';
import { IAdminData } from '../interfaces';

type TGetBilling = IAdminData<TBillingPageInfo>;

export const getBilling = async (id: string): Promise<TGetBilling> => {
	try {
		return ({data: {
			creationDate: '11/11/2011',
			id: '112341234234',
			partnerId: '3242342354',
			income: 123,
			commission: 22,
			amount: 444,
			status: 'dasfsdgdfg',
		} });
	} catch (error) {
		throw new Error(error);
	}
};
