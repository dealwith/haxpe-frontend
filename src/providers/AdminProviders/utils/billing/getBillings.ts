import { GetListParams } from 'react-admin';

import { TBillingPageInfo } from 'interfaces';
import { IAdminDataMany } from '../interfaces';

type TGetBillings = IAdminDataMany<TBillingPageInfo>;

export const getBillings = async (params: GetListParams): Promise<TGetBillings> => {
	try {
		const couponsPageInfo = {
			data: [
				{
					creationDate: '11/11/2011',
					id: '112341234234',
					partnerId: '3242342354',
					income: 123,
					commission: 22,
					amount: 444,
					status: 'dasfsdgdfg',
				},
				{
					creationDate: '01/01/2011',
					id: '22224235',
					partnerId: '435dsggds',
					income: 555,
					commission: 55,
					amount: 777,
					status: 'dasfsdgdfg',
				},
			],
			total: 2,
		};

		return Promise.resolve(couponsPageInfo);
	} catch (error) {
		console.log(error);
	}
};
