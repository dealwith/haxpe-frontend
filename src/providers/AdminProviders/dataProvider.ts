import { DataProvider, GetListParams, GetOneParams } from 'ra-core';

import { TRange } from 'interfaces';
import { EResources } from './interfaces';
import {
	createWorker,
	getDashboard,
	createCoupon,
	deleteCoupon,
	editCoupon,
} from './utils';
import { getListSwitch, getOneSwitch } from './utils/switches';

export const dataProvider = {
	getDashboard: async(date: TRange) => {
		try {
			const [
				{ body: customerChart },
				{ body: orderChart },
				{ body: partnerChart },
				{ body: workerChart },
			] = await getDashboard(date);

			const dashboardPageInfo = {
				data: [{
					customerChart,
					orderChart,
					partnerChart,
					workerChart,
				}],
				total: 0,
			};

			return Promise.resolve(dashboardPageInfo);
		} catch (error) {
			throw new Error(error);
		}
	},
	getList: async (resource: string, params: GetListParams) => {
		return getListSwitch({
			resource,
			params,
		});
	},

	getOne: async (resource: string, params: GetOneParams) => {
		return getOneSwitch({
			resource,
			params,
		});
	},

	getMany: () => Promise.resolve({data: []}),

	getManyReference: () => Promise.resolve({data: [], total: 0}),

	update: (resource, params) => {
		switch (resource) {
			case EResources.settings:
				return editCoupon(params);
			default:
				return Promise.resolve({ data: {}});
		}
	},

	updateMany: () => Promise.resolve({data: []}),

	create: (resource, params) =>  {
		switch (resource) {
			case EResources.worker:
				return createWorker(params);

			case EResources.settings:
				return createCoupon(params);
			default:
				return Promise.resolve({data: [], total: 0});
		}
	},

	delete: (resource, params) => {
		console.log(params);

		switch (resource) {
			case EResources.settings:
				return deleteCoupon(params);
			default:
				return Promise.resolve({ data: {}});
		}
	},

	deleteMany: () => {
		return Promise.resolve({data: []});
	},
} as DataProvider;
