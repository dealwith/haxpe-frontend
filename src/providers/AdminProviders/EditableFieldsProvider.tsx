import { FunctionComponent } from 'react';

import { EditableFieldsContext } from 'context';

import { useProvideEditableFields } from 'hooks';

export const EditableFieldsProvider: FunctionComponent = ({ children }) => {
	const value = useProvideEditableFields();

	return <EditableFieldsContext.Provider value={value}>
		{children}
	</EditableFieldsContext.Provider>;
};
