import { FunctionComponent, ReactNode } from 'react';

import { NotifyContext } from 'context';

import { useProvideNotify } from 'hooks';

type TProps = {
	children?: ReactNode;
}

export const NotifyProvider: FunctionComponent<TProps> = ({ children }) => {
	const notify = useProvideNotify();

	return <NotifyContext.Provider value={notify}>{children}</NotifyContext.Provider>;
};
