import { FunctionComponent, ReactNode } from 'react';

import { AuthContext } from 'context';

import { useProvideAuth } from 'hooks';

type TProps = {
	children?: ReactNode;
}

export const AuthProvider: FunctionComponent<TProps> = ({ children }) => {
	const auth = useProvideAuth();

	return <AuthContext.Provider value={auth}>{children}</AuthContext.Provider>;
};
