import { FunctionComponent, ReactNode } from 'react';

import { PDFDownloadHandlerContext } from 'context';

import { useProvidePDFDownloadHandler } from 'hooks';

type TProps = {
	children: ReactNode;
}

export const PDFDownloadHandlerProvider: FunctionComponent<TProps> = ({
	children,
}) => {
	const value = useProvidePDFDownloadHandler();

	return <PDFDownloadHandlerContext.Provider value={value}>
		{children}
	</PDFDownloadHandlerContext.Provider>;
};
