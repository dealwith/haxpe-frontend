import { FunctionComponent, ReactNode } from 'react';

import { ServicesIndustriesContext } from 'context';
import { useProvideServicesIndustries } from 'hooks';

type TProps = {
	children: ReactNode;
}

export const ServicesIndustriesProvider: FunctionComponent<TProps> = ({ children }) => {
	const useServicesIndustries = useProvideServicesIndustries();

	return (
		<ServicesIndustriesContext.Provider value={useServicesIndustries}>
			{children}
		</ServicesIndustriesContext.Provider>
	);
};
