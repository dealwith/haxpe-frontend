import { FunctionComponent } from 'react';

import { BeforeWorkCompletedContext } from 'context';

import { useProvideBeforeWorkCompletedInfo } from 'hooks';

export const BeforeWorkCompletedProvider: FunctionComponent = ({ children }) => {
	const info = useProvideBeforeWorkCompletedInfo();

	return (
		<BeforeWorkCompletedContext.Provider value={info}>
			{children}
		</BeforeWorkCompletedContext.Provider>
	);
};
