import { FunctionComponent } from 'react';

import { AfterWorkCompletedContext } from 'context';

import { useProvideAfterWorkCompleteInfo } from 'hooks';

export const AfterWorkCompletedProvider: FunctionComponent = ({ children }) => {
	const info = useProvideAfterWorkCompleteInfo();

	return (
		<AfterWorkCompletedContext.Provider value={info}>
			{children}
		</AfterWorkCompletedContext.Provider>
	);
};
