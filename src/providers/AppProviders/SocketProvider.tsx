import { FunctionComponent, ReactNode } from 'react';

import { SocketContext } from 'context';
import { useProvideSocket } from 'hooks';

type TProps = {
	children: ReactNode;
}

export const SocketProvider: FunctionComponent<TProps> = ({ children }) => {
	const socketValue = useProvideSocket();

	return (
		<SocketContext.Provider value={socketValue}>
			{children}
		</SocketContext.Provider>
	);
};
