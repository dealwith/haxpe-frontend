import { FunctionComponent } from 'react';

import { SelectedOrderWorkerContext } from 'context';

import { useProvideSelectedWorkerOrder } from 'hooks';

export const SelectedOrderProvider: FunctionComponent = ({ children }) => {
	const selectedOrder = useProvideSelectedWorkerOrder();

	return (
		<SelectedOrderWorkerContext.Provider value={selectedOrder}>
			{children}
		</SelectedOrderWorkerContext.Provider>
	);
};
