import { FunctionComponent } from 'react';

import { LocationContext } from 'context';
import { useCurrentWorkerLocation } from 'hooks';

export const CurrentLocationProvider: FunctionComponent = ({ children }) => {
	const locationValue = useCurrentWorkerLocation();

	return (
		<LocationContext.Provider value={locationValue}>
			{children}
		</LocationContext.Provider>
	);
};
