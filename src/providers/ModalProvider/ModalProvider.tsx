import { FunctionComponent } from 'react';

import { ModalContext } from 'context';
import { useProvideModalContext } from 'hooks';

export const ModalProvider: FunctionComponent = ({ children }) => {
	const context = useProvideModalContext();

	return (
		<ModalContext.Provider value={context}>{children}</ModalContext.Provider>
	);
};
