import { useState, useEffect } from 'react';
import Centrifuge from 'centrifuge';
import SockJS from 'sockjs-client';

import { IOrderRes, IWorkerSocket, TSocketReturnType } from 'interfaces';
import { AccountService } from 'services';

import { useAuth } from 'hooks';

const devCallbacks = {
	'join': function(message) {
		console.log('join');
		console.log(message);
	},
	'leave': function(message) {
		console.log('leave');
		console.log(message);
	},
	'subscribe': function(context) {
		console.log('subscribe');
		console.log(context);
	},
	'error': function(errContext) {
		console.log('error');
		console.log(errContext);
	},
	'unsubscribe': function(context) {
		console.log('unsubscribe');
		console.log(context);
	},
};

export const useProvideSocket = (): TSocketReturnType => {
	const [orderOffer, setOrderOffer] = useState<IOrderRes>();
	const [orderChanged, setOrderChanged] = useState<IOrderRes>();
	const auth = useAuth();
	const userId = auth?.user?.id;

	const clearOrderOffer = () => setOrderOffer(null);
	const clearOrderChanged = () => setOrderChanged(null);

	useEffect(() => {
		if (auth?.isAuthenticated && userId) {
			const websocketConnection = async () => {
				const centrifuge = new Centrifuge('wss://haxpepwa.com:8000/connection/websocket', {
					sockjs: SockJS,
				});
				const { body: token } = await AccountService.getWSToken();

				centrifuge.setToken(token);

				const callbacks = {
					'publish': (message: IWorkerSocket) => {
						const { type } = message?.data;

						switch (type) {
							case 'order.changed':
								setOrderChanged(message?.data?.payload as unknown as IOrderRes);

								break;

							case 'order.offer': {
								// Actual orderId place is on the level of payload
								const incomingOrder = message?.data?.payload?.order;
								const transformedOrder = {
									...incomingOrder,
									id: message?.data?.payload?.orderId,
								};

								setOrderOffer(transformedOrder);

								break;
							}

							default:
								console.log('UNKNOWN ORDER TYPE IN SOCKET');

								break;
						}
					},
					...(process.env.NODE_ENV === 'development' && devCallbacks),
				};

				centrifuge.subscribe(`events#${userId}`, callbacks);

				centrifuge.connect();
			};

			websocketConnection();
		}
	}, [auth, auth?.isAuthenticated, userId]);

	return {
		orderOffer,
		orderChanged,
		clearOrderOffer,
		clearOrderChanged,
	};
};
