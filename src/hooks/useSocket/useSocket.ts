import { useContext } from 'react';

import { SocketContext } from 'context';
import { TSocketReturnType } from 'interfaces';

export const useSocket = (): TSocketReturnType => useContext(SocketContext);
