import { useState, useEffect } from 'react';

import { TEither } from 'interfaces';

type TQuery = TEither<{
	minWidth: number;
}, {
	maxWidth: number;
}>

type TReturnType = {
	isMatches: boolean;
}

export const useMediaQuery = (query: TQuery): TReturnType => {
	const [isMatches, setIsMatches] = useState(false);

	const key = Object.keys(query)[0].split('').map(w => {
		return w === w.toLowerCase()
			? w
			: `-${w.toLowerCase()}`;
	}).join('');

	const transformedQuery = `(${key}: ${Object.values(query)[0]}px)`;

	useEffect(() => {
		const media = window.matchMedia(transformedQuery);

		if (media.matches !== isMatches)
			setIsMatches(media.matches);

		const listener = () => {
			setIsMatches(media.matches);
		};

		media.addEventListener('change', listener);

		return () => media.removeEventListener('change', listener);
	}, [isMatches, query]);

	return { isMatches };
};
