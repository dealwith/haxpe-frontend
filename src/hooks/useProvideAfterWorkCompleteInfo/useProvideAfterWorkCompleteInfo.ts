import { useState } from 'react';

import { TAfterWorkCompletedReturnType } from 'interfaces';

import { useSelectedWorkerOrder, useServiceTable } from 'hooks';

export const useProvideAfterWorkCompleteInfo = (): TAfterWorkCompletedReturnType => {
	const { selectedOrder } = useSelectedWorkerOrder();
	const [reduction, setReduction] = useState(selectedOrder?.reduction || 0);
	const [report, setReport] = useState(selectedOrder?.report || '');
	const {
		additionalServices,
		editService,
		addService,
		removeService,
	} = useServiceTable();

	const handleAddReduction = (reduction: number) => setReduction(reduction);

	const handleAddReport = (report: string) => setReport(report);

	const handleClearReport = () => setReport('');

	return ({
		additionalServices,
		reduction,
		report,
		editService,
		addService,
		removeService,
		handleAddReduction,
		handleAddReport,
		handleClearReport,
	});
};
