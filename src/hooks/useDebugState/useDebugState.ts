import { useState, useDebugValue } from 'react';

export const useDebugState = (hookName: string, initialValue: unknown): ReturnType<typeof useState> => {
	const [ state, setState ] = useState(initialValue);

	useDebugValue(hookName);

	return [state, setState];
};
