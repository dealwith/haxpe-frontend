import { useParams } from 'react-router-dom';

type TType = {
	payoutId: string;
	invoiceId: string;
}

export const useBillingParams = (): TType => useParams<TType>();
