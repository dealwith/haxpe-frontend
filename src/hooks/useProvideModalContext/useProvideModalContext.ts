import { useState } from 'react';

interface IModal {
	isOpen: boolean;
	text: string;
	isBlocking?: boolean;
}

const initialState = {
	isOpen: false,
	text: '',
	isBlocking: false,
};

export const useProvideModalContext = () => {
	const [modal, setModal] = useState<IModal>(initialState);

	const toggleModal = () => {
		setModal(prevState => ({
			...prevState,
			isOpen: !prevState.isOpen,
		}));
	};

	return {
		toggleModal,
		modal,
		setModal,
	};
};
