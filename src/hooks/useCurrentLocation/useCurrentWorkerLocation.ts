// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-nocheck
import { useEffect, useState } from 'react';

import { useAuth } from 'hooks';
import { WorkerService } from 'services';
import { WorkerLocationTrackerService } from 'services/workerLocationTracker';
import { getCurrentLocation, isObjectWithProperties } from 'utils';

export const useCurrentWorkerLocation = (): { lat: number, lon: number } => {
	const [loc, setCurLoc] = useState({ lat: 0, lon: 0 });
	const [workerId, setWorkerId] = useState('');
	const { isWorker } = useAuth();

	useEffect(() => {
		if (isWorker) {
			const fetchWorkerInfo = async () => {
				const { body: workerInfoRes } = await WorkerService.workerInfo();

				setWorkerId(workerInfoRes.id);
			};

			fetchWorkerInfo();
		}
	}, [isWorker]);

	useEffect(() => {
		if (isWorker) {
			const fetchLocation = async () => {
				const currentLocation = await getCurrentLocation();

				if (isObjectWithProperties(currentLocation)) {
					const {
						lat,
						lon,
					} = currentLocation;

					setCurLoc({ lat, lon });
				}
			};

			fetchLocation();

			const location = setInterval(() => {
				if (workerId && loc.lat && loc.lon) {
					WorkerLocationTrackerService.setWorkerLocation(
						{ workerId }, { latitude: loc.lat, longitude: loc.lon },
					);
				}
			}, 1000 * 5);

			return () => clearInterval(location);
		}
	}, [isWorker]);

	return loc;
};
