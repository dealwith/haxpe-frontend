import { useContext } from 'react';

import { AuthContext } from 'context';
import { TUseAuthReturnType } from 'hooks/interfaces';

export const useAuth = (): TUseAuthReturnType => useContext(AuthContext);
