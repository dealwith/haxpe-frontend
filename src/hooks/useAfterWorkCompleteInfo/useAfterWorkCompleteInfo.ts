import { useContext } from 'react';

import { AfterWorkCompletedContext } from 'context';
import { TAfterWorkCompletedReturnType } from 'interfaces';

export const useAfterWorkCompleteInfo = (): TAfterWorkCompletedReturnType => useContext(AfterWorkCompletedContext);
