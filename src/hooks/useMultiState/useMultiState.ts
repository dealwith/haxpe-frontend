import { Dispatch, useReducer } from 'react';

export const useMultiState = <T>(initialState: T): [T, Dispatch<Partial<T>>]  => {
	const [state, setState] =  useReducer(
		(state: T, newState: T) => ({ ...state, ...newState }),
		initialState,
	);

	return [state, setState];
};
