import { useState } from 'react';

import { IOrderRes } from 'interfaces';
import { OrderService, WorkerService } from 'services';
import { sortByCreationDate } from 'utils';

type TReturnType = [
	latestActiveOrder: IOrderRes,
	checkLatestActiveOrder: () => Promise<void>,
]

export const useLatestActiveOrder = (): TReturnType => {
	const [latestActiveOrder, setLatestActiveOrder] = useState<IOrderRes>();

	const checkLatestActiveOrder = async () => {
		try {
			const { body: createdOrders } = await OrderService.getOrder({
				isActive: true,
				Status: 'created',
			});
			const { body: { serviceTypes } } = await WorkerService.workerInfo();
			const lastActiveOrder = [...createdOrders]
				.filter(({ serviceTypeId }) => serviceTypes
					.find(service => serviceTypeId === service.serviceTypeId) )
				.sort(sortByCreationDate)[0];

			if (lastActiveOrder)
				setLatestActiveOrder(lastActiveOrder);

		} catch (err) {
			console.error(err);
		}
	};

	return [
		latestActiveOrder,
		checkLatestActiveOrder,
	];
};
