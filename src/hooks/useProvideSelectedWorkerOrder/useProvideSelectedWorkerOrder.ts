import { useEffect, useState } from 'react';

import { OrderService, WorkerService } from 'services';
import { TSelectedOrderReturnType, IOrderRes } from 'interfaces';

import { useAuth, useSocket } from 'hooks';

export const useProvideSelectedWorkerOrder = (): TSelectedOrderReturnType => {
	const { isWorker } = useAuth();
	const [selectedOrder, setSelectedOrder] = useState<IOrderRes>();
	const {
		orderOffer,
		orderChanged,
		clearOrderChanged,
		clearOrderOffer,
	} = useSocket();

	const changeSelectedOrder = async (orderId: string) => {
		const { body: { id } } = await WorkerService.workerInfo();
		const { body } = await OrderService.getOrder({ WorkerId: id });
		const selectedOrder = body.find(({ id }) => id === orderId);

		setSelectedOrder(selectedOrder);
	};

	const getCurrentOrder = (orders: IOrderRes[]) => {
		let currentOrder = orders.find(({ orderStatus }) => orderStatus === 'workCompleted');

		if (!currentOrder)
			currentOrder = orders.find(({ orderStatus }) => orderStatus === 'workerFounded');

		if (!currentOrder)
			currentOrder = orders.find(({ orderStatus }) => orderStatus === 'workerOnWay');

		if (!currentOrder)
			currentOrder = orders.find(({ orderStatus }) => orderStatus === 'workerArrived');

		if (!currentOrder)
			currentOrder = orders.find(({ orderStatus }) => orderStatus === 'inProgress');

		if (!currentOrder)
			currentOrder = orders.find(({ orderStatus }) => orderStatus === 'paused');

		if (!currentOrder)
			currentOrder = orders.find(({ orderStatus }) => orderStatus === 'reserved');

		return currentOrder;
	};

	const clearSelectedOrder = () => setSelectedOrder(null);

	useEffect(() => {
		const fetchAndSetLastOrder = async () => {
			try {
				const { body: { id } } = await WorkerService.workerInfo();
				const { body } = await OrderService.getOrder({ WorkerId: id });

				const currentOrder = getCurrentOrder(body);

				setSelectedOrder(currentOrder);
			} catch (err) {
				console.error(err);
			}
		};

		if (isWorker && !selectedOrder)
			fetchAndSetLastOrder();

	}, [isWorker, selectedOrder]);

	useEffect(() => {
		if (orderChanged) {
			setSelectedOrder(orderChanged);
			clearOrderChanged();
		}

		if (orderOffer) {
			setSelectedOrder(orderOffer);
			clearOrderOffer();
		}
	}, [orderChanged, orderOffer]);

	return {
		selectedOrder,
		changeSelectedOrder,
		clearSelectedOrder,
	};
};
