import { useEffect, useState } from 'react';

import { AddressService, CustomerService, UserService } from 'services';
import { concatAddress } from 'utils';

import { useSelectedWorkerOrder, useServicesIndustries } from 'hooks';

type TBeforeWorkCompletedPageInfo = {
	orderId: string;
	fullName: string;
	address: string;
	phoneNumber: string;
	industry?: string;
	service?: string;
	industryId: number;
	serviceTypeId: number;
}

export const useProvideBeforeWorkCompletedInfo = (): TBeforeWorkCompletedPageInfo => {
	const { selectedOrder } = useSelectedWorkerOrder();
	const [ orderInfo, setOrderInfo ] = useState<TBeforeWorkCompletedPageInfo>({} as TBeforeWorkCompletedPageInfo);

	const {
		getTranslatedIndustryById,
		getTranslatedServiceById,
	} = useServicesIndustries();

	useEffect(() => {
		const fetchAndSetOrderInfo = async () => {
			try {
				const {
					id: orderId,
					customerId,
					addressId,
					industryId,
					serviceTypeId,
				} = selectedOrder;

				const { body: { userId } } = await CustomerService.getCustomerById(customerId);
				const { body: { fullName, phoneNumber } } = await UserService.getUserById(userId);
				const { body: {
					city,
					street,
					country,
				} } = await AddressService.getAddressById(addressId);

				const address = concatAddress({city, street, country});
				const industry = getTranslatedIndustryById(industryId);
				const service = getTranslatedServiceById(serviceTypeId);

				const pageInfo: TBeforeWorkCompletedPageInfo = {
					orderId,
					fullName,
					address,
					phoneNumber,
					industryId,
					serviceTypeId,
					industry,
					service,
				};

				setOrderInfo(pageInfo);
			} catch (err) {
				console.error(err);
			}
		};

		if (selectedOrder)
			fetchAndSetOrderInfo();

	}, [selectedOrder]);

	return orderInfo;
};
