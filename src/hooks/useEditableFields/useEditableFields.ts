import { useContext } from 'react';

import { EditableFieldsContext } from 'context';

import { TUseEditableFieldsReturnType } from 'hooks/interfaces';

export const useEditableFields = <T>(): TUseEditableFieldsReturnType<T> => useContext(EditableFieldsContext);
