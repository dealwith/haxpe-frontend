import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { useAuth, useMultiState } from 'hooks';
import { IndustryService, ServiceTypeService } from 'services';
import {
	TServicesTypeResult,
	IIndustry,
	TExtendedIndustryOption,
	TExtendedServiceOption,
} from 'interfaces';

type TMultiState = {
	selectedIndustry: TExtendedIndustryOption | null;
	selectedService: TExtendedServiceOption | null;
}

type TReturnType = {
	services: TServicesTypeResult;
	industries: IIndustry[];
	servicesForSelect: TExtendedServiceOption[];
	industriesForSelect: TExtendedIndustryOption[];
	getTranslatedIndustryById: (id: number) => string;
	getTranslatedServiceById: (id: number) => string;
	getSelectedItems: () => TMultiState;
	setSelectedIndustriesAndServices: (selectedItems: TMultiState) => void;
	handleServiceChange: (value: TExtendedServiceOption) => void;
	handleIndustryChange: (value: TExtendedIndustryOption) => void;
}

export const useProvideServicesIndustries = (): TReturnType => {
	const { isAuthenticated } = useAuth();
	const { t } = useTranslation(['services', 'industries']);
	const [services, setServices] = useState<TServicesTypeResult>([]);
	const [industries, setIndustries] = useState<IIndustry[]>([]);
	const [servicesForSelect, setServicesForSelect] = useState<TExtendedServiceOption[]>([]);
	const [industriesForSelect, setIndustriesForSelect] = useState<TExtendedIndustryOption[]>([]);

	const [selectedItems, setSelectedItems] = useMultiState<TMultiState>({
		selectedIndustry: null,
		selectedService: null,
	});

	useEffect(() => {
		const fetchServicesIndustries = async () => {
			const [
				industriesRes,
				servicesRes,
			] = await Promise.allSettled([
				IndustryService.getAll(),
				ServiceTypeService.getAll(),
			]);

			if (industriesRes.status === 'fulfilled' && industriesRes.value) {
				setIndustries(industriesRes.value.body);

				const transformedForSelect = industriesRes.value.body.map(({ key, id }) => ({
					label: t(`industries:${key}`),
					value: id,
					id,
				}));

				setIndustriesForSelect(transformedForSelect);
			}

			if (servicesRes.status === 'fulfilled' && servicesRes.value) {
				setServices(servicesRes.value.body);

				const transformedForSelect = servicesRes.value.body.map(({ key, id, industryId }) => ({
					label: t(`services:${key}`),
					value: id,
					id,
					industryId,
				}));

				setServicesForSelect(transformedForSelect);
			}

		};

		fetchServicesIndustries();
	}, [isAuthenticated]);

	const getTranslatedIndustryById = (industryId: number) => {
		const industry = industries.find(industry => industry.id === industryId);

		return t(`industries:${industry?.key}`);
	};

	const getTranslatedServiceById = (serviceId: number) => {
		const service = services.find(service => service.id === serviceId);

		return t(`services:${service?.key}`);
	};

	const setSelectedIndustriesAndServices = (selectedItems: TMultiState) => {
		setSelectedItems(selectedItems);
	};

	const handleServiceChange = (value: TExtendedServiceOption) => {
		setSelectedItems({ selectedService: value });
	};
	const handleIndustryChange = (value: TExtendedIndustryOption) => {
		setSelectedItems({ selectedIndustry: value });
		setSelectedItems({selectedService: null});
	};

	const getSelectedItems = () => selectedItems;

	return {
		services,
		industries,
		servicesForSelect,
		industriesForSelect,
		getTranslatedIndustryById,
		getTranslatedServiceById,
		getSelectedItems,
		setSelectedIndustriesAndServices,
		handleServiceChange,
		handleIndustryChange,
	};
};
