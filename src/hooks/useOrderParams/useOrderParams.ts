import { useParams } from 'react-router-dom';

type TType = {
	orderId: string;
}

export const useOrderParams = (): TType => useParams<TType>();
