import { useEffect, useState } from 'react';

import { TNotify, TNotifyReturnType } from 'interfaces';

export const useProvideNotify = (): TNotifyReturnType => {
	const [notify, setNotify] = useState<TNotify>({
		isNotify: false,
		errorText: '',
	});

	const handleSetNotify = (notify: TNotify) => setNotify(notify);

	useEffect(() => {
		let id = null;

		if (notify.isNotify) {
			id = setTimeout(() => {
				setNotify({
					errorText: '',
					isNotify: false,
				});
			}, 5000);
		}

		return () => clearTimeout(id);
	}, [notify]);

	return {
		notify,
		handleSetNotify,
	};
};
