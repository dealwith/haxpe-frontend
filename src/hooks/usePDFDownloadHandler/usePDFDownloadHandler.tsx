import { useContext } from 'react';

import { PDFDownloadHandlerContext } from 'context';
import { TUsePDFDownloadHandlerReturnType } from 'hooks/interfaces';

export const usePDFDownloadHandler = (): TUsePDFDownloadHandlerReturnType =>
	useContext(PDFDownloadHandlerContext);
