import { useContext } from 'react';

import { ServicesIndustriesContext } from 'context';
import { useProvideServicesIndustries } from 'hooks';

type TResultUseServicesIndustries = ReturnType<typeof useProvideServicesIndustries>;

export const useServicesIndustries = (): TResultUseServicesIndustries => useContext(ServicesIndustriesContext);
