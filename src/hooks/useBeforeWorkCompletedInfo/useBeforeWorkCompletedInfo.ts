import { useContext } from 'react';

import { BeforeWorkCompletedContext } from 'context';
import { TBeforeWorkCompletedReturnType } from 'interfaces';

export const useBeforeWorkCompletedInfo = (): TBeforeWorkCompletedReturnType => useContext(BeforeWorkCompletedContext);
