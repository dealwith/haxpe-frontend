import { useState } from 'react';

import { Dictionary, TServicesTypeResult } from 'interfaces';

import { TUseEditableFieldsReturnType } from 'hooks/interfaces';

type TEditableFieldsType = {
	services?: TServicesTypeResult;
}

export const useProvideEditableFields = (): TUseEditableFieldsReturnType<any> => {
	const [editedFields, setEditedFields] = useState<Dictionary<TEditableFieldsType>>();

	const handleAddEditedFields = (data: Dictionary<TEditableFieldsType>) => {
		setEditedFields(prev => ({
			...prev,
			...data,
		}));
	};

	const handleClearEditableFields = () => setEditedFields(null);

	return {
		editedFields,
		handleAddEditedFields,
		handleClearEditableFields,
	};
};
