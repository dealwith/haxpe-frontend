import { useContext } from 'react';

import { TSelectedOrderReturnType } from 'interfaces';
import { SelectedOrderWorkerContext } from 'context';

export const useSelectedWorkerOrder = (): TSelectedOrderReturnType => useContext(SelectedOrderWorkerContext);
