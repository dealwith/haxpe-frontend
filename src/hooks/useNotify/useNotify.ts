import { TNotifyReturnType } from 'interfaces';
import { useContext } from 'react';
import { NotifyContext } from 'context';

export const useNotify = (): TNotifyReturnType => useContext(NotifyContext);
