import { useEffect, useState } from 'react';
import moment, { Moment } from 'moment';

import { getPreferLang } from 'utils';

type TResult = {
	format: string;
	use12Hours: boolean;
	now: Moment;
}

export const useLocalTimeFormat = (): TResult => {
	const [format, setFormat] = useState('HH:mm');

	const now = moment();
	const lng = getPreferLang();

	const localFormat = lng === 'de'
		? 'HH:mm'
		: 'h:mm a';
	const use12Hours = lng === 'de'
		? false
		: true;

	useEffect(() => {
		setFormat(localFormat);
	}, [localFormat]);

	return { format, use12Hours, now };
};
