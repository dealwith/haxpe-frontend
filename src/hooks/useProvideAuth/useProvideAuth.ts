import { useState, useEffect } from 'react';

import { AccountService } from 'services';
import {
	IAccount,
	EResponseStatus,
	ERoles,
	TRoles,
	ISignIn,
} from 'interfaces';
import { TUseAuthReturnType } from 'hooks/interfaces';

export const useProvideAuth = (): TUseAuthReturnType => {
	const [account, setAccount] = useState<IAccount | null>(null);
	const [isAuthenticated, setIsAuthenticated] = useState(false);
	const [isAdminPanelAccess, setIsAdminPanelAccess] = useState<boolean | undefined>();

	const checkUser = async () => {
		try {
			const {
				body: accountProfile,
				status,
				errorCode,
			} = await AccountService.getAccount();

			if (errorCode)
				throw new Error(errorCode);

			if (status === EResponseStatus.success) {
				setAccount(accountProfile);
				setIsAuthenticated(true);

				const isAdminPanelAccess = !(accountProfile.roles.includes('customer'));

				if (isAdminPanelAccess)
					setIsAdminPanelAccess(true);
			}
		} catch (err) {
			setIsAuthenticated(false);
			setIsAdminPanelAccess(false);
			console.error(err);
		}
	};

	//TODO: is setters need into this new function?
	const getUser = async () => {
		try {
			const {
				body: accountProfile,
				status,
			} = await AccountService.getAccount();

			if (status === EResponseStatus.success) {
				setAccount(accountProfile);
				setIsAuthenticated(true);

				return accountProfile;
			}

			setIsAuthenticated(false);
		} catch (err) {
			setIsAuthenticated(false);
			console.error(err);
		}
	};

	useEffect(() => {
		checkUser();
	}, []);

	const signin = async (data: ISignIn) => {
		const { body: accountRes } = await AccountService.login(data);

		const isWorker = accountRes?.roles?.some(role => role === ERoles.worker);
		const account = await getUser();

		return {
			...account,
			isWorker,
		};
	};

	//TODO: add args type later
	const facebookAuth = async res => {
		const { userID: userId, accessToken } = res;

		const response = await AccountService.loginFacebook({
			accessToken: accessToken,
			userId,
		});

		if (response.status === EResponseStatus.success)
			return getUser();
	};

	//with void problems into FacebookButton
	const googleAuth = async (googleUser): Promise<IAccount> => {
		const token = googleUser.getAuthResponse().id_token;

		const googleLogin = await AccountService.loginGoogle(token);

		if (googleLogin.status === EResponseStatus.success)
			return getUser();
	};

	const logout = async () => {
		try {
			await AccountService.logout();
			setAccount(null);
			setIsAuthenticated(false);
			setIsAdminPanelAccess(false);
		} catch (err) {
			console.error(err);
		}
	};

	const isRole = (inputRole: TRoles) => account?.roles?.some(role => role === inputRole);
	const isWorker = isRole(ERoles.worker);
	const isAdmin = isRole(ERoles.admin);
	const isPartner = isRole(ERoles.partner);
	const isCustomer = isRole(ERoles.customer);

	return {
		signin,
		checkUser,
		facebookAuth,
		googleAuth,
		logout,
		user: account,
		isAdminPanelAccess,
		isWorker,
		isAuthenticated,
		isAdmin,
		isPartner,
		isCustomer,
	};
};
