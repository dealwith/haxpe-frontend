import { useEffect, useState } from 'react';

import { THandleChangeStep } from 'hooks/interfaces';

type TReturnType<T> = {
	step: keyof T;
	handleChangeStep: THandleChangeStep<T>;
}

export const useStep = <T>(status: string, steps: T): TReturnType<T>  => {
	const [step, setStep] = useState<keyof T>();

	const handleChangeStep: THandleChangeStep<T> = (to): void => {
		setStep(to as keyof T);
	};

	useEffect(() => {
		for (const [key, value] of Object.entries(steps)) {
			if (Array.isArray(value)) {
				const isFind = value.find(step => step === status );

				if (isFind) {
					setStep(key as keyof T);

					break;
				}
			}

			if (status === value) {
				setStep(key as keyof T);

				break;
			}

		}
	}, [status, steps]);

	return { step, handleChangeStep };
};

