import { MouseEvent, useState } from 'react';

export const useDatagridRowOnClick = () => {
	const [prevElem, setPrevElem] = useState<HTMLTableRowElement>();

	const handleToggleClickedAttribute = (e: MouseEvent<HTMLElement>) => {
		const elem = (e.target as Element).closest('tr');

		if (!elem)
			return;

		if (elem.hasAttribute('data-clicked')) {
			elem.removeAttribute('data-clicked');

			return;
		}

		if (prevElem)
			prevElem.removeAttribute('data-clicked');

		setPrevElem(() => elem);
		elem.dataset.clicked = 'clicked';
	};

	return { onRowClick: handleToggleClickedAttribute };
};
