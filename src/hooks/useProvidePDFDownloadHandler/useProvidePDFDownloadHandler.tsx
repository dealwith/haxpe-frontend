import { useRef } from 'react';

import { TUsePDFDownloadHandlerReturnType } from 'hooks/interfaces';

export const useProvidePDFDownloadHandler = (): TUsePDFDownloadHandlerReturnType => {
	const downloadPDFRefAsHandler = useRef<() => void>();

	const setDownloadHandler = ({ toPdf }) => {
		if (toPdf)
			downloadPDFRefAsHandler.current = () => toPdf();

		return <></>;
	};

	return {
		setDownloadHandler,
		downloadHandler: () => downloadPDFRefAsHandler.current(),
	};
};
