import { useState } from 'react';

import {
	TAddService,
	TEditService,
	TExtraService,
	TExtraServices,
	TRemoveService,
} from 'interfaces';

import { useSelectedWorkerOrder } from 'hooks';

type TReturnType = {
	additionalServices: TExtraServices;
	addService: TAddService;
	editService: TEditService;
	removeService: TRemoveService;
}

export const useServiceTable = (): TReturnType => {
	const { selectedOrder } = useSelectedWorkerOrder();
	const [
		additionalServices,
		setServicesTableInfo,
	] = useState<TExtraServices>(selectedOrder?.extraServices || []);

	const addService = (data: TExtraService) => {
		setServicesTableInfo(services => [...services, data]);
	};

	const removeService = (index: number) => {
		setServicesTableInfo(services => {
			return services.filter((_, i) => index !== i);
		});
	};

	const editService = (index: number, data: TExtraService) => {
		setServicesTableInfo(services => {
			return services.map((service, i) => {
				if (i === index)
					return data;

				return service;
			});
		});
	};

	return {
		additionalServices,
		addService,
		editService,
		removeService,
	};
};
