import { useState } from 'react';
import { RowClickFunction, Record } from 'react-admin';

type TReturnType<T> = {
	isShow: boolean;
	record: T;
	handleRowClick: RowClickFunction;
}

export const useDatagridSelectedItem = <T>(): TReturnType<T> => {
	const [isShow, setIsShow] = useState(false);
	const [prevId, setPrevId] = useState('');
	const [record, setRecord] = useState<T>(null);

	const handleRowClick = (id: string, basePath: string, record: Record) => {
		setRecord(record as unknown as T);

		if (prevId === id) {
			setIsShow(false);
			setPrevId(null);

			return '';
		}

		setIsShow(true);
		setPrevId(id);

		return '';
	};

	return {
		isShow,
		record,
		handleRowClick,
	};
};
