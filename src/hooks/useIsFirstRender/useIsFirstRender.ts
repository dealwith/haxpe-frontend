import { useRef, useEffect } from 'react';

type TReturnType = {
	isFirstRender: boolean;
}

export const useIsFirstRender = (): TReturnType => {
	const isMountRef = useRef(true);

	useEffect(() => {
		isMountRef.current = false;
	}, []);

	return {isFirstRender: isMountRef.current};
};
