export type TUsePDFDownloadHandlerReturnType = {
	setDownloadHandler: (obj: { toPdf: () => void }) => JSX.Element;
	downloadHandler: () => void;
}
