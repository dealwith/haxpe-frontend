export type THandleChangeStep<T> = (to: keyof T) => void;
