import { Dictionary } from 'interfaces';

export type TUseEditableFieldsReturnType<T> = {
	editedFields: T;
	handleAddEditedFields: (data: Dictionary<any>) => void;
	handleClearEditableFields: () => void;
}
