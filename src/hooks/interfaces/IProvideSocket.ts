import { IWorkerSocket } from 'interfaces';

export interface IProvideSocket {
	socketState: IWorkerSocket | Record<string, never>;
}
