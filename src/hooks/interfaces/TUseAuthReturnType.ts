import { IAccount, ISignIn } from 'interfaces';

type TModifiedAccount = IAccount & { isWorker: boolean }

export type TUseAuthReturnType = {
	signin: (data: ISignIn) => Promise<TModifiedAccount>;
	checkUser: () => Promise<void>;
	facebookAuth: (res: any) => Promise<IAccount>;
	googleAuth: (res: any) => Promise<IAccount>;
	logout: () => void;
	user: IAccount;
	isWorker: boolean;
	isAuthenticated: boolean;
	isAdminPanelAccess: boolean;
	isAdmin: boolean;
	isPartner: boolean;
	isCustomer: boolean;
}
