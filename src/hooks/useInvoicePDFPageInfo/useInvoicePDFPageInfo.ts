import { useEffect, useState } from 'react';

import { AddressService, OrderService, PartnerService } from 'services';
import { IAddress, IOrderRes, IPartner } from 'interfaces';

import { useOrderParams } from 'hooks';

type TPageInfo = {
	order: IOrderRes;
	partner: IPartner;
	address: IAddress;
}

type TReturnType = {
	pageInfo: TPageInfo;
}

export const useInvoicePDFPageInfo = (): TReturnType => {
	const { orderId } = useOrderParams();
	const [pageInfo, setPageInfo] = useState<TPageInfo>();

	useEffect(() => {
		const fetchAndSetPageInfo = async () => {
			try {
				const {
					body: order,
				} = await OrderService.getOrderById(orderId);
				const {
					body: partner,
				} = await PartnerService.getPartnerById(order.partnerId);
				const {
					body: address,
				} = await AddressService.getAddressById(partner.addressId);

				setPageInfo({
					order: order,
					partner: partner,
					address: address,
				});
			} catch (err) {
				console.error(err);
			}
		};

		fetchAndSetPageInfo();
	}, []);

	return { pageInfo };
};
