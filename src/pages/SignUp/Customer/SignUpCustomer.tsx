import { FunctionComponent, useState } from 'react';
import { useTranslation } from 'react-i18next';

import mailIcon from './mailIcon.svg';

import {
	Container,
	P,
	Image,
	SignUpCustomerForm,
} from 'components';

import './sign-up-customer.scss';

export const SignUpCustomer: FunctionComponent = () => {
	const [isAuthSuccess, setAuthSuccess] = useState(false);
	const { t } = useTranslation('signUp');

	const baseClassName = 'sign-up-customer';
	const alternativeClassName = 'alternative';
	const signUpClassName = {
		component: baseClassName,
		socialButtons: `${baseClassName}__social-buttons`,
		alternativeContainer: `${baseClassName}__alternative-container ${alternativeClassName}`,
		alternativeLine: `${alternativeClassName}__line`,
		alternativeText: `${alternativeClassName}__text`,
		inputContainer: `${baseClassName}__input-container`,
	};

	return (
		<Container centered='center' isInner={true}>
			{isAuthSuccess
				? <div className='successful-screen'>
					<P isBold={true}>
						{t('thankYouForYourRegistration')}
					</P>
					<Image src={mailIcon} alt='Email icon' />
					<div>
						<P align='center'>
							{t('weSentYouAnEmail')}
						</P>
						<P isBold={true} align='center'>
							{t('checkInboxAndConfirmEmail')}
						</P>
					</div>
				</div>
				: <SignUpCustomerForm
					setAuthSuccess={setAuthSuccess}
					className={signUpClassName}
				/>
			}
		</Container>
	);
};
