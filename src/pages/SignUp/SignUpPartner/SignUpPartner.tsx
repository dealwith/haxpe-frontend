import { FunctionComponent, useState } from 'react';
import { Steps, Step } from 'react-step-builder';
import { useForm } from 'react-hook-form';

import { EResponseStatus, ISignUpPartnerForm } from 'interfaces';
import {
	AccountService,
	AddressService,
	PartnerService,
} from 'services';
import {
	getGeocodingData,
	getPreferLang,
	isObjIncludesType,
} from 'utils';

import {
	Container,
	SignUpPartnerGeneralForm,
	SignUpPartnerPasswordForm,
	SignUpPartnerSuccessfulForm,
} from 'components';

import './sign-up-partner.scss';

export const SignUpPartner: FunctionComponent = () => {
	const [isSend, setSend] = useState(false);
	const { control, handleSubmit: validateBeforeSubmit } = useForm<ISignUpPartnerForm>();

	const preferLanguage = getPreferLang();
	const handleSubmit = async (data: ISignUpPartnerForm) => {
		const {
			email,
			password,
			firstName,
			lastName,
			phone,
			city,
			street,
			buildingNum,
			zipCode,
			craftsmanAmount,
			companyName,
		} = data;

		try {
			const accountRegisterRaw = {
				email,
				password,
				firstName,
				lastName,
				phone,
				preferLanguage: preferLanguage,
			};
			const { body: { id: userId } } = await AccountService.register(accountRegisterRaw);

			const addressRaw = {
				street,
				buildingNum,
				city,
				country: '',
				lat: 0,
				lon: 0,
				zipCode,
				externalId: '',
			};

			const geocodingData = await getGeocodingData(street, buildingNum, city, zipCode);

			const { lat, lng } = geocodingData.geometry.location;
			const country = geocodingData
				?.address_components
				?.find(address_components => isObjIncludesType(address_components, 'country'))
				?.long_name;

			addressRaw.lat = lat;
			addressRaw.lon = lng;
			addressRaw.country = country;
			addressRaw.externalId = geocodingData.place_id;

			const { body: { id: addressId } } = await AddressService.createAddress(addressRaw);

			const partnerRaw = {
				name: companyName,
				description: '',
				addressId,
				ownerUserId: userId,
				numberOfWorkers: craftsmanAmount,
				industries: [],
			};

			partnerRaw.industries = data
				.industries
				.map(industry => ({ industryId: industry.industryId }));

			const { status: partnerResStatus } = await PartnerService.createPartner(partnerRaw);

			if (partnerResStatus === EResponseStatus.success)
				setSend(true);
		} catch (err) {
			console.error(err);
		}
	};

	return (
		<Container centered='center'>
			<form onSubmit={validateBeforeSubmit(handleSubmit)}>
				{isSend
					? <SignUpPartnerSuccessfulForm setSend={setSend}/>
					: <Steps>
						<Step
							component={({ ...stepProps }) => (
								<SignUpPartnerGeneralForm
									control={control}
									{...stepProps}
								/>
							)}
						/>
						<Step component={() => (
							<SignUpPartnerPasswordForm control={control}/>
						)} />
					</Steps>
				}
			</form>
		</Container>
	);
};
