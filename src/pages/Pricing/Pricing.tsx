import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import pricingIcon from './pricingIcon.svg';
import { H1, Image, P } from 'components';
import { PricesTable } from 'components/Table/PricesTable';

import './pricing.scss';

export const Pricing: FunctionComponent = () => {
	const { t } = useTranslation('pricingPage');

	const baseClassName = 'pricing';
	const pricingClassName = {
		component: baseClassName,
		title: `${baseClassName}__title`,
		table: `${baseClassName}__table`,
		iconContainer: `${baseClassName}__icon-container`,
		icon: `${baseClassName}__icon`,
		description: `${baseClassName}__description`,
		descriptionText: `${baseClassName}__description-text`,
	};

	return (
		<section className={pricingClassName.component}>
			<div className={pricingClassName.iconContainer}>
				<Image
					className={pricingClassName.icon}
					src={pricingIcon}
					alt='pricing icon'
					size='fluid'
				/>
			</div>
			<div className={pricingClassName.description}>
				<H1 className={pricingClassName.title} isGreen={true} align='left'>
					{t('title')}
				</H1>
				<PricesTable className={pricingClassName.table} />
				<P align='left' className={pricingClassName.descriptionText}>
					{t('description')}
				</P>
				<P align='left' className={pricingClassName.descriptionText}>
					{t('description2')}
				</P>
			</div>
		</section>
	);
};
