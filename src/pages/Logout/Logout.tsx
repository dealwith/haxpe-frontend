import { FunctionComponent, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { ROUTES } from 'constants/routes';

import { useAuth } from 'hooks';

export const Logout: FunctionComponent = () => {
	const { logout } = useAuth();
	const { push } = useHistory();

	useEffect(() => {
		logout();
		push(ROUTES.HOME);
	}, []);

	return null;
};
