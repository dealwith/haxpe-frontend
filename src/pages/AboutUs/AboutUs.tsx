import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { H1, P, Image } from 'components';

import icon from './plumberIcon.svg';

import './about-us.scss';

export const AboutUs: FunctionComponent = () => {
	const { t } = useTranslation('aboutUsPage');
	const baseClassName = 'about-us';
	const componentClassName = {
		component: baseClassName,
		textBlock: `${baseClassName}__text-block`,
		textBlockContainer: `${baseClassName}__text-block-container`,
		title: `${baseClassName}__title`,
		description: `${baseClassName}__description`,
		icon: `${baseClassName}__icon`,
		iconContainer: `${baseClassName}__icon-container`,
	};

	return (
		<div className={componentClassName.component}>
			<div className={componentClassName.textBlockContainer}>
				<div className={componentClassName.textBlock}>
					<H1
						isGreen={true}
						className={componentClassName.title}
						align='left'
					>
						{t('title')}
					</H1>
					<P align='left' className={componentClassName.description}>
						{t('description')}
					</P>
					<br/>
					<P align='left' className={componentClassName.description}>
						{t('description2')}
					</P>
				</div>
				<div className={componentClassName.textBlock}>
					<H1 isGreen={true} className={componentClassName.title} align='left'>
						{t('title2')}
					</H1>
					<P align='left' className={componentClassName.description}>
						{t('description3')}
					</P>
					<br/>
					<P align='left' className={componentClassName.description}>
						{t('description4')}
					</P>
				</div>
			</div>
			<div className={componentClassName.iconContainer}>
				<Image
					src={icon}
					alt='plumber icon'
					className={componentClassName.icon}
					size='fluid'
				/>
			</div>
		</div>
	);
};
