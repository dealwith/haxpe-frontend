import { FunctionComponent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ListProps } from 'react-admin';

import { concatAddress, transformEditedToOriginUserFields } from 'utils';
import { getSettings } from 'providers';
import { IDatagridRow, TSettingsPageInfo } from 'interfaces';

import { useEditableFields } from 'hooks';
import { DatagridRow, ResetPasswordForm, UpdateSettingsButton } from 'components';
import { Coupons } from '../Coupons';

import './settings.scss';

export const Settings: FunctionComponent<ListProps> = props => {
	const { t } = useTranslation([
		'user',
		'payment',
		'form',
	]);
	const [record, setRecord] = useState<TSettingsPageInfo>();
	const [pageData, setPageData] = useState<TSettingsPageInfo>();
	const [ changedFieldsNames, setChangedFieldsNames ] = useState([]);
	const {
		editedFields,
		handleClearEditableFields,
	} = useEditableFields<Pick<TSettingsPageInfo, 'account'>>();
	const editedUserFields = transformEditedToOriginUserFields({ editedFields });

	useEffect(() => {
		if (record)
			setPageData(record);
	}, [record]);

	useEffect(() => {
		return () => handleClearEditableFields();
	}, []);

	useEffect(() => {
		const fetchAndSetSettings = async() => {
			const { data }  = await getSettings();

			setRecord(data);
		};

		fetchAndSetSettings();
	}, []);

	const name = t('user:name');
	const surname = t('user:surname');
	const address = t('user:address');
	const tel = t('user:tel');
	const email = t('user:email');
	const status = t('user:status');
	const role = t(`user:${record?.account?.roles[0]}`);
	const userInformation = t('user:userInformation');
	const changePassword = t('form:changePassword');
	const taxRate = t('payment:taxRate');

	const transformEditedFieldsToOriginResponseFields = () => {
		const { account } = editedFields;

		setChangedFieldsNames(prev => {
			const edited = Object.entries(editedFields)
				.map(([key, value]) => value && key);

			return [...prev, ...edited];
		});

		const origin: TSettingsPageInfo = {
			...pageData,
			...(account && { account: {
				...pageData.account,
				...editedUserFields,
			} }),
		};

		setPageData(prev => ({...prev, ...origin}));
	};

	useEffect(() => {
		if (editedFields)
			transformEditedFieldsToOriginResponseFields();
	}, [editedFields]);

	const wrapper = 'settings-reset-password-form-wrapper';

	const userAddress = pageData?.address && concatAddress(pageData.address);
	const settingsInfo: IDatagridRow = {
		body: [
			[
				{
					title: userInformation,
					isEditable: true,
					editedFieldName: 'account',
					body: [
						{
							[name]: pageData?.account?.name,
							[surname]: pageData?.account?.surname,
							[address]: userAddress,
							[tel]: pageData?.account?.phoneNumber,
							[email]: pageData?.account?.email,
						},
					],
				},
				{
					title: status,
					textArray: [`${role}`],
				},
			],
			[
				{
					title: changePassword,
					component: () => <div className={wrapper}>
						<ResetPasswordForm/>
					</div>,

				},
			],
		],
	};

	const baseClassName = 'settings';

	return (
		<div className={baseClassName}>
			<DatagridRow info={settingsInfo} />
			<UpdateSettingsButton
				changedFieldsNames={changedFieldsNames}
				updateInfo={pageData}
				isDisabled={!editedFields}
			/>
			<Coupons {...props}/>
		</div>
	);
};
