import { FunctionComponent, useEffect } from 'react';
import {
	Datagrid,
	List,
	ListProps,
} from 'ra-ui-materialui';
import { useTranslation } from 'react-i18next';
import { useRefresh, Link } from 'react-admin';

import { IDatagridRow, TBillingPageInfo, TCustomerPageInfo } from 'interfaces';
import { ModifiedDate, concatAddress, getPreferLang } from 'utils';
import { ROUTES } from 'constants/index';

import {
	useDatagridRowOnClick,
	useDatagridSelectedItem,
	useIsFirstRender,
} from 'hooks';
import {
	AdminContainer,
	CorrectDate,
	DatagridRow,
	ShortTextPopup,
	FunctionField,
	TextField,
	AdminShowButton,
	PayoutIntervalRadioGroup,
	ButtonsContainer,
	AdminButton,
	Filter,
} from 'components';

export const Billing: FunctionComponent<ListProps> = props => {
	const { t } = useTranslation([
		'user',
		'customer',
		'payment',
		'button',
		'dateRange',
	]);
	const refresh = useRefresh();
	const { onRowClick } = useDatagridRowOnClick();
	const {
		handleRowClick,
		isShow,
		record,
	} = useDatagridSelectedItem<TBillingPageInfo>();
	const { isFirstRender } = useIsFirstRender();

	const { createDateToShow } = new ModifiedDate();

	const creationDate = t('dateRange:creationDate');

	const date = t('dateRange:date');
	const customerInformation = t('customer:customerInformation');
	const partnerId = `${t('partner:partner')}-ID`;
	const payoutId = `${t('payment:payout')}-ID`;
	const income = t('payment:income');
	const commission = t('payment:commission');
	const amount = t('payment:amount');
	const status = t('payment:status');
	const payoutInterval = t('payment:payoutInterval');
	const paymentInformation = t('payment:paymentInformation');
	const vatID = `${t('payment:vat')}-ID`;
	const bank = t('payment:bank');
	const iban = t('payment:iban');
	const bic = t('payment:bic');
	const name = t('user:name');
	const address = t('user:address');
	const tel = t('user:tel');
	const eMail = t('user:email');
	const showPayoutOverView = t('button:showPayoutOverView');
	const showInvoice = t('button:showInvoice');

	const userAddress = record?.address && concatAddress(record.address);
	const correctDate = record && createDateToShow({
		dateArg: record.creationDate,
	});

	const lng = getPreferLang();

	useEffect(() => {
		if (!isFirstRender)
			refresh();
	}, [lng]);

	const info: IDatagridRow = {
		title: {
			[date]: correctDate,
			[payoutId]: record?.id,
			[partnerId]: record?.partnerId,
			[income]: record?.income?.toString(),
			[commission]: record?.commission?.toString(),
			[amount]: record?.amount?.toString(),
		},
		body: [
			[
				{
					title: customerInformation,
					body: [
						{
							[name]: record?.user?.fullName,
							[address]: userAddress,
							[tel]: record?.user?.phoneNumber,
							[eMail]: record?.user?.email,
						},
					],
				},
				{
					title: paymentInformation,
					body: [
						{
							[bank]: `any text`,
							[iban]: `any text`,
							[bic]: `any text`,
							[vatID]: `any text`,
						},
					],
				},
				{
					component: () => <PayoutIntervalRadioGroup
						defaultCheckedPaymentInterval='weekly'
						title={payoutInterval}
					/>,
				},
			],
		],
	};

	return (
		<AdminContainer>
			<List exporter={false} filters={<Filter/>} {...props}>
				<Datagrid onClick={onRowClick} rowClick={handleRowClick}>
					<FunctionField
						sortBy='creationDate'
						label={creationDate}
						render={({ creationDate }: TCustomerPageInfo) => (
							<CorrectDate date={creationDate}/>
						)}
					/>
					<FunctionField
						// sortBy='id'
						label={payoutId}
						source='id'
						render={({ id }: TCustomerPageInfo) => (
							<ShortTextPopup>
								{id.toString()}
							</ShortTextPopup>
						)}
					/>
					{/* <FunctionField
						sortBy='partner.id'
						label={partnerId}
						source='partner.id'
						render={({ id }: TCustomerPageInfo) => (
							<ShortTextPopup>
								{id.toString()}
							</ShortTextPopup>
						)}
					/> */}
					<TextField
						// sortBy='name'
						label={income}
						source='income'
					/>
					<TextField
						// sortBy='latestOrder'
						label={commission}
						source='commission'
					/>
					<TextField
						// sortBy='latestOrder'
						label={amount}
						source='amount'
					/>
					<TextField
						// sortBy='latestOrder'
						label={status}
						source='status'
					/>
				</Datagrid>
			</List>
			{isShow
				&&  <>
					<ButtonsContainer centered='center'>
						<AdminShowButton position='center' record={record} />
						<AdminButton
							component={Link}
							to={ROUTES.BILLING.PAYOUT_OVERVIEW_PDF_AS_LINK(record.id)}
							label={showPayoutOverView}
						/>
						<AdminButton
							component={Link}
							to={ROUTES.BILLING.SHOW_INVOICE_PDF_AS_LINK(record?.id)}
							label={showInvoice}
						/>
					</ButtonsContainer>
					<DatagridRow info={info} />
				</>
			}
		</AdminContainer>
	);
};
