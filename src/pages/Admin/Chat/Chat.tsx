import { FunctionComponent, useState } from 'react';
import { Route } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { ModifiedDate } from 'utils';
import { IDatagridRow } from 'interfaces';

import {
	CustomerList,
	DatagridRow,
	MessagesList,
	ChatForm,
} from 'components';

import './chat.scss';

type TChatHeaderInfo = {
	creationDate: string;
	duration: string;
	orderId: string;
	customerId: string;
	workerId: string;
	service: string;
	industry: string;
	status: string;
}

const fakeInfo = {
	creationDate: '111',
	duration: '11:11:11',
	orderId: '111',
	customerId: '111',
	workerId: '111',
	service: '111',
	industry: '111',
	status: '111',
};

const fakeList: any[] = [
	{
		id: '2',
		creationDate: '222',
	},
	{
		id: '3',
		creationDate: '222',
	},
	{
		id: '4',
		creationDate: '222',
	},
	{
		id: '5',
		creationDate: '222',
	},
];

export const Chat: FunctionComponent = () => {
	const [headerInfo, setHeaderInfo] = useState<TChatHeaderInfo>(fakeInfo);
	const { t } = useTranslation([
		'dateRange',
		'time',
		'order',
		'customer',
		'worker',
		'industries',
		'services',
		'button',
	]);

	const { createDateToShow, createTimeToShow } = new ModifiedDate();
	const date = t('dateRange:date');
	const time = t('time:time');
	const orderId = `${t('order:order')}-ID`;
	const customerId = `${t('customer:customer')}-ID`;
	const workerId = `${t('worker:worker')}-ID`;
	const service = t('services:service');
	const industry = t('industries:industry');
	const status = t('order:status');
	const currentDate = headerInfo && createDateToShow({
		dateArg: headerInfo.creationDate,
	});
	const currentTime = headerInfo && createTimeToShow({
		durationSecond: headerInfo.duration,
	});

	const info: IDatagridRow = {
		title: {
			[date]: currentDate,
			[time]: currentTime,
			[orderId]: headerInfo?.orderId,
			[customerId]: headerInfo?.customerId,
			[workerId]: headerInfo?.workerId,
			[industry]: headerInfo?.industry,
			[service]: headerInfo?.service,
			[status]: headerInfo?.status,
		},
	};

	const baseClassName = 'chat';
	const componentClassName = {
		component: baseClassName,
		body: `${baseClassName}__body`,
		customers: `${baseClassName}__customers`,
		messages: `${baseClassName}__messages`,
		messagesListWrapper: `${baseClassName}__message-list-wrapper`,
		formWrapper: `${baseClassName}__form-wrapper`,
	};

	return (
		<div className={componentClassName.component}>
			<div>
				<DatagridRow info={info}/>
			</div>
			<div className={componentClassName.body}>
				<div className={componentClassName.customers}>
					<CustomerList list={fakeList}/>
				</div>
				<div className={componentClassName.messages}>
					<div className={componentClassName.messagesListWrapper}>
						<Route path={'/chat/:id'} component={MessagesList}/>
					</div>
					<div className={componentClassName.formWrapper}>
						<ChatForm/>
					</div>
				</div>
			</div>
		</div>
	);
};
