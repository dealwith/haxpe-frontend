import { FunctionComponent, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
	Logo,
	Button,
} from 'components';
import { AdminSignInForm } from './AdminSignInForm';
import { AdminRestorePasswordForm } from './AdminRestorePasswordForm';

import './admin-sign-in.scss';

export const AdminSignIn: FunctionComponent = () => {
	const [isForgot, setIsForgot] = useState(false);
	const { t } = useTranslation([
		'adminPage',
		'links',
	]);

	const baseClassName = 'admin-sign-in';
	const componentClassName = {
		component: baseClassName,
		languageSwitcher: `${baseClassName}__language-switcher`,
		innerContainer: `${baseClassName}__inner-container`,
		formContainer: `${baseClassName}__form-container`,
		dashboard: `${baseClassName}__dashboard`,
		logotypeText: `${baseClassName}__logotype-text`,
	};

	const handleForgot = () => setIsForgot(prev => !prev);

	return (
		<div className={componentClassName.component}>
			<div className={componentClassName.innerContainer}>
				<div className={componentClassName.dashboard}>
					<Logo isWhite={true}/>&nbsp;
					<span className={componentClassName.logotypeText}>- {t('adminPage:dashboard')}</span>
				</div>
				<div className={componentClassName.formContainer}>
					<AdminSignInForm/>
					<Button position='center' onClick={handleForgot} theme='transparent'>
						{t('links:forgot')}
					</Button>
					{isForgot
						&& <AdminRestorePasswordForm/>
					}
				</div>
			</div>
		</div>
	);
};
