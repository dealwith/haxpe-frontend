import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { AccountService } from 'services';

import { useAuth } from 'hooks';
import { Button, Fieldset, Input, Legend, Span } from 'components';

import './admin-sign-in-form.scss';

interface ISignInForm {
	email: string;
	password: string;
}

export const AdminSignInForm: FunctionComponent = () => {
	const {
		handleSubmit: validateBeforeSubmit,
		control,
		setError,
		formState: {
			errors,
		},
	} = useForm<ISignInForm>();
	const { checkUser } = useAuth();
	const { t } = useTranslation(['input', 'button', 'errors']);

	const handleSubmit = async (data: ISignInForm) => {
		try {
			await AccountService.login(data);
			checkUser();
		} catch (err) {
			if (err instanceof Error) {
				setError('email', {
					message: 'accountInvalidUserNameOrPassword',
				});
			}
			else
				console.error(err);

			console.error(err);
		}
	};

	console.log(errors);

	const baseClassName = 'admin-sign-in-form';
	const componentClassName = {
		component: baseClassName,
		legend: `${baseClassName}__legend`,
		fieldset: `${baseClassName}__fieldset`,
	};

	return (
		<form
			onSubmit={validateBeforeSubmit(handleSubmit)}
			className={componentClassName.component}
		>
			<Legend className={componentClassName.legend}>
				{t('adminPage:legend')}
			</Legend>
			<Fieldset className={componentClassName.fieldset}>
				{errors?.email?.message && <Span theme='error'>
					{t(`errors:${errors.email.message}`)}
				</Span>}
				<Input
					type='email'
					name='email'
					placeholder={t('input:email')}
					icon='email'
					size='full'
					control={control}
					rules={{ required: true }}
				/>
				<Input
					type='password'
					name='password'
					placeholder={t('input:password')}
					control={control}
					rules={{ required: true }}
					icon='lock'
					size='full'
				/>
				<Button type='submit' theme='green' size='full' rounded='m'>
					{t('button:login')}
				</Button>
			</Fieldset>
		</form>
	);
};
