import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { AccountService } from 'services';

import { useNotify } from 'hooks';
import { Button, Fieldset, Input } from 'components';

import './admin-restore-password-form.scss';

interface IRestoreForm {
	email: string;
}

export const AdminRestorePasswordForm: FunctionComponent = () => {
	const { handleSubmit: validateBeforeSubmit, control } = useForm<IRestoreForm>();
	const { handleSetNotify } = useNotify();
	const { t } = useTranslation(['input', 'button']);

	const handleSubmit = async ({ email }: IRestoreForm) => {
		try {
			await AccountService.setNewPassword(email);
		} catch (err) {
			if (err instanceof Error) {
				handleSetNotify({
					errorText: 'failed',
					isNotify: true,
				});
			}
		}
	};

	const baseClassName = 'admin-restore-password-form';
	const componentClassName = {
		component: baseClassName,
		fieldset: `${baseClassName}__fieldset`,
	};

	return (
		<form className={componentClassName.component} onSubmit={validateBeforeSubmit(handleSubmit)}>
			<Fieldset className={componentClassName.fieldset}>
				<Input
					name='email'
					type='email'
					icon='email'
					autoFocus={true}
					placeholder={t('input:email')}
					control={control}
					size='full'
				/>
				<Button
					type='submit'
					theme='green'
					size='full'
					rounded='m'
				>
					{t('button:resetPassword')}
				</Button>
			</Fieldset>
		</form>
	);
};
