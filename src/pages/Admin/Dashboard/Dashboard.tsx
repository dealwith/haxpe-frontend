import { FunctionComponent, useEffect, useMemo, useState } from 'react';
import { Error, Loading, useDataProvider } from 'react-admin';
import { useTranslation } from 'react-i18next';
import { Range, OnChangeProps } from 'react-date-range';
import * as dateFns from 'date-fns';

import { ModifiedDate } from 'utils';
import { getTotalCount, isDateRangesEqual } from './utils';
import { IDashboardMultiState, TDateRangesEqualProp } from './interfaces';

import { useMultiState } from 'hooks';
import { Chart, DateRangePicker, TitleAndBody } from 'components';

import './dashboard.scss';

type TItem = {
	selection: Range;
}

export const Dashboard: FunctionComponent = () => {
	const { t } = useTranslation('adminPage');
	const dataProvider = useDataProvider();
	const [charts, setCharts] = useMultiState<IDashboardMultiState>({
		customerChart: [],
		orderChart: [],
		partnerChart: [],
		workerChart: [],
	});
	const {
		customerChart,
		orderChart,
		partnerChart,
		workerChart,
	} = charts;
	const [loading, setLoading] = useState(true);
	const [error, setError] = useState('');

	const memoizedCustomersTotal = useMemo(() => getTotalCount(customerChart), [customerChart]);
	const memoizedOrdersTotal = useMemo(() => getTotalCount(orderChart), [orderChart]);
	const memoizedWorkersTotal = useMemo(() => getTotalCount(workerChart), [workerChart]);
	const memoizedPartnersTotal = useMemo(() => getTotalCount(partnerChart), [partnerChart]);

	const { weekStart, fullDate } = new ModifiedDate();

	fullDate.setDate(weekStart);

	const [date, setDate] = useState<Range[]>([
		{
			startDate: fullDate,
			endDate: dateFns.addDays(fullDate, 6),
			key: 'selection',
		},
	]);

	const [ stateDate ] = date;
	const { startDate: stateStartDate } = stateDate;
	const { endDate: stateEndDate } = stateDate;

	let isYearRange = false;
	const isStartDateMonthMin = stateStartDate.getMonth() === 0;
	const isEndDateMonthMax = stateEndDate.getMonth() === 11;

	if (isStartDateMonthMin && isEndDateMonthMax)
		isYearRange = true;

	useEffect(() => {
		const getAndSetCharts = async () => {
			try {
				const { data } = await dataProvider.getDashboard(stateDate);
				const [ chartsRes ] = data;

				setCharts({...chartsRes});
				setLoading(false);
			} catch (error) {
				setError(error);
				setLoading(false);
				console.warn(error);
			}
		};

		getAndSetCharts();
	}, [stateDate]);

	if (loading)
		return <Loading />;

	if (error.length)
		return <Error />;

	const handleChange = ({ selection, selection: {startDate, endDate} }: TItem & OnChangeProps) => {
		const date: TDateRangesEqualProp = {
			firstDate: {
				startDate: stateStartDate,
				endDate: stateEndDate,
			},
			secondDate: {
				startDate,
				endDate,
			},
		};

		const isEqualDateRanges = isDateRangesEqual(date);

		!isEqualDateRanges && setDate([selection]);
	};

	const baseClassName = 'dashboard';
	const componentClassName = {
		component: baseClassName,
		chartGroup: `${baseClassName}__chart-group`,
		chart: `${baseClassName}__chart`,
		dateRangePickerWrapper: `${baseClassName}__date-range-picker-wrapper`,
	};

	const totalNumberText = t('totalNumber');

	return (
		<div className={componentClassName.component}>
			<div className={componentClassName.chartGroup}>
				<Chart
					title={t('orders')}
					className={componentClassName.chart}
					chartData={orderChart}
					isYearRange={isYearRange}
				>
					<TitleAndBody
						color='white'
						title={totalNumberText}
						body={memoizedOrdersTotal}
					/>
					<TitleAndBody
						color='white'
						title={t('totalTurnover')}
						body='any text'
					/>
				</Chart>
				<Chart
					title={t('customers')}
					className={componentClassName.chart}
					chartData={customerChart}
					isYearRange={isYearRange}
				>
					<TitleAndBody
						color='white'
						title={totalNumberText}
						body={memoizedCustomersTotal}
					/>
				</Chart>
			</div>
			<div className={componentClassName.chartGroup}>
				<Chart
					title={t('partnerFirms')}
					className={componentClassName.chart}
					chartData={partnerChart}
					isYearRange={isYearRange}
				>
					<TitleAndBody
						color='white'
						title={totalNumberText}
						body={memoizedPartnersTotal}
					/>
				</Chart>
				<Chart
					title={t('craftsmen')}
					className={componentClassName.chart}
					chartData={workerChart}
					isYearRange={isYearRange}
				>
					<TitleAndBody
						color='white'
						title={totalNumberText}
						body={memoizedWorkersTotal}
					/>
				</Chart>
			</div>
			<div className={componentClassName.dateRangePickerWrapper}>
				<DateRangePicker state={date} handleChange={handleChange}/>
			</div>
		</div>
	);
};
