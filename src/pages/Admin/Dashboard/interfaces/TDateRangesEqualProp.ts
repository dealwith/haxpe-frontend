type TDate = {
	startDate: Date;
	endDate: Date;
}

export type TDateRangesEqualProp = {
	firstDate: TDate;
	secondDate: TDate;
}
