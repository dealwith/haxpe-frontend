import { IStatistic } from 'interfaces';

export interface IDashboardMultiState {
	customerChart: IStatistic[];
	orderChart: IStatistic[];
	partnerChart: IStatistic[];
	workerChart: IStatistic[];
}
