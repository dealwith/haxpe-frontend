import { IStatistic } from 'interfaces';

export const getTotalCount = (arr: IStatistic[]): string => {
	return arr.reduce((acc, { count }) => acc + count, 0).toString();
};
