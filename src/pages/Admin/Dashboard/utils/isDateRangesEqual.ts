import { ModifiedDate } from 'utils';
import { TDateRangesEqualProp } from '../interfaces';

export const isDateRangesEqual = (date: TDateRangesEqualProp): boolean => {
	const { firstDate, secondDate } = date;
	const firstStartDate = new ModifiedDate(firstDate.startDate);
	const firstEndDate = new ModifiedDate(firstDate.endDate);
	const secondStartDate = new ModifiedDate(secondDate.startDate);
	const secondEndDate = new ModifiedDate(secondDate.endDate);

	if (
		firstStartDate.dayInWeek === secondStartDate.dayInWeek
		&& firstStartDate.month === secondStartDate.month
		&& firstStartDate.year === secondStartDate.year
		&& firstEndDate.dayInWeek === secondEndDate.dayInWeek
		&& firstEndDate.month === secondEndDate.month
		&& firstEndDate.year === secondEndDate.year
	)
		return true;

	return false;
};
