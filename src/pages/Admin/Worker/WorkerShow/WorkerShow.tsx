import { FunctionComponent, useEffect, useState } from 'react';
import { ShowProps, useShowController } from 'react-admin';
import { useTranslation } from 'react-i18next';

import { transformEditedToOriginUserFields } from 'utils';
import { IDatagridRow, TWorkerPageInfo } from 'interfaces';

import { useEditableFields } from 'hooks';
import {
	DatagridRow,
	createSectionBody,
	ServicesEditForm,
	UpdateWorkerButton,
} from 'components';

export const WorkerShow: FunctionComponent<ShowProps> = props => {
	const { t } = useTranslation(['adminPage', 'industries', 'services']);
	const { record } = useShowController<TWorkerPageInfo>(props);
	const [ changedFieldsNames, setChangedFieldsNames ] = useState([]);
	const [pageData, setPageData] = useState<TWorkerPageInfo>();
	const {
		editedFields,
		handleClearEditableFields,
	} = useEditableFields<Pick<TWorkerPageInfo, 'user' | 'services'>>();
	const editedUserFields = transformEditedToOriginUserFields({ editedFields });

	useEffect(() => {
		if (record)
			setPageData(record);

	}, [record]);

	useEffect(() => {
		return () => handleClearEditableFields();
	}, []);

	const workerId = `${t('craftsmen')}-ID`;
	const partnerId = `${t('partner')}-ID`;
	const workerInformation = `${t('craftsmen')} ${t('information')}`;
	const services = t('services');
	const name = t('name');
	const surname = t('user:surname');
	const industries = t('industries');
	const status = t('status');
	const orderTime = t('orderTime');
	const orderSales = t('orderSales');
	const finishedOrders = t('finishedOrders');
	const cancelledOrders = t('cancelledOrders');
	const rejectedOrders = t('rejectedOrders');
	const annualSales = t('annualSales');
	const quarterlySales = t('quarterlySales');
	const monthlySales = t('monthlySales');
	const weeklySales = t('weeklySales');
	const dailySales = t('dailySales');
	const paymentMethods = t('paymentMethods');
	const newSales = t('newSales');
	const cashReceipts = t('cashReceipts');
	const tel = 'Tel';
	const eMail = 'E-Mail';

	const transformEditedFieldsToOriginResponseFields = () => {
		const {
			services,
			user,
		} = editedFields;

		setChangedFieldsNames(prev => {
			const edited = Object.entries(editedFields)
				.map(([key, value]) => value && key);

			return [...prev, ...edited];
		});

		const origin: TWorkerPageInfo = {
			...pageData,
			...(user && { user: {
				...pageData.user,
				...editedUserFields,
			} }),
			...(services && { services }),
		};

		setPageData(prev => ({...prev, ...origin}));
	};

	useEffect(() => {
		if (editedFields)
			transformEditedFieldsToOriginResponseFields();

	}, [editedFields]);

	const serviceSectionBodyArgs = {
		data: pageData?.services,
		keyName: 'service',
		namespace: 'services',
	};

	const servicesSectionBody = createSectionBody(serviceSectionBodyArgs);

	const info: IDatagridRow = {
		title: {
			[workerId]: pageData?.id,
			[partnerId]: pageData?.partnerId,
			[name]: pageData?.user?.fullName,
			[industries]: t(`industries:${pageData?.industry?.key}`),
			[status]: `any text`,
		},
		body: [
			[
				{
					title: workerInformation,
					isEditable: true,
					editedFieldName: 'user',
					body: [
						{
							[name]: pageData?.user?.name,
							[surname]: pageData?.user?.surname,
							[tel]: pageData?.user?.phoneNumber,
							[eMail]: pageData?.user?.email,
						},
					],
				},
				{
					title: services,
					isEditable: true,
					component: ({
						body,
						handleClose,
					}) => <ServicesEditForm
						body={body}
						handleClose={handleClose}
						partnerServiceTypes={pageData.partnerServiceTypes}
						partnerIndustries={pageData.partnerIndustries}
					/>,
					body: servicesSectionBody,
				},
			],
			[
				{
					title: `Ø ${orderTime}`,
					textArray: [`any text`],
				},
				{
					title: `Ø ${orderSales}`,
					textArray: [`any text`],
				},
				{
					title: `Ø ${finishedOrders}`,
					textArray: [`any text`],
				},
				{
					title: `Ø ${cancelledOrders}`,
					textArray: [`any text`],
				},
				{
					title: `Ø ${rejectedOrders}`,
					textArray: [`any text`],
				},
			],
			[
				{
					title: annualSales,
					textArray: [`any text`],
				},
				{
					title: quarterlySales,
					textArray: [`any text`],
				},
				{
					title: monthlySales,
					textArray: [`any text`],
				},
				{
					title: weeklySales,
					textArray: [`any text`],
				},
				{
					title: dailySales,
					textArray: [`any text`],
				},
			],
			[
				{
					title: paymentMethods,
					textArray: [`any text`, `any text`, `any text`, `any text`, `any text`],
				},
			],
			[
				{
					title: newSales,
					textArray: [`any text`],
				},
				{
					title: cashReceipts,
					textArray: [`any text`],
				},
			],
		],
		footerButtonsGroup: [
			<UpdateWorkerButton
				changedFieldsNames={changedFieldsNames}
				updateInfo={pageData}
				isDisabled={!editedFields}
			/>,
		],
	};

	return <DatagridRow info={info}/>;
};
