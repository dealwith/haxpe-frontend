import { FunctionComponent } from 'react';
import {
	Datagrid,
	List,
	ListProps,
} from 'ra-ui-materialui';
import { useTranslation } from 'react-i18next';

import { IDatagridRow, TWorkerPageInfo } from 'interfaces';

import {
	useDatagridRowOnClick,
	useDatagridSelectedItem,
} from 'hooks';
import {
	DatagridRow,
	createSectionBody,
	FunctionField,
	TextField,
	AdminShowButton,
	AdminContainer,
	ShortTextPopup,
	Filter,
} from 'components';
import { useAdminRefresh } from '../hooks';

export const Worker: FunctionComponent<ListProps> = props => {
	useAdminRefresh();

	const { t } = useTranslation([
		'adminPage',
		'industries',
		'services',
		'partner',
		'worker',
		'user',
	]);
	const { onRowClick } = useDatagridRowOnClick();
	const {
		handleRowClick,
		isShow,
		record,
	} = useDatagridSelectedItem<TWorkerPageInfo>();

	const handleIndustryRender = ({ industry }: TWorkerPageInfo) => {
		if (industry?.key)
			return t(`industries:${industry.key}`);

		return null;
	};

	const workerInformation = `${t('worker:worker')} ${t('worker:workerInformation')}`;
	const workerId = `${t('worker:worker')}-ID`;
	const status = t('worker:status');
	const partnerId = `${t('partner:partner')}-ID`;
	const name = t('user:name');
	const tel = t('user:tel');
	const eMail = t('user:email');
	const industries = t('industries:industries');
	const services = t('services:services');
	//TODO: add when user API completed and remove record.user.accountStatus and isLocked
	const activated = t('user:activated');
	const deactivated = t('user:deactivated');
	const requested = t('user:requested');

	const args = {
		data: record?.services,
		keyName: 'service',
		namespace: 'services',
	};

	const sectionBody = createSectionBody(args);

	const info: IDatagridRow = {
		title: {
			[workerId]: record?.id,
			[partnerId]: record?.partnerId,
			[name]: record?.user?.fullName,
			[industries]: t(`industries:${record?.industry?.key}`),
			[status]: record?.user?.accountStatus,
		},
		body: [
			[
				{
					title: workerInformation,
					body: [
						{
							[name]: record?.user?.fullName,
							[tel]: record?.user?.phoneNumber,
							[eMail]: record?.user?.email,
						},
					],
				},
				{
					title: services,
					body: sectionBody,
				},
			],
		],
	};

	return (
		<AdminContainer>
			<List {...props} filters={<Filter/>}>
				<Datagrid onClick={onRowClick} rowClick={handleRowClick}>
					<FunctionField
						sortBy='id'
						label={workerId}
						render={({ id }: TWorkerPageInfo) => (
							<ShortTextPopup>
								{id.toString()}
							</ShortTextPopup>
						)}
					/>
					<FunctionField
						sortBy='partnerId'
						label={partnerId}
						render={({ partnerId }: TWorkerPageInfo) => (
							<ShortTextPopup>
								{partnerId.toString()}
							</ShortTextPopup>
						)}
					/>
					<TextField
						sortBy='name'
						label={name}
						source='user.fullName'
					/>
					<FunctionField
						sortBy='industries'
						label={industries}
						render={handleIndustryRender} />
					<TextField
						label={status}
						source='user.lockoutEnabled'
					/>
				</Datagrid>
			</List>
			{isShow
				&& <>
					<AdminShowButton position='center' record={record}/>
					<DatagridRow info={info}/>
				</>
			}
		</AdminContainer>
	);
};
