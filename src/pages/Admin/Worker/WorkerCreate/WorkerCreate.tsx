import { useEffect, FunctionComponent, useState } from 'react';
import { Create, CreateProps, SimpleForm } from 'react-admin';
import { useTranslation } from 'react-i18next';

import { IndustryService, PartnerService, ServiceTypeService } from 'services';
import { isObjectWithProperties } from 'utils';
import { IPartner, IMultiServiceState } from 'interfaces';

import { useMultiState, useNotify } from 'hooks';
import { WorkerCreateForm } from 'components';

export const WorkerCreate: FunctionComponent<CreateProps> = props => {
	const [partnerInfo, setPartnerInfo] = useState<IPartner | null>(null);
	const { t } = useTranslation(['industries', 'services']);
	const { handleSetNotify } = useNotify();
	const [multiServiceState, setMultiServiceState] = useMultiState<IMultiServiceState>({
		industries: [],
		services: [],
		responseServices: [],
		responseIndustries: [],
		selectedIndustry: {},
		selectedService: {},
	});
	const { selectedIndustry, responseIndustries } = multiServiceState;

	const isNonEmptySelectedIndustry =  isObjectWithProperties(selectedIndustry);

	useEffect(() => {
		PartnerService.getPartnerInfo()
			.then(({ status, body }) => {
				if (status === 'success')
					setPartnerInfo(body);

			})
			.catch(err => console.error(err));
	}, []);

	useEffect(() => {
		if (partnerInfo) {
			try {
				const fetchAndSetMultiServiceState = async () => {
					const [
						industriesRes,
						servicesRes,
					] = await Promise.all([
						IndustryService.getAll(),
						ServiceTypeService.getAll(),
					]);
					const { body: industries } = industriesRes;
					const { body: services } = servicesRes;

					setMultiServiceState({responseServices: services});
					setMultiServiceState({responseIndustries: industries});
				};

				fetchAndSetMultiServiceState();
			} catch (err) {
				if (err instanceof Error) {
					handleSetNotify({
						errorText: 'failed',
						isNotify: true,
					});
				} else
					console.error(err);

				console.error(err);
			}
		}
	}, [partnerInfo]);

	useEffect(() => {
		if (responseIndustries.length) {
			const setIndustries = () => {
				const partnerIndustries = partnerInfo.industries.map(({ industryId }) => industryId);
				const { responseIndustries } = multiServiceState;
				const filteredIndustries = responseIndustries.filter(({ id }) => partnerIndustries.includes(id));
				const transformedIndustries = filteredIndustries.map(({ id, key }) => {
					const translation = t(`industries:${key}`);

					return {
						id,
						name: translation,
					};
				});

				setMultiServiceState({
					industries: transformedIndustries,
				});
			};

			setIndustries();
		}
	}, [responseIndustries, t]);

	useEffect(() => {
		if (isNonEmptySelectedIndustry) {
			const setServices = async () => {
				const { responseServices } = multiServiceState;
				const { value: id } = multiServiceState.selectedIndustry;
				const filteredServices = responseServices.filter(service => service.industryId === id);
				const transformedServices = filteredServices.map(({ id, key }) => {
					const translation = t(`services:${key}`);

					return {
						id,
						name: translation,
					};
				});

				setMultiServiceState({
					services: transformedServices,
				});
			};

			setServices();
		}
	}, [selectedIndustry, t]);

	const formProps = {
		setMultiServiceState,
		multiServiceState,
	};

	return (
		<Create {...props}>
			<SimpleForm>
				<WorkerCreateForm {...formProps}/>
			</SimpleForm>
		</Create>
	);
};
