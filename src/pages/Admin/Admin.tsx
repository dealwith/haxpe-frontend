import { FunctionComponent } from 'react';

import { useAuth } from 'hooks';

import { AdminApp } from 'components';
import { AdminSignIn } from './AdminSignIn';

import './admin.scss';

export const Admin: FunctionComponent = () => {
	const { isAdminPanelAccess } = useAuth();

	const baseClassName = 'admin-page';

	const bodyRender = () => {
		if (isAdminPanelAccess)
			return <AdminApp/>;

		return <AdminSignIn/>;
	};

	const renderedBody = bodyRender();

	return <div className={baseClassName}>
		{renderedBody}
	</div>;
};
