import { FunctionComponent } from 'react';
import {
	Datagrid,
	List,
	ListProps,
} from 'ra-ui-materialui';
import { useTranslation } from 'react-i18next';

import { IDatagridRow, TCustomerPageInfo } from 'interfaces';
import { ModifiedDate, concatAddress } from 'utils';

import {
	useDatagridRowOnClick,
	useDatagridSelectedItem,
} from 'hooks';
import {
	AdminContainer,
	CorrectDate,
	DatagridRow,
	ShortTextPopup,
	FunctionField,
	TextField,
	AdminShowButton,
	Filter,
} from 'components';
import { useAdminRefresh } from '../hooks';

export const Customers: FunctionComponent<ListProps> = props => {
	useAdminRefresh();

	const { t } = useTranslation([
		'user',
		'customer',
		'payment',
		'order',
	]);
	const { onRowClick } = useDatagridRowOnClick();
	const {
		handleRowClick,
		isShow,
		record,
	} = useDatagridSelectedItem<TCustomerPageInfo>();

	const { createDateToShow } = new ModifiedDate();

	const customerID = `${t('customer:customer')}-ID`;
	const customerInformation = t('customer:customerInformation');
	const name = t('user:name');
	const latestOrder = t('order:latestOrder');
	const registration = t('user:registration');
	const address = t('user:address');
	const tel = t('user:tel');
	const eMail = t('user:email');
	const paymentInformation = t('payment:paymentInformation');
	const vatID = `${t('payment:vat')}-ID`;
	const bank = t('payment:bank');
	const iban = t('payment:iban');
	const bic = t('payment:bic');

	const userAddress = record?.address && concatAddress(record.address);
	const correctDate = record && createDateToShow({
		dateArg: record.creationDate,
	});
	const latestOrderCorrectDate = record?.latestOrderDate && createDateToShow({
		dateArg: record.latestOrderDate,
	});

	const info: IDatagridRow = {
		title: {
			[customerID]: record?.id,
			[name]: record?.user?.fullName,
			[registration]: correctDate,
			[latestOrder]: latestOrderCorrectDate,
		},
		body: [
			[
				{
					title: customerInformation,
					body: [
						{
							[name]: record?.user?.fullName,
							[address]: userAddress,
							[tel]: record?.user?.phoneNumber,
							[eMail]: record?.user?.email,
						},
					],
				},
				{
					title: paymentInformation,
					body: [
						{
							[bank]: `any text`,
							[iban]: `any text`,
							[bic]: `any text`,
							[vatID]: `any text`,
						},
					],
				},
			],
		],
	};

	return (
		<AdminContainer>
			<List exporter={false} filters={<Filter/>} {...props}>
				<Datagrid onClick={onRowClick} rowClick={handleRowClick}>
					<TextField
						sortBy='name'
						label={name}
						source='user.fullName'
					/>
					<FunctionField
						sortBy='id'
						label={customerID}
						source='id'
						render={({ id }: TCustomerPageInfo) => (
							<ShortTextPopup>
								{id.toString()}
							</ShortTextPopup>
						)}
					/>
					<FunctionField
						sortBy='creationDate'
						label={registration}
						render={({ creationDate }: TCustomerPageInfo) => <CorrectDate date={creationDate}/>}
					/>
					<FunctionField
						sortBy='latestOrderDate'
						label={latestOrder}
						render={({ latestOrderDate }: TCustomerPageInfo) => <CorrectDate date={latestOrderDate}/>}
					/>
				</Datagrid>
			</List>
			{isShow
					&&  <>
						<AdminShowButton position='center' record={record}/>
						<DatagridRow info={info}/>
					</>
			}
		</AdminContainer>
	);
};
