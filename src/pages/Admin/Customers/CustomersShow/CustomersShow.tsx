import { FunctionComponent, useEffect, useState } from 'react';
import {
	DeleteButton,
	ShowProps,
	useShowController,
} from 'react-admin';
import { useTranslation } from 'react-i18next';

import { IDatagridRow, TCustomerPageInfo } from 'interfaces';
import {
	concatAddress,
	ModifiedDate,
	transformEditedToOriginUserFields,
} from 'utils';

import { useEditableFields } from 'hooks';
import { DatagridRow, UpdateCustomerButton, H3 } from 'components';
import { CustomerOrderList } from './CustomerOrderList';
import { useAdminRefresh } from 'pages/Admin/hooks';

export const CustomersShow: FunctionComponent<ShowProps> = props => {
	useAdminRefresh();

	const { t } = useTranslation([
		'button',
		'order',
		'payment',
		'customer',
		'user',
	]);
	const { record } = useShowController<TCustomerPageInfo>(props);
	const [pageData, setPageData] = useState<TCustomerPageInfo>();
	const [ changedFieldsNames, setChangedFieldsNames ] = useState([]);
	const {
		editedFields,
		handleClearEditableFields,
	} = useEditableFields<Pick<TCustomerPageInfo, 'user'>>();
	const editedUserFields = transformEditedToOriginUserFields({ editedFields });
	const { createDateToShow } = new ModifiedDate();

	useEffect(() => {
		if (record)
			setPageData(record);

	}, [record]);

	useEffect(() => {
		return () => handleClearEditableFields();
	}, []);

	const customerID = `${t('customer:customer')}-ID`;
	const customerInformation = `${t('customer:customer')} ${t('user:information')}`;
	const paymentInformation = `${t('payment:payment')} ${t('user:information')}`;
	const registration = t('user:registration');
	const name = t('user:name');
	const address = t('user:address');
	const surname = t('user:surname');
	const vatID = `${t('payment:vat')}-ID`;
	const orders = t('order:order');
	const latestOrder = t('order:latestOrder');
	const bank = 'Bank';
	const iban = 'IBAN';
	const bic = 'BIC';
	const tel = 'Tel';
	const eMail = 'E-Mail';

	const transformEditedFieldsToOriginResponseFields = () => {
		const { user } = editedFields;

		setChangedFieldsNames(prev => {
			const edited = Object.entries(editedFields)
				.map(([key, value]) => value && key);

			return [...prev, ...edited];
		});

		const origin: TCustomerPageInfo = {
			...pageData,
			...(user && { user: {
				...pageData.user,
				...editedUserFields,
			} }),
		};

		setPageData(prev => ({...prev, ...origin}));
	};

	useEffect(() => {
		if (editedFields)
			transformEditedFieldsToOriginResponseFields();

	}, [editedFields]);

	const userAddress = pageData?.address && concatAddress(pageData.address);
	const correctDate = pageData?.creationDate && createDateToShow({
		dateArg: pageData.creationDate,
	});
	const latestOrderCorrectDate = pageData?.latestOrderDate && createDateToShow({
		dateArg: pageData.latestOrderDate,
	});

	const info: IDatagridRow = {
		title: {
			[customerID]: pageData?.id,
			[name]: pageData?.user?.fullName,
			[registration]: correctDate,
			[latestOrder]: latestOrderCorrectDate,
		},
		body: [
			[
				{
					title: customerInformation,
					isEditable: true,
					editedFieldName: 'user',
					body: [
						{
							[name]: pageData?.user?.name,
							[surname]: pageData?.user?.surname,
							[address]: userAddress,
							[tel]: pageData?.user?.phoneNumber,
							[eMail]: pageData?.user?.email,
						},
					],
				},
				{
					title: paymentInformation,
					isEditable: true,
					editedFieldName: 'payment',
					body: [
						{
							[bank]: `any text`,
							[iban]: `any text`,
							[bic]: `any text`,
							[vatID]: `any text`,
						},
					],
				},
			],
		],
		footerButtonsGroup: [
			<UpdateCustomerButton
				changedFieldsNames={changedFieldsNames}
				updateInfo={pageData}
				isDisabled={!editedFields}
			/>,
			<DeleteButton
				record={pageData}
				label={t('button:deleteAccount')}
			/>,
		],
	};

	return (
		<>
			<DatagridRow info={info}/>
			<H3 align='center' color='light-green'>{orders}</H3>
			<CustomerOrderList customerId={pageData?.id}/>
		</>
	);
};
