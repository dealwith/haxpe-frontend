import { FunctionComponent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
	Datagrid,
	TextField,
	Loading,
	ListContextProvider,
	FunctionField,
	useNotify,
} from 'react-admin';

import { IndustriesServiceTypesService, OrderService } from 'services';
import { IIndustry, IOrder, IOrderRes, IServiceType } from 'interfaces';
import { ModifiedDate, transformKeyBy } from 'utils';

type TProps = {
	customerId: string;
}

export const CustomerOrderList: FunctionComponent<TProps> = ({ customerId }) => {
	const [orders, setOrders] = useState<IOrder[]>([]);
	const notify = useNotify();
	const [isOrdersIntoApi, setIsOrdersIntoApi] = useState<boolean>(true);
	const { t } = useTranslation([
		'adminPage',
		'industries',
		'services',
		'time',
		'dateRange',
		'payment',
	]);

	const { createDateToShow, createTimeToShow } = new ModifiedDate();

	useEffect(() => {
		if (customerId) {
			const getAndSetOrders = async () => {
				const { body: orders } = await OrderService.getOrder({ CustomerId: customerId });

				if (!orders.length) {
					setIsOrdersIntoApi(false);
					notify(t('order:ordersNotFound'));

					return;
				}

				const { industries, services } = await IndustriesServiceTypesService.get();

				const getIndustry = (order: IOrderRes): IIndustry => {
					return industries.find(({ id }) => id === order.industryId);
				};
				const getService = (order: IOrderRes): IServiceType => {
					return services.find(({ id }) => id === order.serviceTypeId);
				};

				const transformedOrders = orders.map(order => {
					const copyOrder = {...order};

					return {
						...copyOrder,
						industry: getIndustry(order).key,
						serviceType: getService(order).key,
					};
				});

				setOrders(transformedOrders);
			};

			getAndSetOrders();
		}
	}, [customerId]);

	const handleDataRender = ( { orderDate }: IOrder ) => createDateToShow({
		dateArg: orderDate,
	});

	const handleTimeRender = ( { duration }: IOrder ) => createTimeToShow({
		durationSecond: duration,
	});
	const handleIndustryRender = ( { industry }: IOrder ) => t(`industries:${industry}`);
	const handleServiceTypeRender = ( { serviceType }: IOrder ) => t(`services:${serviceType}`);

	if (isOrdersIntoApi && !orders.length)
		return <Loading />;

	return (
		<ListContextProvider value={{
			data: transformKeyBy(orders, 'id'),
			ids: orders.map(({ id }) => id),
			total: orders.length,
			currentSort: { field: 'id', order: 'ASC' },
			basePath: '/customers',
			resource: '/customers',
			selectedIds: [],
		}}>
			<Datagrid>
				<TextField label={`${t('order')}-ID`} source='id' />
				<TextField label={`${t('partner')}-ID`} source='partnerId' />
				<TextField label={`${t('customer')}-ID`} source='customerId' />
				<FunctionField label={t('dateRange:date')} render={handleDataRender} />
				<FunctionField label={t('time:time')} render={handleTimeRender} />
				<FunctionField label={t('industries:industry')} render={handleIndustryRender} />
				<FunctionField label={t('services:service')} render={handleServiceTypeRender} />
				<TextField label={t('payment:payment')} source='paymentMethod' />
			</Datagrid>
		</ListContextProvider >
	);
};
