import { FunctionComponent, useEffect, useState } from 'react';
import { ShowProps, useShowController } from 'react-admin';
import { useTranslation } from 'react-i18next';

import { EAccountStatus, IDatagridRow, TPartnerPageInfo } from 'interfaces';
import { concatAddress, transformEditedToOriginUserFields } from 'utils';

import { useEditableFields } from 'hooks';
import {
	ActivateAccountButton,
	createSectionBody,
	DatagridRow,
	DeactivateAccountButton,
	PayoutIntervalRadioGroup,
	ServicesEditForm,
	WorkerEditForm,
	UpdatePartnerButton,
} from 'components';
import { ReactComponent as Activate } from '../icons/activate.svg';
import { ReactComponent as Deactivate } from '../icons/deactivate.svg';

type TEditableFields = Pick<TPartnerPageInfo, 'user'
| 'industries'
| 'services'
| 'partnerWorkers'
| 'partner'>

export const PartnerShow: FunctionComponent<ShowProps> = props => {
	const { t } = useTranslation([
		'adminPage',
		'partner',
		'worker',
		'user',
		'industries',
		'services',
		'order',
		'sales',
		'payment',
		'docs',
		'button',
	]);
	const { record } = useShowController<TPartnerPageInfo>(props);
	const [pageData, setPageData] = useState<TPartnerPageInfo>();
	const [ changedFieldsNames, setChangedFieldsNames ] = useState([]);
	const {
		editedFields,
		handleClearEditableFields,
	} = useEditableFields<TEditableFields>();

	const editedUserFields = transformEditedToOriginUserFields({ editedFields });

	useEffect(() => {
		if (record)
			setPageData(record);

	}, [record]);

	useEffect(() => {
		return () => handleClearEditableFields();
	}, []);

	const name = t('user:name');
	const surname = t('user:surname');
	const owner = t('user:owner');
	const address = `${t('user:address')}`;
	const tel = t('user:tel');
	const eMail = t('user:email');
	const partnerId = `${t('partner:partner')}-ID`;
	const status = t('partner:status');
	const firmName = t('partner:firm');
	const industriesTranslate = t('industries:industries');
	const services = `${t('services:services')}`;
	const annualSales = t('sales:annualSales');
	const quarterlySales = t('sales:quarterlySales');
	const monthlySales = t('sales:monthlySales');
	const weeklySales = t('sales:weeklySales');
	const dailySales = t('sales:dailySales');
	const newSales = t('sales:newSales');
	const lastSales = t('sales:lastSales');
	const paymentMethod = t('payment:paymentMethod');
	const latestPayment = t('payment:latestPayment');
	const cashReceipts = t('payment:cashReceipts');
	const nextPaymentAmount = t('payment:nextPaymentAmount');
	const lastPaymentAmount = t('payment:lastPaymentAmount');
	const nextPayment = t('payment:nextPayment');
	const taxInformation = t('payment:taxInformation');
	const payoutInterval = t('payment:payoutInterval');
	const haxpeCommission = t('payment:haxpeCommission');
	const vatId = `${t('payment:vat')}-ID`;
	const bank = t('payment:bank');
	const iban = t('payment:iban');
	const bic = t('payment:bic');
	const workerTranslate = t('worker:worker');
	const companyDocuments = t('docs:companyDocuments');
	const activate = t('button:activate');
	const deactivate = t('button:deactivate');
	const accountStatus = t(`button:${pageData?.user?.accountStatus}`);

	const transformEditedFieldsToOriginResponseFields = () => {
		const {
			industries,
			partner,
			partnerWorkers,
			services,
			user,
		} = editedFields;

		setChangedFieldsNames(prev => {
			const edited = Object.entries(editedFields)
				.map(([key, value]) => value && key);

			return [...prev, ...edited];
		});

		const origin: TPartnerPageInfo = {
			...pageData,
			...(partner && { partner: {
				...pageData.partner,
				name: partner[firmName],
			} }),
			...(user && { user: {
				...pageData.user,
				...editedUserFields,
			} }),
			...(partnerWorkers && { partnerWorkers }),
			...(services && { services }),
			...(industries && { industries }),
		};

		setPageData(prev => ({...prev, ...origin}));
	};

	useEffect(() => {
		if (editedFields)
			transformEditedFieldsToOriginResponseFields();

	}, [editedFields]);

	const serviceSectionBodyArgs = {
		data: pageData?.services,
		keyName: 'service',
		namespace: 'services',
	};

	const workerSectionBodyArgs = {
		data: pageData?.partnerWorkers,
		keyName: 'worker',
		namespace: 'worker',
		value: 'fullName',
	};

	const servicesSectionBody = createSectionBody(serviceSectionBodyArgs);
	const workerSectionBody = createSectionBody(workerSectionBodyArgs);
	const userAddress = pageData?.address && concatAddress(pageData.address);
	const industries: string[] = pageData?.industries?.map(({ key }) => key);
	const isActivatedPartner = pageData?.user?.accountStatus === EAccountStatus.activated;

	const info: IDatagridRow = {
		title: {
			[partnerId]: pageData?.id,
			[owner]: pageData?.user?.fullName,
			[industriesTranslate]: industries,
			[status]: accountStatus,
		},
		body: [
			[
				{
					title: owner,
					isEditable: true,
					editedFieldName: 'user',
					body: [
						{
							[name]: pageData?.user?.name,
							[surname]: pageData?.user?.surname,
							[address]: userAddress,
							[tel]: pageData?.user?.phoneNumber,
							[eMail]: pageData?.user?.email,
						},
					],
				},
				{
					title: taxInformation,
					isEditable: true,
					editedFieldName: 'tax',
					body: [
						{
							[bank]: `any text`,
							[iban]: `any text`,
							[bic]: `any text`,
							[vatId]: `any text`,
						},
					],
				},
				{
					title: services,
					isEditable: true,
					component: ServicesEditForm,
					body: servicesSectionBody,
				},
			],
			[
				{
					title: workerTranslate,
					isEditable: true,
					component: ({ handleClose }) => <WorkerEditForm
						record={record?.partnerWorkers}
						pageData={pageData?.partnerWorkers}
						handleClose={handleClose}
					/>,
					body: workerSectionBody,
				},
				{
					title: firmName,
					isEditable: true,
					editedFieldName: 'partner',
					body: [
						{
							[firmName]: pageData?.partner?.name,
						},
					],
				},
			],
			[
				{
					title: companyDocuments,
					body: [
						{
							'link': 'link to pdf',
						},
					],
				},
			],
			[
				{
					title: annualSales,
					textArray: [`any text`],
				},
				{
					title: quarterlySales,
					textArray: [`any text`],
				},
				{
					title: monthlySales,
					textArray: [`any text`],
				},
				{
					title: weeklySales,
					textArray: [`any text`],
				},
				{
					title: dailySales,
					textArray: [`any text`],
				},
			],
			[
				{
					title: paymentMethod,
					textArray: [`any text`, `any text`, `any text`, `any text`, `any text`],
				},
			],
			[
				{
					title: lastSales,
					textArray: [`any text`],
				},
				{
					title: cashReceipts,
					textArray: [`any text`],
				},
				{
					title: lastPaymentAmount,
					textArray: [`any text`],
				},
				{
					title: latestPayment,
					textArray: [`any text`],
				},
			],
			[
				{
					title: newSales,
					textArray: [`any text`],
				},
				{
					title: cashReceipts,
					textArray: [`any text`],
				},
				{
					title: cashReceipts,
					textArray: [`any text`],
				},
				{
					title: nextPaymentAmount,
					textArray: [`any text`],
				},
				{
					title: nextPayment,
					textArray: [`any text`],
				},
			],
			[
				{
					component: () => <PayoutIntervalRadioGroup
						defaultCheckedPaymentInterval='weekly'
						title={payoutInterval}
					/>,
				},
				{
					title: haxpeCommission,
					isEditable: true,
					editedFieldName: 'commission',
					textArray: ['any text'],
				},
			],
		],
		footerButtonsGroup: [
			<UpdatePartnerButton
				changedFieldsNames={changedFieldsNames}
				updateInfo={pageData}
				isDisabled={!editedFields}
			/>,
			<ActivateAccountButton
				disabled={isActivatedPartner}
				ownerUserId={pageData?.partner?.ownerUserId}
				label={activate}
			>
				<Activate/>
			</ActivateAccountButton>,
			<DeactivateAccountButton
				disabled={!isActivatedPartner}
				ownerUserId={pageData?.partner?.ownerUserId}
				label={deactivate}
			>
				<Deactivate/>
			</DeactivateAccountButton>,
		],
	};

	return <DatagridRow info={info}/>;
};
