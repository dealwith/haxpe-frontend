import { FunctionComponent } from 'react';
import {
	Datagrid,
	List,
	ListProps,
} from 'ra-ui-materialui';
import { useTranslation } from 'react-i18next';

import { concatAddress } from 'utils';
import { IDatagridRow, TPartnerPageInfo } from 'interfaces';

import {
	useDatagridRowOnClick,
	useDatagridSelectedItem,
} from 'hooks';
import {
	AdminContainer,
	AdminShowButton,
	createSectionBody,
	DatagridRow,
	TextField,
	FunctionField,
	ShortTextPopup,
	Filter,
} from 'components';
import { useAdminRefresh } from '../hooks';

export const Partner: FunctionComponent<ListProps> = props => {
	useAdminRefresh();

	const { t } = useTranslation([
		'industries',
		'services',
		'partner',
		'user',
		'payment',
	]);
	const { onRowClick } = useDatagridRowOnClick();
	const {
		handleRowClick,
		isShow,
		record,
	} = useDatagridSelectedItem<TPartnerPageInfo>();

	const partnerId = `${t('partner:partner')}-ID`;
	const firmName = t('partner:firmName');
	const status = t('partner:status');
	const industriesTranslate = t('industries:industries');
	const services = t('services:services');
	const owner = t('user:owner');
	const name = t('user:name');
	const tel = t('user:tel');
	const eMail = t('user:email');
	const address = t('user:address');
	const taxInformation = t('payment:taxInformation');
	const vatId = `${t('payment:vat')}-ID`;
	const bank = t('payment:bank');
	const iban = t('payment:iban');
	const bic = t('payment:bic');

	const userAddress = record?.address && concatAddress(record.address);

	const industries: string[] = record?.industries?.map(({ key }) => key);

	const servicesSectionBodyArgs = {
		data: record?.services,
		keyName: 'service',
		namespace: 'services',
	};

	const servicesSectionBody = createSectionBody(servicesSectionBodyArgs);

	const info: IDatagridRow = {
		title: {
			[partnerId]: record?.id,
			[owner]: record?.user?.fullName,
			[industriesTranslate]: industries,
			[status]: record?.user?.accountStatus,
		},
		body: [
			[
				{
					title: owner,
					body: [
						{
							[firmName]: record?.partner?.name,
							[name]: record?.user?.fullName,
							[address]: userAddress,
							[tel]: record?.user?.phoneNumber,
							[eMail]: record?.user?.email,
						},
					],
				},
				{
					title: taxInformation,
					body: [
						{
							[bank]: `any text`,
							[iban]: `any text`,
							[bic]: `any text`,
							[vatId]: `any text`,
						},
					],
				},
				{
					title: services,
					body: servicesSectionBody,
				},
			],
		],
	};

	const handleIndustryRender = ( { industries }: TPartnerPageInfo ) => {
		return industries?.map(({ key }) => <div>{key}</div>);
	};

	return (
		<AdminContainer>
			<List exporter={false} filters={<Filter/>} {...props}>
				<Datagrid onClick={onRowClick} rowClick={handleRowClick}>
					<FunctionField
						sortBy='id'
						label={partnerId}
						render={({ id }: TPartnerPageInfo) => (
							<ShortTextPopup>
								{id.toString()}
							</ShortTextPopup>
						)}
					/>
					<TextField
						sortBy='name'
						label={owner}
						source='user.fullName'
					/>
					<FunctionField
						label={industriesTranslate}
						render={handleIndustryRender}
					/>
					<TextField
						sortBy='accountStatus'
						label={status}
						source='user.accountStatus'
					/>
				</Datagrid>
			</List>
			{isShow
				&&  <>
					<AdminShowButton record={record} position='center'/>
					<DatagridRow info={info}/>
				</>
			}
		</AdminContainer>
	);
};
