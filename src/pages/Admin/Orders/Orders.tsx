import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-admin';
import {
	Datagrid,
	List,
	ListProps,
} from 'ra-ui-materialui';

import { ROUTES } from 'constants/index';
import { IDatagridRow, IOrder, TOrderPageInfo } from 'interfaces';
import { ModifiedDate, concatAddress } from 'utils';

import {
	useDatagridRowOnClick,
	useDatagridSelectedItem,
} from 'hooks';
import {
	AdminButton,
	AdminContainer,
	AdminShowButton,
	ButtonsContainer,
	DatagridRow,
	FunctionField,
	Filter,
	ShortTextPopup,
} from 'components';
import { useAdminRefresh } from '../hooks';

export const Orders: FunctionComponent<ListProps> = props => {
	useAdminRefresh();

	const { t } = useTranslation([
		'button',
		'time',
		'dateRange',
		'customer',
		'user',
		'order',
		'orderStatus',
		'payment',
		'worker',
		'partner',
		'industries',
		'services',
		'button',
	]);
	const { onRowClick } = useDatagridRowOnClick();
	const {
		handleRowClick,
		isShow,
		record,
	} = useDatagridSelectedItem<TOrderPageInfo>();

	const { createDateToShow, createTimeToShow } = new ModifiedDate();
	const name = t('user:name');
	const rating = t('user:rating');
	const comment = t('user:comment');
	const address = t('user:address');
	const tel = t('user:tel');
	const email = t('user:email');
	const customer = t('customer:customer');
	const creationDate = t('dateRange:date');
	const workerId = `${t('worker:worker')}-ID`;
	const partnerId = `${t('partner:partner')}-ID`;
	const orderId = `${t('order:order')}-ID`;
	const status = t('order:status');
	const service = t('services:service');
	const industry = t('industries:industry');
	const time = t('time:time');
	const duration = t('time:duration');
	const payment = t('payment:payment');
	const listing = t('payment:listing');
	const assignmentFee = t('payment:assignmentFee');
	const hourlyRate = `${t('payment:hourlyRate')} ${t('worker:worker')}`;
	const machineUse = t('payment:machineUse');
	const total = t('payment:total');
	const generateOrderPDF = `${t('button:generate')}-PDF`;
	const translatedIndustry = t(`industries:${record?.industry.key}`);
	const translatedService = t(`services:${record?.service.key}`);

	const userAddress = record?.address && concatAddress(record.address);
	const correctDate = record && createDateToShow({
		dateArg: record.creationDate,
	});

	const correctTime = record && createTimeToShow({
		durationSecond: record.duration,
	});

	const handleDateRender = (record: TOrderPageInfo) => createDateToShow({
		dateArg: record.creationDate,
	});
	const handleTimeRender = ( { duration }: IOrder ) => createTimeToShow({
		durationSecond: duration,
	});
	const handleIndustryRender = ({
		industry: { key },
	}: TOrderPageInfo) => t(`industries:${key}`);
	const handleServiceRender = ({
		service: { key },
	}: TOrderPageInfo) => t(`services:${key}`);
	const handleStatusRender = ({
		orderStatus,
	}: TOrderPageInfo) => t(`orderStatus:${orderStatus}`);

	const info: IDatagridRow = {
		title: {
			[creationDate]: correctDate,
			[time]: correctTime,
			[orderId]: record?.id,
			[partnerId]: record?.partnerId,
			[workerId]: record?.workerId,
			[industry]: translatedIndustry,
			[service]: translatedService,
			[status]: t(`orderStatus:${record?.orderStatus}`),
		},
		body: [
			[
				{
					title: customer,
					body: [
						{
							[name]: record?.user?.fullName,
							[address]: userAddress,
							[tel]: record?.user?.phoneNumber,
							[email]: record?.user?.email,
						},
					],
				},
				{
					title: listing,
					body: [
						{
							[assignmentFee]: `any text`,
							[hourlyRate]: `any text`,
							[machineUse]: `any text`,
							[total]: `any text`,
						},
					],
				},
				{
					title: 'none',
					body: [
						{
							[duration]: `any text`,
							[payment]: `any text`,
							[rating]: {
								name: 'orderRating',
								value: record?.rating,
								editing: false,
								starColor: '#31C963',
							},
							[comment]: `any text`,
						},
					],
				},
			],
		],
	};

	return (
		<AdminContainer>
			<List exporter={false} filters={<Filter/>} {...props}>
				<Datagrid onClick={onRowClick} rowClick={handleRowClick}>
					<FunctionField
						sortBy='creationDate'
						label={creationDate}
						render={handleDateRender}
					/>
					<FunctionField
						sortBy='durationSecond'
						label={time}
						render={handleTimeRender}
					/>
					<FunctionField
						sortBy='id'
						label={orderId}
						render={({ id }: TOrderPageInfo) => (
							<ShortTextPopup>
								{id}
							</ShortTextPopup>
						)}
					/>
					<FunctionField
						sortBy='partnerId'
						label={partnerId}
						render={({ partnerId }: TOrderPageInfo) => (
							<ShortTextPopup>
								{partnerId}
							</ShortTextPopup>
						)}
					/>
					<FunctionField
						sortBy='workerId'
						label={workerId}
						render={({ workerId }: TOrderPageInfo) => (
							<ShortTextPopup>
								{workerId}
							</ShortTextPopup>
						)}
					/>
					<FunctionField
						sortBy='industryId'
						label={industry}
						render={handleIndustryRender}
					/>
					<FunctionField
						sortBy='serviceTypeId'
						label={service}
						render={handleServiceRender}
					/>
					<FunctionField
						sortBy='orderStatus'
						label={status}
						render={handleStatusRender}
					/>
				</Datagrid>
			</List>
			{isShow
				&&  <>
					<ButtonsContainer centered='center'>
						<AdminShowButton position='center' record={record}/>
						<AdminButton
							component={Link}
							to={ROUTES.INVOICE.PDF_AS_LINK(record?.id)}
							label={generateOrderPDF}
						/>
					</ButtonsContainer>
					<DatagridRow info={info}/>
				</>
			}
		</AdminContainer>
	);
};
