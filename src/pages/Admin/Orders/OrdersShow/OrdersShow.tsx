import { FunctionComponent } from 'react';
import { ShowProps, useShowController } from 'react-admin';
import { useTranslation } from 'react-i18next';

import { ModifiedDate, concatAddress } from 'utils';
import { IDatagridRow, TOrderPageInfo } from 'interfaces';

import { CancelOrderButton, DatagridRow } from 'components';
import { useAdminRefresh } from 'pages/Admin/hooks';

export const OrdersShow: FunctionComponent<ShowProps> = props => {
	useAdminRefresh();

	const { t } = useTranslation([
		'time',
		'dateRange',
		'customer',
		'user',
		'order',
		'orderStatus',
		'payment',
		'worker',
		'partner',
		'worker',
		'industries',
		'services',
		'img',
	]);
	const { record } = useShowController<TOrderPageInfo>(props);

	const { createDateToShow, createTimeToShow } = new ModifiedDate();

	const name = t('user:name');
	const customer = t('customer:customer');
	const address = t('user:address');
	const creationDate = t('dateRange:date');
	const workerId = `${t('worker:worker')}-ID`;
	const partnerId = `${t('partner:partner')}-ID`;
	const orderId = `${t('order:order')}-ID`;
	const service = t('services:service');
	const industry = t('industries:industry');
	const status = t('order:status');
	const time = t('time:time');
	const tel = t('user:tel');
	const eMail = t('user:email');
	const duration = t('time:duration');
	const rating = t('user:rating');
	const paymentMethod = t('payment:paymentMethod');
	const customerId = `${t('customer:customer')}-ID`;
	const pictures = t('img:pictures');
	const totalAmount = t('payment:totalAmount');
	const vat = `${t('payment:vat')} (any percent)`;
	const netAmount = t('payment:netAmount');
	const cancelOrder = t('order:cancelOrder');

	const userAddress = record?.address && concatAddress(record.address);
	const correctDate = record && createDateToShow({
		dateArg: record.creationDate,
	});
	const correctTime = record && createTimeToShow({
		durationSecond: record.duration,
	});

	const info: IDatagridRow = {
		title: {
			[creationDate]: correctDate,
			[time]: correctTime,
			[orderId]: record?.id,
			[partnerId]: record?.partnerId,
			[workerId]: record?.workerId,
			[industry]: t(`industries:${record?.industry.key}`),
			[service]: t(`services:${record?.service.key}`),
			[status]: t(`orderStatus:${record?.orderStatus}`),
		},
		body: [
			[
				{
					title: customer,
					body: [
						{
							[name]: record?.user?.fullName,
							[address]: userAddress,
							[tel]: record?.user?.phoneNumber,
							[eMail]: record?.user?.email,
						},
					],
				},
				{
					title: customerId,
					textArray: [record?.customerId],
				},
			],
			[
				{
					title: status,
					textArray: [record?.orderStatus],
				},
				{
					title: duration,
					textArray: [correctTime],
				},
				{
					title: paymentMethod,
					textArray: [`any text`],
				},
				{
					title: rating,
					rate: {
						name: 'orderRating',
						value: record?.rating,
						editing: false,
						starColor: '#31C963',
					},
				},
			],
			[
				{
					title: netAmount,
					textArray: [`any text`],
				},
				{
					title: `${vat} (${record?.invoice?.tax}%)`,
					textArray: [`any text`],
				},
				{
					title: totalAmount,
					textArray: [`any text`],
				},
				{
					title: pictures,
					textArray: [`any text`],
				},
			],
		],
		footerButtonsGroup: [
			<CancelOrderButton
				orderStatus={record?.orderStatus}
				orderId={record?.id}
				color='secondary'
				label={cancelOrder}
			/>,
		],
	};

	return <DatagridRow info={info}/>;
};
