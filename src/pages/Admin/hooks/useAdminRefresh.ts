import { useEffect } from 'react';
import { useRefresh } from 'react-admin';

import { getPreferLang } from 'utils';

import { useIsFirstRender } from 'hooks';
/**
 * Refetches data, if language changed
 */
export const useAdminRefresh = (): void => {
	const refresh = useRefresh();
	const { isFirstRender } = useIsFirstRender();

	const lng = getPreferLang();

	useEffect(() => {
		if (!isFirstRender)
			refresh();
	}, [lng]);
};
