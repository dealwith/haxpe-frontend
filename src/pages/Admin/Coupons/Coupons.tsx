import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { Route, useHistory } from 'react-router-dom';
import {
	Datagrid,
	DeleteButton,
	EditButton,
	List,
	ListProps,
} from 'react-admin';
import { Drawer } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import { TCouponPageInfo } from 'interfaces';
import { ROUTES } from 'constants/index';

import {
	CorrectDate,
	TextField,
	FunctionField,
	Filter,
} from 'components';
import { CouponListActions } from './CouponListActions';
import { CouponCreate } from './CouponCreate';
import { CouponEdit } from './CouponEdit';
import { useAdminRefresh } from '../hooks';

const useStyles = makeStyles({
	drawerContent: {
		width: 200,
	},
});

export const Coupons: FunctionComponent<ListProps> = props => {
	const { push } = useHistory();

	useAdminRefresh();

	const { t } = useTranslation([
		'coupon',
		'dateRange',
		'button',
	]);

	const handleClose = () => push(props.basePath);

	const valueIn = `${t('coupon:value')} %`;
	const couponCode = t('coupon:code');
	const used = t('coupon:used');
	const creationDate = t('dateRange:creationDate');
	const expirationDate = t('dateRange:expirationDate');
	const edit = t('button:edit');
	const status = t('coupon:status');
	const deleteCoupon = t('button:delete');

	const classes = useStyles();

	const baseClassName = 'coupons';

	return (
		<div className={baseClassName}>
			<List
				{...props}
				actions={<CouponListActions/>}
				exporter={false}
				bulkActionButtons={false}
				filters={<Filter/>}
			>
				<Datagrid>
					<FunctionField
						sortBy='value'
						label={valueIn}
						render={({ value }: TCouponPageInfo) => `${value} %`}
					/>
					<TextField
						sortBy='code'
						label={couponCode}
						source='code'
					/>
					<TextField
						label={used}
						source='usedCount'
					/>
					<FunctionField
						sortBy='creationDate'
						label={creationDate}
						render={({ creationDate }: TCouponPageInfo) => (
							<CorrectDate date={creationDate}/>
						)}
					/>
					<FunctionField
						sortBy='expirationDate'
						label={expirationDate}
						render={({ expirationDate }: TCouponPageInfo) => (
							<CorrectDate date={expirationDate}/>
						)}
					/>
					<FunctionField
						sortBy='isDeleted'
						label={status}
						render={({ isDeleted }: TCouponPageInfo) => {
							const field = isDeleted
								? t('button:deactivate')
								: t('button:activate');

							return field;
						}}
					/>
					<EditButton
						color='primary'
						basePath={`${props.basePath}${ROUTES.COUPONS.ORIGIN}`}
						label={edit}
					/>
					<DeleteButton label={deleteCoupon}/>
				</Datagrid>
			</List>
			<Route exact path={`${props.basePath}${ROUTES.COUPONS.CREATE}`}>
				{({ match }) => (
					<Drawer
						open={!!match}
						anchor='right'
						onClose={handleClose}
					>
						<CouponCreate
							onCancel={handleClose}
							{...props}
						/>
					</Drawer>
				)}
			</Route>
			<Route exact path={`${props.basePath}${ROUTES.COUPONS.EDIT}`}>
				{({ match }) => {
					const isMatch = match
							&& match.params
							&& match.params.id !== 'create';

					return (
						<Drawer
							open={!!isMatch}
							anchor='right'
							onClose={handleClose}
						>
							{isMatch
									&& (
										<CouponEdit
											id={isMatch ? match.params.id : null}
											onCancel={handleClose}
											{...props}
										/>
									)}
						</Drawer>
					);
				}}
			</Route>
			<Route path={`${props.basePath}${ROUTES.COUPONS.EDIT}`}>
				{({ match }) => {
					const isMatch
						= match
						&& match.params
						&& match.params.id !== 'create';

					return (
						<Drawer
							open={isMatch}
							anchor='right'
							onClose={handleClose}
						>
							{isMatch
								? (
									<CouponEdit
										id={isMatch ? match.params.id : null}
										onCancel={handleClose}
										{...props}
									/>
								)
								: <div className={classes.drawerContent}/>
							}
						</Drawer>
					);
				}}
			</Route>
		</div>
	);
};
