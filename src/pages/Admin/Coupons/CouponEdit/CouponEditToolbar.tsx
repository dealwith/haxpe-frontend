import { FunctionComponent } from 'react';
import { SaveButton, Toolbar } from 'react-admin';
import { useTranslation } from 'react-i18next';
import Button from '@material-ui/core/Button';

type TProps = {
	onCancel: () => void;
}

export const CouponEditToolbar: FunctionComponent<TProps> = ({ onCancel, ...props }) => {
	const { t } = useTranslation('button');

	return (
		<Toolbar {...props}>
			<SaveButton />
			<Button onClick={onCancel}>{t('button:cancel')}</Button>
		</Toolbar>
	);
};
