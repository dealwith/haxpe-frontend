import { FunctionComponent } from 'react';
import {
	Edit,
	SimpleForm,
	required,
	EditProps,
} from 'react-admin';
import { useTranslation } from 'react-i18next';

import { DateInput, TextInput } from 'components';
import { CouponEditToolbar } from './CouponEditToolbar';

type TProps = {
	onCancel: () => void;
}

export const CouponEdit: FunctionComponent<EditProps & TProps> = ({ onCancel, ...props }) => {
	const { t } = useTranslation(['coupon', 'dateRange', 'button']);

	const expirationDate = t('dateRange:expirationDate');
	const code = t('coupon:code');
	const value = t('coupon:value');

	return (
		<Edit title={`/Coupon ${t('button:edit')}`} {...props}>
			<SimpleForm toolbar={<CouponEditToolbar onCancel={onCancel} />}>
				<DateInput
					label={expirationDate}
					source='expirationDate'
					validate={required()}
				/>
				<TextInput
					label={code}
					source='code'
					validate={required()}
				/>
				<TextInput
					label={value}
					source='value'
					validate={required()}
				/>
			</SimpleForm>
		</Edit>
	);
};
