import { FunctionComponent } from 'react';
import { CreateButton, ListActionsProps, TopToolbar } from 'react-admin';
import { useTranslation } from 'react-i18next';

import { ROUTES } from 'constants/index';

export const CouponListActions: FunctionComponent<ListActionsProps> = props => {
	const { t } = useTranslation('button');

	const create = t('button:create');

	return (
		<TopToolbar>
			<CreateButton
				basePath={`${props?.basePath}${ROUTES.COUPONS.ORIGIN}`}
				label={create}
			/>
		</TopToolbar>
	);
};
