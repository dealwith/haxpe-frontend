import { FunctionComponent } from 'react';
import {
	Create,
	SimpleForm,
	required,
	CreateProps,
} from 'react-admin';
import { useTranslation } from 'react-i18next';

import { TextInput, DateInput } from 'components';
import { CouponCreateToolbar } from './CouponCreateToolbar';

type TProps = {
	onCancel: () => void;
}

export const CouponCreate: FunctionComponent<CreateProps & TProps> = ({ onCancel, ...props }) => {
	const { t } = useTranslation(['coupon', 'dateRange', 'button']);

	const expirationDate = t('dateRange:expirationDate');
	const code = t('coupon:code');
	const value = t('coupon:value');

	return (
		<Create title={`/Coupon ${t('button:create')}`} {...props}>
			<SimpleForm toolbar={<CouponCreateToolbar onCancel={onCancel} />}>
				<DateInput
					label={expirationDate}
					source='expirationDate'
					validate={required()}
				/>
				<TextInput
					label={code}
					source='code'
					validate={required()}
				/>
				<TextInput
					label={value}
					source='value'
					validate={required()}
				/>
			</SimpleForm>
		</Create>
	);
};
