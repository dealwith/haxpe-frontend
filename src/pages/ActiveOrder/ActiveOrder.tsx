import { FunctionComponent, useEffect, useMemo } from 'react';

import { EOrderWorkerSteps, TActiveOrderSteps } from 'interfaces';

import { useSocket, useStep } from 'hooks';
import {
	OrderWorkerIncomingRequest,
	WorkerActiveOrdersList,
} from 'components';

export const ActiveOrder: FunctionComponent = () => {
	const { orderOffer } = useSocket();

	const steps: TActiveOrderSteps = useMemo(() => ({
		orderList: 'orderList',
		incomingRequest: 'incomingRequest',
	}), []);

	const { step, handleChangeStep } = useStep<TActiveOrderSteps>('orderList', steps);

	useEffect(() => {
		if (orderOffer)
			handleChangeStep('incomingRequest');

	}, [orderOffer, handleChangeStep]);

	const stepComponents: TActiveOrderSteps = {
		orderList: <WorkerActiveOrdersList
			handleChangeStep={handleChangeStep}
			title={EOrderWorkerSteps.activeOrderList}
		/>,
		incomingRequest: <OrderWorkerIncomingRequest
			title={EOrderWorkerSteps.incomingRequest}
			handleChangeStep={handleChangeStep}
		/>,
	};

	return <>{stepComponents[step]}</>;
};
