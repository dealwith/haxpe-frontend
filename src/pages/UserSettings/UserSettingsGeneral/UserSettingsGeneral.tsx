import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { ROUTES } from 'constants/index';

import { Link } from 'components';

import './user-settings-general.scss';

export const UserSettingsGeneral: FunctionComponent = () => {
	const { t } = useTranslation(['links', 'button']);

	const baseClassName = 'user-settings-general';
	const componentClassName = {
		component: baseClassName,
		link: `${baseClassName}__link`,
	};

	return (
		<div className={componentClassName.component}>
			<Link
				theme='light-green'
				className={componentClassName.link}
				href={ROUTES.USER_SETTINGS.CHANGE_PASSWORD}
			>
				{t('links:changePassword')}
			</Link>
			<Link
				theme='light-green'
				className={componentClassName.link}
				href={ROUTES.USER_SETTINGS.SET_UP_PAYMENT_METHOD.ORIGIN}
			>
				{t('links:setUpPaymentMethod')}
			</Link>
			<Link
				theme='light-green'
				className={componentClassName.link}
				href={ROUTES.USER_SETTINGS.CHANGE_ACCOUNT_DETAILS}
			>
				{t('links:changeAccountDetails')}
			</Link>
			<Link
				theme='warn'
				className={componentClassName.link}
				href={ROUTES.USER_SETTINGS.DELETE_ACCOUNT}
			>
				{t('button:deleteAccount')}
			</Link>
		</div>
	);
};
