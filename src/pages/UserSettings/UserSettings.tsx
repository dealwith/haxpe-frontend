import { FunctionComponent } from 'react';
import { Route, Switch } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { ROUTES } from 'constants/index';

import {
	BackToPreviousPageButton,
	Container,
	Image,
	PaymentMethod,
	ResetPasswordForm,
	DeleteAccount,
	AccountDetailsForm,
} from 'components';
import { UserSettingsGeneral } from './UserSettingsGeneral';
import { ReactComponent as SettingIcon } from './icons/settingIcon.svg';

import backgroundImage from './icons/bg-img.png';

import './user-settings.scss';

export const UserSettings: FunctionComponent = () => {
	const { t } = useTranslation('settings');

	const baseClassName = 'user-settings';
	const componentClassName = {
		component: baseClassName,
		backgroundImage: `${baseClassName}__background-image`,
		title: `${baseClassName}__title`,
		subtitle: `${baseClassName}__subtitle`,
		icon: `${baseClassName}__icon`,
		button: `${baseClassName}__button`,
	};

	return (
		<div className={componentClassName.component}>
			<Image
				className={componentClassName.backgroundImage}
				src={backgroundImage}
			/>
			<Container
				theme='white'
				centered='vertical'
				flow='vertical'
				maxWidth='376px'
			>
				<div className={componentClassName.title}>
					<SettingIcon className={componentClassName.icon}/>
					<span className={componentClassName.subtitle}>
						{t('settings')}
					</span>
				</div>
				<Switch>
					<Route exact path={ROUTES.USER_SETTINGS.ORIGIN}>
						<UserSettingsGeneral/>
					</Route>
					<Route exact path={ROUTES.USER_SETTINGS.CHANGE_PASSWORD}>
						<ResetPasswordForm/>
					</Route>
					<Route exact path={ROUTES.USER_SETTINGS.CHANGE_ACCOUNT_DETAILS}>
						<AccountDetailsForm/>
					</Route>
					<Route exact path={ROUTES.USER_SETTINGS.SET_UP_PAYMENT_METHOD.ORIGIN}>
						<PaymentMethod/>
					</Route>
					<Route exact path={ROUTES.USER_SETTINGS.DELETE_ACCOUNT}>
						<DeleteAccount/>
					</Route>
				</Switch>
				<BackToPreviousPageButton className={componentClassName.button}/>
			</Container>
		</div>
	);
};
