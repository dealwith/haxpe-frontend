import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { ROUTES, MEDIA_POINT } from 'constants/index';

import {
	H1,
	P,
	Image,
	Button,
} from 'components';
import { useAuth, useMediaQuery } from 'hooks';

import homeIcon from './homeIllustration.svg';

import './home.scss';

const baseClassName = 'home';
const componentClassName = {
	component: baseClassName,
	title: `${baseClassName}__title`,
	content: `${baseClassName}__content`,
	homeDescription: `${baseClassName}__description`,
	image: `${baseClassName}__image`,
	iconContainer: `${baseClassName}__icon-container`,
	button: `${baseClassName}__button`,
};

const IconContainer = () => (
	<div className={componentClassName.iconContainer}>
		<Image
			src={homeIcon}
			className={componentClassName.image}
			size='fluid'
		/>
	</div>
);

export const Home: FunctionComponent = () => {
	const { isAuthenticated, isWorker } = useAuth();
	const { t } = useTranslation(['homePage', 'button']);
	const history = useHistory();
	const { isMatches: isMobile } = useMediaQuery({ maxWidth: MEDIA_POINT.MOBILE });

	const handleClick = () => {
		if (isAuthenticated) {
			if (isWorker)
				return history.push(ROUTES.ORDER.CURRENT);

			return history.push(ROUTES.ORDER.ACTIVE);
		}

		return history.push(ROUTES.GUEST);
	};

	return (
		<div className='home'>
			{isMobile
				&& <IconContainer />
			}
			<H1
				align='left'
				className={componentClassName.title}
				isFirst={true}
			>
				{t('homePage:title')}
			</H1>
			<div className={componentClassName.content}>
				<div>
					<P className={componentClassName.homeDescription}>
						{t('homePage:description')}
					</P>
					<Button
						size='xl'
						rounded='xl'
						className={componentClassName.button}
						onClick={handleClick}
					>
						{t('button:orderCraftsmenNow')}
					</Button>
				</div>
				{!isMobile
					&& <IconContainer />
				}
			</div>
		</div>
	);
};
