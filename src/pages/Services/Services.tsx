import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import pestControlIcon from './icons/pestControlcon.svg';
import insectIcon from './icons/insectIcon.svg';
import keyIcon from './icons/keyIon.svg';
import technicalKeyIcon from './icons/technicalKeyIcon.svg';
import trumpetIcon from './icons/trumpetIcon.svg';

import { H1, Image, P, Button } from 'components';
import { ServiceIconContainer } from './ServiceIconContainer';

import './services.scss';

export const Services: FunctionComponent = () => {
	const { t } = useTranslation(['servicesPage', 'button']);

	const baseClassName = 'services';
	const componentClassName = {
		component: baseClassName,
		descriptionBlock: `${baseClassName}__description-block`,
		title: `${baseClassName}__title`,
		button: `${baseClassName}__button`,
		description: `${baseClassName}__description`,
		icon: `${baseClassName}__icon`,
		iconContainer: `${baseClassName}__icon-container`,
		serviceIcons: `${baseClassName}__service-icons-container`,
	};

	return (
		<section className={componentClassName.component}>
			<div className={componentClassName.iconContainer}>
				<Image
					className={componentClassName.icon}
					src={pestControlIcon}
					size='fluid'
				/>
			</div>
			<div className={componentClassName.descriptionBlock}>
				<H1 align='left' className={componentClassName.title} isGreen={true}>
					{t('servicesPage:title')}
				</H1>
				<div className={componentClassName.serviceIcons}>
					<ServiceIconContainer>
						<Image src={trumpetIcon} />
					</ServiceIconContainer>
					<ServiceIconContainer>
						<Image src={technicalKeyIcon} />
					</ServiceIconContainer>
					<ServiceIconContainer>
						<Image src={insectIcon}/>
					</ServiceIconContainer>
					<ServiceIconContainer>
						<Image src={keyIcon}/>
					</ServiceIconContainer>
				</div>
				<P align='left' className={componentClassName.description}>
					{t('servicesPage:description')}
				</P>
				<br/>
				<P align='left' className={componentClassName.description}>
					{t('servicesPage:description2')}
				</P>
				<Button size='xl' rounded='xl' className={componentClassName.button}>
					{t('button:orderCraftsmenNow')}
				</Button>
			</div>
		</section>
	);
};
