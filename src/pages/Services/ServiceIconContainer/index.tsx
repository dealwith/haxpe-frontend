import { FunctionComponent, ReactNode } from 'react';

import './icon-container.scss';

type TProps = { children: ReactNode };

export const ServiceIconContainer: FunctionComponent<TProps> = ({ children }) => {
	return (
		<div className='icon-container'>
			{children}
		</div>
	);
};
