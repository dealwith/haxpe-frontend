import { FunctionComponent, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import { ROUTES } from 'constants/index';

//TODO: what is it?
export const Order: FunctionComponent = () => {
	const history = useHistory();

	useEffect(() => history.push(ROUTES.ORDER.CUSTOMER), []);

	return (
		<div>
			Order page
		</div>
	);
};
