import { FunctionComponent, Suspense } from 'react';
import { Steps, Step } from 'react-step-builder';

import { ECustomerOrderSteps, EOrderStatus } from 'interfaces';
import { useMediaQuery } from 'hooks';
import { MEDIA_POINT } from 'constants/index';

import {
	Container,
	OrderCustomerForm,
	OrderDetails,
	OrderCustomerCreated,
	OrderCanceled,
	OrderCancelReasonForm,
	WorkerFound,
	ThanksForOrderWorkerOnTheWay,
	WorkerOnWay,
	OrderCustomerInProgress,
	OrderCustomerPaused,
	OrderCustomerCompleted,
	OrderCustomerPayment,
	CircleLoader,
	Image,
	OrderCustomerWorkCompleted,
	OrderCustomerThanksForReview,
} from 'components';
import interfaceImage from './images/interface.png';
import { OrderCustomerReserved } from 'components/Order/Customer/Reserved';

import './order-customer.scss';

export const OrderCustomer: FunctionComponent = () => {
	const { isMatches: isMobile } = useMediaQuery({ maxWidth: MEDIA_POINT.MOBILE });
	const baseClassName = 'order-customer';
	const componentClassName = {
		component: baseClassName,
		container: `${baseClassName}-container`,
		image: `${baseClassName}__image`,
	};

	return (
		<Container isInner={true} centered='center'>
			{!isMobile
				&& <Image
					src={interfaceImage}
					className={componentClassName.image}
				/>
			}
			<Suspense fallback={CircleLoader}>
				<Steps>
					<Step
						title={ECustomerOrderSteps.orderCreation}
						component={OrderCustomerForm}
					/>
					<Step
						title={EOrderStatus.draft}
						component={OrderDetails}
					/>
					<Step
						title={EOrderStatus.created}
						component={OrderCustomerCreated}
					/>
					<Step
						title={ECustomerOrderSteps.cancelReason}
						component={OrderCancelReasonForm}
					/>
					<Step
						title={ECustomerOrderSteps.canceledOrder}
						component={OrderCanceled}
					/>
					<Step
						title={ECustomerOrderSteps.workerFounded}
						component={WorkerFound}
					/>
					<Step
						title={ECustomerOrderSteps.thanksForOrderWorkerInOnHisWayToYou}
						component={ThanksForOrderWorkerOnTheWay}
					/>
					<Step
						title={EOrderStatus.workerOnWay}
						component={WorkerOnWay}
					/>
					<Step
						title={EOrderStatus.workerArrived}
						component={WorkerOnWay}
					/>
					<Step
						title={EOrderStatus.inProgress}
						component={OrderCustomerInProgress}
					/>
					<Step
						title={EOrderStatus.workCompleted}
						component={OrderCustomerWorkCompleted}
					/>
					<Step
						title={EOrderStatus.paused}
						component={OrderCustomerPaused}
					/>
					<Step
						title={EOrderStatus.completed}
						component={OrderCustomerCompleted}
					/>
					<Step
						title={EOrderStatus.payment}
						component={OrderCustomerPayment}
					/>
					<Step
						title={ECustomerOrderSteps.reserved}
						component={OrderCustomerReserved}
					/>
					<Step
						title={ECustomerOrderSteps.thanksForReview}
						component={OrderCustomerThanksForReview}
					/>
				</Steps>
			</Suspense>
		</Container>
	);
};
