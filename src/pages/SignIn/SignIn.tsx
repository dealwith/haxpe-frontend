import { FunctionComponent, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { ROUTES, MEDIA_POINT } from 'constants/index';

import { useAuth, useMediaQuery } from 'hooks';
import {
	Button,
	P,
	Container,
	FacebookButton,
	GoogleButton,
	SignInForm,
	Image,
} from 'components';
import privacyImage from 'images/privacy.png';

import './sign-in.scss';

export const SignIn: FunctionComponent = () => {
	const { t } = useTranslation(['signIn', 'button']);
	// TODO: Using of auth and useEffect hook here is only
	// needed because of problem with redirecting from sign-in form
	// to orders page: need to fix this problem
	const { isAuthenticated, isWorker} = useAuth();
	const history = useHistory();
	const { isMatches: isMobile } = useMediaQuery({ maxWidth: MEDIA_POINT.MOBILE });

	useEffect(() => {
		if (isAuthenticated) {
			if (isWorker)
				history.push(ROUTES.ORDER.ACTIVE);
			else
				history.push(ROUTES.ORDER.CUSTOMER);
		}
	}, [isAuthenticated, isWorker, history]);

	const baseClassName = 'sign-in';
	const signInClassName = {
		component: baseClassName,
		container: `${baseClassName}-container`,
		form: `${baseClassName}__form`,
		formContainer: `${baseClassName}__form-container`,
		fieldset: `${baseClassName}__fieldset`,
		inputLabel: `${baseClassName}__input-label`,
		link: `${baseClassName}__link`,
		socialButtons: `${baseClassName}__social-buttons`,
		fbButton: `${baseClassName}__fb-button`,
		footer: `${baseClassName}__footer`,
		bgImage: `${baseClassName}__bg-image`,
		contentContainer: `${baseClassName}__content-container`,
	};

	return (
		<Container
			isInner={true}
			centered='center'
			className={signInClassName.container}
		>
			{!isMobile
				&& <Image
					src={privacyImage}
					className={signInClassName.bgImage}
				/>
			}
			<Container
				maxWidth={380}
				theme='white'
				isFitContent={true}
			>
				<SignInForm className={signInClassName} />
				<Link to={ROUTES.RESTORE} className={signInClassName.link}>
					{t('signIn:forgotPassword')}
				</Link>
				<div className={signInClassName.footer}>
					<div className={signInClassName.socialButtons}>
						<GoogleButton isSmall={true} />
						<FacebookButton isSmall={true}/>
					</div>
					<Link to={ROUTES.GUEST}>
						<Button theme='gray' size='full'>
							{t('button:continueGuest')}
						</Button>
					</Link>
					<Link to={ROUTES.SIGN_UP.CUSTOMER}>
						<Button theme='green' size='full'>
							{t('button:registrationCustomer')}
						</Button>
					</Link>
					<P
						size='small'
						color='gray'
						align='center'
					>
						{t('signIn:secondaryText')}
					</P>
					<Link
						to={ROUTES.SIGN_UP.PARTNER}
						className={signInClassName.link}
					>
						{t('signIn:registerCraftsman')}
					</Link>
				</div>
			</Container>
		</Container>
	);
};
