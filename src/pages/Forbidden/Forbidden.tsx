import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

export const Forbidden: FunctionComponent = () => {
	const { t } = useTranslation('navigation');

	return (
		<div>
			{t('forbidden')}
		</div>
	);
};
