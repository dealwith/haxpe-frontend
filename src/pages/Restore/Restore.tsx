import { FunctionComponent, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ROUTES } from 'constants/index';

import {
	Button,
	Container,
	Link,
	RestorePasswordForm,
	Image,
	P,
} from 'components';
import mailIcon from './mailIcon.svg';

import './restore.scss';

export const Restore: FunctionComponent = () => {
	const [isRestoreSuccess, setRestoreSuccess] = useState(false);
	const { t } = useTranslation(['button', 'restore']);

	const componentClassName = 'restore';

	return (
		<Container
			theme='white'
			maxWidth={380}
			height={580}
			flow='vertical'
			className={componentClassName}
		>
			{isRestoreSuccess
				? <div className='successful-screen'>
					<P isBold={true}>
						{t('restore:thankYou')}
					</P>
					<Image src={mailIcon} alt='Email icon' />
					<div>
						<P align='center'>
							{t('restore:weSentYouEmail')}
						</P>
						<P align='center' isBold={true}>
							{t('restore:checkInboxToResetPassword')}
						</P>
					</div>
				</div>
				: <RestorePasswordForm setRestoreSuccess={setRestoreSuccess} />
			}
			<Link href={ROUTES.SIGN_IN}>
				<Button
					size='full'
					theme='gray'
					rounded='m'
				>
					{t('button:backToLogin')}
				</Button>
			</Link>
		</Container>
	);
};
