// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck
import { FunctionComponent, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
	H2,
	P,
	ContactForm,
} from 'components';

import './contact.scss';

export const Contact: FunctionComponent = () => {
	const [isFormSent, setFormSent] = useState<boolean>(false);

	const { t } = useTranslation(['contactPage', 'button', 'input']);

	const handleSubmit = (data: IForm) => {
		setFormSent(true);
		setTimeout(() => setFormSent(false), 3000);
	};

	const baseClassName = 'contact';
	const contactClassName = {
		component: baseClassName,
		contactContainer: `${baseClassName}-container`,
		formContainer: `${baseClassName}__form-container`,
		textContent: `${baseClassName}__text-content`,
		mapContainer: `${baseClassName}__map-container`,
		description: `${baseClassName}__description`,
		fieldSet: `${baseClassName}__fieldset`,
		legend: `${baseClassName}__legend`,
		form: `${baseClassName}__form`,
		button: `${baseClassName}__button`,
		formSentText: `${baseClassName}__form-sent-text`,
	};
	const staticMap
		= `https://www.google.com/maps/embed/v1/place?q=place_id:ChIJ6V5XkJN2nkcRSaZegWrKKdM&key=${process.env.REACT_APP_GOOGLE_MAP_API_KEY}`;

	return (
		<div className={contactClassName.contactContainer}>
			<article className={contactClassName.component}>
				<div className={contactClassName.formContainer}>
					<div className={contactClassName.textContent}>
						<H2 className={contactClassName.title}>
							{t('contactPage:title')}
						</H2>
						{isFormSent
							? <P isBold={true} className={contactClassName.formSentText}>
								{t('contactPage:formSubmitted')}
							</P>
							: <>
								<P align='left' className={contactClassName.description}>
									{t('contactPage:description')}
								</P>
								<ContactForm
									handleSubmit={handleSubmit}
									className={contactClassName}
								/>
							</>
						}
					</div>
				</div>
				<div className={contactClassName.mapContainer}>
					<iframe
						title='Helping sevice area'
						width='100%'
						height='100%'
						style={{ border: 0 }}
						loading='lazy'
						allowFullScreen
						src={staticMap}
					/>
				</div>
			</article>
		</div>
	);
};
