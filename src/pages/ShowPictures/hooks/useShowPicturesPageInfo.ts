import { useEffect, useState } from 'react';

import { OrderService } from 'services';
import { IImage } from 'interfaces';

import { useNotify, useOrderParams } from 'hooks';

type TImageState = IImage & {src: string};

type TReturnType = {
	image: TImageState;
	isImages: boolean;
	handleNextImage: () => void;
	handlePrevImage: () => void;
}

export const useShowPicturesPageInfo = (): TReturnType => {
	const { orderId } = useOrderParams();
	const [index, setIndex] = useState(0);
	const { handleSetNotify } = useNotify();
	const [imagesInfo, setImagesInfo] = useState<IImage[]>();
	const [cashedImages, setCashedImages] = useState<TImageState[]>([]);
	const [image, setImage] = useState<TImageState>();
	const [isImages, setIsImages] = useState(true);

	useEffect(() => {
		const fetchAndSetPictures = async () => {
			try {
				const { body } = await OrderService.getImagesByOrderId(orderId);

				if (!body.length)
					setIsImages(false);

				setImagesInfo(body);
			} catch (err) {
				if (err instanceof Error) {
					handleSetNotify({
						errorText: 'haveNotImagesToShow',
						isNotify: true,
					});
				} else
					console.error(err);

				console.error(err);
			}
		};

		fetchAndSetPictures();
	}, []);

	useEffect(() => {
		const fetchAndSetImage = async () => {
			try {
				const currentId = imagesInfo[index].id;
				const existedImage = cashedImages.find(img => img.id === currentId);

				if (!existedImage) {
					const imageSrc = await OrderService.getImageById(orderId, currentId);
					const newImage = {
						...imagesInfo[index],
						src: imageSrc,
					};

					setCashedImages(prev => [...prev, newImage]);
					setImage(newImage);
				} else
					setImage(existedImage);

			} catch (err) {
				if (err instanceof Error) {
					handleSetNotify({
						errorText: 'haveNotImagesToShow',
						isNotify: true,
					});
				} else
					console.error(err);

				console.error(err);
			}
		};

		if (imagesInfo?.length)
			fetchAndSetImage();

	}, [index, imagesInfo]);

	const handleNextImage = () => {
		setIndex(prev => {
			let next = ++prev;

			if (next > imagesInfo.length - 1)
				next = 0;

			return next;
		});
	};

	const handlePrevImage = () => {
		setIndex(prev => {
			let next = --prev;

			if (next < 0)
				next = imagesInfo.length - 1;

			return next;
		});
	};

	return {
		image,
		isImages,
		handleNextImage,
		handlePrevImage,
	};
};
