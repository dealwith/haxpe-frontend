import { FunctionComponent, useEffect } from 'react';

import {
	BackToPreviousPageButton,
	Button,
	ButtonsContainer,
	H4,
} from 'components';
import { useNotify } from 'hooks';
import { useShowPicturesPageInfo } from './hooks';
import { ReactComponent as NextIcon } from './icons/next-svgrepo-com.svg';
import { ReactComponent as PrevIcon } from './icons/previous-svgrepo-com.svg';

import './show-pictures.scss';

export const ShowPictures: FunctionComponent = () => {
	const { handleSetNotify } = useNotify();
	const {
		image,
		isImages,
		handleNextImage,
		handlePrevImage,
	} = useShowPicturesPageInfo();

	useEffect(() => {
		if (!isImages) {
			handleSetNotify({
				isNotify: true,
				errorText: 'haveNotImagesToShow',
			});
		}
	}, [isImages]);

	const imageFileName = image?.fileName;
	const fileName = imageFileName?.substr(0, imageFileName?.lastIndexOf('.'))
	|| imageFileName;

	const baseClassName = 'show-pictures';
	const componentClassName = {
		component: baseClassName,
		icon: `${baseClassName}__icon`,
	};

	return <div className={componentClassName.component}>
		{image
			&& <div>
				<img src={image?.src} alt={fileName} />
				<H4 color='white' align='center'>{fileName}</H4>
				<ButtonsContainer>
					<Button
						theme='transparent'
						size='auto'
						onClick={handlePrevImage}
					>
						<PrevIcon className={componentClassName.icon}/>
					</Button>
					<Button
						theme='transparent'
						size='auto'
						onClick={handleNextImage}
					>
						<NextIcon className={componentClassName.icon}/>
					</Button>
				</ButtonsContainer>
			</div>
		}
		<BackToPreviousPageButton/>
	</div>;
};
