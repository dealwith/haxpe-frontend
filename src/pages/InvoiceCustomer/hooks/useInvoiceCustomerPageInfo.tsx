import { useEffect, useState } from 'react';

import { CustomerService, OrderService } from 'services';

import { IOrderRes } from 'interfaces';

type TReturnType = {
	info: IOrderRes[];
}

export const useInvoiceCustomerPageInfo = (): TReturnType => {
	const [info, setInfo] = useState<IOrderRes[]>();

	useEffect(() => {
		const fetchAndSetData = async () => {
			try {
				const { body: { id } } = await CustomerService.getCustomerInfo();
				const { body } = await OrderService.getOrder({
					CustomerId: id,
				});

				setInfo(body);
			} catch (err) {
				console.error(err);
			}
		};

		fetchAndSetData();
	}, []);

	return {
		info,
	};
};
