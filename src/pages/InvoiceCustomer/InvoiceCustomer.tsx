import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { CURRENCY, ROUTES } from 'constants/index';

import {
	Container,
	ContentContainer,
	Link,
	Title,
} from 'components';
import { useInvoiceCustomerPageInfo } from './hooks';

import './invoice-customer.scss';

export const InvoiceCustomer: FunctionComponent = () => {
	const { t } = useTranslation('invoice');
	const { info } = useInvoiceCustomerPageInfo();

	const { EURO } = CURRENCY;

	const baseClassName = 'invoice-customer';
	const componentClassName = {
		component: baseClassName,
		contentContainer: `${baseClassName}__content-container`,
		link: `${baseClassName}__link`,
	};

	const content = info?.map(({ id: orderId, invoice: { id, total } }) => (
		<Link
			key={id}
			href={ROUTES.INVOICE.PDF_AS_LINK(orderId)}
			className={componentClassName.link}
		>
			<ContentContainer
				isDefault={false}
				className={componentClassName.contentContainer}
				centered='space-between'
				isInner={true}
			>
				<span>{t('invoiceId')} {id}</span>
				<span>{EURO} {total}</span>
			</ContentContainer>
		</Link>
	));

	return (
		<Container
			className={componentClassName.component}
			isFitContent={true}
			minHeight={500}
			maxWidth={380}
			theme='white'
		>
			<Title px='16'>{t('invoice')}</Title>
			{content}
		</Container>
	);
};
