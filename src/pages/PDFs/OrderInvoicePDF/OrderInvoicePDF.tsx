import { FunctionComponent } from 'react';

import { TPDF } from 'interfaces';
import { concatAddress, getPreferLang, ModifiedDate } from 'utils';
import { CURRENCY } from 'constants/index';

import { useInvoicePDFPageInfo } from 'hooks';
import {
	H4,
	P,
	OrderInvoicePDFTable,
	PDFPreview,
	Span,
	PDFFooter,
	PDFHeader,
	PDFAddress,
	PDFMain,
	FlexibleContainer,
} from 'components';

import './order-invoice-pdf.scss';

export const OrderInvoicePDF: FunctionComponent<TPDF> = ({
	size,
}) => {
	const { pageInfo } = useInvoicePDFPageInfo();
	const { createDateToShow } = new ModifiedDate();

	if (!pageInfo)
		return null;

	const correctAddress = pageInfo?.address && concatAddress(pageInfo.address);
	const correctDate = pageInfo?.order && createDateToShow({
		dateArg: pageInfo.order.creationDate,
	});

	const language = getPreferLang();
	const { EURO } = CURRENCY;

	const net = `${pageInfo?.order?.invoice?.net} ${EURO}`;
	const paymentDate = '0/0/0';
	const fileName = 'order';

	const serviceStartRes = pageInfo?.order?.invoice?.serviceStart;
	const serviceEndRes = pageInfo?.order?.invoice?.serviceEnd;

	const serviceStart = serviceStartRes && createDateToShow({
		isFull: true,
		dateArg: serviceStartRes,
	});
	const serviceEnd = serviceEndRes && createDateToShow({
		isFull: true,
		dateArg: serviceEndRes,
	});

	const baseClassName = 'order-invoice-pdf';
	const componentClassName = {
		component: baseClassName,
		title: `${baseClassName}__title`,
		firmSection: `${baseClassName}__firm-section`,
		dataSection: `${baseClassName}__data-section`,
		subTitle: `${baseClassName}__sub-title`,
		description: `${baseClassName}__description`,
		text: `${baseClassName}__text`,
	};

	return (
		<PDFPreview
			className={componentClassName.component}
			fileName={fileName}
			size={size}
		>
			<PDFHeader/>
			<PDFMain>
				<PDFAddress/>
				<FlexibleContainer className={componentClassName.firmSection}>
					<div>
						<div>
							<Span>Firma: </Span>
							<Span>{pageInfo?.partner?.name}</Span>
						</div>
						<div>
							<Span>{correctAddress}</Span>
						</div>
						<div>
							<Span>{language}</Span>
						</div>
					</div>
					<div>
						<div>
							<Span theme='silver-chalice-color'>Kunden-Nr: </Span>
							<Span>{pageInfo?.order?.customerId}</Span>
						</div>
						<div>
							<Span theme='silver-chalice-color'>Aufttrags-Nr: </Span>
							<Span>{pageInfo?.order?.id}</Span>
						</div>
						<div>
							<Span theme='silver-chalice-color'>Rechnungs-Nr: </Span>
							<Span>{pageInfo?.order?.invoice?.id}</Span>
						</div>
						<div>
							<Span theme='silver-chalice-color'>Datum: </Span>
							<Span>{correctDate}</Span>
						</div>
					</div>
				</FlexibleContainer>
				<div className={componentClassName.title}>
					<H4 color='black'>RECHNUNG</H4>
					<Span className={componentClassName.subTitle}>Haxpe Dienstleistung</Span>
				</div>
				<FlexibleContainer className={componentClassName.dataSection}>
					<div>
						<div>
							<Span>Einsatz-Datum: </Span>
							<Span>{correctDate}</Span>
						</div>
						<div>
							<Span>Einsatz-Beginn: </Span>
							<Span>{serviceStart}</Span>
						</div>
					</div>
					<div>
						<div>
							<Span>Einsatzort: </Span>
							<Span>{correctAddress}</Span>
						</div>
						<div>
							<Span>Einsatz-Ende: </Span>
							<Span>{serviceEnd}</Span>
						</div>
					</div>
					<div>
						<div>
							<Span>Zahlungsart: </Span>
							<Span>{pageInfo?.order?.paymentMethod}</Span>
						</div>
					</div>
				</FlexibleContainer>
				<OrderInvoicePDFTable info={pageInfo?.order}/>
				<section className={componentClassName.description}>
					<P
						color='black'
						size='medium'
						className={componentClassName.text}
					>
						{
							`Der Gesamtbetrag von ${net} wurde am ${paymentDate} per ${pageInfo?.order?.paymentMethod}`
						}
					</P>
					<P
						color='black'
						size='medium'
						className={componentClassName.text}
					>
							Falls Sie Fragen haben, besuchen Sie das Haxpe Support Center&nbsp;
						<a
							href='https://haxpe.com/contact' target='_blank'
							rel='noopener noreferrer'
						>https://haxpe.com/contact
						</a>&nbsp;
							oder kontactieren Sie uns im Live-Chat.
					</P>
					<P
						color='black'
						size='medium'
						className={componentClassName.text}
					>
							Vielen Dank, dass Sie die Haxpe App nutzen! Ihr Haxpe Team
					</P>
				</section>
			</PDFMain>
			<PDFFooter/>
		</PDFPreview>
	);
};
