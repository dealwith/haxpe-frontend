import { FunctionComponent } from 'react';

import { OrderInvoicePDF } from 'pages';
import {
	ButtonsContainer,
	Container,
	DownloadPDFButton,
	ShowPicturesButton,
	Title,
	ForwardInvoiceButton,
	FlexibleContainer,
} from 'components';

import './order-invoice-pdf-for-user-app.scss';

export const OrderInvoicePDFForUserApp: FunctionComponent = () => {
	const baseClassName = 'order-invoice-pdf-for-user-app';

	return (
		<Container
			className={baseClassName}
			isDefault={false}
			height={500}
			maxWidth={380}
			centered='space-between'
			flexDirection='column'
			theme='white'
		>
			<Title px='16'>Invoice</Title>
			<OrderInvoicePDF size='small'/>
			<div>
				<ButtonsContainer centered='center'>
					<DownloadPDFButton theme='green'/>
				</ButtonsContainer>
				<FlexibleContainer>
					<ShowPicturesButton
						iconTheme='transparent'
						theme='transparent-mine-shaft'
					/>
					<ForwardInvoiceButton
						iconTheme='transparent'
						theme='transparent-mine-shaft'
					/>
				</FlexibleContainer>
			</div>
		</Container>
	);
};
