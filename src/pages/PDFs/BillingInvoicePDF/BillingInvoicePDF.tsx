import { FunctionComponent } from 'react';

import { concatAddress, ModifiedDate } from 'utils';
import { CURRENCY } from 'constants/index';

import {
	H4,
	PDFAddress,
	PDFFooter,
	PDFHeader,
	PDFMain,
	PDFPreview,
	FlexibleContainer,
	BillingInvoicePDFTable,
	Span,
} from 'components';
import { useBillingInvoicePageInfo } from './hooks';

export const BillingInvoicePDF: FunctionComponent = () => {
	const { pageInfo } = useBillingInvoicePageInfo();
	const { createDateToShow } = new ModifiedDate();

	const correctAddress = pageInfo?.address && concatAddress(pageInfo.address);
	const correctDate = pageInfo?.order && createDateToShow({
		dateArg: pageInfo.order.creationDate,
	});

	const total = '0000';
	const { EURO } = CURRENCY;
	const fileName = 'billing-invoice';

	return (
		<PDFPreview fileName={fileName}>
			<PDFHeader/>
			<PDFMain>
				<PDFAddress/>
				<FlexibleContainer>
					<div>
						<div>
							<Span>Firma: </Span>
							<Span>{pageInfo?.partner?.name}</Span>
						</div>
						<div>
							<Span>{correctAddress}</Span>
						</div>
						<div>
							<Span>DE</Span>
						</div>
					</div>
					<div>
						<div>
							<Span theme='silver-chalice-color'>Kunden-Nr: </Span>
							<Span>{pageInfo?.order?.customerId}</Span>
						</div>
						<div>
							<Span theme='silver-chalice-color'>Handelsregister-Nr: </Span>
							{/* <Span>//TODO: add id</Span> */}
						</div>
						<div>
							<Span theme='silver-chalice-color'>Datum: </Span>
							<Span>{correctDate}</Span>
						</div>
						<div>
							<Span theme='silver-chalice-color'>Rechnungs-Nr: </Span>
							{/* TODO: invoice id for partner */}
							<Span>{pageInfo?.order?.invoice?.id}</Span>
						</div>
					</div>
				</FlexibleContainer>
				<div>
					<H4 color='black'>RECHNUNG</H4>
					<Span>Haxpe Gebühren für Juni 2020</Span>
				</div>
				<BillingInvoicePDFTable info={{}}/>
				<section>
					Der Gesamtbetrag von {total} {EURO}
				</section>
			</PDFMain>
			<PDFFooter/>
		</PDFPreview>
	);
};
