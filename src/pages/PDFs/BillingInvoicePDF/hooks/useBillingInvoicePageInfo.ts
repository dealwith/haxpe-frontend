import { useState } from 'react';

import { useBillingParams } from 'hooks';

type TReturnType = {
	pageInfo: any;
}

export const useBillingInvoicePageInfo = (): TReturnType => {
	const [pageInfo, setPageInfo] = useState<any>();
	const { payoutId } = useBillingParams();

	return { pageInfo };
};
