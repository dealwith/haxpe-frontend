import { useState } from 'react';

import { useBillingParams } from 'hooks';

//TODO: complete logic
export const useBillingPayoutOverviewPageInfo = () => {
	const [pageInfo, setPageInfo] = useState<any>();
	const { payoutId } = useBillingParams();

	return { pageInfo };
};
