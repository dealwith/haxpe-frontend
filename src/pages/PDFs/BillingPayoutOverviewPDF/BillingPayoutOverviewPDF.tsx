import { FunctionComponent } from 'react';

import { ModifiedDate } from 'utils';

import {
	BillingPayoutOverviewPDFTable,
	H4,
	P,
	PDFFooter,
	PDFHeader,
	PDFMain,
	PDFPreview,
	FlexibleContainer,
	Span,
} from 'components';
import { useBillingPayoutOverviewPageInfo } from './hooks';

export const BillingPayoutOverviewPDF: FunctionComponent = () => {
	const { pageInfo } = useBillingPayoutOverviewPageInfo();
	const { createDateToShow } = new ModifiedDate();

	const correctDate = pageInfo?.order && createDateToShow({
		dateArg: pageInfo.order.creationDate,
	});

	const fileName = 'billing-payout-overview';

	return (
		<PDFPreview fileName={fileName}>
			<PDFHeader/>
			<PDFMain>
				<H4>
					Auszahlungsübersicht
				</H4>
				<FlexibleContainer>
					<div>
						<div>
							<Span theme='silver-chalice-color'>Kunden-Nr: </Span>
							<Span>{pageInfo?.order?.customerId}</Span>
						</div>
						<div>
							<Span theme='silver-chalice-color'>Datum: </Span>
							<Span>{correctDate}</Span>
						</div>
					</div>
				</FlexibleContainer>
				<H4>Aufräge</H4>
				<BillingPayoutOverviewPDFTable info={{}}/>
				<H4 align='center'>
					Fortsetzung auf nächster Seite
				</H4>
				<P>
					Dies ist keine Rechnung. Sie werden separat eine monatilche
					Rechnung erhalten. Da es sich um eine Finanzdeienstleistung
					handelf, sind die Haxpe Gebühren von der Umsatzsteuer befreit.
					Das Auszahlungsdatum und das Datum, an dem Sie die Auszahlung auf
					lhrem Bankkonto erhalten, können voneinander abweichen, abhängig
					von der Geschwindigkeit lhner Bank.
				</P>
				<P align='center' isBold={true}>Seite 1 von 2</P>
			</PDFMain>
			<PDFFooter/>
		</PDFPreview>
	);
};
