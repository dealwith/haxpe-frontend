export enum EIconsList {
	person = 'person',
	persons = 'persons',
	phone = 'phone',
	email = 'email',
	lock = 'lock',
	target = 'target',
	plus = 'plus',
	house = 'house',
	wrench = 'wrench',
	success = 'success',
	'wrench-and-screwdriver' = 'wrench-and-screwdriver',
	key = 'key',
	'pest-control' = 'pest-control',
	pipe = 'pipe',
}

export type TIcon = keyof typeof EIconsList;
