import { FunctionComponent } from 'react';

import { TSize } from 'components/Image/interfaces';
import { TIcon } from './interfaces';

import { Image } from 'components';
import personIcon from './icons/personIcon.svg';
import emailIcon from './icons/emailIcon.svg';
import lockIcon from './icons/lockIcon.svg';
import targetIcon from './icons/targetIcon.svg';
import phoneIcon from './icons/phoneIcon.svg';
import plusIcon from './icons/plusIcon.svg';
import houseIcon from './icons/houseIcon.svg';
import personsIcon from './icons/personsIcon.svg';
import wrench from './icons/wrench.svg';
import success from './icons/success.svg';
import wrenchAndScrewdriver from './icons/wrenchAndScrewdriver.svg';
import keyIcon from './icons/keyIcon.svg';
import pestControlIcon from './icons/pestControlIcon.svg';
import pipeIcon from './icons/pipeIcon.svg';

import './render-icon.scss';

type TProps = {
	icon: TIcon;
	width?: number;
	height?: number;
	size?: TSize;
}

export const RenderIcon: FunctionComponent<TProps> = ({
	icon,
	width,
	height,
	size,
}) => {
	const baseClassName = 'render-icon';

	const renderIcon = (icon: TIcon) => {
		let component;

		switch (icon) {
			case 'person':
				component = personIcon;

				break;

			case 'persons':
				component = personsIcon;

				break;

			case 'email':
				component = emailIcon;

				break;

			case 'lock':
				component = lockIcon;

				break;

			case 'target':
				component = targetIcon;

				break;

			case 'phone':
				component = phoneIcon;

				break;

			case 'plus':
				component = plusIcon;

				break;

			case 'house':
				component = houseIcon;

				break;

			case 'wrench':
				component = wrench;

				break;

			case 'success':
				component = success;

				break;

			case 'wrench-and-screwdriver':
				component = wrenchAndScrewdriver;

				break;

			case 'key':
				component = keyIcon;

				break;

			case 'pest-control':
				component = pestControlIcon;

				break;

			case 'pipe':
				component = pipeIcon;

				break;

			default:
				return;
		}

		return (
			<div className={baseClassName}>
				<Image
					size={size}
					width={width}
					height={height}
					src={component}
				/>
			</div>
		);
	};

	return icon
		? renderIcon(icon)
		: null;
};
