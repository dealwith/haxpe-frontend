import { FunctionComponent } from 'react';
import { DateInput as AdminDateInput, DateInputProps } from 'react-admin';

import './date-input.scss';

export const DateInput: FunctionComponent<DateInputProps> = props => {
	const baseClassName = 'date-input';

	return (
		<AdminDateInput
			{...props}
			className={baseClassName}
		/>
	);
};
