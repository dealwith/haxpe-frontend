import { FunctionComponent } from 'react';
import { TextInput as AdminTextInput, TextInputProps } from 'react-admin';

import './text-input.scss';

export const TextInput: FunctionComponent<TextInputProps> = props => {
	const baseClassName = 'text-input';

	return (
		<AdminTextInput
			{...props}
			className={baseClassName}
		/>
	);
};
