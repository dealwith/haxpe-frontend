import { FunctionComponent } from 'react';
import { Filter as MaterialUiFilter, SearchInput } from 'ra-ui-materialui';

export const Filter: FunctionComponent<unknown> = props => {
	return (
		<MaterialUiFilter {...props}>
			<SearchInput source='q' alwaysOn />
		</MaterialUiFilter>
	);
};
