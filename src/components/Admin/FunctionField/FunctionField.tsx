import { FunctionComponent } from 'react';
import { FunctionField as AdminFunctionField, FunctionFieldProps } from 'react-admin';

import './function-field.scss';

export const FunctionField: FunctionComponent<FunctionFieldProps> = props => {
	const baseClassName = 'function-field';

	return (
		<AdminFunctionField
			className={baseClassName}
			{...props}
		/>
	);
};
