import { FunctionComponent } from 'react';

import { Menu as AdminMenu, MenuProps } from 'react-admin';

import './admin-menu.scss';

export const Menu: FunctionComponent<MenuProps> = props => {
	const baseClassName = 'admin-menu';

	return <AdminMenu className={baseClassName} {...props}/>;
};
