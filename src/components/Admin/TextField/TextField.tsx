import { FunctionComponent } from 'react';
import { TextField as AdminTextField, TextFieldProps } from 'react-admin';

import './text-field.scss';

export const TextField: FunctionComponent<TextFieldProps> = props => {
	const baseClassName = 'text-field';

	return (
		<AdminTextField
			className={baseClassName}
			{...props}
		/>
	);
};
