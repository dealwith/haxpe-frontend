import { FunctionComponent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { StepComponentProps } from 'react-step-builder';

import { ECustomerOrderSteps } from 'interfaces';

import { Container, P } from 'components';
import { ReactComponent as CancelIcon } from './cancelIcon.svg';

import './order-canceled.scss';

export const OrderCanceled: FunctionComponent<StepComponentProps> = ({
	title,
	allSteps,
	jump,
}) => {
	const { t } = useTranslation(['orderCustomer']);

	useEffect(() => {
		const timeout = setTimeout(() => {
			const { order: orderCreationStep } = allSteps
				.find(({ title }) => title === ECustomerOrderSteps.orderCreation);

			jump(orderCreationStep);
		}, 1500);

		return () => clearTimeout(timeout);
	}, []);

	const baseClassName = 'order-canceled';
	const componentClassName = {
		component: baseClassName,
		title: `${baseClassName}__title`,
	};

	return (
		<Container
			maxWidth={380}
			isFitContent={true}
			theme='white'
			className={componentClassName.component}
		>
			<CancelIcon />
			<P
				isBold={true}
				align='center'
				className={componentClassName.title}
			>
				{t(`orderCustomer:${title}`)}
			</P>
		</Container>
	);
};
