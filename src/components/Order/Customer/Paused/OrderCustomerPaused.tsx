import { FunctionComponent, useEffect } from 'react';
import { StepComponentProps } from 'react-step-builder';

import { EOrderStatus } from 'interfaces';

import { useSocket } from 'hooks';
import { OrderCustomerProcessLayout } from 'components';
import { jumpToCorrectStep } from '../utils';

export const OrderCustomerPaused: FunctionComponent<StepComponentProps> = ({
	title,
	getState,
	allSteps,
	jump,
}) => {
	const socket = useSocket();

	useEffect(() => {
		if (socket?.orderChanged && socket?.orderChanged?.orderStatus !== EOrderStatus.paused)
			jumpToCorrectStep(socket.orderChanged.orderStatus, jump, allSteps);
	}, [allSteps, jump, socket?.orderChanged, socket?.orderChanged?.orderStatus]);

	return (
		<OrderCustomerProcessLayout title={title} getState={getState} />
	);
};
