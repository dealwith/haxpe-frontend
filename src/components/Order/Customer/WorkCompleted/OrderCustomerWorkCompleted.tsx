import { FunctionComponent, useEffect } from 'react';
import { StepComponentProps } from 'react-step-builder';

import { OrderCustomerProcessLayout } from 'components';
import { useSocket } from 'hooks';
import { jumpToCorrectStep } from '../utils';
import { TCustomerOrderSteps, TOrderStatuses } from 'interfaces';

export const OrderCustomerWorkCompleted: FunctionComponent<StepComponentProps> = props => {
	const {
		allSteps,
		title,
		jump,
	} = props;
	const socket = useSocket();

	useEffect(() => {
		const isOrderStatusChanged = socket?.orderChanged
			&& socket.orderChanged?.orderStatus !== title;

		if (isOrderStatusChanged) {
			jumpToCorrectStep<TOrderStatuses | TCustomerOrderSteps>(
				socket.orderChanged.orderStatus,
				jump,
				allSteps,
			);
		}
	}, [
		allSteps,
		jump,
		socket.orderChanged,
		socket.orderChanged?.orderStatus,
		title,
	]);

	return <OrderCustomerProcessLayout {...props} />;
};

