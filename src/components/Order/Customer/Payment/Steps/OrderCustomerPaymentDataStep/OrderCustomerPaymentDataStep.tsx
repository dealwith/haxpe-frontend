import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { StepComponentProps } from 'react-step-builder';
import { useForm } from 'react-hook-form';

import { jumpToCorrectStep } from 'components/Order/Customer/utils';
import { TOrderCustomerPaymentSteps } from 'interfaces/order/customer';

import { useAuth } from 'hooks';
import {
	Button,
	ButtonsContainer,
	ContentContainer,
	Input,
	P,
} from 'components';

import './order-customer-payment-data-step.scss';

interface IBillingAddress {
	firstName: string;
	lastName: string;
	street: string;
	buildingNum: number;
	zipCode: number;
	city: string;
	isDifferentBilling: boolean;
}

interface IDifferentBillingAddress {
	company: string;
	differentFirstName: string;
	differentLastName: string;
	differentStreet: string;
	differentBuildingNum: number;
	differentZipCode: string;
	differentCity: string;
	differentEmail: string;
}

export const OrderCustomerPaymentDataStep: FunctionComponent<StepComponentProps> = ({
	allSteps,
	jump,
}) => {
	const { t } = useTranslation(['orderCustomer', 'input']);
	const { user } = useAuth();
	const {
		control,
		handleSubmit: validateBeforeSubmit,
		watch,
	} = useForm<IBillingAddress>({
		defaultValues: {
			firstName: user?.name,
			lastName: user?.surname,
		},
	});

	const handleBackClick = () => {
		jumpToCorrectStep<TOrderCustomerPaymentSteps>('invoice', jump, allSteps);
	};

	const handleContinueClick = () => {
		jumpToCorrectStep<TOrderCustomerPaymentSteps>('paymentMethods', jump, allSteps);
	};

	const handleSubmit = (data: IBillingAddress | IDifferentBillingAddress) => {
		console.log(data);
	};

	const baseClassName = 'order-customer-payment-data';
	const componentClassName = {
		component: baseClassName,
		container: `${baseClassName}-container`,
		checkbox: `${baseClassName}__checkbox`,
		buttonContainer: `${baseClassName}__button-container`,
	};
	const isDifferentBilling = watch('isDifferentBilling');

	return (
		<>
			<form
				className={componentClassName.component}
				onSubmit={validateBeforeSubmit(handleSubmit)}
			>
				<ContentContainer>
					<P isBold={true}>{t('orderCustomer:yourData')}</P>
				</ContentContainer>
				<Input
					name='firstName'
					placeholder={t('input:firstName')}
					icon='person'
					control={control}
					size='full'
				/>
				<Input
					name='lastName'
					placeholder={t('input:lastName')}
					icon='person'
					control={control}
					size='full'
				/>
				<Input
					name='street'
					placeholder={t('input:street')}
					icon='target'
					control={control}
					size='full'
				/>
				<Input
					name='buildingNum'
					placeholder={t('input:buildingNum')}
					icon='target'
					control={control}
					size='full'
				/>
				<Input
					name='zipCode'
					placeholder={t('input:zipCode')}
					icon='target'
					control={control}
					size='full'
				/>
				<Input
					name='city'
					placeholder={t('input:city')}
					icon='target'
					control={control}
					size='full'
				/>
				<Input
					type='checkbox'
					name='isDifferentBilling'
					control={control}
					labelClassName={componentClassName.checkbox}
					placeholder={t('input:differentBillingAddress')}
				/>
				{isDifferentBilling
					&& <>
						<Input
							name='company'
							control={control}
							placeholder={t('input:differentBillingAddress')}
							size='full'
							icon='house'
						/>
						<Input
							name='differentFirstName'
							control={control}
							icon='person'
							size='full'
							placeholder={t('input:firstName')}
						/>
						<Input
							name='differentLastName'
							control={control}
							icon='person'
							size='full'
							placeholder={t('input:lastName')}
						/>
						<Input
							name='differentStreet'
							control={control}
							icon='target'
							size='full'
							placeholder={t('input:street')}
						/>
						<Input
							name='differentStreet'
							control={control}
							icon='target'
							size='full'
							placeholder={t('input:street')}
						/>
						<Input
							name='differentBuildingNum'
							control={control}
							icon='target'
							size='full'
							placeholder={t('input:buildingNum')}
						/>
						<Input
							name='differentZipCode'
							control={control}
							icon='target'
							size='full'
							placeholder={t('input:zipCode')}
						/>
						<Input
							name='differentCity'
							control={control}
							icon='target'
							size='full'
							placeholder={t('input:city')}
						/>
						<Input
							name='differentEmail'
							control={control}
							icon='target'
							size='full'
							placeholder={t('input:email')}
						/>
					</>
				}
				<ButtonsContainer
					className={componentClassName.buttonContainer}
				>
					<Button
						theme='gray'
						onClick={handleBackClick}
						rounded='xxl'
					>
						{t('button:back')}
					</Button>
					<Button
						theme='green'
						onClick={handleContinueClick}
						rounded='xxl'
					>
						{t('button:continue')}
					</Button>
				</ButtonsContainer>
			</form>
		</>
	);
};
