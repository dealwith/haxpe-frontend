import { FunctionComponent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { StepComponentProps, GetState, SetState } from 'react-step-builder';
import { useForm } from 'react-hook-form';

import { isObjectWithProperties } from 'utils';
import { OrderService } from 'services';
import { IInvoice } from 'interfaces';

import { useServicesIndustries, useSocket } from 'hooks';
import {
	Button,
	ContentContainer,
	Input,
	P,
	Span,
	OrderInvoiceTable,
} from 'components';

import './order-customer-payment.scss';

interface IForm {
	couponCode?: string;
}

type TProps = {
	orderCustomerGetState: GetState;
	orderCustomerSetState: SetState;
} & StepComponentProps;

export const OrderCustomerPaymentInvoiceStep: FunctionComponent<TProps> = ({
	next,
	orderCustomerGetState,
	orderCustomerSetState,
}) => {
	const { orderChanged } = useSocket();
	const [invoiceTable, setInvoiceTable] = useState<IInvoice>(null);
	const [workerReport, setWorkerReport] = useState('');
	const [promoCode, setPromoCode] = useState('');
	const { control, getValues } = useForm<IForm>();

	const { t } = useTranslation([
		'order',
		'orderCustomer',
		'orderCustomerPayment',
		'input',
		'button',
	]);
	const {
		orderId,
		industryId,
		serviceTypeId,
		invoice,
		report,
		couponCode: stepsCouponCode,
	} = orderCustomerGetState('order', '');
	const {
		getTranslatedIndustryById,
		getTranslatedServiceById,
	} = useServicesIndustries();

	useEffect(() => {
		if (orderChanged?.invoice && isObjectWithProperties(orderChanged.invoice)) {
			orderCustomerSetState('order', { invoice: orderChanged.invoice });

			setInvoiceTable(orderChanged.invoice);
		} else
			setInvoiceTable(invoice);
	}, []);

	useEffect(() => {
		if (orderChanged?.report) {
			orderCustomerSetState('order', { report: orderChanged.report });

			setWorkerReport(orderChanged.report);
		} else
			setWorkerReport(report);
	}, []);

	useEffect(() => {
		if (orderChanged?.couponCode) {
			orderCustomerSetState('order', { couponCode: orderChanged.couponCode });

			setPromoCode(orderChanged.report);
		} else
			setPromoCode(stepsCouponCode);
	}, []);

	const handleClick = async () => {
		try {
			const coupon = getValues('couponCode');

			if (coupon)
				await OrderService.applyCoupon(orderId, coupon);

			next();
		} catch (err) {
			console.error(err);
		}
	};

	const industry = getTranslatedIndustryById(industryId);
	const service = getTranslatedServiceById(serviceTypeId);

	const baseClassName = 'order-customer-payment';
	const componentClassName = {
		component: baseClassName,
		generalInfo: `${baseClassName}__general-info`,
		reportContainer: `${baseClassName}__report-container`,
		buttonContainer: `${baseClassName}__button-container`,
		reportTitle: `${baseClassName}__report-title`,
	};

	return (
		<>
			<div className={componentClassName.generalInfo}>
				<ContentContainer>
					<P isBold={true}>
						{t(`orderCustomer:yourOrder`)}
					</P>
				</ContentContainer>
				<ContentContainer>
					<P color='dark-gray'>{industry}</P>
				</ContentContainer>
				<ContentContainer>
					<P color='dark-gray'>{service}</P>
				</ContentContainer>
			</div>
			<OrderInvoiceTable invoice={invoiceTable} />
			<ContentContainer
				centered='flex-start'
				className={componentClassName.reportContainer}
			>
				<Span className={componentClassName.reportTitle} isBold={true}>
					{t('orderCustomerPayment:report')}:&nbsp;
				</Span>
				{workerReport}
			</ContentContainer>
			<Input
				name='couponCode'
				placeholder={t('input:promoCode')}
				icon='plus'
				size='full'
				control={control}
				value={promoCode}
			/>
			<div className={componentClassName.buttonContainer}>
				<Button
					rounded='xxl'
					theme='green'
					onClick={handleClick}
				>
					{t('button:continue')}
				</Button>
			</div>
		</>
	);
};
