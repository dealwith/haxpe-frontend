import { FunctionComponent } from 'react';
import { StepComponentProps, Steps, Step } from 'react-step-builder';
import { loadStripe } from '@stripe/stripe-js';
import { Elements } from '@stripe/react-stripe-js';
import { PayPalScriptProvider } from '@paypal/react-paypal-js';
import type { PayPalScriptOptions } from '@paypal/paypal-js/types/script-options';

import {
	ApplePayGPay,
	CardCheckoutForm,
	Container,
} from 'components';

import { EOrderCustomerPaymentSteps } from 'interfaces/order/customer';
import { OrderCustomerPaymentDataStep, OrderCustomerPaymentInvoiceStep } from './Steps';
import { PaymentMethodGeneral } from 'components/PaymentMethod/PaymentMethodGeneral';

const payPalClientId = process.env.REACT_APP_PAYPAL_API_TEST_KEY;
const paypalOptions: PayPalScriptOptions  = {
	'client-id': payPalClientId,
	currency: 'EUR',
	debug: true,
	'disable-funding': 'card',
};

export const OrderCustomerPayment: FunctionComponent<StepComponentProps> = ({
	getState: orderCustomerGetState,
	jump: orderCustomerJump,
	allSteps: orderCustomerAllSteps,
	setState: setStepState,
}) => {
	// const [invoiceTable, setInvoiceTable] = useState<IInvoice>(null);
	// const { getValues } = useForm<IForm>();
	// const {
	// 	orderId,
	// } = orderCustomerGetState('order', '');

	// const handleContinueClick = async () => {
	// 	try {
	// 		const coupon = getValues('couponCode');

	// 		if (coupon)
	// 			await OrderService.applyCoupon(orderId, coupon);

	// 		jumpToCorrectStep(
	// 			ECustomerOrderSteps.paymentData,
	// 			orderCustomerJump,
	// 			orderCustomerAllSteps,
	// 		);
	// 	} catch (err) {
	// 		console.error(err);
	// 	}
	// };

	const baseClassName = 'order-customer-payment';
	const componentClassName = {
		component: baseClassName,
		generalInfo: `${baseClassName}__general-info`,
		reportContainer: `${baseClassName}__report-container`,
		buttonContainer: `${baseClassName}__button-container`,
	};

	const stripeWrapperModule = loadStripe(process.env.REACT_APP_STRIPE_API_TEST_KEY);

	return (
		<Elements stripe={stripeWrapperModule}>
			<PayPalScriptProvider options={paypalOptions}>

				<Container
					maxWidth={380}
					isFitContent={true}
					theme='white'
					className={componentClassName.component}
				>
					<Steps>
						<Step
							title={EOrderCustomerPaymentSteps.invoice}
							component={stepProps => (
								<OrderCustomerPaymentInvoiceStep
									orderCustomerGetState={orderCustomerGetState}
									orderCustomerSetState={setStepState}
									{...stepProps}
								/>
							)}
						/>
						<Step
							title={EOrderCustomerPaymentSteps.paymentData}
							component={OrderCustomerPaymentDataStep}
						/>
						<Step
							title={EOrderCustomerPaymentSteps.paymentMethods}
							component={stepProps => (
								<PaymentMethodGeneral
									orderCustomerGetState={orderCustomerGetState}
									orderCustomerAllSteps={orderCustomerAllSteps}
									orderCustomerJump={orderCustomerJump}
									{...stepProps}
								/>
							)}
						/>
						<Step
							title={EOrderCustomerPaymentSteps.cardCheckout}
							component={customerPaymentProps => (
								<CardCheckoutForm
									{...customerPaymentProps}
									orderCustomerGetState={orderCustomerGetState}
									orderCustomerAllSteps={orderCustomerAllSteps}
									orderCustomerJump={orderCustomerJump}
								/>
							)}
						/>
						<Step
							title={EOrderCustomerPaymentSteps.GPayApplePay}
							component={customerPaymentProps => (
								<ApplePayGPay
									{...customerPaymentProps}
									orderCustomerGetState={orderCustomerGetState}
								/>
							)}
						/>
					</Steps>
				</Container>
			</PayPalScriptProvider>
		</Elements>
	);
};
