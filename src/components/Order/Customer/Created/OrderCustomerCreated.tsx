// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-nocheck
import { FunctionComponent, useEffect, useState } from 'react';
import { StepComponentProps } from 'react-step-builder';
import { useTranslation } from 'react-i18next';

import { isObjectWithProperties } from 'utils';
import { AddressService, OrderService, UserService } from 'services';
import { ECustomerOrderSteps, EResponseStatus } from 'interfaces';
import { jumpToCorrectStep } from '../utils';

import { useSocket } from 'hooks';
import {
	P,
	Map,
	DotFlashingLoader,
	Container,
	Button,
} from 'components';

import './order-customer-created.scss';

export const OrderCustomerCreated: FunctionComponent<StepComponentProps> = ({
	title,
	getState,
	jump,
	allSteps,
	setState: setStepState,
}) => {
	const { t } = useTranslation(['button', 'orderCustomer']);
	const { orderId } = getState('order', '');
	const [coords, setCoords] = useState({lat: 0, lon: 0});
	const { orderChanged } = useSocket();

	useEffect(() => {
		const orderStatus = orderChanged?.orderStatus;

		if (orderStatus && orderStatus !== title) {
			const fetchData = async () => {
				let workerFullName, workerPhone;

				const workerId = orderChanged?.workerId;

				if (workerId) {
					const {
						status: userStatus,
						body: userRes,
					} = await UserService.getUserByWorkerId(workerId);

					if (userStatus === EResponseStatus.success) {
						workerFullName = userRes.fullName;
						workerPhone = userRes.phoneNumber;
					}
				}

				setStepState('order', {
					workerId: orderChanged.workerId,
					serviceTypeId: orderChanged.serviceTypeId,
					orderId: orderChanged.id,
					expectedArrival: orderChanged.expectedArrival,
					industryId: orderChanged.industryId,
					...(workerId && {
						workerFullName,
						workerPhone,
					}),
				});

				jumpToCorrectStep(orderStatus, jump, allSteps);
			};

			fetchData();
		}

	}, [allSteps, jump, orderChanged?.orderStatus, title]);

	useEffect(() => {
		try {
			const fetchData = async () => {
				const { body: { addressId } } = await OrderService.getOrderById(orderId);
				const { body: { lat, lon } } = await AddressService.getAddressById(addressId);

				if (lat && lon)
					setCoords({ lat, lon });
			};

			fetchData();
		} catch (err) {
			console.error(err);
		}
	}, []);

	const handleCancelClick = () => jumpToCorrectStep(ECustomerOrderSteps.cancelReason, jump, allSteps);

	const baseClassName = 'order-progress';
	const componentClassName = {
		loaderContainer: `${baseClassName}__loader-container`,
		mapContainer: `${baseClassName}__map-container`,
		cancelButton: `${baseClassName}__cancel-button`,
		searchInProgress: `${baseClassName}__search-in-progress`,
		patience: `${baseClassName}__patience`,
	};

	const translatedText = `${t('button:cancel')} ${t('button:search')}`;
	const isMapReady = isObjectWithProperties(coords);

	return (
		<Container
			maxWidth={380}
			isFitContent={true}
			theme='white'
		>
			<P align='center' isBold={true}>
				{t(`orderCustomer:${title}`)}
			</P>
			<Container
				className={componentClassName.loaderContainer}
				isInner={true}
				centered='center'
			>
				<DotFlashingLoader />
			</Container>
			{isMapReady
				&& <div className={componentClassName.mapContainer}>
					<Map coords={coords} />
				</div>
			}
			<P align='center' className={componentClassName.searchInProgress}>
				{t('orderCustomer:searchInProgressDescription')}
			</P>
			<P align='center' isBold={true} className={componentClassName.patience}>
				{t('orderCustomer:patience')}
			</P>
			<Button
				className={componentClassName.cancelButton}
				theme='red'
				rounded='xxl'
				onClick={handleCancelClick}
			>
				{translatedText}
			</Button>
		</Container>
	);
};
