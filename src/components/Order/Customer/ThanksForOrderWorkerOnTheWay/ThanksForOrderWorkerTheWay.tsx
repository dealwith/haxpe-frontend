import { FunctionComponent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { StepComponentProps } from 'react-step-builder';

import { EOrderStatus } from 'interfaces';
import { jumpToCorrectStep } from '../utils';

import { Container, CurrentOrderShortInfo, P } from 'components';
import { ReactComponent as GreenCheckIcon } from 'components/Icons/GreenCheckIcon.svg';

import './thanks-for-order-worker-on-the-way.scss';

export const ThanksForOrderWorkerOnTheWay: FunctionComponent<StepComponentProps> = ({
	allSteps,
	getState,
	title,
	jump,
}) => {
	const { t } = useTranslation(['orderCustomer']);

	useEffect(() => {
		setTimeout(() => {
			jumpToCorrectStep(EOrderStatus.workerOnWay, jump, allSteps);
		}, 3000);
	}, []);

	const baseClassName = 'thanks-for-order-worker-on-the-way';
	const componentClassName = {
		component: baseClassName,
		content: `${baseClassName}__content`,
		text: `${baseClassName}__text`,
	};

	return (
		<Container
			className={componentClassName.component}
			height={560}
			flow='vertical'
			centered='vertical'
			isInner={true}
			maxWidth={380}
			theme='white'
		>
			<CurrentOrderShortInfo
				title={title}
				getState={getState}
			/>
			<div className={componentClassName.content}>
				<GreenCheckIcon />
				<div className={componentClassName.text}>
					<P
						isBold={true}
						color='dark-gray'
						align='center'
					>
						{t(`orderCustomer:${title}`)}
					</P>
				</div>
			</div>
		</Container>
	);
};
