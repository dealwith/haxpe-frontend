import { FunctionComponent } from 'react';
import { StepComponentProps } from 'react-step-builder';

import { OrderCustomerProcessLayout, Container } from 'components';

export const OrderCustomerReserved: FunctionComponent<StepComponentProps> = ({
	title,
	getState,
}) => {
	return (
		<Container>
			<OrderCustomerProcessLayout
				title={title}
				getState={getState}
			/>
		</Container>
	);
};
