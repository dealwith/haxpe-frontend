import { useTranslation } from 'react-i18next';
import { FunctionComponent } from 'react';

import { P } from 'components';
import { PrivacyPolicyLink } from './PrivacyPolicyLink';
import { TermsConditionsLink } from './TermsConditionsLink';

export const AgreementText: FunctionComponent = () => {
	const { t } = useTranslation('orderDetails');

	return (
		<P align='left'>
			{t('agreement')}&nbsp;
			<PrivacyPolicyLink />&nbsp;
			{t('agreement2')}&nbsp;
			<TermsConditionsLink />
		</P>
	);
};
