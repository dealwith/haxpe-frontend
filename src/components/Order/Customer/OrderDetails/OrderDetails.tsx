import { FunctionComponent, useEffect } from 'react';
import { StepComponentProps } from 'react-step-builder';
import { useTranslation } from 'react-i18next';
import { useForm } from 'react-hook-form';

import { CURRENCY } from 'constants/index';
import { OrderService } from 'services';
import { ECustomerOrderSteps, EOrderStatus, EResponseStatus } from 'interfaces';
import { jumpToCorrectStep } from '../utils';

import { useMultiState, useServicesIndustries } from 'hooks';
import {
	P,
	Input,
	Button,
	ContentContainer,
	Container,
} from 'components';
import { AgreementText } from './AgreementText';

import './order-details.scss';

type TOrderDetails = {
	industry: string;
	service: string;
	fee: string;
	workingTime: string;
	materialCosts: string;
}

interface IForm {
	couponCode: string;
	agreement: boolean;
}

export const OrderDetails: FunctionComponent<StepComponentProps> = ({
	title,
	getState,
	allSteps,
	jump,
}) => {
	const { orderId, industryId, serviceTypeId } = getState('order', '');
	const { EURO } = CURRENCY;
	const {
		control,
		handleSubmit: validateBeforeSubmit,
		register,
	} = useForm<IForm>({
		defaultValues: {
			couponCode: '',
			agreement: false,
		},
	});
	const { t } = useTranslation([
		'industries',
		'services',
		'orderDetails',
		'input',
		'orderCustomer',
		'button',
	]);
	const [orderDetails, setOrderDetails] = useMultiState<TOrderDetails>({
		industry: '',
		service: '',
		fee: `0${EURO}`,
		workingTime: `from 59${EURO}`,
		materialCosts: t('orderDetails:materialCosts'),
	});
	const {
		getTranslatedIndustryById,
		getTranslatedServiceById,
	} = useServicesIndustries();

	useEffect(() => {
		const fetchAndSetOrderDetails = async () => {
			try {
				const {
					body: hourRate,
				} = await OrderService.hourRate();

				const industryName = getTranslatedIndustryById(industryId);
				const serviceTypeName = getTranslatedServiceById(serviceTypeId);

				setOrderDetails({
					industry: industryName,
					service: serviceTypeName,
					fee: `29${EURO}`,
					workingTime: `from ${hourRate}${EURO}`,
				});
			} catch (err) {
				console.error(err);
			}
		};

		fetchAndSetOrderDetails();
	}, [
		EURO,
		getTranslatedIndustryById,
		getTranslatedServiceById,
		orderId,
		setOrderDetails,
	]);

	const handleCancel = async () => {
		try	{
			const { status } = await OrderService.customerReject(orderId);

			console.log('rerender');

			if (status === EResponseStatus.success)
				jumpToCorrectStep(ECustomerOrderSteps.canceledOrder, jump, allSteps);
		} catch (err) {
			throw new Error(err.message);
		}
	};

	const handleSubmit = async ({ couponCode }: IForm) => {
		if (couponCode)
			await OrderService.applyCoupon(orderId, couponCode);

		const { status } = await OrderService.customerStartOrder(orderId);

		if (status === EResponseStatus.success)
			jumpToCorrectStep(EOrderStatus.created, jump, allSteps);
	};

	const baseClassName = 'order-details';
	const baseCostClassName = 'cost';
	const componentClassName = {
		component: baseClassName,
		industry: `${baseClassName}__industry`,
		service: `${baseClassName}__service`,
		cost: `${baseClassName}__${baseCostClassName} ${baseCostClassName}`,
		costRow: `${baseCostClassName}__row`,
		promoCode: `${baseClassName}__promo-code`,
		agreement: `${baseClassName}__agreement`,
		buttons: `${baseClassName}__buttons`,
	};

	return (
		<Container
			maxWidth={380}
			isFitContent={true}
			theme='white'
			className={componentClassName.component}
		>
			<ContentContainer >
				<P isBold={true}>
					{t(`orderCustomer:${title}`)}
				</P>
			</ContentContainer>
			<ContentContainer className={componentClassName.industry}>
				{orderDetails.industry}
			</ContentContainer>
			<ContentContainer className={componentClassName.service}>
				{orderDetails.service}
			</ContentContainer>
			<form
				onSubmit={validateBeforeSubmit(handleSubmit)}
				className={componentClassName.cost}
			>
				<div className={componentClassName.costRow}>
					<ContentContainer maxWidth='70%'>
						{t('orderDetails:assignmentFee')}
					</ContentContainer>
					<ContentContainer maxWidth='30%'>
						<P isBold={true}>{orderDetails.fee}</P>
					</ContentContainer>
				</div>
				<div className={componentClassName.costRow}>
					<ContentContainer maxWidth='70%'>
						{t('orderDetails:workingTime')}
					</ContentContainer>
					<ContentContainer maxWidth='30%'>
						<P isBold={true}>
							{orderDetails.workingTime}
						</P>
					</ContentContainer>
				</div>
				<div className={componentClassName.costRow}>
					<ContentContainer maxWidth='70%'>
						{t('orderDetails:materialCosts')}
					</ContentContainer>
					<ContentContainer maxWidth={'30%'}>
						<P align='center' isBold={true}>
							{t('orderDetails:effortDependent')}
						</P>
					</ContentContainer>
				</div>
				<Input
					name='couponCode'
					placeholder={t('input:promoCode')}
					icon='plus'
					size='full'
					control={control}
					labelClassName={componentClassName.promoCode}
				/>
				<label className={componentClassName.agreement}>
					<input
						type='checkbox'
						{...register('agreement', { required: true })}
					/>
					<AgreementText/>
				</label>
				<div className={componentClassName.buttons}>
					<Button
						theme='red'
						rounded='xxl'
						onClick={handleCancel}
					>
						{t('button:cancelOrder')}
					</Button>
					<Button
						type='submit'
						theme='green'
						rounded='xxl'
					>
						{t('button:bindingOrder')}
					</Button>
				</div>
			</form>
		</Container>
	);
};
