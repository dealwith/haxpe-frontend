import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { ROUTES } from 'constants/index';

import { Link } from 'components';

export const PrivacyPolicyLink: FunctionComponent = () => {
	const { t } = useTranslation('links');
	const text = t('privacyPolicy').toLowerCase();

	return <Link href={ROUTES.PRIVACY_POLICY} isInner={true}>{text}</Link>;
};
