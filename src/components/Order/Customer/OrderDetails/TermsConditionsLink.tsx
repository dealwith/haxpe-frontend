import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { ROUTES } from 'constants/index';

import { Link } from 'components/Link';

export const TermsConditionsLink: FunctionComponent = () => {
	const { t } = useTranslation('links');

	const text = t('termsConditions').toLowerCase();

	return <Link href={ROUTES.TERMS_AND_CONDITIONS}>{text}</Link>;
};
