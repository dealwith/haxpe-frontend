import {
	EOrderStatus,
	TCustomerOrderSteps,
	TOrderStatuses,
} from 'interfaces';

export const calculateThemeProp = (
	title: TOrderStatuses | TCustomerOrderSteps,
) => {
	switch (title) {
		case EOrderStatus.paused:
			return 'yellow';

		case EOrderStatus.inProgress:
			return 'green';

		case EOrderStatus.reserved:
			return 'blue';

		default:
			return 'green';
	}
};
