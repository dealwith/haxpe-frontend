import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { StepComponentProps } from 'react-step-builder';

import { useServicesIndustries } from 'hooks';
import { Container, P, Span } from 'components';
import { calculateThemeProp } from './utils';
import { TCustomerOrderSteps, TOrderStatuses } from 'interfaces';

type TProps = Pick<StepComponentProps, 'getState' | 'title'>;

export const CurrentOrderShortInfo: FunctionComponent<TProps> = ({
	getState,
	title,
}) => {
	const { getTranslatedServiceById } = useServicesIndustries();
	const { t } = useTranslation(['order']);

	const { orderId, serviceTypeId } = getState('order', '');
	const serviceName = getTranslatedServiceById(serviceTypeId);

	const containerThemeProp = calculateThemeProp(title as TOrderStatuses | TCustomerOrderSteps);
	const componentClassName = 'current-order-short-info';

	return (
		<Container
			theme={containerThemeProp}
			height={45}
			centered='vertical'
			className={componentClassName}
		>
			<P color='white'>
				<Span isBold={true}>{t('order:order')}-ID:</Span>&nbsp;
				{orderId} - {serviceName}
			</P>
		</Container>
	);
};
