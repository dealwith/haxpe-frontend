import { FunctionComponent, useCallback, useState } from 'react';
import { StepComponentProps } from 'react-step-builder';
import { useTranslation } from 'react-i18next';
import { useForm } from 'react-hook-form';
import { StarsRating, Config } from 'stars-rating-react-hooks';

import { OrderService } from 'services';
import { EResponseStatus, TCustomerOrderSteps, TOrderStatuses } from 'interfaces';
import { jumpToCorrectStep } from '../utils';

import { Button, Container, P, Textarea } from 'components';
import starOutlineSrc from './icons/star-outline.svg';
import starFullSrc from './icons/star-full.svg';

import './order-customer-completed.scss';

interface IForm {
	comment?: string;
}

export const OrderCustomerCompleted: FunctionComponent<StepComponentProps> = ({
	getState,
	allSteps,
	jump,
}) => {
	const { t } = useTranslation(['orderCustomer', 'button', 'input']);
	const { orderId } = getState('order', '');
	const {
		register,
		handleSubmit: validateBeforeSubmit,
		watch,
	} = useForm<IForm>();
	const [rating, setRating] = useState(null);

	const handleSubmit = async ({ comment }: IForm) => {
		const { status } = await OrderService.rate(orderId, { comment, rating });

		if (status === EResponseStatus.success) {
			return jumpToCorrectStep<TCustomerOrderSteps | TOrderStatuses>(
				'thanksForReview',
				jump,
				allSteps,
			);
		}
	};

	const handleRatingClick = useCallback((val: number) => setRating(val), []);

	const baseClassName = 'order-customer-completed';
	const componentClassName = {
		component: baseClassName,
		text: `${baseClassName}__text`,
		form: `${baseClassName}__form`,
		textArea: `${baseClassName}__textarea`,
		button: `${baseClassName}__button`,
	};

	const commentLength = watch('comment')?.length;
	const ratingConfig: Config = {
		totalStars: 5,
		renderFull: (
			<img src={starFullSrc} alt='full star' />
		),
		renderEmpty: (
			<img src={starOutlineSrc} alt='empty star' />
		),
	};

	return (
		<Container
			height={560}
			flow='vertical'
			centered='vertical'
			theme='white'
			maxWidth={380}
		>
			<form
				onSubmit={validateBeforeSubmit(handleSubmit)}
				className={componentClassName.form}
			>
				<StarsRating
					config={ratingConfig}
					onStarsRated={handleRatingClick}
				/>
				<div className={componentClassName.text}>
					<P isBold={true} align='center'>
						{t('orderCustomer:howSatisfiedWithPerformance')}
					</P>
					<br />
					<P isBold={true} align='center'>
						{t('orderCustomer:yourRatingHelpUs')}
					</P>
				</div>
				<Textarea
					size='full'
					theme='white'
					maxSymbolLength={300}
					placeholder={t('input:addComment')}
					valueLength={commentLength}
					containerClassName={componentClassName.textArea}
					{...register('comment')}
				/>
				<Button
					type='submit'
					theme='green'
					rounded='xxl'
					className={componentClassName.button}
				>
					{t('button:submitRating')}
				</Button>
			</form>
		</Container>
	);
};
