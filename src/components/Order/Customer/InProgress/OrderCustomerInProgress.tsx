import { FunctionComponent, useEffect } from 'react';
import { StepComponentProps } from 'react-step-builder';

import { useSocket } from 'hooks';
import { OrderCustomerProcessLayout } from 'components';

export const OrderCustomerInProgress: FunctionComponent<StepComponentProps> = ({
	title,
	getState,
	jump,
	allSteps,
}) => {
	const socket = useSocket();

	useEffect(() => {
		const isOrderStatusChanged = socket?.orderChanged
			&& socket.orderChanged?.orderStatus !== title;

		if (isOrderStatusChanged) {
			const { order } = allSteps
				.find(({title}) => title === socket?.orderChanged?.orderStatus);

			jump(order);
		}
	}, [
		allSteps,
		jump,
		socket.orderChanged,
		socket.orderChanged?.orderStatus,
		title,
	]);

	return <OrderCustomerProcessLayout title={title} getState={getState} />;
};
