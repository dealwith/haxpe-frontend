import { FunctionComponent } from 'react';

import { EOrderStatus, TOrderStatuses } from 'interfaces';

import { RenderIcon, TIcon, Image } from 'components';
import calculationImage from './images/calculation.png';

type TProps = {
	id: number;
	title?: TOrderStatuses;
}

const bigIcons: TIcon[] = [null, 'key', 'pest-control', 'pipe', 'wrench'];

export const OrderCustomerProcessLayoutImage: FunctionComponent<TProps> = ({
	id,
	title,
}) => {
	if (title === EOrderStatus.workCompleted)
		return <Image src={calculationImage} />;

	return (
		<RenderIcon size='70' icon={bigIcons[id]} />
	);
};
