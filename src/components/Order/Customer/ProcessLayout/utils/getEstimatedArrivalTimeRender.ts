import { EOrderStatus, TOrderStatuses } from 'interfaces';

const allowedStatuses: TOrderStatuses[] = [EOrderStatus.paused];

export const getEstimatedArrivalTimeRender = (title: TOrderStatuses): boolean => {
	return allowedStatuses.includes(title);
};
