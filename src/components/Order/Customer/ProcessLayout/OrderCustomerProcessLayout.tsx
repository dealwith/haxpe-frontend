import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { StepComponentProps } from 'react-step-builder';

import { TOrderStatuses } from 'interfaces';
import { getEstimatedArrivalTimeRender } from './utils/getEstimatedArrivalTimeRender';
import { humanizeEsimatedArrival } from '../utils';

import {
	Container,
	CurrentOrderShortInfo,
	DotFlashingLoader,
	P,
	ContentContainer,
	Link,
} from 'components';
import { OrderCustomerProcessLayoutImage } from './OrderCustomerProcessLayoutImage';
import { ReactComponent as WrenchIcon } from '../icons/wrenchIcon.svg';
import { ReactComponent as PhoneIcon } from '../icons/phoneIcon.svg';

import './order-process-layout.scss';

type TProps = Pick<StepComponentProps, 'title' | 'getState'>;

export const OrderCustomerProcessLayout: FunctionComponent<TProps> = ({
	title,
	getState,
}) => {
	const { t } = useTranslation(['orderCustomer']);
	const {
		expectedArrival,
		industryId,
		workerPhone,
		workerFullName,
	} = getState('order', '');

	const humanizedEsimatedArrival = humanizeEsimatedArrival(expectedArrival);
	const isEstimatedArrivalTimeRender = getEstimatedArrivalTimeRender(title as TOrderStatuses);

	const baseClassName = 'order-process-layout';
	const componentClassName = {
		component: baseClassName,
		phoneIcon: `${baseClassName}__phone-icon`,
		wrenchIcon: `${baseClassName}__wrench-icon`,
		contentContainer: `${baseClassName}__content-container`,
		contentWrapper: `${baseClassName}__content-wrapper`,
		loaderContainer: `${baseClassName}__loader-container`,
		loader: `${baseClassName}__loader`,
		title: `${baseClassName}__title`,
	};

	return (
		<Container
			height={560}
			flow='vertical'
			centered='vertical'
			theme='white'
			maxWidth={380}
		>
			<CurrentOrderShortInfo title={title} getState={getState} />
			<Container
				className={componentClassName.loaderContainer}
				centered='vertical'
				flow='vertical'
				isInner={true}
			>
				<OrderCustomerProcessLayoutImage title={title as TOrderStatuses} id={industryId} />
				<DotFlashingLoader className={componentClassName.loader} />
				<P
					isBold={true}
					color='dark-gray'
					align='center'
					className={componentClassName.title}
				>
					{t(`orderCustomer:${title}`)}
				</P>
			</Container>
			<div className={componentClassName.contentWrapper}>
				<ContentContainer
					className={componentClassName.contentContainer}
					centered='vertical'
				>
					<WrenchIcon className={componentClassName.wrenchIcon} />
					<P
						isBold={true}
						color='dark-gray'
					>
						{t('orderCustomer:yourCraftsman')}: {workerFullName}
					</P>
				</ContentContainer>
				<ContentContainer
					className={componentClassName.contentContainer}
					centered='vertical'
				>
					<PhoneIcon className={componentClassName.phoneIcon} />
					<Link
						href={workerPhone}
						hrefType='tel'
						isBold={true}
					>
						{workerPhone}
					</Link>
				</ContentContainer>
				{isEstimatedArrivalTimeRender
					&& <ContentContainer
						className={componentClassName.contentContainer}
						centered='vertical'
					>
						<P
							isBold={true}
							color='dark-gray'
						>
							{t('orderCustomer:estimatedArrivalTime')}: {humanizedEsimatedArrival}
						</P>
					</ContentContainer>
				}
			</div>
		</Container>
	);
};
