import { FunctionComponent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { StepComponentProps } from 'react-step-builder';

import { ECustomerOrderSteps, EOrderStatus } from 'interfaces';
import { jumpToCorrectStep } from '../utils';

import { useSocket } from 'hooks';
import { P, Container } from 'components';
import { ReactComponent as GreenCheckIcon } from 'components/Icons/GreenCheckIcon.svg';

import './worker-found.scss';

export const WorkerFound: FunctionComponent<StepComponentProps> = ({
	title,
	jump,
	allSteps,
}) => {
	const { t } = useTranslation(['orderCustomer']);

	const { orderChanged } = useSocket();

	useEffect(() => {
		const orderStatus = orderChanged?.orderStatus;

		if (orderStatus && orderStatus === EOrderStatus.workerOnWay) {
			jumpToCorrectStep(
				ECustomerOrderSteps.thanksForOrderWorkerInOnHisWayToYou,
				jump,
				allSteps,
			);
		}
	}, [allSteps, jump, orderChanged?.orderStatus, title]);

	const baseClassName = 'worker-found';
	const componentClassName = {
		component: baseClassName,
		text: `${baseClassName}__text`,
	};

	return (
		<Container
			className={componentClassName.component}
			height={560}
			maxWidth={380}
			flow='vertical'
			isInner={true}
			centered='center'
			theme='white'
		>
			<GreenCheckIcon />
			<P
				align='center'
				isBold={true}
				color='dark-gray'
				className={componentClassName.text}
			>
				{t(`orderCustomer:${title}`)}
			</P>
		</Container>
	);
};
