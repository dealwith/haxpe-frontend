import { AllSteps, JumpFn } from 'react-step-builder';

/**
 * Jump to the correct step with the usage of step name
 * @param orderStatus
 * @param jump
 * @param allSteps
 * @returns
 */

export const jumpToCorrectStep = <T> (
	orderStatus: T,
	jump: JumpFn,
	allSteps: AllSteps,
): void => {
	const step = allSteps
		.find(({ title }) => title === orderStatus as unknown as string);

	if (step?.order)
		jump(step?.order);
};
