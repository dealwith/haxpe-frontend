import { FunctionComponent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { StepComponentProps } from 'react-step-builder';

import {
	P,
	Container,
	Image,
	ContentContainer,
	Link,
	DotFlashingLoader,
	CurrentOrderShortInfo,
} from 'components';
import haxpeAutoSrc from '../images/HaxpeAuto.png';
import { ReactComponent as WrenchIcon } from '../icons/wrenchIcon.svg';
import { ReactComponent as PhoneIcon } from '../icons/phoneIcon.svg';

import { useSocket } from 'hooks';
import { humanizeEsimatedArrival, jumpToCorrectStep } from '../utils';

import './worker-on-way.scss';

export const WorkerOnWay: FunctionComponent<StepComponentProps> = ({
	title,
	getState,
	jump,
	allSteps,
}) => {
	const order = getState('order', '');
	const { t } = useTranslation(['orderCustomer']);

	const { orderChanged } = useSocket();

	useEffect(() => {
		const orderStatus = orderChanged?.orderStatus;

		if (orderStatus && orderStatus !== title)
			jumpToCorrectStep(orderStatus, jump, allSteps);
	}, [allSteps, jump, orderChanged?.orderStatus, title]);

	const expectedArrival = order?.expectedArrival || orderChanged?.expectedArrival;
	const humanizedArrivalTime = humanizeEsimatedArrival(expectedArrival);

	const baseClassName = 'your-craftsman-is-on-the-way';
	const componentClassName = {
		component: baseClassName,
		phoneIcon: `${baseClassName}__phone-icon`,
		wrenchIcon: `${baseClassName}__wrench-icon`,
		containerWorker: `${baseClassName}__container-worker`,
		containerPhone: `${baseClassName}__container-phone`,
		contentWrapper: `${baseClassName}__content-wrapper`,
		loaderContainer: `${baseClassName}__loader-container`,
		loader: `${baseClassName}__loader`,
		title: `${baseClassName}__title`,
	};

	return (
		<Container
			height={560}
			flow='vertical'
			centered='vertical'
			maxWidth={380}
			theme='white'
		>
			<CurrentOrderShortInfo
				title={title}
				getState={getState}
			/>
			<Container
				className={componentClassName.loaderContainer}
				centered='vertical'
				flow='vertical'
				isInner={true}
			>
				<Image src={haxpeAutoSrc} />
				<DotFlashingLoader className={componentClassName.loader} />
				<P
					isBold={true}
					color='dark-gray'
					align='center'
					className={componentClassName.title}
				>
					{t(`orderCustomer:${title}`)}
				</P>
			</Container>
			<div className={componentClassName.contentWrapper}>
				<ContentContainer
					className={componentClassName.containerWorker}
					centered='vertical'
				>
					<WrenchIcon className={componentClassName.wrenchIcon} />
					<P
						isBold={true}
						color='dark-gray'
					>
						{t('orderCustomer:yourCraftsman')}: {order.workerFullName}
					</P>
				</ContentContainer>
				<ContentContainer
					className={componentClassName.containerPhone}
					centered='vertical'
				>
					<PhoneIcon className={componentClassName.phoneIcon} />
					<Link href={order.workerPhone} hrefType='tel' isBold={true}>
						{order.workerPhone}
					</Link>
				</ContentContainer>
				{expectedArrival
					&& <ContentContainer
						className={componentClassName.containerPhone}
						centered='vertical'
					>
						<P
							isBold={true}
							color='dark-gray'
						>
							{t('orderCustomer:estimatedArrivalTime')}: {humanizedArrivalTime}
						</P>
					</ContentContainer>
				}
			</div>
		</Container>
	);
};
