import { FunctionComponent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { StepComponentProps } from 'react-step-builder';

import { TCustomerOrderSteps, TOrderStatuses } from 'interfaces';
import { jumpToCorrectStep } from '../utils';

import { Container, P } from 'components';
import { ReactComponent as GreenCheckIcon } from 'components/Icons/GreenCheckIcon.svg';

import './order-customer-thanks-for-review.scss';

export const OrderCustomerThanksForReview: FunctionComponent<StepComponentProps> = ({
	allSteps,
	jump,
}) => {
	const { t } = useTranslation('orderCustomer');

	const baseClassName = 'order-customer-thanks-for-review';
	const componentClassName = {
		component: baseClassName,
		text: `${baseClassName}__text`,
	};

	useEffect(() => {
		setTimeout(() => {
			jumpToCorrectStep<TOrderStatuses | TCustomerOrderSteps>('orderCreation', jump, allSteps);
		}, 3000);
	}, []);

	return (
		<Container
			theme='white'
			height={560}
			maxWidth={380}
			flow='vertical'
			centered='center'
		>
			<GreenCheckIcon />
			<P className={componentClassName.text}>
				{t('thanksForReview')}
			</P>
		</Container>
	);
};
