import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { Container, P } from 'components';
import { ReactComponent as NoActiveOrderIcon } from '../icons/noActiveOrderIcon.svg';

import './worker-no-active-order.scss';

export const WorkerNoActiveOrder: FunctionComponent = () => {
	const { t } = useTranslation('order');

	const baseClassName = 'worker-no-active-order';
	const componentClassName = {
		component: baseClassName,
		text: `${baseClassName}__text`,
	};

	return (
		<Container
			flow='vertical'
			centered='center'
			isInner={true}
		>
			<NoActiveOrderIcon />
			<Container
				flow='vertical'
				centered='center'
				isInner={true}
				maxWidth={260}
				className={componentClassName.text}
			>
				<P isBold={true}>
					{t('currentlyNoActiveOrders')}
				</P>
				<br />
				<P isBold={true}>
					{t('checkWaitingList')}
				</P>
			</Container>
		</Container>
	);
};
