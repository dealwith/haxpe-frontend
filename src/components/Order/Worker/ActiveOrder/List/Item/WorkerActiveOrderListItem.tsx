import { CSSProperties, FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';

import { IWorkerActiveOrder } from 'interfaces';
import { ROUTES } from 'constants/index';
import { getColorFromOrderStatus } from '../../utils';

import { useServicesIndustries, useSelectedWorkerOrder } from 'hooks';
import { P, ShortTextPopup, Span } from 'components';

import './worker-active-order-list-item.scss';

export const WorkerActiveOrderListItem: FunctionComponent<IWorkerActiveOrder> = ({
	orderStatus,
	id,
	serviceTypeId,
}) => {
	const { getTranslatedServiceById } = useServicesIndustries();
	const { changeSelectedOrder } = useSelectedWorkerOrder();
	const { t } = useTranslation(['order']);
	const { push } = useHistory();

	const handleSetCurrentOrder = async () => {
		await changeSelectedOrder(id);
		push(ROUTES.ORDER.CURRENT);
	};

	const backgroundColor = getColorFromOrderStatus(orderStatus);
	const serviceName = getTranslatedServiceById(serviceTypeId);

	const style: CSSProperties = {
		backgroundColor,
	};
	const baseClassName = 'worker-active-order-list-item';

	return (
		<div
			onClick={handleSetCurrentOrder}
			className={baseClassName}
			style={style}
		>
			<P color='white'>
				<Span isBold={true}>
					{t('order:order')}-Id:&nbsp;
					<ShortTextPopup>{id}</ShortTextPopup>
				</Span>
			</P>
			<P isBold={true} color='white'>
			&nbsp;-&nbsp;{serviceName}
			</P>
		</div>
	);
};
