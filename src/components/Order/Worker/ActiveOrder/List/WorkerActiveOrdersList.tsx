/* eslint-disable padding-line-between-statements */
import { FunctionComponent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import {
	EOrderStatus,
	EResponseStatus,
	IWorkerActiveOrder,
	TActiveOrderSteps,
} from 'interfaces';
import { OrderService, WorkerService } from 'services';
import { THandleChangeStep } from 'hooks/interfaces';
import { TAvailableStatus } from '../interfaces';

import { useLatestActiveOrder, useSelectedWorkerOrder } from 'hooks';
import {
	P,
	WorkerActiveOrderCircle,
	Container,
	Button,
	ActiveOrderStepsInnerContainer,
} from 'components';
import { WorkerActiveOrderListItem } from './Item';
import { WorkerNoActiveOrder } from './NoActiveOrder';

import './worker-active-orders-list.scss';

const circleArr = [
	EOrderStatus.inProgress,
	EOrderStatus.paused,
	EOrderStatus.reserved,
];

type TProps = {
	title: string;
	handleChangeStep: THandleChangeStep<TActiveOrderSteps>;
}

export const WorkerActiveOrdersList: FunctionComponent<TProps> = ({
	title,
	handleChangeStep,
}) => {
	const [latestActiveOrder, checkLatestActiveOrder] = useLatestActiveOrder();
	const [activeOrders, setActiveOrders] = useState<IWorkerActiveOrder[]>([]);
	const { clearSelectedOrder } = useSelectedWorkerOrder();
	const { t } = useTranslation(['order', 'button']);

	useEffect(() => {
		clearSelectedOrder();
	}, []);

	useEffect(() => {
		const fetchAndSetWorkerActiveOrders = async () => {
			try {
				const {
					status: workerStatus,
					body: workerRes,
				} = await WorkerService.workerInfo();

				if (workerStatus === EResponseStatus.success) {
					const { body } = await OrderService.getOrder({
						isActive: true,
						WorkerId: workerRes.id,
					});

					const orders = body.filter(order => {
						switch (order.orderStatus) {
							case EOrderStatus.inProgress:
							case EOrderStatus.paused:
							case EOrderStatus.workerFounded:
							case EOrderStatus.reserved:
								return true;
							default:
								return false;
						}
					});

					const transformedOrders = orders.map(
						({ id, serviceTypeId, orderStatus }) => {
							return {
								id,
								serviceTypeId,
								orderStatus,
							};
						},
					);

					setActiveOrders(transformedOrders as IWorkerActiveOrder[]);
				}
			} catch (err) {
				console.error(err);
			}
		};

		fetchAndSetWorkerActiveOrders();
	}, []);

	useEffect(() => {
		if (latestActiveOrder)
			handleChangeStep('incomingRequest');
	}, [latestActiveOrder, handleChangeStep]);

	const handleReload = () => checkLatestActiveOrder();

	const baseClassName = 'worker-active-orders-list';
	const componentClassName = {
		component: baseClassName,
		container: `${baseClassName}-container`,
		statusInfo: `${baseClassName}__status-info`,
		statusCircle: `${baseClassName}__status-circle`,
		statusInfoContainer: `${baseClassName}__status-info-container`,
		reloadIcon: `${baseClassName}__reload-icon`,
		reloadButton: `${baseClassName}__reload-button`,
		title: `${baseClassName}__title`,
		header: `${baseClassName}__header`,
	};

	const renderedCircle = circleArr.map((status, i) => (
		<div className={componentClassName.statusInfo} key={i}>
			<WorkerActiveOrderCircle
				className={componentClassName.statusCircle}
				orderStatus={status as TAvailableStatus}
			/>
			{t(`order:${status}`)}
		</div>
	));

	const renderedWorkerActiveOrderListItem = activeOrders?.map(order => (
		<WorkerActiveOrderListItem key={order.id} {...order} />
	));

	return (
		<ActiveOrderStepsInnerContainer>
			<Container
				flow='vertical'
				className={componentClassName.container}
				isInner={true}
			>
				<div className={componentClassName.header}>
					<Button
						onClick={handleReload}
						rounded='l'
						theme='green'
						size='auto'
						className={componentClassName.reloadButton}
					>
						{t('button:getOffer')}
					</Button>
					<P className={componentClassName.title} align='center' isBold={true}>
						{t(`order:${title}`)}
					</P>
				</div>
				<div className={componentClassName.component}>
					{activeOrders?.length
						? renderedWorkerActiveOrderListItem
						: <WorkerNoActiveOrder />
					}
				</div>
				<div className={componentClassName.statusInfoContainer}>
					{renderedCircle}
				</div>
			</Container>
		</ActiveOrderStepsInnerContainer>
	);
};
