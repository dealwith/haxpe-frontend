import { CSSProperties, FunctionComponent } from 'react';
import cn from 'classnames';

import { TAvailableStatus } from '../interfaces';
import { getColorFromOrderStatus } from '../utils';

import './worker-active-order-circle.scss';

type TProps = {
	orderStatus: TAvailableStatus;
	className?: string;
}

export const WorkerActiveOrderCircle: FunctionComponent<TProps> = ({
	orderStatus,
	className: propsClassName,
}) => {
	const style: CSSProperties = {
		backgroundColor: getColorFromOrderStatus(orderStatus),
	};

	const baseClassName = cn('worker-active-order-circle', propsClassName);

	return (
		<div className={baseClassName} style={style}></div>
	);
};
