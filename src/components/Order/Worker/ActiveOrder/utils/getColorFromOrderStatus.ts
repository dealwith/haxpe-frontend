/* eslint-disable padding-line-between-statements */
import { EOrderStatus } from 'interfaces';
import { COLORS } from 'constants/index';

import { TAvailableStatus } from '../interfaces';

export const getColorFromOrderStatus = (option: TAvailableStatus): string => {
	switch (option) {
		case EOrderStatus.inProgress:
			return COLORS.GREEN;

		case EOrderStatus.paused:
			return COLORS.YELLOW;

		case EOrderStatus.reserved:
			return COLORS.BLUE;

		default:
			return COLORS.GREEN;
	}
};
