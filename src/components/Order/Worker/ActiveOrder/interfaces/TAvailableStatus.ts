import { EOrderStatus } from 'interfaces';

export type TAvailableStatus = keyof Pick<typeof EOrderStatus,
	EOrderStatus.inProgress
	| EOrderStatus.paused
	| EOrderStatus.reserved
	| EOrderStatus.workerFounded
>;
