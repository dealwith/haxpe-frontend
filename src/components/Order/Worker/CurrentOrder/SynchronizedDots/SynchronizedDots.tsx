import { FunctionComponent } from 'react';

import { EOrderStatus, TOrderStatuses } from 'interfaces';

import { DotFlashingLoader } from 'components';

type TProps = {
	orderStatus: TOrderStatuses;
}

export const SynchronizedDots: FunctionComponent<TProps> = ({ orderStatus }) => {
	return orderStatus === EOrderStatus.inProgress
		&& <DotFlashingLoader/>;
};
