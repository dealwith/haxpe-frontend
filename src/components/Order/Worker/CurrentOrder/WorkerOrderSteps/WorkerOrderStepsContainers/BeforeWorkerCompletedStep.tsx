import { FunctionComponent, ReactElement } from 'react';

import {
	ContentContainer,
	BackToPreviousPageButton,
	ButtonsContainer,
} from 'components';
import { BeforeWorkCompleted } from '../../CurrentOrderParents';

import './before-worker-completed-step.scss';

type TProps = {
	buttons: ReactElement[] | ReactElement;
	body: ReactElement;
	isBackButton?: boolean;
}

export const BeforeWorkerCompletedStep: FunctionComponent<TProps> = ({
	buttons,
	body,
	isBackButton = false,
}) => {
	const bodyClassName = 'before-worker-completed-step-body';

	return (
		<BeforeWorkCompleted>
			<ContentContainer
				className={bodyClassName}
				centered='center-column'
				minHeight={187}
			>
				{body}
			</ContentContainer>
			<ButtonsContainer>
				{buttons}
			</ButtonsContainer>
			{isBackButton
				&& <BackToPreviousPageButton
					size='auto'
					position='center'
				/>
			}
		</BeforeWorkCompleted>
	);
};
