import { FunctionComponent, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { EOrderStatus } from 'interfaces';
import { IStepProps } from '../interfaces';

import {
	CompleteOrderButton,
	PauseOrderButton,
	RenderIcon,
	Span,
	ResumeOrderButton,
} from 'components';
import { BeforeWorkerCompletedStep } from './WorkerOrderStepsContainers';
import { SynchronizedDots } from '../SynchronizedDots';

export const InProgress: FunctionComponent<IStepProps> = ({
	buttonProps,
	handleChangeStep,
	orderId,
	orderStatus,
}) => {
	const { t } = useTranslation('order');

	const currentButton = useMemo(() => {
		if (orderStatus === EOrderStatus.paused) {
			return <ResumeOrderButton
				key='resumeOrder'
				orderId={orderId}
				buttonProps={buttonProps}
			/>;
		}

		if (orderStatus === EOrderStatus.inProgress) {
			return <PauseOrderButton
				key='pauseOrder'
				orderId={orderId}
				buttonProps={buttonProps}
			/>;
		}
	}, [orderStatus]);

	return (
		<BeforeWorkerCompletedStep
			buttons={[
				currentButton,
				<CompleteOrderButton
					isDisabled={orderStatus === EOrderStatus.paused}
					key='completeOrder'
					orderId={orderId}
					buttonProps={buttonProps}
					handleChangeStep={handleChangeStep}
				/>,
			]}
			body={
				<>
					<RenderIcon
						size='70'
						icon='wrench-and-screwdriver'
					/>
					<div>
						<Span>{`${t('order:orderIsBeingExecuted')}...`}</Span>
					</div>
					<div>
						<SynchronizedDots orderStatus={orderStatus}/>
					</div>
				</>
			}
		/>
	);
};
