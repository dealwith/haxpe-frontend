import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { IStepProps } from '../interfaces';

import {
	CurrentOrderCancelAndReportProblemButton,
	RenderIcon,
	Span,
	StartOrderButton,
} from 'components';
import { BeforeWorkerCompletedStep } from './WorkerOrderStepsContainers';
//TODO: use RenderIcon(this and another steps) or import right there?
export const WorkerArrived: FunctionComponent<IStepProps> = ({
	buttonProps,
	handleChangeStep,
	orderId,
}) => {
	const { t } = useTranslation('order');

	return (
		<BeforeWorkerCompletedStep
			buttons={[
				<CurrentOrderCancelAndReportProblemButton
					key='currentOrderCancelAndReportProblem'
					orderId={orderId}
					buttonProps={buttonProps}
					handleChangeStep={handleChangeStep}
				/>,
				<StartOrderButton
					key='startOrder'
					orderId={orderId}
					buttonProps={buttonProps}
					handleChangeStep={handleChangeStep}
				/>,
			]}
			body={
				<>
					<RenderIcon
						size='70'
						icon='success'
					/>
					<div>
						<Span>{`${t('order:arrivedAtCustomer')}.`}</Span>
					</div>

				</>
			}
		/>
	);
};
