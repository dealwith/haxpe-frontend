import { FunctionComponent } from 'react';

import { StepContainer } from 'components';

import './invoice.scss';

export const Invoice: FunctionComponent = ({ children }) => {

	const baseClassName = 'worker-order-invoice';

	return (
		<StepContainer className={baseClassName}>
			{children}
		</StepContainer>
	);
};
