import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { OrderService } from 'services';
import { TOrderInvoiceTableInputWidth } from 'interfaces';
import { IStepProps } from 'components/Order/Worker/CurrentOrder/interfaces';

import {
	useAfterWorkCompleteInfo,
	useSelectedWorkerOrder,
	useServicesIndustries,
} from 'hooks';
import {
	ContentContainer,
	P,
	AddServiceForm,
	AddReportForm,
	OrderInvoiceServicesTable,
	Button,
} from 'components';
import { OrderDuration } from '../OrderDuration';
import { Invoice } from '../Invoice';

import './set-invoice.scss';

export const SetInvoice: FunctionComponent<IStepProps> = ({
	handleChangeStep,
	buttonProps,
}) => {
	const { t } = useTranslation(['button', 'invoice']);
	const { selectedOrder, changeSelectedOrder } = useSelectedWorkerOrder();
	const {
		editService,
		addService,
		removeService,
		additionalServices,
		reduction,
		report,
	} = useAfterWorkCompleteInfo();
	const {
		getTranslatedIndustryById,
		getTranslatedServiceById,
	} = useServicesIndustries();

	const handleContinue = async () => {
		try {
			await OrderService.addReport(selectedOrder.id, report);
			await OrderService.addExtraServices(selectedOrder.id, additionalServices);
			await OrderService.addReduction(selectedOrder.id, reduction);
			await changeSelectedOrder(selectedOrder.id);
			handleChangeStep('sendInvoice');
		} catch (err) {
			console.error(err);
		}
	};

	const customWidthStyles: TOrderInvoiceTableInputWidth = {
		small: 42,
		large: 180,
		normal: 111,
	};

	const service = getTranslatedServiceById(selectedOrder.serviceTypeId);
	const industry = getTranslatedIndustryById(selectedOrder.industryId);
	const continueButtonClassName = 'set-invoice__continue-button';

	return (
		<Invoice>
			<ContentContainer>
				<P isBold={true}>
					{t('invoice:invoice')}-ID:&nbsp;
				</P>
				{selectedOrder?.invoice?.id}
			</ContentContainer>
			<OrderDuration/>
			<ContentContainer>
				{industry}
			</ContentContainer>
			<ContentContainer>
				{service}
			</ContentContainer>
			<OrderInvoiceServicesTable
				customWidthStyles={customWidthStyles}
				orderId={selectedOrder.id}
				servicesTableInfo={additionalServices}
				removeService={removeService}
				editService={editService}
				isEditable={true}
			/>
			<AddServiceForm
				customWidthStyles={customWidthStyles}
				setServicesForTable={addService}
			/>
			<AddReportForm/>
			<Button
				{...buttonProps}
				onClick={handleContinue}
				size='xxxl'
				className={continueButtonClassName}
			>
				{t('button:continue')}
			</Button>
		</Invoice>
	);
};
