import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { ModifiedDate } from 'utils';

import { useSelectedWorkerOrder } from 'hooks';
import { ContentContainer, Span } from 'components';

import './order-duration.scss';

export const OrderDuration: FunctionComponent = () => {
	const { selectedOrder } = useSelectedWorkerOrder();
	const { t } = useTranslation(['dateRange', 'time']);
	const { createDateToShow } = new ModifiedDate();

	if (!selectedOrder.invoice)
		return null;

	const startDate = createDateToShow({
		dateArg: selectedOrder.creationDate,
		format: 'd:m:y',
		isFull: true,
		exclude: 's',
		isFullYear: false,
	});

	const endDate = createDateToShow({
		dateArg: selectedOrder?.invoice?.creationDate ?? '0',
		format: 'd:m:y',
		isFull: true,
		exclude: 's',
		isFullYear: false,
	});

	const baseClassName = 'order-duration';

	return (
		<div className={baseClassName}>
			<ContentContainer flow='vertical'>
				<Span isBold={true}>{t('dateRange:startOfTheService')}</Span>
				{startDate}
			</ContentContainer>
			<ContentContainer flow='vertical'>
				<Span isBold={true}>{t('dateRange:endOfService')}</Span>
				{endDate}
			</ContentContainer>
		</div>
	);
};
