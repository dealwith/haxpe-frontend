import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { useDropzone } from 'react-dropzone';

import { TOrderInvoiceTableInputWidth } from 'interfaces';
import { CURRENCY, ROUTES } from 'constants/index';
import { OrderService } from 'services';
import { FILES } from 'constants/files';
import { IStepProps } from 'components/Order/Worker/CurrentOrder/interfaces';
import { formatBytes } from 'components/Input/InputDropzone/utils';

import {
	useAfterWorkCompleteInfo,
	useSelectedWorkerOrder,
	useServicesIndustries,
} from 'hooks';
import {
	Button,
	ButtonsContainer,
	ContentContainer,
	OrderInvoiceServicesTable,
	P,
	Span,
	Image,
} from 'components';
import { Invoice } from '../Invoice';
import clipIconSrc from './clip.svg';
import { OrderDuration } from '../OrderDuration';

import './send-invoices.scss';

export const SendInvoice: FunctionComponent<IStepProps> = ({
	buttonProps,
	handleChangeStep,
}) => {
	const history = useHistory();
	const { t } = useTranslation([
		'payment',
		'button',
		'invoice',
		'total',
		'input',
	]);
	const {
		additionalServices,
		report,
	} = useAfterWorkCompleteInfo();
	const { selectedOrder } = useSelectedWorkerOrder();
	const {
		getTranslatedIndustryById,
		getTranslatedServiceById,
	} = useServicesIndustries();

	const orderId = selectedOrder.id;
	const invoiceId = selectedOrder.invoice.id;

	const service = getTranslatedServiceById(selectedOrder.serviceTypeId);
	const industry = getTranslatedIndustryById(selectedOrder.industryId);

	const { EURO } = CURRENCY;
	const tax = `${EURO} ${selectedOrder.invoice.tax}`;
	const total = `${EURO} ${selectedOrder.invoice.total}`;

	const customWidthStyles: TOrderInvoiceTableInputWidth = {
		small: 42,
		large: 180,
		normal: 111,
	};

	const {
		acceptedFiles,
		getInputProps,
		getRootProps,
	} = useDropzone({ maxSize: FILES.MAX_FILE_SIZE });

	const handleCorrectDetails = () => handleChangeStep('workCompleted');

	const handleSendInvoice = async () => {
		try {
			const formData = new FormData();

			acceptedFiles.forEach(file => formData.append('uploads', file));

			await OrderService.addImages(orderId, formData);
			await OrderService.sendInvoice(orderId);

			history.push(ROUTES.ORDER.ACTIVE);
		} catch (err) {
			console.error(err);
		}
	};

	const acceptedFileItems = acceptedFiles?.map((file, i) => (
		<P align='center' size='medium'>
			{file.name} - {formatBytes(file.size)}
		</P>
	));

	const baseClassName = 'send-invoice';
	const componentClassName = {
		dropContainer: `${baseClassName}__drop-container`,
		clipIcon: `${baseClassName}__clip-icon`,
		buttonContainer: `${baseClassName}__button-container`,
	};

	return (
		<Invoice>
			<ContentContainer theme='warn'>
				{t('invoice:areAllDetailsCorrect')}
			</ContentContainer>
			<ContentContainer>
				<P isBold={true}>
					{t('invoice:invoice')}-ID:&nbsp;
				</P>
				{invoiceId}
			</ContentContainer>
			<OrderDuration/>
			<ContentContainer>
				{industry}
			</ContentContainer>
			<ContentContainer>
				{service}
			</ContentContainer>
			<OrderInvoiceServicesTable
				customWidthStyles={customWidthStyles}
				orderId={orderId}
				servicesTableInfo={additionalServices}
				isEditable={false}
			>
				<ContentContainer
					theme='white'
					centered='space-between'
				>
					<ContentContainer
						maxWidth={customWidthStyles.large + customWidthStyles.small + 6}
						flow='vertical'
						position='left'
					>
						<Span isBold={true}>
							{t('payment:total')}
						</Span>
						<Span>
							{t('payment:amountIncludes19vat')}
						</Span>
					</ContentContainer>
					<ContentContainer
						maxWidth={customWidthStyles.normal}
						flow='vertical'
						position='left'
					>
						<Span isBold={true}>{total}</Span>
						<Span>{tax}</Span>
					</ContentContainer>
				</ContentContainer>
			</OrderInvoiceServicesTable>
			{report
				&& <ContentContainer>{report}</ContentContainer>
			}
			<div className={componentClassName.dropContainer} {...getRootProps()}>
				<Image className={componentClassName.clipIcon} src={clipIconSrc} />
				<input {...getInputProps()} />
				<P align='center' size='medium' isBold={true}>
					{t('input:dropFileOrSend')}
				</P>
				{acceptedFileItems}
			</div>
			<ButtonsContainer className={componentClassName.buttonContainer}>
				<Button
					size='xxxl'
					{...buttonProps}
					theme='red'
					onClick={handleCorrectDetails}
				>
					{t('button:correctDetails')}
				</Button>
				<Button
					size='xxxl'
					onClick={handleSendInvoice}
					{...buttonProps}
				>
					{t('button:sendInvoice')}
				</Button>
			</ButtonsContainer>
		</Invoice>
	);
};
