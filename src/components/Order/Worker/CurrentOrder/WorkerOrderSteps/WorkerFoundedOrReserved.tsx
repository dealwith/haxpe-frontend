import { FunctionComponent, useEffect, useState } from 'react';

import { AddressService } from 'services';
import { IStepProps } from '../interfaces';

import { useCurrentWorkerLocation, useSelectedWorkerOrder } from 'hooks';
import { ArrivedAtCustomerButton, TimePicker } from 'components';
import { BeforeWorkerCompletedStep } from './WorkerOrderStepsContainers';
import { GOOGLE_LINKS } from 'constants/index';

const Map = () => {
	const { lat, lon } = useCurrentWorkerLocation();
	const { selectedOrder } = useSelectedWorkerOrder();
	const [src, setSrc] = useState('');

	useEffect(() => {
		try {
			const fetchData = async () => {
				const {
					body: addressRes,
				} = await AddressService.getAddressById(selectedOrder.addressId);

				setSrc(
					GOOGLE_LINKS.EMBEDED_MAP_DIRECTION_API(
						{lat, lon},
						{lat: addressRes.lat, lon: addressRes.lon},
					),
				);
			};

			if (selectedOrder)
				fetchData();

		} catch (err) {
			console.error(err);
		}
	}, [lat, lon, selectedOrder]);

	return src
		? <iframe
			title='directionMap'
			width='346'
			height='280'
			frameBorder='0'
			style={{ border: 0}}
			src={src}
			allowFullScreen
		>
		</iframe>
		: null;
};

export const WorkerFoundedOrReserved: FunctionComponent<IStepProps> = ({
	buttonProps,
	handleChangeStep,
	orderId,
}) => {
	const { selectedOrder } = useSelectedWorkerOrder();

	const buttons = [
		<TimePicker
			orderId={orderId}
			key='timePicker'
			buttonProps={buttonProps}
		/>,
		<ArrivedAtCustomerButton
			orderId={orderId}
			key='arrivedAtCustomer'
			buttonProps={buttonProps}
			handleChangeStep={handleChangeStep}
		/>,
	];

	if (selectedOrder.expectedArrival)
		buttons.shift();

	return <BeforeWorkerCompletedStep
		isBackButton={true}
		buttons={buttons}
		body={<Map />}
	/>;
};
