import { TOrderStatuses, TOrderWorkerCurrentOrderSteps } from 'interfaces';
import { THandleChangeStep } from 'hooks/interfaces';
import { TButtonProps } from 'components/Button/interfaces';

export interface IStepProps {
	buttonProps: TButtonProps;
	handleChangeStep: THandleChangeStep<TOrderWorkerCurrentOrderSteps>;
	orderId: string;
	orderStatus: TOrderStatuses;
}
