import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { isObjectWithProperties } from 'utils';

import { useBeforeWorkCompletedInfo } from 'hooks';
import {
	RenderIcon,
	P,
	ShortTextPopup,
	ContentContainer,
	CircleLoader,
	StepContainer,
} from 'components';

import './before-worker-completed.scss';

export const BeforeWorkCompleted: FunctionComponent = ({
	children,
}) => {
	const { t } = useTranslation('order');
	const orderInfo = useBeforeWorkCompletedInfo();
	const {
		orderId,
		fullName,
		address,
		phoneNumber,
		service,
		industry,
	} = orderInfo;

	const baseClassName = 'before-worker-completed';

	return (
		isObjectWithProperties(orderInfo)
			? <StepContainer className={baseClassName} minHeight={602}>
				<ContentContainer
					theme='green'
					centered='center'
				>
					<P color='white'>{`${t('order:order')}-ID: `}</P>
					<ShortTextPopup>{orderId}</ShortTextPopup>
				</ContentContainer>
				<ContentContainer
					theme='gray'
					centered='flex-start'
				>
					<RenderIcon icon='wrench'/>
					<P>{`${industry} ${service}`}</P>
				</ContentContainer>
				<ContentContainer
					theme='gray'
					centered='flex-start'
				>
					<RenderIcon icon='person'/>
					<P>{fullName}</P>
				</ContentContainer>
				<ContentContainer
					theme='gray'
					centered='flex-start'
				>
					<RenderIcon icon='target'/>
					<P>{address}</P>
				</ContentContainer>
				<ContentContainer
					theme='gray'
					centered='flex-start'
				>
					<RenderIcon icon='phone'/>
					<P>{phoneNumber}</P>
				</ContentContainer>
				{children}
			</StepContainer>
			: <CircleLoader/>
	);
};
