import { FunctionComponent, useMemo } from 'react';

import { AfterWorkCompletedProvider, BeforeWorkCompletedProvider } from 'providers';
import { TOrderWorkerCurrentOrderSteps } from 'interfaces';
import { IStepProps } from './interfaces';

import { useSelectedWorkerOrder, useStep } from 'hooks';
import { CircleLoader, OrderWorkerCancelReasonForm } from 'components';
import {
	WorkerFoundedOrReserved,
	WorkerArrived,
	InProgress,
	SetInvoice,
	SendInvoice,
} from './WorkerOrderSteps';

export const OrderWorkerCurrentOrder: FunctionComponent = () => {
	const { selectedOrder } = useSelectedWorkerOrder();

	const currentOrderSteps: TOrderWorkerCurrentOrderSteps = useMemo(() => ({
		reserved: ['reserved', 'workerFounded', 'workerOnWay', 'created'],
		arrived: 'workerArrived',
		inProgress: ['inProgress', 'paused'],
		workCompleted: 'workCompleted',
		sendInvoice: 'sendInvoice',
		orderCancelReasonForm: 'orderCancelReasonForm',
	}), []);

	const orderStatus = selectedOrder?.orderStatus;
	const orderId = selectedOrder?.id;

	const { step, handleChangeStep } = useStep(orderStatus, currentOrderSteps);

	if (!selectedOrder === undefined)
		return <CircleLoader/>;

	const isAfter = step === 'workCompleted' || step === 'sendInvoice';

	const stepProps: IStepProps = {
		handleChangeStep,
		orderId,
		orderStatus,
		buttonProps: {
			rounded: 'xxl',
			theme: 'green',
			size: 'full',
		},
	};

	const stepComponents: TOrderWorkerCurrentOrderSteps = {
		reserved: <WorkerFoundedOrReserved {...stepProps}/>,
		arrived: <WorkerArrived {...stepProps}/>,
		inProgress: <InProgress {...stepProps}/>,
		workCompleted: <SetInvoice {...stepProps}/>,
		sendInvoice: <SendInvoice {...stepProps}/>,
		orderCancelReasonForm: <OrderWorkerCancelReasonForm {...stepProps}/>,
	};

	const render = () => isAfter
		? <AfterWorkCompletedProvider>
			{stepComponents[step]}
		</AfterWorkCompletedProvider>
		: <BeforeWorkCompletedProvider>
			{stepComponents[step]}
		</BeforeWorkCompletedProvider>;

	const component = render();

	return component;

};
