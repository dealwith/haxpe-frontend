import { FunctionComponent, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { AddressService, OrderService, WorkerService } from 'services';
import { EOrderStatus, IAddress, TActiveOrderSteps } from 'interfaces';
import { THandleChangeStep } from 'hooks/interfaces';
import { ROUTES } from 'constants/index';

import {
	useLatestActiveOrder,
	useServicesIndustries,
	useSocket,
} from 'hooks';
import {
	ActiveOrderStepsInnerContainer,
	Button,
	CircleLoader,
	ContentContainer,
	P,
} from 'components';

import './order-worker-incoming-request.scss';

type TProps = {
	title: string;
	handleChangeStep: THandleChangeStep<TActiveOrderSteps>;
}

export const OrderWorkerIncomingRequest: FunctionComponent<TProps> = ({
	title,
	handleChangeStep,
}) => {
	const { orderOffer, clearOrderOffer, orderChanged } = useSocket();
	const [latestActiveOrder, checkLatestActiveOrder] = useLatestActiveOrder();
	const { t } = useTranslation(['order', 'button']);
	const [address, setAddress] = useState<IAddress>(null);
	const [distance, setDistance] = useState('');
	const history = useHistory();
	const { getTranslatedIndustryById, getTranslatedServiceById } = useServicesIndustries();

	const order = orderOffer || latestActiveOrder;

	useEffect(() => {
		if (!orderOffer)
			checkLatestActiveOrder();
	}, []);

	useEffect(() => {
		if (order?.workerDistances[0]?.distance)
			setDistance(order?.workerDistances[0]?.distance);
	}, [order?.workerDistances]);

	useEffect(() => {
		const isOrderChangedToCancel = orderChanged?.orderStatus === EOrderStatus.canceled;
		const isOrderOfferIdEqualsChangedId = orderOffer?.id === orderChanged?.id;

		if (isOrderOfferIdEqualsChangedId && isOrderChangedToCancel)
			handleReject();
	}, [orderChanged?.id, orderChanged?.orderStatus, orderOffer?.id]);

	useEffect(() => {
		try {
			const fetchAddress = async () => {
				const {
					body: addressRes,
				} = await AddressService.getAddressById(order?.addressId);

				setAddress(addressRes);
			};

			if (order?.addressId)
				fetchAddress();
		} catch (err) {
			console.error(err);
		}
	}, [order?.addressId]);

	const handleAccept = async () => {
		try {
			const { body: workerInfoRes } = await WorkerService.workerInfo();

			await OrderService.assignWorker({
				orderId: order.id,
				workerId: workerInfoRes.id,
			});
			clearOrderOffer();
			history.push(ROUTES.ORDER.CURRENT);

		} catch (err) {
			console.error(err);
		}
	};

	const handleReject = () => {
		clearOrderOffer();
		handleChangeStep('orderList');
	};

	const industry = getTranslatedIndustryById(order?.industryId);
	const service = getTranslatedServiceById(order?.serviceTypeId);

	const baseClassName = 'order-worker-incoming-request';
	const componentClassName = {
		component: baseClassName,
		block: `${baseClassName}__block`,
		actions: `${baseClassName}__actions`,
	};

	return order
		? (
			<ActiveOrderStepsInnerContainer>
				<div className={componentClassName.component}>
					<ContentContainer theme='green'>
						<P
							isBold={true}
							transform='uppercase'
							color='white'
						>
							{t(`order:${title}`)}
						</P>
					</ContentContainer>
					<ContentContainer className={componentClassName.block}>
						<P size='medium' isBold={true}>
							{industry}
						</P>
					</ContentContainer>
					<ContentContainer className={componentClassName.block}>
						<P size='medium' isBold={true}>
							{service}
						</P>
					</ContentContainer>
					<ContentContainer
						centered='vertical'
						className={componentClassName.block}
					>
						<P size='medium' isBold={true}>
							{address?.street} {address?.buildingNum}
						</P>
					</ContentContainer>
					<ContentContainer
						centered='vertical'
						className={componentClassName.block}
					>
						<P size='medium' isBold={true}>
							{address?.city} {address?.zipCode}
						</P>
					</ContentContainer>
					<ContentContainer
						centered='vertical'
						className={componentClassName.block}
					>
						<P size='medium' isBold={true}>
							{t('order:distance')}: {distance}
						</P>
					</ContentContainer>
					<div className={componentClassName.actions}>
						<Button
							theme='red'
							rounded='xxl'
							onClick={handleReject}
						>
							{t('button:decline')}
						</Button>
						<Button
							theme='green'
							rounded='xxl'
							onClick={handleAccept}
						>
							{t('button:accept')}
						</Button>
					</div>
				</div>
			</ActiveOrderStepsInnerContainer>
		)
		: <CircleLoader/>;
};
