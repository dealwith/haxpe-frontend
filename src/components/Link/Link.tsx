import { FunctionComponent, ReactNode } from 'react';
import { NavLink as RouterLink } from 'react-router-dom';
import cn from 'classnames';

import { TSize, TTarget, TTheme, THrefType } from './interfaces';

import './link.scss';

type TProps = {
	href: string;
	hrefType?: THrefType;
	children: ReactNode;
	isWhite?: boolean;
	target?: TTarget;
	className?: string;
	isInner?: boolean;
	size?: TSize;
	theme?: TTheme;
	isBold?: boolean;
	activeClassName?: string;
}

export const Link: FunctionComponent<TProps> = ({
	href: hrefProps,
	hrefType,
	children,
	isWhite,
	target,
	className: propsClassName,
	isInner,
	size,
	theme = 'transparent',
	isBold,
	activeClassName,
}) => {
	const getHref = ()  => {
		if (hrefType === 'email')
			return `mailto:${hrefProps}`;

		if (hrefType === 'tel')
			return `tel:${hrefProps}`;

		return hrefProps;
	};
	const href = getHref();

	const baseClassName = 'link';
	const componentClassName = cn(
		baseClassName,
		propsClassName,
		{
			[`${baseClassName}--white`]: isWhite,
			[`${baseClassName}--size-${size}`]: size,
			[`${baseClassName}--theme-${theme}`]: theme,
			[`${baseClassName}--bold`]: isBold,
		},
	);

	return isInner
		? <a className={componentClassName} href={href} target={target}>
			{children}
		</a>
		: <RouterLink
			className={componentClassName}
			to={href}
			activeClassName={activeClassName}
		>
			{children}
		</RouterLink>;
};
