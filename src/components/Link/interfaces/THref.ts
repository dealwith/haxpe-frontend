export enum EHrefType {
	email = 'email',
	tel = 'tel',
}

export type THrefType = keyof typeof EHrefType;
