export enum ETarget {
	_blank,
	_self,
	_parent,
	_top
}

export type TTarget = keyof typeof ETarget;
