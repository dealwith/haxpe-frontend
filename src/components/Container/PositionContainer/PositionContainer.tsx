import { FunctionComponent } from 'react';
import cn from 'classnames';

import { TPosition } from 'interfaces';

import './position-container.scss';

type TProps = {
	position: TPosition;
}

export const PositionContainer: FunctionComponent<TProps> = ({
	position,
	children,
}) => {
	const baseClassName = 'position-container';
	const componentClassName = cn(baseClassName, {
		[`${baseClassName}--position-${position}`]: position,
	});

	return (
		<div className={componentClassName}>
			{children}
		</div>
	);
};
