import { FunctionComponent } from 'react';
import cn from 'classnames';

import { TCentered } from 'interfaces';

import { useMediaQuery } from 'hooks';
import { ContentContainer } from 'components';

import './buttons-container.scss';

type TProps = {
	className?: string;
	centered?: TCentered;
}

export const ButtonsContainer: FunctionComponent<TProps> = ({
	children,
	className: propsClassName,
	centered = 'space-between',
}) => {
	const { isMatches } = useMediaQuery({ maxWidth: 400 });

	const baseClassName = cn('buttons-container', propsClassName);

	return (
		<ContentContainer
			theme='transparent'
			centered={centered}
			className={baseClassName}
			isWrap={isMatches}
		>
			{children}
		</ContentContainer>
	);
};
