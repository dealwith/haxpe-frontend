import { FunctionComponent } from 'react';

import { Container } from 'components';

import './active-order-steps-inner-container.scss';

export const ActiveOrderStepsInnerContainer: FunctionComponent = ({
	children,
}) => {
	const baseClassName = 'active-order-steps-inner-container';

	return (
		<Container centered='center'>
			<Container
				isFitContent={true}
				maxWidth={380}
				className={baseClassName}
				maxHeight={560}
			>
				{children}
			</Container>
		</Container>
	);
};
