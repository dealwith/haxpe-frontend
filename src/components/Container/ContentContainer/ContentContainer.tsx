import { FunctionComponent } from 'react';

import { IContentContainer } from 'interfaces';

import { Container } from 'components';

export const ContentContainer: FunctionComponent<IContentContainer> = ({
	children,
	theme = 'gray',
	centered = 'center',
	className: propsClassName,
	maxWidth = '100%',
	minHeight = 45,
	position,
	isWrap,
	height,
	flow,
	isInner = true,
	isDefault,
}) => {

	return (
		<Container
			isDefault={isDefault}
			isWrap={isWrap}
			position={position}
			minHeight={minHeight}
			maxWidth={maxWidth}
			isFitContent={true}
			isInner={isInner}
			theme={theme}
			centered={centered}
			className={propsClassName}
			flow={flow}
			{...(height !== 'auto' && {height})}
		>
			{children}
		</Container>
	);
};
