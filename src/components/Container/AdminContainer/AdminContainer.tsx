import { FunctionComponent } from 'react';
import './admin-container.scss';

export const AdminContainer: FunctionComponent = ({ children }) => {
	const baseClassName = 'admin-container';

	return <div className={baseClassName}>
		{children}
	</div>;
};
