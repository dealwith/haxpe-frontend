import { FunctionComponent } from 'react';
import cn from 'classnames';

import { IContainer, ECentered } from 'interfaces';

import './container.scss';

export const Container: FunctionComponent<IContainer> = ({
	children,
	centered,
	className: propsClassName,
	flow,
	isInner,
	maxWidth,
	isFitContent,
	theme,
	height,
	minHeight,
	maxHeight,
	position = 'center',
	isWrap,
	isDefault = true,
	isFillContent,
	flexDirection,
}) => {
	const baseClassName = 'container';
	const componentClassName = cn(baseClassName,
		`${baseClassName}--position-${position}`,
		{
			[`${baseClassName}--default`]: isDefault,
			[`${baseClassName}--centered ${baseClassName}--centered-${centered}`]: centered
				&& ECentered[centered],
			[`${baseClassName}--wrap`]: isWrap,
			[`${baseClassName}--theme-${theme}`]: theme,
			[`${baseClassName}--fit-content`]: isFitContent,
			[`${baseClassName}--fill-content`]: isFillContent,
			[`${baseClassName}--flow ${baseClassName}--flow-${flow}`]: flow,
			[`${baseClassName}--flex-direction-${flexDirection}`]: flexDirection,
		},
		propsClassName,
	);

	const attrs = {
		style: {
			...(maxWidth && { maxWidth }),
			...(height && { height: `${height}px` }),
			...(minHeight && { minHeight: `${minHeight}px` }),
			...(!isInner && { padding: '30px 17px 20px' }),
			...(maxHeight && { maxHeight, height: '100%'}),
		},
	};

	return (
		<div
			className={componentClassName}
			{...attrs}
		>
			{children}
		</div>
	);
};
