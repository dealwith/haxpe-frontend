import { FunctionComponent } from 'react';
import cn from 'classnames';

import './flexible-container.scss';

type TProps = {
	className?: string;
}

export const FlexibleContainer: FunctionComponent<TProps> = ({
	children,
	className: propsClassName,
}) => {
	const baseClassName = 'simple-flexible-container';
	const componentClassName = cn(baseClassName, propsClassName);

	return (
		<div className={componentClassName}>
			{children}
		</div>
	);
};
