import { FunctionComponent } from 'react';
import cn from 'classnames';

import { Container } from '../Container';

type TProps = {
	className?: string;
	minHeight?: number;
}

export const StepContainer: FunctionComponent<TProps> = ({
	children,
	className: propsClassName,
	minHeight = 604,
}) => {
	const baseClassName = cn('step-container', propsClassName);

	return (
		<Container
			className={baseClassName}
			isFitContent={true}
			minHeight={minHeight}
			maxWidth={380}
			theme='white'
		>
			{children}
		</Container>
	);
};
