import { FunctionComponent } from 'react';

import { ContentContainer } from 'components';

import './pdf-table-title-container.scss';

export const PDFTableTitleContainer: FunctionComponent = ({ children }) => {
	const baseClassName = 'pdf-table-title-container';

	return (
		<ContentContainer
			centered='space-between'
			className={baseClassName}
			theme='white'
		>
			{children}
		</ContentContainer>
	);
};
