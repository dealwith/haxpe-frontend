import { FunctionComponent } from 'react';

import { ContentContainer } from 'components';

import './pdf-table-container.scss';

export const PDFTableContainer: FunctionComponent = ({ children }) => {
	const baseClassName = 'pdf-table-container';

	return (
		<ContentContainer
			className={baseClassName}
			centered='space-between'
			theme='white'
			flow='vertical'
		>
			{children}
		</ContentContainer>
	);
};
