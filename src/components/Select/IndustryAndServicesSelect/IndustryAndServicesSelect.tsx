import { FunctionComponent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import { TExtendedIndustryOption, TExtendedServiceOption } from 'interfaces';

import { useServicesIndustries } from 'hooks';
import { Select } from '../Select';

type TSelectedArgs = {
	selectedIndustry: TExtendedIndustryOption;
	selectedService: TExtendedServiceOption;
}

type TProps = {
	getSelectedIndustriesAndServices: (data: TSelectedArgs) => void;
	className?: string;
}

export const IndustryAndServicesSelect: FunctionComponent<TProps> = ({
	getSelectedIndustriesAndServices,
	className: propsClassName,
}) => {
	const { t } = useTranslation(['services', 'industries']);
	const {
		industriesForSelect,
		servicesForSelect,
		getSelectedItems,
		handleIndustryChange,
		handleServiceChange,
	} = useServicesIndustries();

	const {selectedIndustry, selectedService} = getSelectedItems();

	const servicesItems = servicesForSelect.filter(({ industryId }) => {
		return industryId === selectedIndustry?.value;
	});

	useEffect(() => {
		if (selectedService) {
			getSelectedIndustriesAndServices({
				selectedIndustry,
				selectedService,
			});
		}

	}, [selectedService]);

	return (
		<>
			<div className={propsClassName}>
				<Select
					placeholder={t('industries:selectIndustry')}
					handleChange={handleIndustryChange}
					items={industriesForSelect}
					autoFocus={true}
					value={selectedIndustry}
					isSearchable={false}
					isIcons={true}
				/>
			</div>
			<div className={propsClassName}>
				<Select
					placeholder={t('services:selectService')}
					handleChange={handleServiceChange}
					items={servicesItems}
					value={selectedService}
					isSearchable={false}
				/>
			</div>
		</>
	);
};
