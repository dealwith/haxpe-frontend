import { FunctionComponent } from 'react';
import { useController } from 'react-hook-form';
import { default as ReactSelect } from 'react-select';

import { TSelectOption } from 'interfaces';

type TProps = {
	name: string;
	control: any;
	options: TSelectOption[];
	defaultValue: TSelectOption;
	placeholder: string;
	styles: Record<string, (provided: any, state: any) => any>;
	autoFocus: boolean;
	isSearchable: boolean;
	component: () => JSX.Element;
	isMulti: boolean;
}

export const ControllableSelect: FunctionComponent<TProps> = ({
	name,
	control,
	...props
}) => {
	const { field } = useController({ name, control });

	return (
		<ReactSelect
			{...field}
			{...props}
		/>
	);
};
