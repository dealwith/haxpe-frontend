import { FunctionComponent } from 'react';
import ReactSelect, { StylesConfig } from 'react-select';

import { TExtendedIndustryOption, TExtendedServiceOption, TSelectOption } from 'interfaces';
import { getIcon } from './utils';

import { ControllableSelect } from './ControllableSelect';

type TProps = {
	name?: string;
	items: TSelectOption[];
	autoFocus?: boolean;
	value?: TExtendedIndustryOption | TExtendedServiceOption | Record<string, unknown>;
	defaultValue?: TSelectOption;
	isSearchable?: boolean;
	isIcons?: boolean;
	placeholder?: string;
	isMultiple?: boolean;
	handleChange?: (value: TSelectOption) => void;
	control?: any;
};

export const Select: FunctionComponent<TProps> = ({
	name,
	items,
	handleChange,
	autoFocus,
	value,
	defaultValue,
	isSearchable,
	isIcons,
	placeholder,
	isMultiple,
	control,
}) => {
	const icons = (id: string | number) => {
		const index = typeof id === 'string'
			? Number(id.slice(-1))
			: id;

		return {
			':after': {
				content: "''",
				display: 'block',
				width: '31px',
				height: '31px',
				top: '5px',
				background: ` url('${getIcon(index)}') no-repeat`,
				position: 'absolute',
			},
		};
	};

	const customStyles: StylesConfig = {
		control: (provided: any, state: any) => {
			let index;
			const options: string[] = state.options.map(option => option.label);

			if (value) {
				const { label } = value;

				index = options.indexOf(label as string);
			}

			return {
				...provided,
				backgroundColor: state.menuIsOpen ? '#31C963' : '#F2F2F2',
				color: state.menuIsOpen ? 'white' : '#272727',
				borderRadius: 0,
				padding: '4px 0',
				paddingLeft: '20px',
				...(isIcons && index && icons(index)),
			};
		},
		option: (provided: any, state: any) => {
			const { id } = state.innerProps;

			return {
				...provided,
				backgroundColor: '#F2F2F2',
				border: '1px solid #31C963',
				position: 'relative',
				color: state.isSelected ? '#31C963' : '#272727',
				paddingTop: '12px',
				paddingBottom: '12px',
				paddingLeft: '20px',
				...(isIcons && icons(id)),
			};
		},
		valueContainer: (provided: any) => ({
			...provided,
			justifyContent: 'center',
		}),
		singleValue: (provided: any, state: any) => ({
			...provided,
			opacity: state.isDisabled ? 0.5 : 1,
			transition: 'opacity 300ms',
			color: 'inherit',
			marginLeft: '36px',
		}),
		dropdownIndicator: (provided: any) => ({
			...provided,
			color: 'inherit',
		}),
		menu: (provided, state) => ({
			...provided,
			zIndex: 3,
		}),
	};

	const IndicatorSeparator = () => null;

	return control
		? <ControllableSelect
			name={name}
			control={control}
			options={items}
			defaultValue={defaultValue}
			placeholder={placeholder}
			styles={customStyles}
			autoFocus={autoFocus}
			isSearchable={isSearchable}
			component={IndicatorSeparator}
			isMulti={isMultiple}
		/>
		: <ReactSelect
			name={name}
			onChange={handleChange}
			value={value}
			options={items}
			defaultValue={defaultValue}
			placeholder={placeholder}
			autoFocus={autoFocus}
			isSearchable={isSearchable}
			components={{IndicatorSeparator}}
			isMulti={isMultiple}
			styles={customStyles}
		/>;
};
