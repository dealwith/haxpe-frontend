import { FunctionComponent, useContext } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { ModalContext } from 'context';
import { CustomFetch } from 'utils';
import { URLS } from 'constants/index';

import { Image, Overlay, Input, Button, Title } from 'components';
import closeIcon from './closeIcon.svg';

import styles from './modal.module.scss';
import { AccountService } from 'services';

interface IForm {
	phoneNumber: string;
}

export const Modal: FunctionComponent = () => {
	const { toggleModal, modal } = useContext(ModalContext);
	const { control, handleSubmit: validateBeforeSubmit } = useForm<IForm>();
	const { t } = useTranslation(['button', 'input']);

	const handleSubmit = async ({ phoneNumber }: IForm) => {
		try {
			const { status } = await AccountService.updateAccount({ phoneNumber: phoneNumber });

			if (status === 'success')
				toggleModal();
		} catch (err) {
			console.error(err);
		}
	};

	const handleToggleModal = () => {
		if (modal.isBlocking)
			return;

		toggleModal();
	};

	return (
		<Overlay flow='center' onClick={handleToggleModal}>
			<form
				onSubmit={validateBeforeSubmit(handleSubmit)}
				className={styles.component}
			>
				<Title
					className={styles.title}
					align='center'
					px='16'
				>
					{modal.text}
				</Title>
				{!modal.isBlocking
						&& <Image src={closeIcon} alt='Close icon' />
				}
				<Input
					control={control}
					name='phoneNumber'
					type='tel'
					placeholder={t('input:phone')}
					labelClassName={styles.inputContainer}
					size='full'
				/>
				<Button
					type='submit'
					rounded='m'
					size='s'
					theme='green'
					className={styles.button}
				>
					{t('button:save')}
				</Button>
			</form>
		</Overlay>
	);
};
