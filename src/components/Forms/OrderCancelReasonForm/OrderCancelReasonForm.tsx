import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import { StepComponentProps } from 'react-step-builder';
import { useTranslation } from 'react-i18next';

import { ECustomerOrderSteps, EResponseStatus } from 'interfaces';

import {
	Input,
	Button,
	Legend,
	Textarea,
	Container,
} from 'components';
import { OrderService } from 'services';

import './order-cancel-reason-form.scss';

interface IForm {
	problemAlreadyFixed: boolean;
	other: boolean;
	longWaiting: boolean;
	comment: string;
}

export const OrderCancelReasonForm: FunctionComponent<StepComponentProps> = ({
	jump,
	allSteps,
	title,
	getState,
}) => {
	const { t } = useTranslation([
		'orderCustomer',
		'orderCancelReasonForm',
		'button',
		'input',
		'user',
	]);
	const {
		handleSubmit: validateBeforeSubmit,
		control,
		register,
		watch,
	} = useForm<IForm>();
	const { orderId } = getState('order', '');

	const handleSubmit = async ({
		problemAlreadyFixed,
		longWaiting,
		comment,
	}: IForm) => {
		const calculateReason = () => {
			let reason = '';

			if (problemAlreadyFixed)
				reason += t('orderCancelReasonForm:problemAlreadyFixed');

			if (longWaiting) {
				const text = t('orderCancelReasonForm:longWaiting');

				reason += problemAlreadyFixed
					? `, ${text}`
					: text;
			}

			if (comment) {
				const text = `${t(`user:comment`)}: ${comment}`;

				reason += longWaiting || problemAlreadyFixed
					? `, ${text}`
					: text;
			}

			return reason;
		};
		const reason = calculateReason();

		const { status: cancelStatus } = await OrderService.customerReject(orderId, { reason });

		if (cancelStatus === EResponseStatus.success) {
			const { order: orderCreationStep } = allSteps
				.find(({ title }) => title === ECustomerOrderSteps.orderCreation);

			jump(orderCreationStep);
		}
	};

	const handleBackClick = () => {
		const { order: searchWorkerStep } = allSteps
			.find(({ title }) => title === ECustomerOrderSteps.searchWorker);

		jump(searchWorkerStep);
	};

	const watchOther = watch('other');

	const baseClassName = 'order-cancel-reason-form';
	const componentClassName = {
		buttons: `${baseClassName}__buttons`,
		textarea: `${baseClassName}__textarea`,
		input: `${baseClassName}__input`,
	};

	return (
		<Container
			maxWidth={380}
			isFitContent={true}
			theme='white'
		>
			<form onSubmit={validateBeforeSubmit(handleSubmit)}>
				<Legend theme='gray'>
					{t(`orderCustomer:${title}`)}
				</Legend>
				<Input
					type='checkbox'
					name='problemAlreadyFixed'
					control={control}
					labelClassName={componentClassName.input}
					placeholder={t('orderCancelReasonForm:problemAlreadyFixed')}
				/>
				<Input
					type='checkbox'
					name='longWaiting'
					control={control}
					labelClassName={componentClassName.input}
					placeholder={t('orderCancelReasonForm:longWaiting')}
				/>
				<Input
					type='checkbox'
					name='other'
					control={control}
					labelClassName={componentClassName.input}
					placeholder={t('orderCancelReasonForm:other')}
				/>
				<Textarea
					size='full'
					theme='white'
					placeholder={t('input:comment')}
					isDisabled={!watchOther}
					className={componentClassName.textarea}
					{...register('comment')}
				/>
				<div className={componentClassName.buttons}>
					<Button
						theme='green'
						onClick={handleBackClick}
						rounded='xxl'
					>
						{t('button:back')}
					</Button>
					<Button
						theme='red'
						type='submit'
						rounded='xxl'
					>
						{t('button:cancelOrder')}
					</Button>
				</div>
			</form>
		</Container>
	);
};
