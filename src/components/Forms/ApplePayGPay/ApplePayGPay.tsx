import { useState, useEffect, FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { GetState, StepComponentProps } from 'react-step-builder';
import {PaymentElement, PaymentRequestButtonElement, useStripe} from '@stripe/react-stripe-js';

import { Button, Span } from 'components';

import { jumpToCorrectStep } from 'components/Order/Customer/utils';
import { EOrderCustomerPaymentSteps } from 'interfaces/order/customer';

import styles from './ApplePayGPay.module.scss';
import { OrderService } from 'services';
import { EResponseStatus } from 'interfaces';

type TProps = { orderCustomerGetState: GetState } & StepComponentProps;

export const ApplePayGPay: FunctionComponent<TProps> = ({
	orderCustomerGetState,
}) => {
	const { orderId } = orderCustomerGetState('order', '');
	const stripe = useStripe();
	const [clientSecret, setClientSecret] = useState('');

	useEffect(() => {
		const fetchClientSecret = async () => {
			const paymentStartRes = await OrderService.paymentStart(orderId);

			if (paymentStartRes?.status === EResponseStatus.success)
				setClientSecret(paymentStartRes.body.stripeKey);
		};

		fetchClientSecret();
	}, [orderId]);

	return <PaymentElement  />;
};
