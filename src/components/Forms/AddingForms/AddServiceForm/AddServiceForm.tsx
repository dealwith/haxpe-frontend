import { FunctionComponent, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { isObjectWithProperties } from 'utils';
import { TExtraService, TOrderInvoiceTableInputWidth } from 'interfaces';
import { EOrderInvoiceServiceFormData } from 'components/interfaces';

import {
	Input,
	AddButton,
	Button,
	ContentContainer,
} from 'components';

import './add-service-form.scss';

type TProps = {
	customWidthStyles?: TOrderInvoiceTableInputWidth;
	setServicesForTable: (data: TExtraService) => void;
}

export const AddServiceForm: FunctionComponent<TProps> = ({
	customWidthStyles,
	setServicesForTable,
}) => {
	const {
		handleSubmit: hookHandleSubmit,
		control,
		reset,
		formState: { isSubmitSuccessful },
	} = useForm<TExtraService>();
	const [isShowInput, setIsShowInput] = useState(false);
	const { t } = useTranslation('button');

	const handleSubmit = (data: TExtraService) => {
		setIsShowInput(false);
		setServicesForTable({...data});
	};

	const handleCancel = () => setIsShowInput(false);

	const handleAdd = () => setIsShowInput(true);

	const rules = {
		required: true,
	};

	useEffect(() => {
		if (isSubmitSuccessful)
			reset();

	}, [isSubmitSuccessful, reset]);

	const baseClassName = 'add-service-form';
	const componentClassName = {
		component: baseClassName,
		inputsWrapper: `${baseClassName}__inputs-wrapper`,
		addButtonWrapper: `${baseClassName}__add-button-wrapper`,
		saveButton: `${baseClassName}__save-button`,
		cancelButton: `${baseClassName}__cancel-button`,
		buttonContainer: `${baseClassName}__button-container`,
	};

	const isStyles = isObjectWithProperties(customWidthStyles);

	return (
		<>
			{isShowInput
				? (
					<form className={componentClassName.component}>
						<div className={componentClassName.inputsWrapper}>
							<ContentContainer
								{...(isStyles && {maxWidth: customWidthStyles.small})}
								position='left'
							>
								1
							</ContentContainer>
							<Input
								rules={rules}
								{...(isStyles && {maxWidth: customWidthStyles.large})}
								name={EOrderInvoiceServiceFormData.name}
								type='text'
								control={control}
								size='full'
								textAlign='center'
							/>
							<Input
								rules={rules}
								isCurrencyInput={true}
								{...(isStyles && {maxWidth: customWidthStyles.normal})}
								name={EOrderInvoiceServiceFormData.value}
								type='number'
								control={control}
								size='full'
								textAlign='center'
							/>
						</div>
						<div className={componentClassName.buttonContainer}>
							<Button
								onClick={handleCancel}
								theme='red'
								size='half'
								className={componentClassName.cancelButton}
							>
								{t('button:deleteService')}
							</Button>
							<Button
								theme='light-green'
								onClick={hookHandleSubmit(handleSubmit)}
								size='half'
								className={componentClassName.saveButton}
							>
								{t('button:saveService')}
							</Button>
						</div>
					</form>
				)
				: <div className={componentClassName.addButtonWrapper}>
					<AddButton
						theme='light-green'
						textForTranslate='addService'
						handleOnClick={handleAdd}
					/>
				</div>
			}
		</>
	);
};
