import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { Button, Textarea } from 'components';

import './report-form.scss';

type TReportForm = {
	report: string;
}

type TProps = {
	handleHideReportForm: () => void;
	handleAddReport: (str: string) => void;
}

export const ReportForm: FunctionComponent<TProps> = ({
	handleHideReportForm,
	handleAddReport,
}) => {
	const { register, handleSubmit: hookHandleSubmit } = useForm<TReportForm>();
	const { t } = useTranslation('button');

	const handleSubmit = ({ report }: TReportForm) => {
		handleHideReportForm();
		handleAddReport(report);
	};

	const baseClassName = 'report-form';
	const componentClassName = {
		component: baseClassName,
		saveButton: `${baseClassName}__save-button`,
		cancelButton: `${baseClassName}__cancel-button`,
		textArea: `${baseClassName}__text-area`,
	};

	return (
		<form
			className={componentClassName.component}
			onSubmit={hookHandleSubmit(handleSubmit)}
		>
			<Textarea
				className={componentClassName.textArea}
				{...register('report')}
				size='full'
			/>
			<Button
				type='button'
				theme='red'
				size='full'
				className={componentClassName.cancelButton}
				onClick={handleHideReportForm}
			>
				{t('cancelReport')}
			</Button>
			<Button
				type='submit'
				theme='light-green'
				size='full'
				className={componentClassName.saveButton}
			>
				{t('saveReport')}
			</Button>
		</form>
	);
};
