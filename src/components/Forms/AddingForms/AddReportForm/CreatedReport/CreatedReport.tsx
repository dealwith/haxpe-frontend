import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import {
	ContentContainer,
	P,
	RemoveButton,
	Span,
} from 'components';

import './created-report.scss';

type TProps = {
	handleRemoveReport: () => void;
	children: string;
}

export const CreatedReport: FunctionComponent<TProps> = ({
	handleRemoveReport,
	children,
}) => {
	const { t } = useTranslation('button');

	const baseClassName = 'created-report';
	const componentClassName = {
		component: baseClassName,
		reportContentWrapper: `${baseClassName}__report-content-wrapper`,
		report: `${baseClassName}__report`,
	};

	return (
		<div className={componentClassName.component}>
			<RemoveButton
				theme='red'
				textForTranslate='removeReport'
				handleOnClick={handleRemoveReport}
			/>
			<ContentContainer
				height='auto'
				className={componentClassName.reportContentWrapper}
				centered='flex-start'
			>
				<P className={componentClassName.report}>
					<Span isBold={true}>{t('button:report')}: </Span>
					{children}
				</P>
			</ContentContainer>
		</div>
	);
};
