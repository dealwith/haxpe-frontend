import { FunctionComponent, useState } from 'react';

import { useAfterWorkCompleteInfo } from 'hooks';
import { AddButton, ContentContainer } from 'components';
import { CreatedReport } from './CreatedReport';
import { ReportForm } from './ReportForm';

import './add-report-form.scss';

export const AddReportForm: FunctionComponent = () => {
	const [isShowReportForm, setIsShowReportForm] = useState(false);
	const {
		handleAddReport,
		report,
		handleClearReport,
	} = useAfterWorkCompleteInfo();

	const handleShowReportForm = () => setIsShowReportForm(true);

	const handleHideReportForm = () => setIsShowReportForm(false);

	const baseClassName = 'add-report-form';
	const componentClassName = {
		component: baseClassName,
		createdReportWrapper: `${baseClassName}__created-report-wrapper`,
		addReportWrapper: `${baseClassName}__add-report-wrapper`,
		addButtonText: `${baseClassName}__add-button-text`,
	};

	const renderBody = () => {
		if (report) {
			return (
				<div className={componentClassName.createdReportWrapper}>
					<CreatedReport handleRemoveReport={handleClearReport}>
						{report}
					</CreatedReport>
				</div>
			);
		}

		if (isShowReportForm) {
			return (
				<ReportForm
					handleHideReportForm={handleHideReportForm}
					handleAddReport={handleAddReport}
				/>
			);
		}

		return (
			<ContentContainer
				centered='flex-start'
				className={componentClassName.addReportWrapper}
			>
				<AddButton
					theme='light-green'
					textForTranslate='addReport'
					additionalText='...'
					textClassName={componentClassName.addButtonText}
					handleOnClick={handleShowReportForm}
				/>
			</ContentContainer>
		);
	};

	const renderedBody = renderBody();

	return <div className={componentClassName.component}>
		{renderedBody}
	</div>;
};
