import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { Input, Button } from 'components';

import './visa-form.scss';

type TProps = {
	iconHref: string;
}

export const VisaForm: FunctionComponent<TProps> = ({ iconHref }) => {
	const { control, handleSubmit: hookFormHandleSubmit } = useForm();
	const { t } = useTranslation(['input', 'button']);

	const handleSubmit = data => {
		console.log(data);
	};

	const required = {
		required: true,
	};

	const validateDate = {
		validate: (val: string) => {
			const reg = /^(0[1-9]|1[0-2])\/([0-9]{2})$/;

			return reg.test(val);
		},
	};

	//TODO: '(xxx)? been 3 symbol or?'
	const length = {
		maxLength: 3,
		minLength: 3,
	};

	const baseClassName = 'visa-form';
	const componentClassName = {
		component: baseClassName,
		title: `${baseClassName}__title`,
		label: `${baseClassName}__label`,
		inputGroup: `${baseClassName}__input-group`,
	};

	return (
		<div className={baseClassName}>
			<div className={componentClassName.title}>
				<img src={iconHref} alt='VISA' />
			</div>
			<form onSubmit={hookFormHandleSubmit(handleSubmit)}>
				<Input
					rules={required}
					size='full'
					labelClassName={componentClassName.label}
					control={control}
					name='any1'
					placeholder={t('input:cardholder')}
				/>
				<Input
					rules={required}
					size='full'
					labelClassName={componentClassName.label}
					control={control}
					name='any2'
					placeholder={t('input:creditCardNumber')}
				/>
				<div className={componentClassName.inputGroup}>
					<Input
						rules={{...validateDate, ...required}}
						size='half'
						labelClassName={componentClassName.label}
						control={control}
						name='any3'
						placeholder={`${t('input:validity')} (mm/yyyy)`}
					/>
					<Input
						rules={{...required, ...length}}
						size='half'
						labelClassName={componentClassName.label}
						control={control}
						name='any4'
						placeholder={`${t('input:code')} (xxx)`}
					/>
				</div>
				<Button
					type='submit'
					theme='light-green'
					rounded='xxl'
				>
					{t('button:save')}
				</Button>
			</form>
		</div>
	);
};
