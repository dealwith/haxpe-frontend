import { TExtendedIndustryOption, TExtendedServiceOption } from 'interfaces';

export interface IMultiServiceState {
	industries: TExtendedIndustryOption[];
	services: TExtendedServiceOption[];
	allServices: TExtendedServiceOption[];
	selectedIndustry: TExtendedIndustryOption | Record<string, unknown>;
	selectedService: TExtendedServiceOption | Record<string, unknown>;
}
