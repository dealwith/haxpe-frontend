export interface IForm {
	street: string;
	buildingNum: string;
	zipCode: string;
	city: string;
}
