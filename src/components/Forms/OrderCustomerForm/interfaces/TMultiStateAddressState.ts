import { IAddress } from 'interfaces';

export type TMultiStateAddress = IAddress;
