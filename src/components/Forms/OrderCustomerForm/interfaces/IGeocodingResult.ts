import { IGeocodeResult } from 'interfaces/geolocation/IGeocodingTypes';

export interface IGeocodingResult {
	plus_code: Record<string, unknown>;
	results: IGeocodeResult[];
	status: string;
}
