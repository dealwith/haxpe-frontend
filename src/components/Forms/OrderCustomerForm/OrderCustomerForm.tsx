// eslint-disable-next-line @typescript-eslint/ban-ts-comment
//@ts-nocheck
import { useEffect, FunctionComponent, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useForm } from 'react-hook-form';
import { StepComponentProps } from 'react-step-builder';
import { useHistory } from 'react-router';

import {
	isObjectWithProperties,
	isObjIncludesType,
	getCurrentLocation,
	getGeocodingData,
} from 'utils';
import {
	TSelectOption,
	EResponseStatus,
	ECustomerOrderSteps,
	TCustomerOrderSteps,
	TOrderStatuses,
} from 'interfaces';
import {
	AddressService,
	CustomerService,
	OrderService,
	UserService,
} from 'services';
import { IMultiServiceState, TMultiStateAddress, IForm } from './interfaces';
import { ROUTES } from 'constants/index';
import { jumpToCorrectStep } from 'components/Order/Customer/utils';

import {
	useAuth,
	useMultiState,
	usePrevious,
	useServicesIndustries,
} from 'hooks';
import {
	Button,
	P,
	Container,
	Input,
	Legend,
	Link,
	Select,
	CircleLoader,
} from 'components';

import './order-customer-form.scss';

export const OrderCustomerForm: FunctionComponent<StepComponentProps> = ({
	next,
	jump,
	title = ECustomerOrderSteps.orderCreation,
	setState: setStepState,
	allSteps,
}) => {
	const history = useHistory();
	const { isAuthenticated } = useAuth();
	const [isRenderForm, setIsRenderForm] = useState(false);
	const { servicesForSelect, industriesForSelect } = useServicesIndustries();
	const { t } = useTranslation([
		'industries',
		'services',
		'input',
		'button',
		'orderCustomerForm',
		'orderCustomer',
	]);
	const [multiServiceState, setMultiServiceState] = useMultiState<IMultiServiceState>({
		industries: [],
		services: [],
		allServices: [],
		selectedIndustry: {},
		selectedService: {},
	});

	const [multiStateAddress, setMultiAddressState] = useMultiState<TMultiStateAddress>({
		street: '',
		buildingNum: '',
		city: '',
		country: '',
		lat: 0,
		lon: 0,
		zipCode: '',
		externalId: '',
	});

	useEffect(() => {
		const fetchData = async () => {
			try {
				const {
					status: customerInfoStatus,
					body: customerRes,
				} = await CustomerService.getCustomerInfo();

				if (customerInfoStatus === EResponseStatus.success) {
					const { id: customerId } = customerRes;

					const {
						body: activeOrders,
					} = await OrderService.getOrder({
						isActive: true,
						CustomerId: customerId,
					});

					const isActiveOrderExists = !!(activeOrders[0]?.id);

					if (isActiveOrderExists) {
						const {
							id: activeOrderId,
							orderStatus,
							workerId,
							serviceTypeId,
							expectedArrival,
							industryId,
							addressId,
							invoice,
							couponCode,
							report,
						} = activeOrders[0];

						let workerFullName, workerPhone;

						if (workerId) {
							const {
								status: userStatus,
								body: userRes,
							} = await UserService.getUserByWorkerId(workerId);

							if (userStatus === EResponseStatus.success) {
								workerFullName = userRes.fullName;
								workerPhone = userRes.phoneNumber;
							}
						}

						setStepState('order', {
							workerId,
							serviceTypeId,
							orderId: activeOrderId,
							expectedArrival,
							industryId,
							addressId,
							...(workerId && {
								workerFullName,
								workerPhone,
							}),
							invoice,
							couponCode,
							report,
						});

						jumpToCorrectStep<TCustomerOrderSteps | TOrderStatuses>(orderStatus, jump, allSteps);
					} else
						setIsRenderForm(true);
				}
			} catch (err) {
				console.error(err);
			}
		};

		fetchData();
	}, []);

	// Order customer form in guest mode
	useEffect(() => {
		if (!isAuthenticated)
			setIsRenderForm(true);
	}, [isAuthenticated]);

	useEffect(() => {
		const transformedIndustries = [
			{
				value: t('industries:notSelected').toLowerCase(),
				label: t('industries:notSelected'),
			},
			...industriesForSelect,
		];
		const transformedServices = [
			{
				value: t('services:notSelected').toLowerCase(),
				label: t('services:notSelected'),
			},
			...servicesForSelect,
		];

		setMultiServiceState({
			industries: transformedIndustries,
			allServices: transformedServices,
		});
	}, []);

	const prevSelectedIndustryLabel = usePrevious(
		multiServiceState.selectedIndustry.label,
	);
	const { label: selectedIndustryLabel }: { label: string } = multiServiceState
		.selectedIndustry;

	useEffect(() => {
		if (selectedIndustryLabel !== prevSelectedIndustryLabel) {
			const { allServices, selectedIndustry } = multiServiceState;
			const defaultService = allServices[0];
			const filteredServices = allServices.filter(
				service => service.industryId === selectedIndustry.id,
			);
			const listOfServices = [defaultService, ...filteredServices];

			setMultiServiceState({
				services: listOfServices,
				selectedService: listOfServices[0],
			});
		}

	}, [setMultiServiceState, selectedIndustryLabel]);

	const handleServiceChange = (value: TSelectOption) => setMultiServiceState(
		{ selectedService: value },
	);
	const handleIndustryChange = (value: TSelectOption) => setMultiServiceState(
		{ selectedIndustry: value },
	);

	const handleCurrentLocationClick = async () => {
		const location = await getCurrentLocation();

		if (isObjectWithProperties(location)) {
			const {
				placeId,
				street,
				buildingNum,
				city,
				lat,
				lon,
				zipCode,
				country,
			} = location;

			setMultiAddressState({
				externalId: placeId,
				street,
				buildingNum,
				city,
				lat,
				lon,
				zipCode,
				country,
			});

			setValue('street', street);
			setValue('buildingNum', buildingNum);
			setValue('zipCode', zipCode);
			setValue('city', city);
		}
	};

	const handleSubmit = async (data: IForm) => {
		// Form submitting fot guest
		if (!isAuthenticated)
			return history.push(ROUTES.SIGN_IN);

		const {
			zipCode,
			city,
			street,
			buildingNum,
		} = data;
		const {
			country: multiStateAddressCountry,
			lat,
			lon,
			externalId,
		} = multiStateAddress;

		try {
			const addressData = {
				zipCode,
				city,
				street,
				buildingNum,
				country: multiStateAddressCountry,
				lat,
				lon,
				externalId,
			};

			const isSomeAddressDataEmpty = Object.values(addressData)
				.some(value => !value);

			// Check if he use handleCurrentLocationClick but he want to define another address
			if (isSomeAddressDataEmpty) {
				const data = await getGeocodingData(street, buildingNum, city, zipCode);
				const { place_id: externalId } = data;
				const { lat, lng } = data.geometry.location;
				const countryObject = data
					?.address_components
					.find(address_component => isObjIncludesType(
						address_component, 'country',
					));

				const country = countryObject.long_name ?? multiStateAddressCountry;

				setMultiAddressState({
					lat,
					lon: lng,
					country,
					externalId,
				});

				addressData.lat = lat;
				addressData.lon = lng;
				addressData.country = country;
				addressData.externalId = externalId;
			}

			const { body: { id: addressId } } = await AddressService.createAddress(
				addressData,
			);

			const { body: { id: customerId } } = await CustomerService.getCustomerInfo();

			const {
				body: {
					id: orderId,
					serviceTypeId,
					industryId,
				},
			} = await OrderService.createOrder({
				customerId,
				addressId,
				serviceTypeId: multiServiceState.selectedService.id,
				paymentMethod: 'bar',
			});

			setStepState('order', {
				serviceTypeId,
				orderId,
				industryId,
				addressId,
			});
			next();
		} catch (error) {
			console.error(err);
		}
	};

	const {
		handleSubmit: validateBeforeSubmit,
		setValue,
		control,
	} = useForm<IForm>();

	const industryValue = isObjectWithProperties(multiServiceState.selectedIndustry)
		? multiServiceState.selectedIndustry
		: multiServiceState.industries[0];
	const serviceValue = isObjectWithProperties(multiServiceState.selectedService)
		? multiServiceState.selectedService
		: multiServiceState.allServices[0];

	const getInputEnable = () => {
		const servicesLabels = multiServiceState?.services.map((options: TSelectOption) => options.label);
		const { label: selectedServiceLabel } = multiServiceState.selectedService;

		const isAnySelected = servicesLabels.indexOf(selectedServiceLabel) !== -1;
		const isNotDefaultItemSelected = servicesLabels.indexOf(selectedServiceLabel) !== 0;

		return isAnySelected && isNotDefaultItemSelected;
	};

	const isLocationElementsDisabled = !getInputEnable();

	const baseClassName = 'order-customer-form';
	const componentClassName = {
		component: baseClassName,
		locationButton: `${baseClassName}__location-button`,
		locationButtons: `${baseClassName}__location-buttons`,
		form: `${baseClassName}__form`,
		submitButton: `${baseClassName}__submit-button`,
		description: `${baseClassName}__description`,
		link: `${baseClassName}__link`,
	};

	const buttonProps = {
		theme: 'light-gray',
		size: 'half',
		rounded: 's',
		flow: 'vertical',
		className: componentClassName.locationButton,
		isDisabled: isLocationElementsDisabled,
	};

	return isRenderForm
		? <Container
			maxWidth={380}
			isFitContent={true}
			theme='white'
			className={componentClassName.component}
		>
			<form
				className={componentClassName.form}
				onSubmit={validateBeforeSubmit(handleSubmit)}
			>
				<Legend>{t(`orderCustomer:${title}`)}</Legend>
				<Select
					handleChange={handleIndustryChange}
					items={multiServiceState.industries}
					autoFocus={true}
					value={industryValue}
					isSearchable={false}
					isIcons={true}
				/>
				<Select
					handleChange={handleServiceChange}
					items={multiServiceState.services}
					value={serviceValue}
					isSearchable={false}
					placeholder={t('services:notSelected')}
				/>
				<div className={componentClassName.locationButtons}>
					<Button
						{...buttonProps}
						icon='current-location'
						onClick={handleCurrentLocationClick}
					>
						{t('button:currentLocation')}
					</Button>
					<Button {...buttonProps} icon='navigation'>
						{t('button:enterAddress')}
					</Button>
				</div>
				<div className='input-wrapper'>
					<Input
						name='street'
						icon='target'
						size='full'
						placeholder={t('input:street')}
						isDisabled={isLocationElementsDisabled}
						rules={{ required: 'Please, provide street name' }}
						control={control}
					/>
				</div>
				<div className='input-wrapper'>
					<Input
						name='buildingNum'
						icon='target'
						size='full'
						placeholder={t('input:buildingNum')}
						isDisabled={isLocationElementsDisabled}
						rules={{ required: 'Please, provide street name' }}
						control={control}
					/>
				</div>
				<div className='input-wrapper'>
					<Input
						name='zipCode'
						icon='target'
						size='full'
						placeholder={t('input:zipCode')}
						rules={{ required: true }}
						control={control}
						isDisabled={isLocationElementsDisabled}
					/>
				</div>
				<div className='input-wrapper'>
					<Input
						name='city'
						icon='target'
						size='full'
						placeholder={t('input:city')}
						rules={{ required: true }}
						control={control}
						isDisabled={isLocationElementsDisabled}
					/>
				</div>
				<Container
					isInner={true}
					centered='center'
					flow='vertical'
				>
					<Button
						type='submit'
						theme='green'
						rounded='xxl'
						className={componentClassName.submitButton}
						isDisabled={isLocationElementsDisabled}
					>
						{t('button:search')}
					</Button>
					<P
						color='gray'
						size='small'
						className={componentClassName.description}
					>
						{t('orderCustomerForm:helpText')}
					</P>
					<Link
						href='#'
						className={componentClassName.link}
						size='small'
					>
						{t('orderCustomerForm:contactLink')}
					</Link>
				</Container>
			</form>
		</Container>
		: <CircleLoader />;
};
