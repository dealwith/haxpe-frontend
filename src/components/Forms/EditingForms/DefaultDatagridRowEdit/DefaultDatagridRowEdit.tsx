import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';

import { filterValidateBeforeSubmitData } from 'utils';
import { IEditableComponentProps } from 'interfaces';

import { useEditableFields } from 'hooks';
import { EditForm } from '../EditForm';
import { EditableFields } from '../EditableFields';

export const DefaultDatagridRowEdit: FunctionComponent<IEditableComponentProps> = ({
	body,
	handleClose,
	editedFieldName,
	textArray,
	textArrayTitle,
}) => {
	const { control, handleSubmit: validateBeforeSubmit } = useForm();
	const { handleAddEditedFields } = useEditableFields();

	const handleSubmit = validateBeforeSubmit(data => {
		const filteredData = filterValidateBeforeSubmitData(data);
		const mixedBody = {...body[0], ...filteredData};

		handleAddEditedFields({
			[editedFieldName]: mixedBody,
		});
		handleClose();
	});

	return (
		<EditForm
			handleClose={handleClose}
			handleSubmit={handleSubmit}
		>
			<EditableFields
				control={control}
				body={body}
				textArray={textArray}
				textArrayTitle={textArrayTitle}
			/>
		</EditForm>
	);
};
