import { FunctionComponent, useEffect } from 'react';
import { useForm } from 'react-hook-form';

import {
	EPayoutInterval,
	IEditableComponentProps,
	TPayoutInterval,
} from 'interfaces';

import { useEditableFields } from 'hooks';
import { Input, Title } from 'components';

import './payout-interval-radio-group.scss';

type TProps = {
	defaultCheckedPaymentInterval: TPayoutInterval;
	title: string;
} & IEditableComponentProps

type TForm = {
	payoutInterval: TPayoutInterval;
}

export const PayoutIntervalRadioGroup: FunctionComponent<TProps> = ({
	defaultCheckedPaymentInterval,
	title,
}) => {
	const { control, watch } = useForm<TForm>();
	const { handleAddEditedFields } = useEditableFields();

	const currentRadio = watch('payoutInterval');

	useEffect(() => {
		if (currentRadio) {
			handleAddEditedFields({
				payoutInterval: currentRadio,
			});
		}
	}, [currentRadio]);

	return (
		<>
			<Title px='14' color='light-green' align='center'>
				{title}
			</Title>
			<form className={'payout-interval-radio-group'}>
				<Input
					defaultValue={defaultCheckedPaymentInterval}
					name='payoutInterval'
					value={EPayoutInterval.monthly}
					type='radio'
					control={control}
					placeholder={EPayoutInterval.monthly}
					placeholderTheme='light-green'
				/>
				<Input
					defaultValue={defaultCheckedPaymentInterval}
					placeholderTheme='light-green'
					name='payoutInterval'
					value={EPayoutInterval.weekly}
					type='radio'
					control={control}
					placeholder={EPayoutInterval.weekly}
				/>
			</form>
		</>
	);
};
