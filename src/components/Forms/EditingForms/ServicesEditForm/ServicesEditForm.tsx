import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { filterValidateBeforeSubmitData } from 'utils';
import {
	Dictionary,
	IEditableComponentProps,
	IIndustryId,
	IServiceTypesId,
} from 'interfaces';

import { useEditableFields, useServicesIndustries } from 'hooks';
import { Legend, Fieldset, Input } from 'components';
import { EditForm } from '../EditForm';

type TProps = IEditableComponentProps & {
	partnerServiceTypes?: IServiceTypesId[];
	partnerIndustries?: IIndustryId[];
}

export const ServicesEditForm: FunctionComponent<TProps> = ({
	body,
	handleClose,
	partnerServiceTypes,
	partnerIndustries,
}) => {
	const { control, handleSubmit: validateBeforeSubmit } = useForm();
	const { industries, services } = useServicesIndustries();
	const { handleAddEditedFields } = useEditableFields();
	const { t } = useTranslation(['services', 'industries']);

	const isLoaded = !!(industries.length && services.length);

	const selectedServices = body
		.reduce((curVal, service) => ({...curVal, ...service}), {});

	const handleSubmit = validateBeforeSubmit((data: Dictionary<boolean>) => {
		const selectedCheckbox = filterValidateBeforeSubmitData(data);
		const selectedIds = Object.keys(selectedCheckbox);
		const selectedServices = services
			.filter(({ id }) => selectedIds.includes(id.toString()));

		const selectedServicesIndustriesIds = selectedServices
			.map(({ industryId }) => industryId);

		const selectedIndustries = industries
			.filter(({ id }) => selectedServicesIndustriesIds.includes(id));

		handleAddEditedFields({
			services: selectedServices,
			industries: selectedIndustries,
		});
		handleClose();
	});

	const renderServices = (propsIndustryId: number) => {
		const selectedServicesValuesAsArray = Object.values(selectedServices);

		return services
			.filter(({ industryId }) => industryId === propsIndustryId)
			.map(({ key, id }) => {
				const service = t(`services:${key}`);
				const isServiceChecked = selectedServicesValuesAsArray
					.includes(service);
				const stringifiedId = id.toString();

				return (
					<Input
						name={stringifiedId}
						placeholderTheme='white'
						type='checkbox'
						placeholder={service}
						defaultValue={isServiceChecked}
						control={control}
						key={id}
					/>
				);
			});
	};

	const renderIndustries = () => {
		return industries?.map(({ key, id }) => {
			const services = renderServices(id);

			return (
				<Fieldset key={id}>
					<Legend>{t(`industries:${key}`)}</Legend>
					{services}
				</Fieldset>
			);
		});
	};

	const renderPartnerServices = (
		partnerServices: IServiceTypesId[],
		currentIndustryId,
	) => {
		const acceptedServicesIds = partnerServices
			.map(({ serviceTypeId }) => serviceTypeId);

		return services
			.filter(({ id, industryId }) => acceptedServicesIds.includes(id)
			&& currentIndustryId === industryId)
			.map(({ key, id }) => {
				const service = t(`services:${key}`);
				const isServiceChecked = Object.values(selectedServices)
					.includes(service);
				const stringifiedId = id.toString();

				return (
					<Input
						name={stringifiedId}
						placeholderTheme='white'
						type='checkbox'
						placeholder={service}
						defaultValue={isServiceChecked}
						control={control}
						key={id}
					/>
				);
			});
	};

	const renderPartnerIndustries = () => {
		const acceptedIndustriesIds = partnerIndustries
			.map(({ industryId }) => industryId);

		return industries
			?.filter(({ id }) => acceptedIndustriesIds.includes(id))
			.map(({ key, id }) => {
				const services = renderPartnerServices(partnerServiceTypes, id);

				return (
					<Fieldset key={id}>
						<Legend>{t(`industries:${key}`)}</Legend>
						{services}
					</Fieldset>
				);
			});
	};

	const renderBody = partnerIndustries
		? renderPartnerIndustries()
		: renderIndustries();

	return isLoaded
		&& <EditForm
			handleClose={handleClose}
			handleSubmit={handleSubmit}
		>
			{renderBody}
		</EditForm>;
};
