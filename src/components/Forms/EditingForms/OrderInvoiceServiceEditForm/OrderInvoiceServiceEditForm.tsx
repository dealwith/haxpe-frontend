import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { isObjectWithProperties } from 'utils';
import { TExtraService, TOrderInvoiceTableInputWidth } from 'interfaces';
import { EOrderInvoiceServiceFormData } from 'components/interfaces';

import { Button, ContentContainer, Input } from 'components';

import './order-invoice-service-edit-form.scss';

type TProps = TExtraService
	& {
		customWidthStyles?: TOrderInvoiceTableInputWidth;
		getChangedService?: (service: TExtraService) => void;
	}

export const OrderInvoiceServiceEditForm: FunctionComponent<TProps> = ({
	name: service,
	value: amount,
	customWidthStyles,
	getChangedService,
}) => {
	const { t } = useTranslation('button');
	const {
		control,
		handleSubmit: validateBeforeSubmit,
	} = useForm<TExtraService>({
		defaultValues: {
			name: service,
			value: amount,
		},
	});

	const handleSubmit = (service: TExtraService) => {
		getChangedService(service);
	};

	const isStyles = isObjectWithProperties(customWidthStyles);

	const baseClassName = 'order-invoice-service-edit-form';
	const componentClassName = {
		component: baseClassName,
		input: `${baseClassName}__input`,
		saveButton: `${baseClassName}__save-button`,
	};

	return (
		<form
			className={componentClassName.component}
			onSubmit={validateBeforeSubmit(handleSubmit)}
		>
			<ContentContainer
				{...(isStyles && {maxWidth: customWidthStyles.small})}
				position='left'
			>
				1
			</ContentContainer>
			<Input
				textAlign='center'
				className={componentClassName.input}
				{...(isStyles && {maxWidth: customWidthStyles.large})}
				name={EOrderInvoiceServiceFormData.name}
				control={control}
				size='full'
			/>
			<Input
				textAlign='center'
				className={componentClassName.input}
				{...(isStyles && {maxWidth: customWidthStyles.normal})}
				name={EOrderInvoiceServiceFormData.value}
				control={control}
				size='full'
			/>
			<Button
				className={componentClassName.saveButton}
				theme='light-green'
				type='submit'
			>
				{t('button:saveChanges')}
			</Button>
		</form>
	);
};
