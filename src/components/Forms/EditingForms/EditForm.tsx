import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import cn from 'classnames';

import { Dictionary } from 'interfaces';

import { Button, CloseButton } from 'components';

import './edit-form.scss';

type TProps = {
	handleClose: () => void;
	handleSubmit: (data: Dictionary<any>) => void;
	className?: string;
}

export const EditForm: FunctionComponent<TProps> = ({
	className: propsClassName,
	children,
	handleClose,
	handleSubmit,
}) => {
	const { t } = useTranslation('button');
	const baseClassName = cn(propsClassName, 'edit-form');

	return (
		<form
			onSubmit={handleSubmit}
			className={baseClassName}
		>
			{children}
			<Button
				theme='light-green'
				type='submit'
				size='full'
				rounded='xxl'
			>
				{t('button:save')}
			</Button>
			<CloseButton handleClose={handleClose}/>
		</form>
	);
};
