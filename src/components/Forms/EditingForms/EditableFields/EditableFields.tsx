import { FunctionComponent } from 'react';
import { Control, FieldValues } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { TDatagridRowSectionBody } from 'interfaces';

import { Input, Legend } from 'components';

import './editable-fields.scss';

type TProps = {
	control: Control<FieldValues>;
	body: TDatagridRowSectionBody;
	textArray: string[];
	textArrayTitle?: string;
}

export const EditableFields: FunctionComponent<TProps> = ({
	control,
	body,
	textArray,
	textArrayTitle,
}) => {
	const { t } = useTranslation('user');
	const baseClassName = 'editable-fields';
	const componentClassName = {
		component: baseClassName,
		field: `${baseClassName}__field`,
		input: `${baseClassName}__input`,
		placeholder: `${baseClassName}__placeholder`,
	};

	const renderBody = () => {
		if (body?.length) {
			return body.map((bodyBlock, i) => (
				<div className={baseClassName} key={i}>
					{Object.entries(bodyBlock).map(([key, value]) => {
						if (key === t('user:address'))
							return null;

						return (
							<div className={componentClassName.field}>
								<Legend>{key}</Legend>
								<Input
									className={componentClassName.input}
									name={key}
									control={control}
									size='full'
									placeholder={value as string}
									placeholderClassName={componentClassName.placeholder}
								/>
							</div>
						);
					})}
				</div>
			));
		}

		if (textArray?.length) {
			return <div className={baseClassName}>
				{textArray.map(str => (
					<div className={componentClassName.field}>
						<Legend>{textArrayTitle}</Legend>
						<Input
							className={componentClassName.input}
							name={str}
							control={control}
							size='full'
							placeholder={str as string}
							placeholderClassName={componentClassName.placeholder}
						/>
					</div>
				))}
			</div>;
		}

		return null;
	};

	const renderedBody = renderBody();

	return renderedBody
		&& <>{renderedBody}</>;
};
