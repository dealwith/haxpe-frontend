import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';

import { filterValidateBeforeSubmitData } from 'utils';
import { IEditableComponentProps, IUser } from 'interfaces';

import { useEditableFields } from 'hooks';
import { Input, Legend } from 'components';
import { EditForm } from '../EditForm';

export const WorkerEditForm: FunctionComponent<IEditableComponentProps<IUser[]>> = ({
	handleClose,
	pageData,
	record,
}) => {
	const { control, handleSubmit: validateBeforeSubmit } = useForm();
	const { handleAddEditedFields } = useEditableFields();

	const handleSubmit = validateBeforeSubmit(data => {
		const filteredSubmitData = filterValidateBeforeSubmitData(data);
		const keys = Object.keys(filteredSubmitData);
		const workers = record.filter(({id}) => keys.includes(id));

		handleAddEditedFields({
			partnerWorkers: workers,
		});
		handleClose();
	});

	const renderWorkers = () => {
		if (record?.length) {
			return record.map(({ fullName, id }, i) => {
				const isChecked = !!(pageData?.find(worker => worker.id === id));

				return (
					<div key={id}>
						<Legend>{i + 1}</Legend>
						<Input
							name={id}
							placeholderTheme='white'
							type='checkbox'
							placeholder={fullName as string}
							defaultValue={isChecked}
							control={control}
						/>
					</div>
				);
			});
		}

		return null;
	};
	const renderedWorkers = renderWorkers();

	return <EditForm
		handleClose={handleClose}
		handleSubmit={handleSubmit}
	>
		{renderedWorkers}
	</EditForm>;
};
