import { FunctionComponent } from 'react';
import { useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { OrderService } from 'services';
import { ROUTES } from 'constants/index';
import { IStepProps } from 'components/Order/Worker/CurrentOrder/interfaces';

import {
	Button,
	Legend,
	Textarea,
	Container,
	ButtonsContainer,
} from 'components';

import './order-worker-cancel-reason-form.scss';

interface IForm {
	comment: string;
}

export const OrderWorkerCancelReasonForm: FunctionComponent<IStepProps> = ({
	orderId,
}) => {
	const { t } = useTranslation([
		'orderCustomer',
		'orderCancelReasonForm',
		'button',
		'input',
		'user',
	]);
	const {
		handleSubmit: validateBeforeSubmit,
		register,
	} = useForm<IForm>();
	const { push, goBack } = useHistory();

	const handleBackClick = () => goBack();

	const handleSubmit = async ({ comment }: IForm) => {
		const reason = comment;

		try {
			await OrderService.workerReject(orderId, { reason });
			push(ROUTES.HOME);
		} catch (err) {
			console.error(err);
		}
	};

	const baseClassName = 'order-worker-cancel-reason-form';
	const componentClassName = {
		buttonsWrapper: `${baseClassName}__buttons-wrapper`,
		textarea: `${baseClassName}__textarea`,
		input: `${baseClassName}__input`,
	};

	return (
		<Container
			maxWidth={380}
			isFitContent={true}
			theme='white'
		>
			<form onSubmit={validateBeforeSubmit(handleSubmit)}>
				<Legend theme='gray'>
					{t(`orderCustomer:cancelReason`)}
				</Legend>
				<Textarea
					size='full'
					theme='white'
					placeholder={t('input:comment')}
					className={componentClassName.textarea}
					{...register('comment')}
				/>
				<ButtonsContainer className={componentClassName.buttonsWrapper}>
					<Button
						theme='green'
						onClick={handleBackClick}
						rounded='xxl'
					>
						{t('button:back')}
					</Button>
					<Button
						theme='red'
						type='submit'
						rounded='xxl'
					>
						{t('button:cancelOrder')}
					</Button>
				</ButtonsContainer>
			</form>
		</Container>
	);
};
