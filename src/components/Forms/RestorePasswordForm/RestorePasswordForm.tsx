import { Dispatch, FunctionComponent, SetStateAction, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { AccountService } from 'services';

import { Button, Input, P, Legend } from 'components';

import './restore-password-form.scss';

type TProps = {
	setRestoreSuccess: Dispatch<SetStateAction<boolean>>;
}

interface IForm {
	email: string;
	confirmEmail: string;
}

export const RestorePasswordForm: FunctionComponent<TProps> = ({ setRestoreSuccess }) => {
	const { t } = useTranslation(['button', 'input', 'restorePasswordForm']);
	const {
		control,
		handleSubmit: validateBeforeSubmit,
		formState,
	} = useForm<IForm>();

	const handleSubmit = async ({ email }: IForm) => {
		try {
			await AccountService.setNewPassword(email);

			if (formState.isSubmitSuccessful)
				setRestoreSuccess(true);
		} catch (err) {
			console.warn(err);
		}
	};

	useEffect(() => {
		if (formState.isSubmitSuccessful)
			setRestoreSuccess(true);
	}, [formState.isSubmitSuccessful, setRestoreSuccess]);

	const componentClassName = 'restore-password-form';

	return (
		<form
			className={componentClassName}
			onSubmit={validateBeforeSubmit(handleSubmit)}
		>
			<Legend>
				{t('restorePasswordForm:forgotPassword')}
			</Legend>
			<Input
				name='email'
				type='email'
				icon='email'
				autoFocus={true}
				placeholder={t('input:email')}
				control={control}
				size='full'
			/>
			<Button
				type='submit'
				theme='green'
				size='full'
				rounded='m'
			>
				{t('button:resetPassword')}
			</Button>
			<P
				align='center'
				color='light-green'
				size='small'
				isBold={true}
			>
				{t('restorePasswordForm:receiveResetLink')}
			</P>
		</form>
	);
};
