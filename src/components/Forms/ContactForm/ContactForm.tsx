import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import {
	Legend,
	Input,
	Button,
	Textarea,
} from 'components';

interface IForm {
	fullName: string;
	email: string;
	phone: string;
	message?: string;
}

type TProps = {
	className: Record<string, string>;
	handleSubmit: () => void;
}

export const ContactForm: FunctionComponent<TProps> = ({ className, handleSubmit }) => {
	const { t } = useTranslation(['contactPage', 'input']);
	const { register, handleSubmit: validateBeforeSubmit, control } = useForm<IForm>();

	return (
		<form
			onSubmit={validateBeforeSubmit(handleSubmit)}
			className={className.form}
		>
			<fieldset className={className.fieldSet}>
				<Legend className={className.legend}>
					{t('contactPage:legend')}
				</Legend>
				<Input
					name='fullName'
					placeholder={t('input:fullName')}
					icon='person'
					size='full'
					labelClassName={className.label}
					rules={{ required: true }}
					control={control}
				/>
				<Input
					type='email'
					name='email'
					size='full'
					placeholder={t('input:email')}
					rules={{ required: true }}
					control={control}
					icon='email'
					labelClassName={className.label}
				/>
				<Input
					type='tel'
					name='phone'
					size='full'
					placeholder={t('input:phone')}
					rules={{ required: true }}
					control={control}
					icon='phone'
					labelClassName={className.label}
				/>
				<Textarea
					name='message'
					placeholder='...'
					{...register}
				/>
				<Button
					rounded='sm'
					className={className.button}
					type='submit'
				>
					{t('button:send')}
				</Button>
			</fieldset>
		</form>
	);
};
