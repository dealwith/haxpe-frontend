export interface IResetData {
	password: string;
	confirmPassword: string;
}
