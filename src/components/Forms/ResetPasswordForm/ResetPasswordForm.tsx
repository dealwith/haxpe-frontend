import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { AccountService } from 'services';
import { IResetData } from '../interfaces';

import { AdminButton, Input, Button } from 'components';

import './reset-password-form.scss';

export const ResetPasswordForm: FunctionComponent = () => {
	const { location } = useHistory();
	const isInAdmin = location.pathname.includes('admin');
	const { t } = useTranslation(['input', 'button']);
	const {
		formState: { errors },
		getValues,
		control,
		handleSubmit: hookFormHandleSubmit,
	} = useForm();

	const validate = {
		matchesPreviousPassword: (value: string) => {
			const { password } = getValues();

			return password === value
				|| t('input:passwordsShouldMatch') as string;
		},
	};

	const handleSubmit = async ({ password }: IResetData) => {
		try {
			await AccountService.resetPassword(password);
		} catch (err) {
			console.error(err);
		}

	};

	const baseClassName = 'reset-password-form';
	const componentClassName = {
		component: `${baseClassName}`,
		label: `${baseClassName}__label`,
		input: `${baseClassName}__input`,
	};

	return (
		<form
			className={componentClassName.component}
			onSubmit={hookFormHandleSubmit(handleSubmit)}>
			<Input
				name='password'
				control={control}
				type={'password'}
				rules={{validate}}
				placeholder={t('input:newPassword')}
				icon='lock'
				size='full'
				seeable={true}
				labelClassName={componentClassName.label}
				className={componentClassName.input}
				autoComplete='off'
			/>
			<Input
				name='confirmPassword'
				control={control}
				type={'password'}
				rules={{validate}}
				placeholder={t('input:confirmPassword')}
				icon='lock'
				size='full'
				seeable={true}
				labelClassName={componentClassName.label}
				className={componentClassName.input}
				autoComplete='off'
			/>
			{
				errors.confirmPassword
					&& <span>{errors.confirmPassword.message}</span>
			}
			{isInAdmin
				? <AdminButton
					disabled={!!errors.confirmPassword}
					fullWidth={false}
					label={t('button:save')}
					type='submit'
				/>
				: <Button
					isDisabled={!!errors.confirmPassword}
					size='full'
					theme='light-green'
					type='submit'
				>
					{t('button:save')}
				</Button>
			}
		</form>
	);
};
