import { ChangeEvent, FunctionComponent } from 'react';
import { required, SelectArrayInput, SelectInput, TextInput } from 'react-admin';
import { useForm } from 'react-final-form';
import { useTranslation } from 'react-i18next';

import { IWorkerCreateForm } from 'interfaces';

import './worker-create-form.scss';

export const WorkerCreateForm: FunctionComponent<IWorkerCreateForm & Record<string, unknown>> = ({
	setMultiServiceState,
	multiServiceState,
}) => {
	const form = useForm();
	const { t } = useTranslation(['industries', 'services', 'input']);

	const isIndustriesLoaded = !!multiServiceState.industries.length;
	const isServicesLoaded = !!multiServiceState.services.length;

	const handleServiceChange = (e: ChangeEvent<HTMLInputElement>) => {
		setMultiServiceState({selectedService: e.target});
	};
	const handleIndustryChange = (e: ChangeEvent<HTMLInputElement>) => {
		form.change('workerServiceTypes', null);
		setMultiServiceState({selectedIndustry: e.target});
		setMultiServiceState({selectedService: {}});
		setMultiServiceState({services: []});
	};

	const inputLabelProps = {
		style: {color: 'white'},
	};

	const baseClassName = 'worker-create-form';

	const componentClassName = {
		component: baseClassName,
		items: `${baseClassName}__items`,
	};

	//TODO add justify content: center for SaveButton
	return (
		<div className={componentClassName.component}>
			<SelectInput
				className={componentClassName.items}
				onChange={handleIndustryChange}
				source='industry'
				label={t('industries:notSelected')}
				choices={multiServiceState.industries}
				disabled={!isIndustriesLoaded}
				validate={required()}
				InputLabelProps={inputLabelProps}
			/>
			<SelectArrayInput
				className={componentClassName.items}
				onChange={handleServiceChange}
				source='workerServiceTypes'
				label={t('services:notSelected')}
				choices={multiServiceState.services}
				disabled={!isServicesLoaded}
				validate={required()}
				InputLabelProps={inputLabelProps}
			/>
			<TextInput
				source='firstName'
				label={t('input:firstName')}
				className={componentClassName.items}
				validate={required()}
				InputLabelProps={inputLabelProps}
			/>
			<TextInput
				source='lastName'
				label={t('input:lastName')}
				className={componentClassName.items}
				validate={required()}
				InputLabelProps={inputLabelProps}
			/>
			<TextInput
				source='email'
				label={t('input:email')}
				className={componentClassName.items}
				validate={required()}
				InputLabelProps={inputLabelProps}
			/>
			<TextInput
				source='phone'
				label={t('input:phone')}
				className={componentClassName.items}
				validate={required()}
				InputLabelProps={inputLabelProps}
			/>
		</div>
	);
};
