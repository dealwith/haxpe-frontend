export const clearPhoneNumber = (phoneNumber: string): string => phoneNumber.replaceAll(/[\s()-]/gi, '');
