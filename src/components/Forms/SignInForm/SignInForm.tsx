import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import { ROUTES } from 'constants/index';

import { useAuth } from 'hooks';
import { Fieldset, Legend, Input, Button } from 'components';
import { AccountError } from 'interfaces';

interface IForm {
	email: string;
	password: string;
}

type TProps = {
	className?: Record<string, string>;
}

export const SignInForm: FunctionComponent<TProps> = ({ className }) => {
	const {
		formState: {
			errors,
		},
		handleSubmit: validateBeforeSubmit,
		control,
		setError,
	} = useForm<IForm>();
	const auth = useAuth();
	const history = useHistory();
	const { t } = useTranslation([
		'signInForm',
		'button',
		'input',
		'errors',
	]);

	const handleSubmit = async (data: IForm) => {
		try {
			const signinRes = await auth.signin(data);

			if (signinRes?.isWorker)
				return history.push(ROUTES.ORDER.ACTIVE);

			history.push(ROUTES.ORDER.CUSTOMER);
		} catch (err) {
			if ((err as AccountError).message === 'accountInvalidUserNameOrPassword') {
				setError('password', {
					message: t('errors:accountInvalidUserNameOrPassword'),
					shouldFocus: true,
				});
			}
		}
	};

	return (
		<form
			onSubmit={validateBeforeSubmit(handleSubmit)}
			className={className.form}
		>
			<Fieldset className={className.fieldset}>
				<Legend>{t('legend')}</Legend>
				<Input
					type='email'
					name='email'
					placeholder={t('input:email')}
					icon='email'
					size='full'
					labelClassName={className.inputLabel}
					rules={{ required: true }}
					control={control}
				/>
				<Input
					type='password'
					name='password'
					placeholder={t('input:password')}
					icon='lock'
					size='full'
					labelClassName={className.inputLabel}
					rules={{ required: true }}
					control={control}
					errors={errors}
				/>
				<Button type='submit' theme='green' size='full'>
					{t('button:login')}
				</Button>
			</Fieldset>
		</form>
	);
};
