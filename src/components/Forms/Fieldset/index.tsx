import { ReactNode, FunctionComponent } from 'react';
import cn from 'classnames';

import './fieldset.css';

type TProps = {
	children: ReactNode,
	className?: string,
};

export const Fieldset: FunctionComponent<TProps> = ({
	children,
	className: propsClassName,
}) => {
	const componentClassName = cn('fieldset', propsClassName);

	return <fieldset className={componentClassName}>{children}</fieldset>;
};
