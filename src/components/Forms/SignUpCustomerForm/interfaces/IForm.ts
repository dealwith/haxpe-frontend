export interface IForm {
	firstName: string;
	lastName: string;
	email: string;
	phone: string;
	street: string;
	buildingNumber: string;
	zipCode: string;
	city: string;
	password: string;
	confirmPassword: string;
}
