import { FunctionComponent } from 'react';
import { useForm } from 'react-hook-form';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { EErrorCodes, EResponseStatus } from 'interfaces';
import { getGeocodingData, getLocaleStorageItem, isObjIncludesType } from 'utils';
import { AccountService, AddressService, CustomerService } from 'services';
import { ROUTES, MEDIA_POINT } from 'constants/index';
import { IAccountForm } from '../interfaces';
import { clearPhoneNumber } from '../utils';

import {
	Legend,
	Input,
	Button,
	GoogleButton,
	FacebookButton,
	P,
	AgreementText,
} from 'components';
import { useMediaQuery } from 'hooks';

type TProps = {
	setAuthSuccess: (isAuthSuccess: boolean) => void;
	className?: Record<string, string>;
}

export const SignUpCustomerForm: FunctionComponent<TProps> = ({
	setAuthSuccess,
	className,
}) => {
	const {
		handleSubmit: validateBeforeSubmit,
		formState: { errors },
		control,
		setError,
		getValues,
	} = useForm<IAccountForm>();
	const history = useHistory();
	const { t } = useTranslation([
		'input',
		'button',
		'signUp',
		'errors',
	]);
	const { isMatches: isMobile } = useMediaQuery({ maxWidth: MEDIA_POINT.MOBILE });

	const handleSubmit = async ({
		email,
		firstName,
		lastName,
		password,
		phone,
		street,
		buildingNumber,
		zipCode,
		city,
	}: Omit<IAccountForm, 'confirmPassword'>) => {
		const transformedPhone = clearPhoneNumber(phone);

		const preferLanguage = getLocaleStorageItem('i18nextLng');
		const raw = {
			email,
			firstName,
			lastName,
			password,
			phone: transformedPhone,
			preferLanguage,
		};

		try {
			const accountRegisterRes  = await AccountService.register(raw);

			if (accountRegisterRes?.status === EResponseStatus.success) {
				const addressRaw = {
					street,
					buildingNum: buildingNumber,
					city,
					country: '',
					lat: 0,
					lon: 0,
					zipCode,
					externalId: '',
				};

				const geocodingData = await getGeocodingData(street, buildingNumber, city, zipCode);

				const { lat, lng } = geocodingData.geometry.location;
				const country = geocodingData
					?.address_components
					?.find(address_components => isObjIncludesType(address_components, 'country'))
					?.long_name;

				addressRaw.lat = lat;
				addressRaw.lon = lng;
				addressRaw.country = country;
				addressRaw.externalId = geocodingData.place_id;

				const { body: { id: addressId } } = await AddressService.createAddress(addressRaw);

				const customerRaw = {
					addressId,
					userId: accountRegisterRes?.body?.id,
				};

				const customerResult = await CustomerService.createCustomer(customerRaw);

				if (customerResult.status === 'success') {
					setAuthSuccess(true);

					setTimeout(() => {
						history.push(ROUTES.SIGN_IN);
					}, 4000);
				}
			}
		} catch (e) {
			setAuthSuccess(false);

			if (e.message === EErrorCodes.accountDuplicateUserName) {
				setError(
					'email',
					{
						type: e.message,
						message: t('errors:accountDuplicateUserName'),
					},
					{ shouldFocus: true },
				);
			}
		}
	};

	const passwordsMismatchText = t('errors:passwordsMismatch');
	const buttonSize = isMobile ? 'full' : 'half';

	return (
		<form
			className={className.component}
			onSubmit={validateBeforeSubmit(handleSubmit)}
		>
			<Legend>{t('signUp:title')}</Legend>
			<div className={className.socialButtons}>
				<GoogleButton size={buttonSize} />
				<FacebookButton size={buttonSize} />
			</div>
			<div className={className.alternativeContainer}>
				<div className={className.alternativeLine} />
				<P className={className.alternativeText}>{t('signUp:or')}</P>
				<div className={className.alternativeLine} />
			</div>
			<div className={className.inputContainer}>
				<Input
					name='firstName'
					icon='person'
					size='full'
					placeholder={t('input:firstName')}
					rules={{ required: true }}
					control={control}
				/>
				<Input
					name='lastName'
					icon='person'
					size='full'
					placeholder={t('input:lastName')}
					rules={{ required: true }}
					control={control}
				/>
				<div className='input-wrapper'>
					<Input
						type='email'
						name='email'
						icon='email'
						size='full'
						placeholder={t('input:email')}
						rules={{ required: true }}
						control={control}
					/>
					{errors?.email?.type
						&& <p className='error-text'>
							{errors?.email?.message}
						</p>
					}
				</div>
				<Input
					type='tel'
					name='phone'
					icon='phone'
					size='full'
					placeholder={t('input:phone')}
					control={control}
					rules={{ required: true }}
				/>
				<div className='input-wrapper'>
					<Input
						name='street'
						icon='target'
						size='full'
						placeholder={t('input:street')}
						control={control}
						rules={{ required: true }}
					/>
				</div>
				<div className='input-wrapper'>
					<Input
						name='buildingNumber'
						icon='target'
						size='full'
						placeholder={t('input:buildingNum')}
						control={control}
						rules={{ required: true }}
					/>
				</div>
				<div className='input-wrapper'>
					<Input
						name='zipCode'
						icon='target'
						size='full'
						placeholder={t('input:zipCode')}
						control={control}
						rules={{ required: true }}
					/>
				</div>
				<div className='input-wrapper'>
					<Input
						name='city'
						icon='target'
						size='full'
						placeholder={t('input:city')}
						control={control}
						rules={{ required: true }}
					/>
				</div>
				<Input
					type='password'
					name='password'
					icon='lock'
					size='full'
					placeholder={t('input:password')}
					control={control}
					rules={{ required: true }}
				/>
				<div className='input-wrapper'>
					<Input
						type='password'
						name='confirmPassword'
						icon='lock'
						size='full'
						placeholder={t('input:confirmPassword')}
						control={control}
						rules={{
							required: true,
							validate: v => v === getValues('password')
								|| passwordsMismatchText,
						}}
					/>
					{errors?.confirmPassword?.type === 'validate'
						&& <p className='error-text'>
							{errors?.confirmPassword?.message}
						</p>
					}
				</div>
				<Button
					type='submit'
					theme='green'
					size='full'
				>
					{t('button:register')}
				</Button>
				<div>
					<AgreementText />
				</div>
			</div>
		</form>
	);
};
