import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { P } from 'components';

type TProps = {
	className?: string;
	isSubTitle?: boolean;
}

export const SignUpPartnerFormTitle: FunctionComponent<TProps> = ({
	className: propsClassName,
	isSubTitle = true,
}) => {
	const { t } = useTranslation('signUp');

	return (
		<div {...(propsClassName && { className: propsClassName })}>
			<div className='sign-up-partner__title'>
				<P isBold={true}>{t('title')}</P>
			</div>
			{isSubTitle
				&& <div className='sign-up-partner__sub-title'>
					<P>{t('subTitle')}</P>
				</div>
			}
		</div>
	);
};
