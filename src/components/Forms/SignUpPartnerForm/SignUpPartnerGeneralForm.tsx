import { FunctionComponent, useEffect } from 'react';
import { Control } from 'react-hook-form';
import { StepComponentProps } from 'react-step-builder';
import { useTranslation } from 'react-i18next';

import { isObjectWithProperties } from 'utils';
import { IndustryService } from 'services';
import { ISignUpPartnerForm, TExtendedIndustryOption, TSelectOption } from 'interfaces';

import { useMultiState } from 'hooks';
import { Input, Container, P, Select, InputDropzone, Button } from 'components';
import { SignUpPartnerFormTitle } from './SignUpPartnerFormTitle';

import './sign-up-partner-general-form.scss';

type TProps = {
	control: Control<ISignUpPartnerForm>;
} & StepComponentProps;

interface IIndustryState {
	industries: TExtendedIndustryOption[];
	selectedIndustry: TExtendedIndustryOption | Record<string, string>,
}

export const SignUpPartnerGeneralForm: FunctionComponent<TProps> = ({
	control,
	next,
}) => {
	const { t } = useTranslation(['input', 'signUp', 'industries']);
	const handleClick = () => next();

	const [industryState, setIndustryState] = useMultiState<IIndustryState>({
		industries: [],
		selectedIndustry: {},
	});

	useEffect(() => {
		const fetchData = async () => {
			const industries = await IndustryService.getAll();

			if (industries.status === 'success') {
				const transformedIndustries = industries
					.body
					.map(({ id, key }) => {
						const text = t(`industries:${key}`);

						return {
							label: text,
							value: text.toLowerCase() as unknown as number,
							industryId: id,
						};
					});

				setIndustryState({ industries: transformedIndustries });
			}
		};

		fetchData();
	}, []);

	const handleSelectChange = (value: TSelectOption) => setIndustryState({ selectedIndustry: value });

	const industryValue = isObjectWithProperties(industryState.selectedIndustry)
		? industryState.selectedIndustry
		: null;
	const isIndustriesLoaded = !!industryState.industries?.length;

	const baseClassName = 'sign-up-partner-general-form';
	const componentClassName = {
		component: baseClassName,
		column: `${baseClassName}__column`,
		inputWrapper: `${baseClassName}__input-wrapper`,
		section: `${baseClassName}__section`,
		sectionTitle: `${baseClassName}__section-title`,
		continueButton: `${baseClassName}__coninue-button`,
	};

	return (
		<Container
			theme='white'
			maxWidth={780}
		>
			<SignUpPartnerFormTitle />
			<Container
				isInner={true}
				flow='vertical'
				className={componentClassName.component}
			>
				<div className={componentClassName.section}>
					<div className={componentClassName.column}>
						<P
							isBold={true}
							transform='uppercase'
							className={componentClassName.sectionTitle}
							align='center'
						>
							{t('signUp:companyData')}
						</P>
						<div className={componentClassName.inputWrapper}>
							<Input
								icon='house'
								name='companyName'
								placeholder={t('input:companyName')}
								control={control}
								size='full'
							/>
							<Input
								icon='email'
								name='email'
								placeholder={t('input:email')}
								control={control}
								size='full'
							/>
							<Input
								name='street'
								icon='target'
								placeholder={t('input:street')}
								control={control}
								size='full'
							/>
							<Input
								name='buildingNum'
								icon='target'
								control={control}
								placeholder={t('input:buildingNum')}
								size='full'
							/>
						</div>
					</div>
					<div className={componentClassName.column}>
						<P
							isBold={true}
							transform='uppercase'
							className={componentClassName.sectionTitle}
						>
							{t('signUp:contactPerson')}
						</P>
						<div className={componentClassName.inputWrapper}>
							<Input
								name='firstName'
								icon='person'
								control={control}
								placeholder={t('input:firstName')}
								size='full'
							/>
							<Input
								name='lastName'
								icon='person'
								control={control}
								placeholder={t('input:lastName')}
								size='full'
							/>
							<Input
								name='phone'
								icon='phone'
								control={control}
								placeholder={t('input:phone')}
								size='full'
							/>
							<Input
								name='zipCode'
								icon='target'
								control={control}
								placeholder={t('input:zipCode')}
								size='full'
							/>
							<Input
								name='city'
								icon='target'
								control={control}
								placeholder={t('input:city')}
								size='full'
							/>
						</div>
					</div>
				</div>
				<div className={componentClassName.section}>
					<div className={componentClassName.column}>
						<P
							align='left'
							isBold={true}
							transform='uppercase'
							className={componentClassName.sectionTitle}
						>
							{t('signUp:companyDetails')}
						</P>
						<div className={componentClassName.section}>
							<div className={componentClassName.column}>
								{isIndustriesLoaded
									&& <Select
										name='industries'
										value={industryValue as TSelectOption}
										control={control}
										isMultiple={true}
										items={industryState.industries}
										handleChange={handleSelectChange}
										placeholder={t('input:enterIndustries')}
									/>
								}
							</div>
							<div className={componentClassName.column}>
								<Input
									type='number'
									name='craftsmanAmount'
									control={control}
									icon='persons'
									placeholder={t('input:craftsmanAmount')}
									size='full'
								/>
							</div>
						</div>
					</div>
				</div>
				<InputDropzone />
				<Button
					theme='transparent'
					icon='arrow'
					onClick={handleClick}
					iconPosition='right'
					className={componentClassName.continueButton}
				>
					Continue
				</Button>
			</Container>
		</Container>
	);
};
