import { FunctionComponent } from 'react';
import { Control } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { ISignUpPartnerForm } from 'interfaces';

import { Input, Button, AgreementText, Container } from 'components';
import { SignUpPartnerFormTitle } from '../SignUpPartnerFormTitle';

import './sign-up-partner-password-form.scss';

type TProps = {
	control: Control<ISignUpPartnerForm>;
}

export const SignUpPartnerPasswordForm: FunctionComponent<TProps> = ({ control }) => {
	const { t } = useTranslation(['input', 'buttton', 'signUp']);

	const baseClassName = 'sign-up-partner-password-form';
	const componentClassName = {
		component: baseClassName,
		container: `${baseClassName}-container`,
	};

	return (
		<Container
			theme='white'
			maxWidth={380}
			height={600}
			className={componentClassName.container}
			flow='vertical'
		>
			<SignUpPartnerFormTitle />
			<div className={componentClassName.component}>
				<Input
					name='password'
					icon='lock'
					type='password'
					control={control}
					placeholder={t('input:password')}
					size='full'
					rules={{ required: true }}
				/>
				<Input
					name='confirmPassword'
					type='password'
					icon='lock'
					size='full'
					control={control}
					placeholder={t('input:confirmPassword')}
					rules={{ required: true }}
				/>
				<Button type='submit' size='full' theme='green'>
					{t('button:register')}
				</Button>
				<AgreementText />
			</div>
		</Container>
	);
};
