import {
	Dispatch,
	FunctionComponent,
	SetStateAction,
	useEffect,
} from 'react';

import { Container, SignUpPartnerFormTitle, P } from 'components';
import { ReactComponent as MailIcon } from './mailIcon.svg';

import './sign-up-partner-successful-form.scss';

type TProps = {
	setSend: Dispatch<SetStateAction<boolean>>;
}

export const SignUpPartnerSuccessfulForm: FunctionComponent<TProps> = ({ setSend }) => {
	const baseClassName = 'sign-up-partner-successful-form';
	const componentClassName = {
		component: baseClassName,
		icon: `${baseClassName}__icon`,
		description: `${baseClassName}__description`,
	};

	useEffect(() => {
		setTimeout(() => {
			setSend(false);
		}, 3000);
	}, [setSend]);

	return (
		<Container
			theme='white'
			maxWidth={380}
			height={600}
		>
			<SignUpPartnerFormTitle isSubTitle={false} />
			<div className={componentClassName.component}>
				<P isBold={true}>
					Thank you for your registration!
				</P>
				<MailIcon className={componentClassName.icon} />
				<div className={componentClassName.description}>
					<P>We have sent you an email.</P>
					<P isBold={true}>Please check your inbox and confirm your email address.</P>
				</div>
			</div>
		</Container>
	);
};
