import { FunctionComponent, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { AllSteps, GetState, JumpFn, StepComponentProps } from 'react-step-builder';
import { CardElement, useElements, useStripe } from '@stripe/react-stripe-js';

import { TOrderCustomerPaymentSteps } from 'interfaces/order/customer';
import { jumpToCorrectStep } from 'components/Order/Customer/utils';
import { OrderService } from 'services';
import { EOrderStatus, EResponseStatus } from 'interfaces';

import { Button, ButtonsContainer } from 'components';

import './card-checkout-form.scss';

type TProps = {
	orderCustomerGetState: GetState,
	orderCustomerAllSteps: AllSteps,
	orderCustomerJump: JumpFn,
} & StepComponentProps;

export const CardCheckoutForm: FunctionComponent<TProps> = ({
	allSteps,
	jump,
	orderCustomerGetState,
	orderCustomerAllSteps,
	orderCustomerJump,
}) => {
	const { orderId } = orderCustomerGetState('order', '');
	const [succeeded, setSucceeded] = useState(false);
	const [error, setError] = useState(null);
	const [processing, setProcessing] = useState(false);
	const [disabled, setDisabled] = useState(true);
	const [clientSecret, setClientSecret] = useState('');
	const { t } = useTranslation(['button']);
	const stripe = useStripe();
	const elements = useElements();

	useEffect(() => {
		const fetchClientSecret = async () => {
			const paymentStartRes = await OrderService.paymentStart(orderId);

			if (paymentStartRes?.status === EResponseStatus.success)
				setClientSecret(paymentStartRes.body.stripeKey);
		};

		fetchClientSecret();
	}, [orderId]);

	const handleChange = async event => {
		setDisabled(event.empty);
		setError(event.error ? event.error.message : '');
	};

	const cardStyle = {
		style: {
			base: {
				color: '#32325d',
				fontFamily: 'Arial, sans-serif',
				fontSmoothing: 'antialiased',
				fontSize: '16px',
				'::placeholder': {
					color: '#32325d',
				},
			},
			invalid: {
				color: '#fa755a',
				iconColor: '#fa755a',
			},
		},
	};

	const handleBackClick = async () => {
		jumpToCorrectStep<TOrderCustomerPaymentSteps>(
			'paymentMethods',
			jump,
			allSteps,
		);
	};

	const handleSubmit = async ev => {
		ev.preventDefault();
		setProcessing(true);

		const payload = await stripe.confirmCardPayment(clientSecret, {
			payment_method: {
				card: elements.getElement(CardElement),
			},
		});

		if (payload.paymentIntent.id) {
			return jumpToCorrectStep(
				EOrderStatus.completed,
				orderCustomerJump,
				orderCustomerAllSteps,
			);
		}

		if (payload.error) {
			setError(`Payment failed ${payload.error.message}`);
			setProcessing(false);
		}
	};

	const baseClassName = 'card-checkout-form';
	const componentClassName = {
		component: baseClassName,
		buttonsContainer: `${baseClassName}__buttons-container`,
	};

	return (
		<form
			onSubmit={handleSubmit}
			className={componentClassName.component}
		>
			<CardElement
				id='card-element'
				options={cardStyle}
				onChange={handleChange}
			/>
			{error && (
				<div className='card-error' role='alert'>
					{error}
				</div>
			)}
			<ButtonsContainer className={componentClassName.buttonsContainer}>
				<Button
					theme='gray'
					onClick={handleBackClick}
					rounded='xxl'
				>
					{t('button:back')}
				</Button>
				<Button
					type='submit'
					isDisabled={processing || disabled || succeeded}
					theme='green'
					rounded='xxl'
				>
					{processing
						? 'Processing'
						: 'Pay now'
					}
				</Button>
			</ButtonsContainer>
		</form>
	);
};
