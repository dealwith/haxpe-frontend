import { FunctionComponent, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { getGeocodingData, getLocaleStorageItem, isObjIncludesType } from 'utils';
import { AccountService, AddressService } from 'services';
import { IUpdateData } from 'services/account/interfaces';
import { IAccountForm } from '../interfaces';
import { clearPhoneNumber } from '../utils';

import { Input, Button } from 'components';

import './account-details-form.scss';

export const AccountDetailsForm: FunctionComponent = () => {
	const { control, handleSubmit: hookFormHandleSubmit } = useForm();
	const { t } = useTranslation(['input', 'button']);
	const [isDisabled, setIsDisabled] = useState(true);

	const required = {
		required: true,
	};

	const handleSubmit = async ({
		email,
		firstName,
		lastName,
		phone,
		street,
		buildingNumber,
		zipCode,
		city,
	}: Omit<IAccountForm, 'password' | 'confirmPassword'>) => {
		const transformedPhone = clearPhoneNumber(phone);
		const preferLanguage = getLocaleStorageItem('i18nextLng');

		const raw: IUpdateData = {
			email,
			name: firstName,
			surname: lastName,
			phoneNumber: transformedPhone,
			preferLanguage,
		};

		try {
			await AccountService.updateAccount(raw);

			const addressRaw = {
				street,
				buildingNum: buildingNumber,
				city,
				country: '',
				lat: 0,
				lon: 0,
				zipCode,
				externalId: '',
			};

			const geocodingData = await getGeocodingData(street, buildingNumber, city, zipCode);

			const { lat, lng } = geocodingData.geometry.location;
			const country = geocodingData
				?.address_components
				?.find(address_components => isObjIncludesType(address_components, 'country'))
				?.long_name;

			addressRaw.lat = lat;
			addressRaw.lon = lng;
			addressRaw.country = country;
			addressRaw.externalId = geocodingData.place_id;

			await AddressService.updateAddress(addressRaw);
		} catch (err) {
			console.error(err);
		}
	};

	const toggleInputDisable = () => setIsDisabled(isDisabled => !isDisabled);

	const baseClassName = 'account-details-form';

	return (
		<form
			className={baseClassName}
			onSubmit={hookFormHandleSubmit(handleSubmit)}
		>
			<Input
				isDisabled={isDisabled}
				name='firstName'
				icon='person'
				size='full'
				placeholder={t('input:firstName')}
				rules={required}
				control={control}
			/>
			<Input
				isDisabled={isDisabled}
				name='lastName'
				icon='person'
				size='full'
				placeholder={t('input:lastName')}
				rules={required}
				control={control}
			/>
			<Input
				isDisabled={isDisabled}
				type='email'
				name='email'
				icon='email'
				size='full'
				placeholder={t('input:email')}
				rules={required}
				control={control}
			/>
			<Input
				isDisabled={isDisabled}
				name='street'
				icon='target'
				size='full'
				placeholder={t('input:street')}
				control={control}
				rules={required}
			/>
			<Input
				isDisabled={isDisabled}
				name='buildingNumber'
				icon='target'
				size='full'
				placeholder={t('input:buildingNum')}
				control={control}
				rules={required}
			/>
			<Input
				isDisabled={isDisabled}
				name='zipCode'
				icon='target'
				size='full'
				placeholder={t('input:zipCode')}
				control={control}
				rules={required}
			/>
			<Input
				isDisabled={isDisabled}
				name='city'
				icon='target'
				size='full'
				placeholder={t('input:city')}
				control={control}
				rules={required}
			/>
			<Input
				isDisabled={isDisabled}
				type='tel'
				name='phone'
				icon='phone'
				size='full'
				placeholder={t('input:phone')}
				control={control}
				rules={required}
			/>
			<Button
				onClick={toggleInputDisable}
				size='full'
				theme='dusty-gray'
			>
				{t('button:changeAccountDetails')}
			</Button>
			<Button
				type='submit'
				isDisabled={isDisabled}
				size='full'
				theme='light-green'
			>
				{t('button:save')}
			</Button>
		</form>
	);
};
