import { FunctionComponent, ReactNode } from 'react';
import cn from 'classnames';

import { TAlignText } from 'interfaces';
import { TTheme } from './interfaces';

import './legend.scss';

type TProps = {
	chlidren?: ReactNode;
	className?: string;
	theme?: TTheme;
	align?: TAlignText;
	isDefaultClassName?: boolean;
};

export const Legend: FunctionComponent<TProps> = ({
	theme,
	children,
	className: propsClassName,
	align = 'center',
	isDefaultClassName = true,
}) => {
	const baseClassName = 'legend';
	const componentClassName = cn(
		baseClassName,
		propsClassName,
		{
			[`${baseClassName}--theme-${theme}`]: theme,
			[`${baseClassName}--align-${align}`]: align,
			[`${baseClassName}--default`]: isDefaultClassName,
		},
	);

	return <legend className={componentClassName}>{children}</legend>;
};
