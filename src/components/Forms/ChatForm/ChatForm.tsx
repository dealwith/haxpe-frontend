import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { Button, Textarea } from 'components';

export const ChatForm: FunctionComponent = () => {
	const { t } = useTranslation('button');

	const baseClassName = 'chat-form';
	const componentClassName = {
		component: baseClassName,
		textArea: `${baseClassName}__text-area`,
	};

	return (
		<form className={componentClassName.component}>
			<Textarea
				size='full'
				className={componentClassName.textArea}
				name='message'
			/>
			<Button
				size='full'
				type='submit'
				theme='light-green'
				rounded='m'
			>
				{t('button:send')}
			</Button>
		</form>
	);
};
