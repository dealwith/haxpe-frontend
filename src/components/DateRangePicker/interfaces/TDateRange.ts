export type TDateRange = {
	rangeStartDate: Date;
	rangeEndDate: Date;
}
