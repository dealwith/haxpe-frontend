import { EFixedDate } from './EFixedDate';

export type TStaticRangeCreator = {
	label: string;
	fixedDate: EFixedDate;
}
