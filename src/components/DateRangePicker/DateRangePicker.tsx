import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import {
	DateRangePicker as ReactDateRangePicker,
	Range,
	OnChangeProps,
	DefinedRange,
} from 'react-date-range';

import { EFixedDate, EPosition, TStaticRangeCreator } from './interfaces';
import { setDate } from './utils';

import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';

type TProps = {
	state: Range[];
	handleChange: (value: OnChangeProps) => void;
}

export const DateRangePicker: FunctionComponent<TProps> = ({ state, handleChange }) => {
	const { t } = useTranslation('dateRange');

	const { startDate } = state[0];
	const year = startDate.getFullYear();
	const month = startDate.getMonth();
	const day = startDate.getDate();
	const maxDayInMonth = 31;
	const maxDate = new Date(year, month, day + maxDayInMonth);
	const minDate = new Date(year, month, day - maxDayInMonth);

	const handleRenderStaticRangeLabel = ({ label }: DefinedRange) => <>{label}</>;

	const staticRangeCreator = ({ label, fixedDate }: TStaticRangeCreator) => {
		const {rangeStartDate, rangeEndDate} = setDate(fixedDate);

		return {
			label,
			hasCustomRendering: true,
			range: () => ({
				startDate: rangeStartDate,
				endDate: rangeEndDate,
			}),
			isSelected() {
				return false;
			},
		};
	};

	return (
		<ReactDateRangePicker
			showSelectionPreview={true}
			moveRangeOnFirstSelection={false}
			months={1}
			ranges={state}
			direction={EPosition.VERTICAL}
			onChange={handleChange}
			renderStaticRangeLabel={handleRenderStaticRangeLabel}
			maxDate={maxDate}
			minDate={minDate}
			staticRanges={[
				staticRangeCreator({ label: t('day'), fixedDate: EFixedDate.DAY}),
				staticRangeCreator({ label: t('week'), fixedDate: EFixedDate.WEEK}),
				staticRangeCreator({ label: t('month'), fixedDate: EFixedDate.MONTH }),
				staticRangeCreator({ label: t('year'), fixedDate: EFixedDate.YEAR }),
			]}
			showMonthArrow={false}
			inputRanges={[]}
			showPreview={false}
		/>
	);
};
