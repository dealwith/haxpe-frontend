import { ModifiedDate } from 'utils';
import { EFixedDate, TDateRange } from '../interfaces';

const changeWeek = ({ rangeStartDate, rangeEndDate }: TDateRange) => {
	let currentDay = rangeStartDate.getDay();

	if (!currentDay)
		currentDay = 6;

	const weekStart = rangeStartDate.getDate() - currentDay;

	rangeStartDate.setDate(weekStart);
	rangeEndDate.setDate(weekStart + 6);

	return {rangeEndDate, rangeStartDate};
};

const changeMonth = ({ rangeStartDate, rangeEndDate }: TDateRange) => {
	const { lastDayInMonth, firstDayInMonth } = new ModifiedDate();

	rangeStartDate.setDate(firstDayInMonth);
	rangeEndDate.setDate(lastDayInMonth);

	return {rangeEndDate, rangeStartDate};
};

const changeYear = ({ rangeStartDate, rangeEndDate }: TDateRange) => {
	rangeStartDate.setMonth(0);
	rangeStartDate.setDate(1);
	rangeEndDate.setMonth(11);
	rangeEndDate.setDate(31);

	return {rangeEndDate, rangeStartDate};
};

export const setDate = (fixedDate: EFixedDate): TDateRange  => {
	const rangeStartDate = new Date();
	const rangeEndDate = new Date();

	const date = {rangeStartDate, rangeEndDate};

	switch (fixedDate) {
		case EFixedDate.WEEK:
			return changeWeek(date);

		case EFixedDate.MONTH:
			return changeMonth(date);

		case EFixedDate.YEAR:
			return changeYear(date);
		default:
			return date;
	}
};
