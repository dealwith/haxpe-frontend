import cn from 'classnames';
import { ReactNode, FunctionComponent, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Location } from 'history';

import { ROUTES } from 'constants/index';
import { useSocket } from 'hooks';

import './main.css';

type TProps = {
	children: ReactNode;
}

export const Main: FunctionComponent<TProps> = ({ children }) => {
	const { pathname } = useLocation<Location>();
	const { push } = useHistory();
	const { orderOffer } = useSocket();

	useEffect(() => {
		if (orderOffer)
			push(ROUTES.ORDER.ACTIVE);
	}, [orderOffer, push]);

	const whitePages = [
		ROUTES.ABOUT_US,
		ROUTES.SERVICES,
		ROUTES.SERVICES,
		ROUTES.PRICING,
	];

	const baseClassName = 'main';
	const componentClassName = cn(baseClassName, {
		[`${baseClassName}--theme-white`]: whitePages.some(page => page === pathname),
	});

	return (
		<main className={componentClassName}>
			{children}
		</main>
	);
};
