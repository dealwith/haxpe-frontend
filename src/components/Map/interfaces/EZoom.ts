export enum EZoom {
	world = 1,
	landmassContinent = 5,
	city = 10,
	streets = 15,
	buildings = 20,
}
