import { FunctionComponent } from 'react';
import GoogleMapReact from 'google-map-react';

import { EZoom } from './interfaces';

import { ReactComponent as OvalIcon } from './oval.svg';

import './map.css';

type TProps = {
	coords: { lat: number, lon: number; };
	zoom?: EZoom;
}

const urlKeys = { key: process.env.REACT_APP_GOOGLE_MAP_API_KEY };

export const Map: FunctionComponent<TProps> = ({ coords, zoom = EZoom.streets }) => {
	const centerProps = Object.values(coords);

	const { lat, lon } = coords;

	const baseClassName = 'map';

	return (
		<div className={baseClassName}>
			<GoogleMapReact
				center={centerProps}
				bootstrapURLKeys={urlKeys}
				zoom={zoom}
			>
				{/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */}
				{/*@ts-ignore*/}
				<OvalIcon lat={lat} lng={lon} />
			</GoogleMapReact>
		</div>
	);
};
