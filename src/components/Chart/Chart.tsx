import { FunctionComponent, ReactNode } from 'react';
import { CartesianGrid, Line, LineChart, Tooltip, XAxis, YAxis } from 'recharts';
import cn from 'classnames';

import { ModifiedDate } from 'utils';

import { Title } from 'components';

import './chart.scss';

type TChartItem = {
	creationDate: string;
	count: number;
}

type TProps = {
	chartData: TChartItem[];
	title: string;
	isYearRange: boolean;
	children?: ReactNode;
	className?: string;
}

export const Chart: FunctionComponent<TProps> = ({
	chartData,
	title,
	children,
	isYearRange,
	className: propsClassName,
}) => {

	const baseClassName = 'chart';
	const componentClassName = cn(baseClassName, propsClassName);

	const xAxisHandler = ({ creationDate }) => {
		const { month, dayInMonth } = new ModifiedDate(creationDate);

		if (isYearRange)
			return month;

		return dayInMonth;
	};

	return (
		<div className={componentClassName}>
			<Title
				px='14'
				color='white'
			>
				{title}
			</Title>
			<LineChart
				width={400}
				height={170}
				data={chartData}
				margin = {{left: -30, right: 30, top: 20}}
			>
				<CartesianGrid
					vertical={false}
				/>
				<XAxis dataKey={xAxisHandler} />
				<YAxis height={1000} />
				<Tooltip />
				<Line
					type='monotone'
					dataKey='count'
					stroke='#31C963' />
			</LineChart>
			{children}
		</div>
	);
};
