import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { ROUTES } from 'constants/index';

import { Logo, P, Link, SocialLinks } from 'components';

import './footer.scss';
import { ModifiedDate } from 'utils';

export const Footer: FunctionComponent = () => {
	const { t } = useTranslation(['links', 'footer']);

	const { year } = new ModifiedDate();

	const emailLink = 'info@haxpe.com';

	const baseClassName = 'footer';
	const componentClassName = {
		component: baseClassName,
		description: `${baseClassName}__description`,
		copyright: `${baseClassName}__copyright`,
		address: `${baseClassName}__address`,
		links: `${baseClassName}__links`,
		link: `${baseClassName}__link`,
	};

	return (
		<footer className={baseClassName}>
			<Link isInner={true} href={ROUTES.HOME}>
				<Logo isWhite={true} />
			</Link>
			<SocialLinks />
			<address className={componentClassName.address}>
				<P
					className={componentClassName.description}
					size='small'
					color='white'
					align='center'
				>
					Lerchenauer Straße 156 <br />
					80935 München <br />
					<Link
						size='small'
						isWhite={true}
						href={emailLink}
						hrefType='email'
						target='_blank'
					>
						{emailLink}
					</Link>
				</P>
				<div className={componentClassName.links}>
					<Link
						className={componentClassName.link}
						isWhite={true}
						isInner={true}
						size='small'
						href={ROUTES.IMPRINT}
					>
						{t('links:imprint')}
					</Link>
					<Link
						className={componentClassName.link}
						isWhite={true}
						isInner={true}
						size='small'
						href={ROUTES.PRIVACY_POLICY}
					>
						{t('links:privacyPolicy')}
					</Link>
				</div>
				<P color='white' size='small' className={componentClassName.copyright}>
					© {year} | Haxpe UG (haftungsbeschränkt)
				</P>
			</address>
		</footer>
	);
};
