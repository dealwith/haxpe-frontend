import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { OrderService } from 'services';

import { Button, Span } from 'components';

import './send-arrival-time.scss';

type TProps = {
	orderId: string;
	handleCancelSendArrivalTime: () => void;
	handleIsSendedArrivalTime: () => void;
	timeToSend: string;
	timeToShow: string;
}

export const SendArrivalTime: FunctionComponent<TProps> = ({
	handleCancelSendArrivalTime,
	orderId,
	timeToSend,
	timeToShow,
	handleIsSendedArrivalTime,
}) => {
	const { t } = useTranslation('button');

	const handleSend = async () => {
		try {
			await OrderService.addArrivalTime({
				expectedArrival: timeToSend,
				orderId,
			});
			handleIsSendedArrivalTime();
		} catch (err) {
			console.error(err);
		}
	};

	const baseClassName = 'send-arrival-time';
	const componentClassName = {
		component: baseClassName,
		sendButton: `${baseClassName}__send-button`,
		cancelButton: `${baseClassName}__cancel-button`,
	};

	return (
		<div className={componentClassName.component}>
			<Button
				className={componentClassName.cancelButton}
				rounded='half'
				theme='red'
				size='auto'
				onClick={handleCancelSendArrivalTime}
			>
				X
			</Button>
			<Button
				className={componentClassName.sendButton}
				isText={false}
				size='full'
				rounded='xxl'
				theme='green'
				onClick={handleSend}
			>
				<Span>
					{t('button:send')}
				</Span>
				<Span>
					{timeToShow}
				</Span>
			</Button>
		</div>
	);
};
