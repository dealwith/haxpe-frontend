import { FunctionComponent, useState } from 'react';
import { useTranslation } from 'react-i18next';
import moment, { Moment } from 'moment';
import RCTimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';

import { TButtonProps } from '../Button/interfaces';

import { useLocalTimeFormat } from 'hooks';
import { SendArrivalTime } from './SendArrivalTime';
import { Button } from '../Button';

import './time-picker.scss';

type TProps = {
	buttonProps?: TButtonProps;
	orderId: string;
}

export const TimePicker: FunctionComponent<TProps> = ({
	buttonProps,
	orderId,
}) => {
	const { t } = useTranslation('button');
	const [isVisible, setIsVisible] = useState(false);
	const [timeToSend, setTimeToSend] = useState('');
	const [timeToShow, setTimeToShow] = useState('');
	const [isOpen, setIsOpen] = useState(false);
	const [isSendedArrivalTime, setIsSendedArrivalTime] = useState(false);
	const { format, use12Hours, now } = useLocalTimeFormat();

	const handleClick = () => setIsVisible(isVisible => !isVisible);

	const handleOpen = () => setIsOpen(true);

	const handleClose = () => setIsOpen(false);

	const handleCancelSendArrivalTime = () => setTimeToSend('');

	const handleChange = (value: Moment) => {
		if (!value)
			return;

		let date = moment(value).format();

		const isTomorrowDate = moment(now).isAfter(date);

		if (isTomorrowDate)
			date = moment(date).add(1, 'days').format();

		setTimeToShow(value.format(format));
		setTimeToSend(date);
	};

	const handleIsSendedArrivalTime = () => setIsSendedArrivalTime(true);

	const baseClassName = 'time-picker';
	const componentClassName = {
		component: baseClassName,
		buttonsWrapper: `${baseClassName}__buttons-wrapper`,
		sendButton: `${baseClassName}__send-button`,
		cancelButton: `${baseClassName}__cancel-button`,
		timePicker: `${baseClassName}__time-picker`,
	};

	return !isSendedArrivalTime
		&& (
			<div className={componentClassName.component}>
				{(timeToSend && !isOpen)
					? <SendArrivalTime
						handleIsSendedArrivalTime={handleIsSendedArrivalTime}
						handleCancelSendArrivalTime={handleCancelSendArrivalTime}
						orderId={orderId}
						timeToSend={timeToSend}
						timeToShow={timeToShow}
					/>
					: <div className={componentClassName.buttonsWrapper}>
						{isVisible
							? <Button
								{...(buttonProps && {...buttonProps})}
								theme='gray'
								size='auto'
								rounded='half'
								onClick={handleClick}
							>
								Х
							</Button>
							: <Button
								{...(buttonProps && {...buttonProps})}
								theme='gray'
								rounded='xxl'
								onClick={handleClick}
							>
								{t('button:communicateEstimatedArrival')}
							</Button>
						}
						{isVisible
							&& <RCTimePicker
								className={componentClassName.timePicker}
								open={isOpen}
								onOpen={handleOpen}
								onClose={handleClose}
								showSecond={false}
								defaultValue={now}
								onChange={handleChange}
								inputReadOnly
								format={format}
								use12Hours={use12Hours}
							/>
						}
					</div>
				}
			</div>
		);
};
