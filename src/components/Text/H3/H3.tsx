import { ReactNode, FunctionComponent } from 'react';
import cn from 'classnames';

import { TAlignText, TColor } from 'interfaces';

import './h3.scss';

type TProps = {
	className?: string;
	align?: TAlignText;
	color?: TColor;
	children: ReactNode;
};

export const H3: FunctionComponent<TProps> = ({
	className: propsClassName,
	children,
	align = 'left',
	color = 'black',
}) => {
	const baseClassName = 'h3';
	const componentClassName = cn(baseClassName, propsClassName, {
		[`${baseClassName}--align-${align}`]: align,
		[`${baseClassName}--color-${color}`]: color,
	});

	return <h3 className={componentClassName}>{children}</h3>;
};
