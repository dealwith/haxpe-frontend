import { ReactNode, FunctionComponent } from 'react';
import cn from 'classnames';

import { TAlignText, TColor } from 'interfaces';

import './h6.scss';

type TProps = {
	className?: string;
	align?: TAlignText;
	color?: TColor;
	children: ReactNode;
};

export const H6: FunctionComponent<TProps> = ({
	className: propsClassName,
	children,
	color = 'black',
	align,
}) => {
	const baseClassName = 'h6';
	const componentClassName = cn(baseClassName, propsClassName, {
		[`${baseClassName}--align-${align}`]: align,
		[`${baseClassName}--color-${color}`]: color,
	});

	return <h6 className={componentClassName}>{children}</h6>;
};
