export enum ETheme {
	dark = 'dark',
	light = 'light',
}

export type TTheme = keyof typeof ETheme;
