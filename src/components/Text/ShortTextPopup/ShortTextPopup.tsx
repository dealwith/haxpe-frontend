import { FunctionComponent } from 'react';
import cn from 'classnames';

import { TTheme, ETheme } from './interfaces';

import './short-text-popup.scss';

type TProps = {
	children: string;
	theme?: TTheme;
}

export const ShortTextPopup: FunctionComponent<TProps> = ({
	children,
	theme = ETheme.dark,
}) => {
	const baseClassName = 'short-text-popup';
	const hiddenTextClassName = `${baseClassName}__hidden-text`;
	const hiddenTextWithThemeClassName = cn(hiddenTextClassName, {
		[`${hiddenTextClassName}--${theme}`]: theme,
	});
	const componentClassName = {
		component: baseClassName,
		hiddenText: hiddenTextWithThemeClassName,
	};

	return (
		<span className={componentClassName.component}>
			<span className={componentClassName.hiddenText}>{children}</span>
			{`${children?.slice(0, 10)}...`}
		</span>
	);
};
