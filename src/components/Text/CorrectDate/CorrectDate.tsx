import { FunctionComponent } from 'react';

import { ModifiedDate } from 'utils';
import { TDate, TDateFormat } from 'interfaces';

type TProps = {
	date: TDate;
	splitArg?: string;
	format?: TDateFormat;
}

export const CorrectDate: FunctionComponent<TProps> = ({ date, splitArg, format }) => {
	const { createDateToShow } = new ModifiedDate();

	return (
		<>
			{createDateToShow({
				dateArg: date,
				splitArg,
				format,
			})}
		</>
	);
};
