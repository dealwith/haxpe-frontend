import { FunctionComponent } from 'react';
import cn from 'classnames';

import { TAlignText, TColor, TFontFamily } from 'interfaces';

import './title.scss';

type TProps = {
	px?: '14' | '13' | '16';
	className?: string;
	align?: TAlignText;
	color?: TColor;
	ff?: TFontFamily;
	isNoWrap?: boolean;
}

export const Title: FunctionComponent<TProps> = ({
	children,
	px = '13',
	className: propsClassName,
	align,
	ff,
	color,
	isNoWrap,
}) => {
	const baseClassName = `title`;

	const componentClassName = cn(baseClassName, propsClassName, {
		[`${baseClassName}--align-${align}`]: align,
		[`${baseClassName}--color-${color}`]: color,
		[`${baseClassName}--ff-${ff}`]: ff,
		[`${baseClassName}--px-${px}`]: px,
		[`${baseClassName}--color-${color}`]: color,
		[`${baseClassName}--no-wrap`]: isNoWrap,
	});

	return (
		<span className={componentClassName}>
			{children}
		</span>
	);
};
