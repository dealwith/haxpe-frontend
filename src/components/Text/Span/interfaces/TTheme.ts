enum ETheme {
	white = 'white',
	gray = 'gray',
	error = 'error',
	'light-green' = 'light-green',
	'silver-chalice-color' = 'silver-chalice-color',
}

export type TTheme = keyof typeof ETheme;
