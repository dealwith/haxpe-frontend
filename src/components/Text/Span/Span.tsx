import { FunctionComponent } from 'react';
import cn from 'classnames';

import { TTheme } from './interfaces';

import './span.scss';

type TProps = {
	isBold?: boolean;
	isSmall?: boolean;
	className?: string;
	theme?: TTheme;
}

export const Span: FunctionComponent<TProps> = ({
	children,
	isBold,
	className: propsClassName,
	theme,
	isSmall,
}) => {
	const baseSpanClassName = 'span';
	const baseClassName = cn(baseSpanClassName, propsClassName, {
		[`${baseSpanClassName}--bold`]: isBold,
		[`${baseSpanClassName}--theme-${theme}`]: theme,
		[`${baseSpanClassName}--small`]: isSmall,
	});

	return (
		<span className={baseClassName}>
			{children}
		</span>
	);
};
