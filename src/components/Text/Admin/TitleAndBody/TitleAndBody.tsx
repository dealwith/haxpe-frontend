import { FunctionComponent } from 'react';
import StarRating from 'react-star-rating-component';
import cn from 'classnames';

import { isObjectWithProperties } from 'utils';
import { IStarRating, TColor, TAlignText, TFontFamily } from 'interfaces';

import { P, ShortTextPopup, Title } from 'components';

import './title-and-body.scss';

type TProps = {
	title: string;
	isFirst?: boolean;
	isInHeader?: boolean;
	body: string | string[] | IStarRating;
	color?: TColor;
	align?: TAlignText;
	ff?: TFontFamily;
}

export const TitleAndBody: FunctionComponent<TProps> = ({
	isInHeader,
	title,
	isFirst = false,
	body,
	color = 'black',
	align = 'left',
	ff,
}) => {
	const baseClassName = 'title-and-body';
	const textContainer = `${baseClassName}__text-container`;
	const text = `${baseClassName}__text`;

	const renderBody = () => {
		if (!body)
			return null;

		if (typeof body === 'string') {
			if (isInHeader && title.toLowerCase().includes('id')) {
				return <P className={text} color='white'>
					<ShortTextPopup>{body}</ShortTextPopup>
				</P>;
			}

			return <P className={text} align='right' color='white'>{body}</P>;
		}

		if (Array.isArray(body)) {
			return (
				<div className={textContainer}>
					{body.map(str => <P className={text} color='white'>{str}</P>)}
				</div>
			);
		}

		if (isObjectWithProperties({...body}))
			return <StarRating {...body} />;

	};

	const renderedBody = renderBody();

	const componentClassName = cn(baseClassName, {
		[`${baseClassName}--horizontal-centering`]: !isFirst,
	});

	return <div className={componentClassName}>
		{isFirst
			? <Title
				ff={ff}
				px='14'
				align={align}
				color={color}
			>
				{title}
			</Title>
			: <Title
				ff={ff}
				color={color}
				isNoWrap={true}
			>
				{title}: &ensp;
			</Title>
		}
		{renderedBody}
	</div>;
};
