import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { ROUTES } from 'constants/index';

import { Link, P } from 'components';

export const AgreementText: FunctionComponent = () => {
	const { t } = useTranslation(['signUp', 'links']);

	return (
		<P size='medium' align='left'>
			{t('signUp:agreement')}&nbsp;
			<Link isInner={true} href={ROUTES.TERMS_AND_CONDITIONS}>
				{t('links:termsConditions')}&nbsp;
			</Link>&nbsp;
			{t('signUp:agreement2')}&nbsp;
			<Link isInner={true} href={ROUTES.PRIVACY_POLICY}>
				{t('links:privacyPolicy')}
			</Link>
		</P>
	);
};
