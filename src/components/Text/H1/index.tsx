import { FunctionComponent, ReactNode } from 'react';
import cn from 'classnames';

import { TAlignText } from 'interfaces';

import './h1.scss';

type TProps = {
	children: ReactNode;
	className?: string;
	isFirst?: boolean;
	align?: TAlignText;
	isGreen?: boolean;
}

export const H1:FunctionComponent<TProps> = ({
	children,
	className: propsClassName,
	isFirst = false,
	isGreen,
	align,
}) => {
	const baseClassName = 'h1';
	const componentClassName = cn(
		baseClassName,
		propsClassName,
		{
			[`${baseClassName}--align-${align}`]: align,
			[`${baseClassName}--color-green`]: isGreen,
		},

	);

	return isFirst
		? <h1 className={componentClassName}>{children}</h1>
		: <h2 className={componentClassName}>{children}</h2>;
};
