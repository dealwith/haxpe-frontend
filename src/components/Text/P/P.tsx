import { FunctionComponent, ReactNode } from 'react';
import cn from 'classnames';

import { TAlignText, TColor } from 'interfaces';

import './p.scss';

type TProps = {
	children: ReactNode;
	className?: string;
	size?: 'small' | 'medium';
	align?: TAlignText;
	isBold?: boolean;
	color?: TColor;
	transform?: 'uppercase';
}

export const P: FunctionComponent<TProps> = ({
	children,
	className: propsClassName,
	size,
	color = 'black',
	align = 'left',
	isBold,
	transform,
}) => {
	const baseClassName = 'p';
	const componentClassName = cn(baseClassName, propsClassName, {
		[`${baseClassName}--size-${size}`]: size,
		[`${baseClassName}--color-${color}`]: color,
		[`${baseClassName}--align-${align}`]: align,
		[`${baseClassName}--bold`]: isBold,
		[`${baseClassName}--transform-${transform}`]: transform,
	});

	return <p className={componentClassName}>{children}</p>;
};
