enum ETransform {
	uppercase = 'uppercase',
}

export type TTransform = keyof typeof ETransform;
