import { FunctionComponent } from 'react';
import { useLocation } from 'react-router';

import { IDatagridRow, TDatagridRowSection } from 'interfaces';

import { BackToPreviousPageButton } from 'components';
import { DatagridRowHeader } from './DatagridRowHeader';
import { DatagridRowBody } from './DatagridRowBody';

import './datagrid-row.scss';

type TProps = {
	info: IDatagridRow;
}

export const DatagridRow: FunctionComponent<TProps> = ({
	info: { body, title, footerButtonsGroup },
}) => {
	const { pathname } = useLocation();
	const isShow = pathname.includes('show');
	const isBody = !!(body?.length);

	const renderBody = () => body?.map((cardsArray: TDatagridRowSection[], i) => (
		<DatagridRowBody key={i} info={cardsArray}/>
	));

	const renderedBody = renderBody();
	const renderedHeader = title && <DatagridRowHeader info={title}/>;

	const baseClassName = 'datagrid-row';
	const componentClassName = {
		component: `${baseClassName}`,
		buttonsContainer: `${baseClassName}__buttons-container`,
		bodyContainer: `${baseClassName}__body-container`,
	};

	return (
		<div className={componentClassName.component}>
			{renderedHeader}
			{isBody
				&& <div>{renderedBody}</div>
			}
			{isShow
				&& <div className={componentClassName.buttonsContainer}>
					<BackToPreviousPageButton/>
					{footerButtonsGroup}
				</div>
			}
		</div>
	);
};
