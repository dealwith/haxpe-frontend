import { FunctionComponent, useState } from 'react';
import StarRatingComponent from 'react-star-rating-component';

import { TDatagridRowSection } from 'interfaces';

import { DefaultDatagridRowEdit, P, Title } from 'components';
import { ReactComponent as EditIcon } from './icons/editIcon.svg';
import { DatagridRowCard } from '../DatagridRowCard';

import './datagrid-row-section.scss';

export const DatagridRowSection: FunctionComponent<TDatagridRowSection> = ({
	title,
	body,
	textArray,
	rate,
	isEditable,
	component: Component,
	editedFieldName,
	color = 'light-green',
}) => {
	const [isEdit, setIsEdit] = useState(false);

	const handleOpenEditForm = () => setIsEdit(true);

	const handleCloseEditForm = () => setIsEdit(false);

	const titleAlign = textArray?.length
		? 'center'
		: 'left';
	const baseClassName = 'datagrid-row-section';
	const componentClassName = {
		component: baseClassName,
		sectionContainer: `${baseClassName}__section-container`,
		textContainer: `${baseClassName}__text-container`,
		cardContainer: `${baseClassName}__card-container`,
		rateContainer: `${baseClassName}__rate-container`,
		text: `${baseClassName}__text`,
		editIcon: `${baseClassName}__edit-icon`,
	};

	const renderEditableBody = (textArrayTitle?: string) => {
		if (Component) {
			return <Component
				handleClose={handleCloseEditForm}
				body={body}
			/>;
		}

		const currentBody = {
			...(textArray?.length) && {textArray},
			...(body?.length) && {body},
			...(textArrayTitle) && {textArrayTitle: title},
		};

		return <DefaultDatagridRowEdit
			{...currentBody}
			handleClose={handleCloseEditForm}
			editedFieldName={editedFieldName}
		/>;
	};

	const renderCard = () => {
		return body?.map((bodyBlock, i) => (
			<div className={componentClassName.cardContainer} key={i}>
				{Object.entries(bodyBlock).map(([key, value]) => (
					<DatagridRowCard
						key={key}
						title={key}
						body={value}
						color={color}
					/>
				))}
			</div>
		));
	};

	const renderTextArray = () => textArray?.map((text, i) => {
		return (
			<P
				className={componentClassName.text}
				color='white'
				key={i}
			>
				{text}
			</P>
		);
	});

	const renderRate = () => rate && <StarRatingComponent {...rate} />;

	const renderedCard = !isEdit && renderCard();
	const renderedTextArray = !isEdit && renderTextArray();
	const renderedRate = !isEdit && renderRate();
	const renderedEditableBody = isEdit && renderEditableBody();

	const renderInfo = () => {
		if (renderedCard) {
			return (
				<div className={componentClassName.sectionContainer}>
					{renderedCard}
				</div>
			);
		}

		if (renderedTextArray) {
			return (
				<div className={componentClassName.textContainer}>
					{renderedTextArray}
				</div>
			);
		}

		if (renderedRate) {
			return (
				<div className={componentClassName.rateContainer}>
					{renderedRate}
				</div>
			);
		}

		if (renderedEditableBody)
			return renderedEditableBody;

		if (Component)
			return <Component/>;

		return null;
	};

	const renderedInfo = renderInfo();

	return (
		<div className={componentClassName.component}>
			{title
				&& <Title
					px='14'
					align={titleAlign}
					color={color}
				>
					{title}
					{isEditable
						&& <EditIcon
							className={componentClassName.editIcon}
							onClick={handleOpenEditForm}/>
					}
				</Title>
			}
			{renderedInfo}
		</div>
	);
};
