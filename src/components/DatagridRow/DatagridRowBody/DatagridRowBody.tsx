import { FunctionComponent } from 'react';
import cn from 'classnames';

import { TDatagridRowSection, TColor } from 'interfaces';
import { TTheme } from './interfaces';

import { DatagridRowSection } from '../DatagridRowSection';

import './datagrid-row-body.scss';

type TProps = {
	info: TDatagridRowSection[];
	className?: string;
	color?: TColor;
	theme?: TTheme;
}

export const DatagridRowBody: FunctionComponent<TProps> = ({
	info,
	className: propsClassName,
	color = 'light-green',
	theme = 'gray',
}) => {
	const renderSection = () => info.map(({
		title,
		body,
		textArray,
		rate,
		isEditable,
		editedFieldName,
		component,
	}, i) => (
		<DatagridRowSection
			key={i}
			title={title}
			body={body}
			textArray={textArray}
			rate={rate}
			isEditable={isEditable}
			editedFieldName={editedFieldName}
			component={component as never}
			color={color}
		/>
	));

	const renderedSection = renderSection();

	const baseClassName = 'datagrid-row-body';
	const componentClassName = cn(baseClassName, propsClassName, {
		[`${baseClassName}--theme-${theme}`]: theme,
	});

	return (
		<div className={componentClassName}>
			{renderedSection}
		</div>
	);
};
