export enum Theme {
	white = 'white',
	gray = 'gray',
}

export type TTheme = keyof typeof Theme;
