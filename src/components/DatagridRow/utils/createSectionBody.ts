import i18next from 'i18next';

import { Dictionary } from 'interfaces';

type TArgsType = {
	data: Dictionary<any>[];
	keyName: string;
	namespace: string;
}

type TReturnType = Dictionary<string>[];

export const createSectionBody = ({
	data,
	keyName,
	namespace,
}: TArgsType): TReturnType => {
	const sectionBody = [];

	if (data?.length) {
		let section = {};
		const denominator = 5;
		let itemCount = 0;

		const sectionInfoKey = i18next.t(`${namespace}:${keyName}`);

		data.forEach(( item, i) => {
			itemCount++;

			const value = i18next.t(`${namespace}:${item.key}`);

			const sectionInfo = {
				[`${sectionInfoKey} ${i + 1}`]: value,
			};

			section = {...section, ...sectionInfo};

			if (itemCount === denominator || i === data.length - 1) {
				sectionBody.push({...section});
				section = {};
				itemCount = 0;
			}
		});
	}

	return sectionBody;
};
