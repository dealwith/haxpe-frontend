import { FunctionComponent } from 'react';

import { IStarRating, TColor } from 'interfaces';

import { TitleAndBody } from 'components';

type TProps = {
	title: string;
	body: string | string[] | IStarRating;
	isFirst?: boolean;
	isInHeader?: boolean;
	color?: TColor;
}

export const DatagridRowCard: FunctionComponent<TProps> = ({
	title,
	body,
	isFirst,
	isInHeader,
	color = 'light-green',
}) => {
	const baseClassName = 'datagrid-row-card';

	return (
		<div key={title} className={baseClassName}>
			<TitleAndBody
				color={color}
				title={title}
				body={body}
				isFirst={isFirst}
				isInHeader={isInHeader}
			/>
		</div>
	);
};
