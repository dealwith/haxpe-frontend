import { FunctionComponent } from 'react';

import { Dictionary } from 'interfaces';

import { DatagridRowCard } from '../DatagridRowCard';

import './datagrid-row-header.scss';

type TProps = {
	info: Dictionary<string | string[]>;
}

export const DatagridRowHeader: FunctionComponent<TProps> = ({ info }) => {
	const renderHeader = () =>
		Object.entries(info).map(([key, value]) => (
			<DatagridRowCard
				isInHeader={true}
				key={key}
				title={key}
				body={value}
				isFirst={true}
			/>
		));

	const renderedHeader = renderHeader();

	const baseClassName = 'datagrid-row-header';

	return (
		<div className={baseClassName}>
			{renderedHeader}
		</div>
	);
};
