import { FunctionComponent } from 'react';

import { ReactComponent as CircleLoaderIcon } from './circle-loader-icon.svg';

import './circle-loader.css';

export const CircleLoader: FunctionComponent = () => (
	<div className='circle-loader'>
		<CircleLoaderIcon />
	</div>
);
