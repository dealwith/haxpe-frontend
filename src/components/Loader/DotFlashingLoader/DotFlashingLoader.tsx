import { FunctionComponent } from 'react';
import cn from 'classnames';

import './dot-flashing-loader.css';

type TProps = {
	className?: string;
}

export const DotFlashingLoader: FunctionComponent<TProps> = ({ className: propsClassName }) => (
	<div className={cn('dot-flashing', propsClassName)}></div>
);
