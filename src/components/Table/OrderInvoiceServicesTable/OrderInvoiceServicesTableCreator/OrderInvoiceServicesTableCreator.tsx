import {
	Fragment,
	FunctionComponent,
	MouseEvent,
	useCallback,
	useState,
} from 'react';
import cn from 'classnames';

import { isObjectWithProperties } from 'utils';
import {
	TEditService,
	TExtraService,
	TExtraServices,
	TOrderInvoiceTableInputWidth,
	TRemoveService,
	TTheme,
} from 'interfaces';
import { CURRENCY } from 'constants/index';

import { ContentContainer, OrderInvoiceServiceEditForm } from 'components';
import {
	OrderInvoiceServicesTableCreatorActionButtons,
} from './OrderInvoiceServicesTableCreatorActionButtons';

import './order-invoice-services-table-creator.scss';

type TProps = {
	servicesFormData: TExtraServices;
	editService: TEditService;
	removeService: TRemoveService;
	customWidthStyles?: TOrderInvoiceTableInputWidth;
	isEditable: boolean;
}

export const OrderInvoiceServicesTableCreator: FunctionComponent<TProps> = ({
	isEditable: propsIsEditable,
	servicesFormData,
	editService,
	removeService,
	customWidthStyles,
}) => {
	const [currentServiceIndex, setCurrentServiceIndex] = useState<number | null>();
	const [isEditable, setIsEditable] = useState(false);
	const { EURO } = CURRENCY;

	const handleServicesTableClick = (e: MouseEvent<HTMLDivElement>) => {
		setCurrentServiceIndex(parseInt(e.currentTarget.dataset.index));
	};

	const handleServiceDelete = () => {
		removeService(currentServiceIndex);
		setCurrentServiceIndex(null);
	};

	const handleServiceEdit = () => setIsEditable(true);

	const getEditedService = (service: TExtraService) => {
		editService(currentServiceIndex, service);
		setIsEditable(false);
		setCurrentServiceIndex(null);
	};

	const isStyles = isObjectWithProperties(customWidthStyles);

	const baseClassName = 'order-invoice-services-table-creator';
	const componentClassName = {
		component: baseClassName,
		servicesWrapper: `${baseClassName}__services-wrapper`,
	};

	const serviceWrapperModifiedClassName = `${componentClassName.servicesWrapper}--green-border`;

	const getTheme = useCallback((index: number): TTheme => {
		if (!propsIsEditable)
			return 'gray';

		return index === servicesFormData.length - 1
			? 'gray-green'
			: 'gray';
	}, [servicesFormData.length]);

	const getServiceWrapperClassName = useCallback((index: number): string => {
		if (!propsIsEditable)
			return componentClassName.servicesWrapper;

		return index === servicesFormData.length - 1
			? cn(componentClassName.servicesWrapper, serviceWrapperModifiedClassName)
			: componentClassName.servicesWrapper;
	}, [
		propsIsEditable,
		componentClassName.servicesWrapper,
		serviceWrapperModifiedClassName,
		servicesFormData.length,
	]);

	return (
		<div className={componentClassName.component}>
			{servicesFormData?.map(({
				value: amount,
				name: service,
			}, i: number) => {
				const theme = getTheme(i);
				const servicesWrapperClassName = getServiceWrapperClassName(i);

				if (i === currentServiceIndex && isEditable) {
					return (
						<OrderInvoiceServiceEditForm
							key={i}
							customWidthStyles={customWidthStyles}
							name={service}
							value={amount}
							getChangedService={getEditedService}
						/>
					);
				}

				return (
					<Fragment key={i}>
						<div
							onClick={handleServicesTableClick}
							data-index={i}
							className={servicesWrapperClassName}
						>
							<ContentContainer
								position='left'
								{...(isStyles && {maxWidth: customWidthStyles.small})}
								theme={theme}
							>
								1
							</ContentContainer>
							<ContentContainer
								position='left'
								{...(isStyles && {maxWidth: customWidthStyles.large})}
								theme={theme}
							>
								{service}
							</ContentContainer>
							<ContentContainer
								position='left'
								{...(isStyles && {maxWidth: customWidthStyles.normal})}
								theme={theme}
							>
								{`${EURO} ${amount}`}
							</ContentContainer>
						</div>
						{(currentServiceIndex === i && propsIsEditable)
								&& (
									<OrderInvoiceServicesTableCreatorActionButtons
										handleServiceDelete={handleServiceDelete}
										handleServiceEdit={handleServiceEdit}
									/>
								)
						}
					</Fragment>
				);
			})
			}
		</div>
	);

};
