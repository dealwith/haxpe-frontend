import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { Button } from 'components';

import './order-invoice-services-table-creator-action-buttons.scss';

type TProps = {
	handleServiceDelete: () => void;
	handleServiceEdit: () => void;
}

export const OrderInvoiceServicesTableCreatorActionButtons: FunctionComponent<TProps> = ({
	handleServiceDelete,
	handleServiceEdit,
}) => {
	const { t } = useTranslation('button');
	const baseClassName = 'order-invoice-services-table-creator-action-buttons';
	const componentClassName = {
		component: baseClassName,
		editButton: `${baseClassName}__edit-button`,
		removeButton: `${baseClassName}__remove-button`,
	};

	return (
		<div className={componentClassName.component}>
			<Button
				className={componentClassName.editButton}
				onClick={handleServiceEdit}
				theme='yellow'
			>
				{t('button:edit')}
			</Button>
			<Button
				className={componentClassName.removeButton}
				onClick={handleServiceDelete}
				theme='red'
			>
				{t('button:delete')}
			</Button>
		</div>
	);
};
