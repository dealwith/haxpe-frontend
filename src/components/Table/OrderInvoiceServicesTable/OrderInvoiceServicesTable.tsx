import { FunctionComponent, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'react-i18next';

import { isObjectWithProperties } from 'utils';
import {
	TEditService,
	TExtraServices,
	TOrderInvoiceTableInputWidth,
	TRemoveService,
} from 'interfaces';
import { CURRENCY } from 'constants/index';

import { useAfterWorkCompleteInfo, useSelectedWorkerOrder } from 'hooks';
import { ContentContainer, Input, Span } from 'components';
import { OrderInvoiceServicesTableCreator } from './OrderInvoiceServicesTableCreator';

type TProps = {
	orderId: string;
	isEditable?: boolean;
	servicesTableInfo: TExtraServices;
	removeService?: TRemoveService;
	editService?: TEditService;
	customWidthStyles?: TOrderInvoiceTableInputWidth;
}

export const OrderInvoiceServicesTable: FunctionComponent<TProps> = ({
	children,
	servicesTableInfo,
	customWidthStyles,
	isEditable,
	removeService,
	editService,
}) => {
	const { control, watch } = useForm<{reduction: string}>();
	const {
		reduction,
		handleAddReduction,
	} = useAfterWorkCompleteInfo();
	const { t } = useTranslation(['coupon', 'servicesTable']);
	const { selectedOrder } = useSelectedWorkerOrder();

	const { EURO } = CURRENCY;
	const assignmentFeePriceWithoutTax = `${EURO} ${selectedOrder?.invoice?.assignedFee[0]}`;
	const workingTimePriceWithoutTax = `${EURO} ${selectedOrder?.invoice?.workingTime[0]}`;

	const watchedReduction = watch('reduction');

	const isStyles = isObjectWithProperties(customWidthStyles);

	useEffect(() => {
		if (watchedReduction !== undefined)
			handleAddReduction(+watchedReduction);

	}, [watchedReduction, handleAddReduction]);

	return (
		<>
			<ContentContainer centered='space-between' theme='white'>
				<ContentContainer
					{...(isStyles && {maxWidth: customWidthStyles.small})}
					theme='gray'
					position='left'
				>
					<Span isBold={true}>{t('servicesTable:qu')}</Span>
				</ContentContainer>
				<ContentContainer
					{...(isStyles && {maxWidth: customWidthStyles.large})}
					theme='gray'
					position='left'
				>
					<Span isBold={true}>{t('servicesTable:service')}</Span>
				</ContentContainer>
				<ContentContainer
					{...(isStyles && {maxWidth: customWidthStyles.normal})}
					theme='gray'
					position='left'
				>
					<Span isBold={true}>
						{t('servicesTable:amount')}
					</Span>
				</ContentContainer>
			</ContentContainer>
			<ContentContainer centered='space-between' theme='white'>
				<ContentContainer
					maxWidth={customWidthStyles.small}
					theme='gray'
					position='left'
				>
					<Span isBold={true}>1</Span>
				</ContentContainer>
				<ContentContainer
					maxWidth={customWidthStyles.large}
					theme='gray'
					position='left'
					flow='vertical'
				>
					<Span>
						{t('servicesTable:assignmentFee')}
					</Span>
					<Span isSmall={true} theme='gray'>
						{`(${t('servicesTable:unitPrice')} ${assignmentFeePriceWithoutTax})`}
					</Span>
				</ContentContainer>
				<ContentContainer
					maxWidth={customWidthStyles.normal}
					theme='gray'
					position='left'
				>
					{assignmentFeePriceWithoutTax}
				</ContentContainer>
			</ContentContainer>
			<ContentContainer centered='space-between' theme='white'>
				<ContentContainer
					maxWidth={customWidthStyles.small}
					theme='gray'
					position='left'
				>
					<Span isBold={true}>1</Span>
				</ContentContainer>
				<ContentContainer
					maxWidth={customWidthStyles.large}
					theme='gray'
					position='left'
					flow='vertical'
				>
					<Span>
						{t('servicesTable:workingTime')}
					</Span>
					<Span isSmall={true} theme='gray'>
						({t('servicesTable:unitPrice')} {workingTimePriceWithoutTax})
					</Span>
				</ContentContainer>
				<ContentContainer
					maxWidth={customWidthStyles.normal}
					theme='gray'
					position='left'
				>
					{workingTimePriceWithoutTax}
				</ContentContainer>
			</ContentContainer>
			{!!(servicesTableInfo.length)
				&& <OrderInvoiceServicesTableCreator
					customWidthStyles={customWidthStyles}
					editService={editService}
					removeService={removeService}
					servicesFormData={servicesTableInfo}
					isEditable={isEditable}
				/>
			}
			{(isEditable || reduction)
				&& <ContentContainer centered='space-between' theme='white'>
					<ContentContainer
						maxWidth={customWidthStyles.small}
						theme='gray'
						position='left'
					>
						<Span isBold={true}>1</Span>
					</ContentContainer>
					<ContentContainer
						maxWidth={customWidthStyles.large}
						theme='gray'
						position='left'
					>
						{t('coupon:coupon')}
					</ContentContainer>
					<ContentContainer
						maxWidth={customWidthStyles.normal}
						theme='gray'
						position='left'
					>
						{isEditable
							? <Input
								type='number'
								defaultValue={reduction?.toString()}
								textAlign='center'
								maxWidth={customWidthStyles.normal}
								name='reduction'
								control={control}
								size='full'
								isCurrencyInput={true}
							/>
							: <ContentContainer
								theme='gray'
								maxWidth={customWidthStyles.normal}
							>
								{`${EURO} ${reduction}`}
							</ContentContainer>
						}
					</ContentContainer>
				</ContentContainer>
			}
			{children}
		</>
	);
};
