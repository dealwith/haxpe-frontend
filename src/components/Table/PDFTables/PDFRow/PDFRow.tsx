import { FunctionComponent } from 'react';

import { ContentContainer } from 'components';

export const PDFRow: FunctionComponent = ({ children }) => {
	const baseClassName = 'pdf-row';

	return (
		<ContentContainer
			className={baseClassName}
			centered='space-between'
			theme='white'
		>
			{children}
		</ContentContainer>
	);
};
