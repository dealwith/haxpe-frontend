import { FunctionComponent } from 'react';

import { ContentContainer, PDFTableTitleContainer, Span } from 'components';

export const OrderInvoicePDFTableTitle: FunctionComponent = () => {

	return (
		<PDFTableTitleContainer>
			<ContentContainer>
				<Span isBold={true}>Menge</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Beschreibung</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Einzelpreis</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Netto</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>MwSt.</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Brutto</Span>
			</ContentContainer>
		</PDFTableTitleContainer>
	);
};
