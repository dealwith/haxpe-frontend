import { FunctionComponent } from 'react';

import { CURRENCY } from 'constants/index';

import { ContentContainer, Span, PDFRow } from 'components';

type TInfo = {
	info?: number[];
}

export const OrderInvoicePDFTableFinalRow: FunctionComponent<TInfo> = ({ info }) => {
	const [net, tax, total] = info;
	const { EURO } = CURRENCY;

	return (
		<PDFRow>
			<ContentContainer theme='white'></ContentContainer>
			<ContentContainer theme='white'></ContentContainer>
			<ContentContainer theme='white'>
				<Span isBold={true}>GESAMT</Span>
			</ContentContainer>
			<ContentContainer theme='white'>
				{net} {EURO}
			</ContentContainer>
			<ContentContainer theme='white'>
				{tax} {EURO}
			</ContentContainer>
			<ContentContainer theme='white'>
				{total} {EURO}
			</ContentContainer>
		</PDFRow>
	);
};
