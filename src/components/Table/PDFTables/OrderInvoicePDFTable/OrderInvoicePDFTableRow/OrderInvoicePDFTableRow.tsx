import { FunctionComponent } from 'react';

import { Dictionary } from 'interfaces';
import { CURRENCY } from 'constants/index';

import { ContentContainer } from 'components';
import { PDFRow } from '../../PDFRow';

type TInfo = {
	info?: Dictionary<number[]>
}

export const OrderInvoicePDFTableRow: FunctionComponent<TInfo> = ({ info }) => {

	const [[name, [net, tax, total]]] = Object.entries(info);
	const { EURO } = CURRENCY;

	return (
		<PDFRow>
			<ContentContainer>
				1
			</ContentContainer>
			<ContentContainer>
				{name}
			</ContentContainer>
			<ContentContainer>
				{net} {EURO}
			</ContentContainer>
			<ContentContainer>
				{net} {EURO}
			</ContentContainer>
			<ContentContainer>
				{tax} {EURO}
			</ContentContainer>
			<ContentContainer>
				{total} {EURO}
			</ContentContainer>
		</PDFRow>
	);
};
