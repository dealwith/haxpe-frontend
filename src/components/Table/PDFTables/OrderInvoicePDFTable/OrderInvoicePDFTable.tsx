import { FunctionComponent } from 'react';

import { IOrderRes } from 'interfaces';
import { CURRENCY } from 'constants/index';

import {
	ContentContainer,
	Span,
	PDFTableContainer,
	OrderInvoicePDFTableFinalRow,
	OrderInvoicePDFTableTitle,
	OrderInvoicePDFTableRow,
} from 'components';

type TProps = {
	info: IOrderRes;
}

export const OrderInvoicePDFTable: FunctionComponent<TProps> = ({ info }) => {
	if (!info?.invoice)
		return null;

	const { EURO } = CURRENCY;
	const {
		reduction,
		invoice: {
			assignedFee,
			workingTime,
			extraServices,
			net,
			tax,
			total: resTotal,
		},
	} = info;

	const total = [net, tax, resTotal];

	return (
		<PDFTableContainer>
			<ContentContainer>
				<Span isBold={true}>COUPON: </Span>
				{reduction} {EURO}
			</ContentContainer>
			<OrderInvoicePDFTableTitle/>
			<OrderInvoicePDFTableRow info={{'assignedFee': assignedFee}}/>
			<OrderInvoicePDFTableRow info={{'workingTime': workingTime}}/>
			{extraServices.map(({ name, value }) => {
				return <OrderInvoicePDFTableRow info={{
					[name]: value,
				}}/>;
			})}
			<OrderInvoicePDFTableFinalRow info={total}/>
		</PDFTableContainer>
	);
};
