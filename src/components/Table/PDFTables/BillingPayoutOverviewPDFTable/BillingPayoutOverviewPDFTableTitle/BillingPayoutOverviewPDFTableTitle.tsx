import { FunctionComponent } from 'react';

import {
	ContentContainer,
	PDFTableTitleContainer,
	Span,
} from 'components';

export const BillingPayoutOverviewPDFTableTitle: FunctionComponent = () => {

	return (
		<PDFTableTitleContainer>
			<ContentContainer>
				<Span isBold={true}>Rechungs-Nr.</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Datum</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Handwerker</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Bezahlung</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Bar-Einnahmen</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Umsatz</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Haxpe-Gebühr</Span>
			</ContentContainer>
		</PDFTableTitleContainer>
	);
};
