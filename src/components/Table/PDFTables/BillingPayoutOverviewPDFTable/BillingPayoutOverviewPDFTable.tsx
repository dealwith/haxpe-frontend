import { FunctionComponent } from 'react';

import { PDFTableContainer } from 'components';
import {
	BillingPayoutOverviewPDFTableTitle,
} from './BillingPayoutOverviewPDFTableTitle';

type TProps = {
	info: any;
}

export const BillingPayoutOverviewPDFTable: FunctionComponent<TProps> = ({
	info,
}) => {
	return (
		<PDFTableContainer>
			<BillingPayoutOverviewPDFTableTitle/>
		</PDFTableContainer>
	);
};
