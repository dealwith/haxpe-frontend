import { FunctionComponent } from 'react';

import { ContentContainer, Span, PDFRow } from 'components';

type TInfo = {
	info?: any;
}

export const BillingInvoicePDFTableFinalRow: FunctionComponent<TInfo> = ({ info }) => {

	return (
		<PDFRow>
			<ContentContainer theme='white'></ContentContainer>
			<ContentContainer theme='white'></ContentContainer>
			<ContentContainer theme='white'>
				<Span isBold={true}>TOTAL</Span>
			</ContentContainer>
			<ContentContainer theme='white'></ContentContainer>
			<ContentContainer theme='white'></ContentContainer>
		</PDFRow>
	);
};
