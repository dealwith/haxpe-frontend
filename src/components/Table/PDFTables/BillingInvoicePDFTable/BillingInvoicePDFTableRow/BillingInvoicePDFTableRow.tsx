import { FunctionComponent } from 'react';

import { CURRENCY } from 'constants/index';

import { ContentContainer } from 'components';
import { PDFRow } from '../../PDFRow';

type TInfo = {
	info?: any;
}

export const BillingInvoicePDFTableRow: FunctionComponent<TInfo> = ({ info }) => {
	const { EURO } = CURRENCY;

	return (
		<PDFRow>
			<ContentContainer>
			</ContentContainer>
			<ContentContainer>
			</ContentContainer>
			<ContentContainer>
			</ContentContainer>
			<ContentContainer>
			</ContentContainer>
			<ContentContainer>
			</ContentContainer>
		</PDFRow>
	);
};
