import { FunctionComponent } from 'react';

import {
	ContentContainer,
	PDFTableTitleContainer,
	Span,
} from 'components';

export const BillingInvoicePDFTableTitle: FunctionComponent = () => {

	return (
		<PDFTableTitleContainer>
			<ContentContainer>
				<Span isBold={true}>Auszahlung</Span>
				{/* TODO: serial number */}
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Datum</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Transaktions-ID</Span>
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Gesamt-Umsatz</Span>
				{/* TODO: total */}
			</ContentContainer>
			<ContentContainer>
				<Span isBold={true}>Haxpe-Gebühr.</Span>
				{/* TODO: haxpe percent */}
			</ContentContainer>
		</PDFTableTitleContainer>
	);
};
