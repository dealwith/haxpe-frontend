import { FunctionComponent } from 'react';

import { PDFTableContainer } from 'components';
import { BillingInvoicePDFTableTitle } from './BillingInvoicePDFTableTitle';
import { BillingInvoicePDFTableRow } from './BillingInvoicePDFTableRow';
import { BillingInvoicePDFTableFinalRow } from './BillingInvoicePDFTableFinalRow';

type TProps = {
	info: any;
}

export const BillingInvoicePDFTable: FunctionComponent<TProps> = ({ info }) => {
	return (
		<PDFTableContainer>
			<BillingInvoicePDFTableTitle/>
			<BillingInvoicePDFTableRow/>
			<BillingInvoicePDFTableRow/>
			<BillingInvoicePDFTableRow/>
			<BillingInvoicePDFTableRow/>
			<BillingInvoicePDFTableRow/>
			<BillingInvoicePDFTableRow/>
			<BillingInvoicePDFTableFinalRow/>
		</PDFTableContainer>
	);
};
