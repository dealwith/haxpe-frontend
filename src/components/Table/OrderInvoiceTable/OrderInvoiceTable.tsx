import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { CURRENCY } from 'constants/index';
import { IInvoice } from 'interfaces';

import { P } from 'components';

import './order-invoice-table.scss';

type TProps = {
	invoice: IInvoice;
}

export const OrderInvoiceTable: FunctionComponent<TProps> = ({
	invoice,
}) => {
	const { t } = useTranslation(['servicesTable']);

	const unitPriceText = t('servicesTable:unitPrice');
	const baseClassName = 'order-invoice-table';
	const componentClassName = {
		component: baseClassName,
		tr: `${baseClassName}__tr`,
		td: `${baseClassName}__td`,
		th: `${baseClassName}__th`,
	};

	return (
		<table className={componentClassName.component}>
			<thead>
				<tr>
					<th className={componentClassName.th}>
						{t('servicesTable:qu')}
					</th>
					<th className={componentClassName.th}>
						{t('servicesTable:service')}
					</th>
					<th className={componentClassName.th}>
						{t('servicesTable:amount')}
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td className={componentClassName.td}>
						1
					</td>
					<td className={componentClassName.td}>
						<P size='medium' align='center'>
							{t('servicesTable:assignmentFee')}
						</P>
						<P size='small' color='dark-gray' align='center'>
							{unitPriceText}
						</P>
					</td>
					<td className={componentClassName.td}>
						{CURRENCY.EURO} {invoice?.assignedFee[2]}
					</td>
				</tr>
				<tr>
					<td className={componentClassName.td}>1</td>
					<td className={componentClassName.td}>
						<P align='center' size='medium'>
							{t('servicesTable:workingTime')}
						</P>
						<P size='small' color='dark-gray' align='center'>
							{unitPriceText}
						</P>
					</td>
					<td className={componentClassName.td}>
						{CURRENCY.EURO} {invoice?.workingTime[2]}
					</td>
				</tr>
				{invoice?.extraServices.map((extraService, i) => (
					<tr key={i}>
						<td className={componentClassName.td}>
							1
						</td>
						<td className={componentClassName.td}>
							{extraService.name}
						</td>
						<td className={componentClassName.td}>
							{CURRENCY.EURO} {extraService.value[2]}
						</td>
					</tr>
				))}
				<tr>
					<td colSpan={2} className={componentClassName.td}>
						<P align='center' transform='uppercase' isBold={true} size='medium'>
							{t('servicesTable:total')}
						</P>
						<P align='center' color='dark-gray' size='medium'>
							{t('servicesTable:amountIncludesVat', { vat: `${invoice?.tax}%` })}
						</P>
					</td>
					<td className={componentClassName.td}>
						<P isBold={true} align='center'>
							{CURRENCY.EURO} {invoice?.total}
						</P>
					</td>
				</tr>
			</tbody>
		</table>
	);
};
