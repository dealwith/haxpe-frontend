import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import cn from 'classnames';

import { CURRENCY } from 'constants/index';

import { ReactComponent as CheckIcon } from './check-icon.svg';

import './table.scss';

type TProps = {
	className?: string;
};

export const PricesTable: FunctionComponent<TProps> = ({ className: propsClassName }) => {
	const { t } = useTranslation('pricingTable');
	const { EURO } = CURRENCY;

	const baseClassName = 'table';
	const componentClassName = cn(baseClassName, propsClassName);

	const tableClassName = {
		component: componentClassName,
		thead: `${baseClassName}__thead`,
		tr: `${baseClassName}__tr`,
		th: `${baseClassName}__th`,
		tbody: `${baseClassName}__tbody`,
		td: `${baseClassName}__td`,
	};

	return (
		<table className={tableClassName.component}>
			<thead className={tableClassName.thead}>
				<tr className={tableClassName.tr}>
					<th className={tableClassName.th}>
						{t('headCol')}
					</th>
					<th className={tableClassName.th}>
						{t('headCol2')}
					</th>
					<th className={tableClassName.th}>
						{t('headCol2')}
					</th>
					<th className={tableClassName.th}>
						{t('headCol3')}
					</th>
				</tr>
			</thead>
			<tbody className={tableClassName.tbody}>
				<tr className={tableClassName.tr}>
					<td className={tableClassName.td}>
						{t('bodyRow1Col1')}
					</td>
					<td className={tableClassName.td}>{`59${EURO}`}</td>
					<td className={tableClassName.td}>{`89${EURO}`}</td>
					<td className={tableClassName.td}>{`119${EURO}`}</td>
				</tr>
				<tr className={tableClassName.tr}>
					<td className={tableClassName.td}>
						{t('bodyRow2Col1')}
					</td>
					<td className={tableClassName.td}>{`29${EURO}`}</td>
					<td className={tableClassName.td}>{`29${EURO}`}</td>
					<td className={tableClassName.td}>{`29${EURO}`}</td>
				</tr>
				<tr className={tableClassName.tr}>
					<td className={tableClassName.td}>
						{t('bodyRow3Col1')}
					</td>
					<td className={tableClassName.td}><CheckIcon /></td>
					<td className={tableClassName.td}><CheckIcon /></td>
					<td className={tableClassName.td}><CheckIcon /></td>
				</tr>
			</tbody>
		</table>
	);
};
