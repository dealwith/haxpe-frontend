import { FunctionComponent, Suspense } from 'react';
import { useLocation } from 'react-router';

import { MEDIA_POINT, ROUTES } from 'constants/index';

import { useAuth, useMediaQuery } from 'hooks';
import { CircleLoader, SidebarNavigation } from 'components';

import './sidebar.css';

const blockedRoutes = [
	ROUTES.HOME,
	ROUTES.ABOUT_US,
	ROUTES.SERVICES,
	ROUTES.PRICING,
	ROUTES.CONTACT,
	ROUTES.SIGN_UP.CUSTOMER,
	ROUTES.SIGN_UP.URL,
];

export const Sidebar: FunctionComponent = () => {
	const auth = useAuth();
	const { pathname } = useLocation();
	const { isMatches: isMobileMenu } = useMediaQuery({ maxWidth: MEDIA_POINT.MOBILE });

	const isRender = auth.isAuthenticated
		&& !isMobileMenu
		&& !blockedRoutes.includes(pathname);

	const componentClassName = 'sidebar';

	return isRender
		&& <aside className={componentClassName}>
			<Suspense fallback={<CircleLoader />}>
				<SidebarNavigation />
			</Suspense>
		</aside>;
};
