import { useEffect, useState, FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { getLocaleStorageItem } from 'utils';

import { useAuth } from 'hooks';
import { AccountService } from 'services';

type TProps = {
	className?: string;
}

interface ILanguage{
	isReferenceLanguage: boolean;
	name: string;
	nativeName: string;
}

type TLanguages = Record<string, ILanguage>;

export const LanguageSwitcher: FunctionComponent<TProps> = ({ className }) => {
	const auth = useAuth();
	const { i18n } = useTranslation('description');
	const getInitialState = (): any => {
		i18n.services.backendConnector.backend.getLanguages((err, ret) => {
			if (err) {
				console.error(err);

				return;
			}

			return ret[getLocaleStorageItem('i18nextLng')];
		});
	};
	const [lngs, setLngs] = useState<TLanguages>(getInitialState);

	useEffect(() => {
		i18n.services.backendConnector.backend.getLanguages((err, ret) => {
			if (err) {
				console.error(err);

				return;
			}

			setLngs(ret);
		});
	}, []);

	const handleSelectChange = ({ target: { value } }) => {
		i18n.changeLanguage(value);

		if (auth.isAuthenticated)
			AccountService.updateLanguage({ preferLanguage: value });
	};

	return lngs
		? <select
			className={className}
			onChange={handleSelectChange}
			defaultValue={getLocaleStorageItem('i18nextLng')}
		>
			{Object.keys(lngs).map(lng => (
				<option
					key={lng}
					value={lng}
				>
					{lngs[lng].nativeName}
				</option>
			))}
		</select>
		: null;
};
