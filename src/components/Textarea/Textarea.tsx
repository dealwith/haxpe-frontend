import { forwardRef, FunctionComponent } from 'react';
import { RegisterOptions } from 'react-hook-form';
import cn from 'classnames';

import { TSize, TTheme } from './interfaces';

import './textarea.scss';

type TProps = {
	name: string;
	className?: string;
	placeholder?: string;
	isDisabled?: boolean;
	size?: TSize;
	theme?: TTheme;
	rules?: Exclude<RegisterOptions, 'valueAsNumber'
	| 'valueAsDate'
	| 'setValueAs'>;
	maxSymbolLength?: number;
	autoFocus?: boolean;
	valueLength?: number;
	containerClassName?: string;
}

export const Textarea: FunctionComponent<TProps> = forwardRef(({
	className: propsClassName,
	name,
	placeholder,
	isDisabled,
	size,
	theme,
	rules,
	maxSymbolLength,
	autoFocus,
	valueLength,
	containerClassName,
	...props
}, ref) => {
	const baseClassName = 'textarea';
	const baseTextSybmolsClassName = `${baseClassName}__text-symbols`;
	const textAreaClassName = cn(
		baseClassName, {
			[`${baseClassName}--size-${size}`]: size,
			[`${baseClassName}--theme-${theme}`]: theme,
		},
		propsClassName,
	);
	const componentClassName = {
		component: textAreaClassName,
		container: cn(`${baseClassName}-container`, containerClassName),
		textSybmols: cn(baseTextSybmolsClassName, {
			[`${baseTextSybmolsClassName}--theme-red`]: valueLength === maxSymbolLength,
		}),
	};

	return (
		<div className={componentClassName.container}>
			<textarea
				className={componentClassName.component}
				name={name}
				placeholder={placeholder}
				disabled={isDisabled}
				maxLength={maxSymbolLength}
				autoFocus={autoFocus}
				{...{...props, ...ref}}
			/>
			{maxSymbolLength
				&& <span className={componentClassName.textSybmols}>
					({valueLength}/{maxSymbolLength})
				</span>
			}
		</div>
	);
});
