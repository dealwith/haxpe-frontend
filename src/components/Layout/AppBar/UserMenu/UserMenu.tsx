import { FunctionComponent, ReactElement } from 'react';
import { UserMenu as AdminUserMenu } from 'react-admin';

import { AdminLogoutButton } from 'components';

type TProps = {
	label?: string;
    logout?: ReactElement;
    icon?: JSX.Element;
}

export const UserMenu: FunctionComponent<TProps> = props => (
	<AdminUserMenu {...props}>
		<AdminLogoutButton/>
	</AdminUserMenu>
);
