import { FunctionComponent } from 'react';
import { AppBar as AdminAppBar, AppBarProps } from 'react-admin';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import { LanguageSwitcher } from 'components';
import { UserMenu } from './UserMenu';

import './app-bar.scss';

const useStyles = makeStyles({
	title: {
		flex: 1,
		textOverflow: 'ellipsis',
		whiteSpace: 'nowrap',
		overflow: 'hidden',
	},
	spacer: {
		flex: 1,
	},
});

export const AppBar: FunctionComponent<AppBarProps> = props => {
	const classes = useStyles();

	const appBarLanguageSwitcherClassName = 'app-bar__language-switcher';

	return (
		<AdminAppBar {...props} logout={false} userMenu={<UserMenu/>}>
			<Typography
				variant='h6'
				color='inherit'
				className={classes.title}
				id='react-admin-title'
			/>
			<span className={classes.spacer} />
			<LanguageSwitcher className={appBarLanguageSwitcherClassName}/>
		</AdminAppBar>
	);
};
