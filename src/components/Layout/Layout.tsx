import { FunctionComponent } from 'react';
import { Layout as AdminLayout, LayoutProps } from 'react-admin';

import { AppBar } from './AppBar';

export const Layout: FunctionComponent<LayoutProps> = props => (
	<AdminLayout {...props} appBar={AppBar} />
);
