import { FunctionComponent } from 'react';
import cn from 'classnames';

import { TTheme } from '../interfaces';

import { Span } from 'components';

import './message.scss';

type TProps = {
	message: {id: string; text: string; creationDate: string};
	theme: TTheme;
}

export const Message: FunctionComponent<TProps> = ({
	message,
	theme,
}) => {
	const position = theme === 'light-green'
		? 'left'
		: 'right';
	const order = theme === 'light-green'
		? 'first'
		: 'second';

	const baseClassName = 'message';
	const messageTextBaseClassName = `${baseClassName}__message-text`;

	const messageTextClassName = cn(messageTextBaseClassName,
		`${messageTextBaseClassName}--position-${position}`, {
			[`${messageTextBaseClassName}--theme-${theme}`]: theme,
		});

	const timeClassName = cn(`${baseClassName}__time`, `${baseClassName}__time--order-${order}`);

	const componentClassName = {
		component: message,
		time: timeClassName,
		messageText: messageTextClassName,
	};

	return (
		<li className={baseClassName}>
			<Span className={componentClassName.time}>{message.creationDate}</Span>
			<Span className={componentClassName.messageText}>{message.text}</Span>
		</li>
	);
};
