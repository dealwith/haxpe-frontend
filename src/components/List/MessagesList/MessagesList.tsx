import { FunctionComponent, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import { TParams, TTheme } from './interfaces';

import { Message } from './Message';

import './message-list.scss';

const fakeMessage = [
	{
		id: '1',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '2',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '3',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '4',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '5',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '11',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '12',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '13',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '14',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '15',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '21',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '22',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '23',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '24',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
	{
		id: '25',
		text: 'aaaaaaaaaaaaaa',
		creationDate: '2141234',
	},
];

export const MessagesList: FunctionComponent = () => {
	const { id } = useParams<TParams>();
	const [messages, setMessages] = useState<{id: string; text: string; creationDate: string}[]>(fakeMessage);

	localStorage.setItem('adminUserId', '3');

	useEffect(() => {
		//TODO: fetch from message
	}, []);

	if (!messages.length)
		return null;

	const renderedMessagesList = messages.map(message => {
		let theme: TTheme = 'light-grey';

		if (message.id === localStorage.getItem('adminUserId'))
			theme = 'light-green';

		return <Message theme={theme} message={message}/>;
	});

	const baseClassName = 'message-list';

	return (
		<ul className={baseClassName}>
			{renderedMessagesList}
		</ul>
	);
};
