export enum ETheme {
	'light-grey' = 'light-grey',
	'light-green' = 'light-green',
}

export type TTheme = keyof typeof ETheme;
