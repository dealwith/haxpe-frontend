import { FunctionComponent } from 'react';
import { useHistory } from 'react-router-dom';

import { TTheme } from '../interfaces';

import { Button, Span } from 'components';

type TProps = {
	theme: TTheme;
	customer: {
		creationDate: string;
		id: string;
	};
	handleChangeSelectedId: (id: string) => void;
}

export const CustomerListItem: FunctionComponent<TProps> = ({
	customer,
	theme,
	handleChangeSelectedId,
}) => {
	const { push } = useHistory();

	const handleClick = () => {
		localStorage.setItem('selectedItemId', customer.id);
		handleChangeSelectedId(customer.id);
		push(`/chat/${customer.id}`);
	};

	return (
		<li>
			<Span>{customer.creationDate}</Span>
			<Button
				size='full'
				onClick={handleClick}
				theme={theme}
			>
				{customer.id}
			</Button>
		</li>
	);
};
