export enum ETheme {
	blue = 'blue',
	red = 'red',
	'dusty-gray' = 'dusty-gray',
}

export type TTheme = keyof typeof ETheme;
