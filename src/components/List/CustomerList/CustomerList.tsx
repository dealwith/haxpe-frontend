import { FunctionComponent, useState } from 'react';

import { TTheme } from './interfaces';

import { CustomerListItem } from './CustomerListItem';

import './customer-list.scss';

type TCustomerInList = {
	creationDate: string;
	id: string;
};

type TCustomerList = {
	list: TCustomerInList[]
}

type TProps = TCustomerList

export const CustomerList: FunctionComponent<TProps> = ({
	list,
}) => {
	const baseClassName = 'customer-list';
	const [selectedId, setSelectedId] = useState('');

	if (!list.length)
		return null;

	const handleChangeSelectedId = (id: string) => setSelectedId(id);

	const renderedList = list.map(customer => {
		let theme: TTheme = 'dusty-gray';
		const isInLS = customer.id === localStorage.getItem('selectedItemId');

		if (customer.id === selectedId || isInLS)
			theme = 'blue';

		return <CustomerListItem
			key={customer.id}
			theme={theme}
			customer={customer}
			handleChangeSelectedId={handleChangeSelectedId}
		/>;
	});

	return (
		<ul className={baseClassName}>
			{renderedList}
		</ul>
	);
};
