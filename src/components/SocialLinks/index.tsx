import { FunctionComponent } from 'react';

import { SocialLink } from './SocialLink';

import './social-links.css';

export const SocialLinks: FunctionComponent = () => {
	return (
		<article className='social-links'>
			<SocialLink iconName='facebook' />
			<SocialLink iconName='instagram' />
			<SocialLink iconName='twitter' />
			<SocialLink iconName='youtube' />
		</article>
	);
};
