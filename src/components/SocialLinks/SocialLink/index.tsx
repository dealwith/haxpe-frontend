import { FunctionComponent } from 'react';

import { Link } from 'components';

import { ReactComponent as FacebookIcon } from './icons/facebook.svg';
import { ReactComponent as InstagramIcon } from './icons/instagram.svg';
import { ReactComponent as TwitterIcon } from './icons/twitter.svg';
import { ReactComponent as YoutubeIcon } from './icons/youtube.svg';

import './social-link.scss';

const Icon = {
	facebook: {
		component: FacebookIcon,
		link: 'https://www.facebook.com/haxpe',
	},
	instagram: {
		component: InstagramIcon,
		link: 'https://www.instagram.com/haxpeapp/',
	},
	twitter: {
		component: TwitterIcon,
		link: 'https://mobile.twitter.com/HaxpeU',
	},
	youtube: {
		component: YoutubeIcon,
		link: 'https://www.youtube.com/channel/UC0SRek9C4tSh027xHCNc0CA',
	},
};

type TProps = {
	iconName: keyof typeof Icon;
}

export const SocialLink: FunctionComponent<TProps> = ({ iconName }) => {
	const { link, component: Component } = Icon[iconName];

	return (
		<Link href={link} target='_blank' className='social-link'>
			<Component />
		</Link>
	);
};
