import { FunctionComponent, ReactNode, Suspense } from 'react';

import {
	AuthProvider,
	PDFDownloadHandlerProvider,
	ServicesIndustriesProvider,
} from 'providers';
import { CircleLoader } from 'components';

type TProps = {
	children: ReactNode;
}

export const AdminProviders: FunctionComponent<TProps> = ({ children }) => {

	return (
		<Suspense fallback={<CircleLoader/>}>
			<AuthProvider>
				<ServicesIndustriesProvider>
					<PDFDownloadHandlerProvider>
						{children}
					</PDFDownloadHandlerProvider>
				</ServicesIndustriesProvider>
			</AuthProvider>
		</Suspense>
	);
};
