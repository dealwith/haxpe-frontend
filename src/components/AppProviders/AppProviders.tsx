import { FunctionComponent, ReactNode, Suspense } from 'react';

import {
	AuthProvider,
	ServicesIndustriesProvider,
	SocketProvider,
	CurrentLocationProvider,
	SelectedOrderProvider,
	PDFDownloadHandlerProvider,
} from 'providers';
import { CircleLoader } from 'components';
import { ModalProvider } from 'providers/ModalProvider';

type TProps = {
	children: ReactNode;
}

export const AppProviders: FunctionComponent<TProps> = ({ children }) => (
	<AuthProvider>
		<Suspense fallback={<CircleLoader />}>
			<ServicesIndustriesProvider>
				<CurrentLocationProvider>
					<SocketProvider>
						<SelectedOrderProvider>
							<PDFDownloadHandlerProvider>
								<ModalProvider>
									{children}
								</ModalProvider>
							</PDFDownloadHandlerProvider>
						</SelectedOrderProvider>
					</SocketProvider>
				</CurrentLocationProvider>
			</ServicesIndustriesProvider>
		</Suspense>
	</AuthProvider>
);
