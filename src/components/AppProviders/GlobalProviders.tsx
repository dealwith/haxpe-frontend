import { FunctionComponent, ReactNode, Suspense } from 'react';

import { NotifyProvider } from 'providers';

type TProps = {
	children: ReactNode;
}

export const GlobalProviders: FunctionComponent<TProps> = ({ children }) => {
	return <Suspense fallback={<></>}>
		<NotifyProvider>
			{children}
		</NotifyProvider>
	</Suspense>;
};
