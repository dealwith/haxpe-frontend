import { FunctionComponent, ReactNode } from 'react';

import './header.css';

type TProps = {
	children: ReactNode;
}

export const Header: FunctionComponent<TProps> = ({
	children,
}) => <header className='header'>{children}</header>;
