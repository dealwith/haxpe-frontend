enum EInputType {
	text = 'text',
	number = 'number',
	checkbox = 'checkbox',
	email = 'email',
	tel = 'tel',
	password = 'password',
	radio = 'radio',
}

export type TInputType = keyof typeof EInputType;
