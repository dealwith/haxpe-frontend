export type TTextAlign = 'left'
| 'center'
| 'right';
