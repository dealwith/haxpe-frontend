/**
 * Function converts bytes into human readable format.
 *
 * @param {number} bytes - Size of a file in bytes.
 * @param {number} radix - An integer in the range 2 through 36 specifying the base to use
 * for representing numeric values.
 * @returns {string} Human readable size of the item.
 */

export const formatBytes = (bytes: number, radix?: number): string => {
	if (bytes == 0)
		return '0 Bytes';

	const k = 1024,
		dm = radix || 2,
		sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
		i = Math.floor(Math.log(bytes) / Math.log(k));

	return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
};
