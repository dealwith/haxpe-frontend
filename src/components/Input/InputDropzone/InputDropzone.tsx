import { FunctionComponent } from 'react';
import { useDropzone } from 'react-dropzone';

import { FILES } from 'constants/index';
import { formatBytes } from './utils';

import { P } from 'components';

import './input-dropzone.scss';

const MAX_FILES_AMOUNT = 5;

export const InputDropzone: FunctionComponent = () => {
	const {
		acceptedFiles,
		getRootProps,
		getInputProps,
	} = useDropzone({
		maxSize: FILES.MAX_FILE_SIZE,
		maxFiles: MAX_FILES_AMOUNT,
	});

	const baseClassName = 'input-dropzone';
	const baseAcceptedFilesClassName = 'accepted-files';
	const componentClassName = {
		component: baseClassName,
		acceptedFiles: baseAcceptedFilesClassName,
	};

	const acceptedFileItems = acceptedFiles.map((file, i) => (
		<li key={i}>
			{file.name} - {formatBytes(file.size)}
		</li>
	));

	return (
		<section className={componentClassName.component}>
			<div {...getRootProps({ className: 'dropzone' })}>
				<input {...getInputProps()} />
				<P>Drag 'n' drop some files here, or click to select files</P>
				<em>({MAX_FILES_AMOUNT} files are the maximum number of files you can drop here)</em>
			</div>
			<aside>
				<h4>Accepted files</h4>
				<ul className={componentClassName.acceptedFiles}>{acceptedFileItems}</ul>
			</aside>
		</section>
	);
};
