import { FunctionComponent, memo, useState } from 'react';
import { RegisterOptions, useController } from 'react-hook-form';
import { ErrorMessage } from '@hookform/error-message';
import cn from 'classnames';

import { CURRENCY } from 'constants/index';
import { TInputType, TTextAlign } from './interfaces';
import { calculateDefaultValue } from './utils';

import { Image, Button, RenderIcon, TIcon, Span } from 'components';
import eyeIcon from './icons/eye.svg';

import './input.scss';

export type TInputProps = {
	maxWidth?: number;
	value?: string;
	name: string;
	type?: TInputType;
	defaultValue?: boolean | string;
	placeholder?: string;
	placeholderTheme?: 'light-green' | 'white';
	placeholderClassName?: string;
	className?: string;
	icon?: TIcon;
	seeable?: boolean;
	labelClassName?: string;
	size?: 'full' | 'half' | '48';
	errors?: Record<string, unknown>;
	isDisabled?: boolean;
	isUpperCase?: boolean;
	control: any;
	autoComplete?: 'on' | 'off';
	rules?: Exclude<RegisterOptions, 'valueAsNumber'
	| 'valueAsDate'
	| 'setValueAs'>;
	textAlign?: TTextAlign;
	autoFocus?: boolean;
	isCurrencyInput?: boolean;
}

export const Input: FunctionComponent<TInputProps> = memo(function Input({
	name,
	type: propsType,
	placeholder,
	placeholderTheme,
	placeholderClassName: propsPlaceholderClassName,
	className: propsClassName,
	labelClassName: propsLabelClassName,
	size,
	icon,
	isDisabled,
	control,
	rules,
	defaultValue: propsDefaultValue,
	seeable,
	autoComplete = 'on',
	textAlign = 'left',
	maxWidth,
	isCurrencyInput,
	autoFocus,
	errors,
}) {
	const [type, setType] = useState<TInputType>(propsType);
	const { field } = useController({
		name,
		control,
		rules,
		defaultValue: propsDefaultValue || calculateDefaultValue(type),
	});

	const isCheckbox = type === 'checkbox' || type === 'radio';
	const isRadio = type === 'radio';

	const baseClassName = 'input';
	const baseLabelClassName = 'label';
	const labelClassName = cn(
		baseLabelClassName,
		propsLabelClassName,
		{
			[`${baseLabelClassName}--size-${size}`]: size,
			[`${baseLabelClassName}--disabled`]: isDisabled,
			[`${baseLabelClassName}--checkbox`]: isCheckbox,
		},
	);
	const inputClassName = cn(
		baseClassName,
		propsClassName,
		{
			[`${baseLabelClassName}--size-${size}`]: size !== 'half' && size !== '48',
			[`${baseClassName}--checkbox`]: isCheckbox,
			[`${baseClassName}--radio`]: isRadio,
			[`${baseClassName}--text-align-${textAlign}`]: textAlign,
			[`${baseClassName}--theme-currency`]: isCurrencyInput,
		},
	);
	const basePlaceholderClassName = `${baseClassName}__placeholder`;
	const placeholderClassName = cn(
		basePlaceholderClassName,
		propsPlaceholderClassName, {
			[`${basePlaceholderClassName}--active`]: !isCheckbox && field.value,
			[`${basePlaceholderClassName}--checkbox`]: isCheckbox,
			[`${basePlaceholderClassName}--theme-icon`]: icon,
		},
	);
	const componentClassName = {
		input: inputClassName,
		label: labelClassName,
		placeholder: placeholderClassName,
		showButton: `${baseClassName}__show-button`,
		currency: `${baseClassName}__currency`,
		error: `${baseClassName}__error`,
	};

	const changeInputType = () => {
		setType(prev => prev === 'password'
			? 'text'
			: 'password');
	};

	const labelStyles = {
		style: {
			...(maxWidth && {
				maxWidth: `${maxWidth}px`,
				width: '100%',
			}),
		},
	};

	const isError = errors && errors[name];

	return (
		<label
			className={componentClassName.label}
			htmlFor={name}
			{...labelStyles}
		>
			{icon
				&& <RenderIcon icon={icon}/>
			}
			{placeholder
				&& <Span
					theme={placeholderTheme}
					className={componentClassName.placeholder}
				>
					{placeholder}
				</Span>
			}
			{isCurrencyInput
				&& <span className={componentClassName.currency}>
					{CURRENCY.EURO}
				</span>
			}
			<input
				id={name}
				name={name}
				type={type}
				autoComplete={autoComplete}
				className={componentClassName.input}
				placeholder={placeholder}
				disabled={isDisabled}
				autoFocus={autoFocus}
				{...field}
			/>
			{isError
				&& <ErrorMessage
					errors={errors}
					name={name}
					render={({ message }) => (
						<Span
							className={componentClassName.error}
							theme='error'
							isSmall={true}
						>
							{message}
						</Span>
					)}
				/>
			}
			{seeable
				&& <Button
					onClick={changeInputType}
					theme='transparent'
					size='m'
				>
					<Image src={eyeIcon} />
				</Button>
			}
		</label>
	);
});
