import { forwardRef, ReactNode } from 'react';
import  ReactToPDF from 'react-to-pdf';

type TProps = {
	fileName?: string;
	children?: ReactNode;
	getDownloadHandler?: (obj: { toPdf: () => void }) => JSX.Element;
}

export const PDF = forwardRef<HTMLElement, TProps>((
	{
		fileName = 'unnamed',
		getDownloadHandler,
	},
	ref,
) => {
	return (
		<ReactToPDF
			targetRef={ref}
			filename={`${fileName}.pdf`}
		>
			{getDownloadHandler}
		</ReactToPDF>
	);
});
