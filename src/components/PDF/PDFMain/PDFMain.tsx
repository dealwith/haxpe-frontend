import { FunctionComponent } from 'react';

import './pdf-main.scss';

export const PDFMain: FunctionComponent = ({ children }) => {
	const baseClassName = 'pdf-main';

	return (
		<div className={baseClassName}>
			{children}
		</div>
	);
};
