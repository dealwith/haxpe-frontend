import { FunctionComponent } from 'react';

import { Span } from 'components';

import './pdf-footer.scss';

export const PDFFooter: FunctionComponent = () => {
	const baseClassName = 'pdf-footer';
	const componentClassName = {
		component: baseClassName,
		element: `${baseClassName}__element`,
		elementTitle: `${baseClassName}__element-title`,
	};

	return (
		<footer className={componentClassName.component}>
			<Span className={componentClassName.element}>
				<Span
					isBold={true}
					className={componentClassName.elementTitle}
				>
					Haxpe-UG (haftungsbeschrankt):
				</Span>
				Lerchenauer Str 156 - 80935 München
			</Span>
			<Span className={componentClassName.element}>
				<Span
					isBold={true}
					className={componentClassName.elementTitle}
				>
					E-Mail:
				</Span>
				haxpeapp@gmail.com
			</Span>
			<Span className={componentClassName.element}>
				<Span
					isBold={true}
					className={componentClassName.elementTitle}
				>
					Steuernummer:
				</Span>
				143/144/71327
			</Span>
			<Span className={componentClassName.element}>
				<Span
					isBold={true}
					className={componentClassName.elementTitle}
				>
					Ust-Idnr:
				</Span>
				DE326260336
			</Span>
			<Span className={componentClassName.element}>
				<Span
					isBold={true}
					className={componentClassName.elementTitle}
				>
					Bank:
				</Span>
				Stadtsparkasse München
			</Span>
			<Span className={componentClassName.element}>
				<Span
					isBold={true}
					className={componentClassName.elementTitle}
				>
					Kontoinhaber:
				</Span>
				Haxpe UG
			</Span>
			<Span className={componentClassName.element}>
				<Span
					isBold={true}
					className={componentClassName.elementTitle}
				>
					Bic:
				</Span>
				SSKMDEMMXXX
			</Span>
			<Span className={componentClassName.element}>
				<Span
					isBold={true}
					className={componentClassName.elementTitle}
				>
					IBAN:
				</Span>
				DE86 7015 0000 1005 7633 94
			</Span>
		</footer>
	);
};
