import { FunctionComponent, useRef } from 'react';
import cn from 'classnames';

import { TPDF } from 'interfaces';

import { usePDFDownloadHandler } from 'hooks';
import {
	ButtonsContainer,
	DownloadPDFButton,
	PDF,
	ShowPicturesButton,
} from 'components';

import './pdf-preview.scss';

type TProps = {
	fileName: string;
	className?: string;
} & TPDF;

export const PDFPreview: FunctionComponent<TProps> = ({
	fileName,
	children,
	className: propsClassName,
	size,
}) => {
	const ref = useRef<HTMLDivElement>();
	const { setDownloadHandler } = usePDFDownloadHandler();

	const isSmallSize = size === 'small';

	const baseClassName = 'pdf-preview';
	const wrapperBaseClassName = 'pdf-preview-wrapper';
	const wrapperClassName = cn(
		wrapperBaseClassName,
		`${wrapperBaseClassName}--size-${size}`,
	);
	const componentClassName = cn(
		baseClassName,
		propsClassName,
		{
			[`${baseClassName}__small`]: isSmallSize,
		},
	);

	const downloadComponentClassName = cn(
		baseClassName,
		propsClassName,
	);

	return (
		<div className={wrapperClassName}>
			{!isSmallSize
				&& <ButtonsContainer>
					<ShowPicturesButton iconTheme='white' theme='transparent'/>
					<DownloadPDFButton/>
				</ButtonsContainer>
			}
			<PDF
				fileName={fileName}
				ref={ref}
				getDownloadHandler={setDownloadHandler}
			/>
			<div {...!isSmallSize && {ref: ref}} className={componentClassName}>
				{children}
			</div>
			{isSmallSize
			&& <div
				className={downloadComponentClassName}
				ref={ref}
			>
				{children}
			</div>}
		</div>
	);
};
