import { FunctionComponent } from 'react';

import { Span } from 'components';

import './pdf-address.scss';

export const PDFAddress: FunctionComponent = () => {
	const baseClassName = 'pdf-address';
	const componentClassName = {
		component: baseClassName,
		title: `${baseClassName}__title`,
	};

	return (
		<address className={componentClassName.component}>
			<Span className={componentClassName.title}>
				Haxpe-UG (haftungsbeschrankt):
			</Span>
			Lerchenauer Str 156 - 80935 München
		</address>
	);
};
