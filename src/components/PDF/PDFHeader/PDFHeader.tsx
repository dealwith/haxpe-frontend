import { FunctionComponent } from 'react';

import { Logo } from 'components';

export const PDFHeader: FunctionComponent = () => (
	<header>
		<Logo/>
	</header>
);

