import { FunctionComponent, useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';

import { useAuth } from 'hooks';
import { ROUTES } from 'constants/index';
import { TRoles } from 'interfaces';

type TProps = {
	component: FunctionComponent<Record<string, unknown>>;
	roles: TRoles[];
	path: string;
	exact?: boolean;
}

export const PrivateRoute: FunctionComponent<TProps> = ({ component: Component, roles, ...rest }) => {
	const {
		checkUser,
		isAuthenticated,
		user,
	} = useAuth();

	useEffect(() => {
		checkUser();
	}, []);

	const render = props => {
		const isForbidden = roles?.indexOf(user?.roles[0]) === -1;

		if (isAuthenticated && isForbidden)
			return <Redirect to={ROUTES.FORBIDDEN} />;

		return isAuthenticated
			? <Component {...props} />
			: <Redirect to={ROUTES.SIGN_IN} />;
	};

	return <Route {...rest} render={render} />;
};
