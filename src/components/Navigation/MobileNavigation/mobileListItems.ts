import { ROUTES } from 'constants/index';

import { ReactComponent as SearchIcon } from '../icons/searchIcon.svg';
import { ReactComponent as CheckListIcon } from '../icons/checklistIcon.svg';
import { ReactComponent as CheckIcon } from '../icons/checkIcon.svg';
import { ReactComponent as DocIcon } from '../icons/docIcon.svg';
import { ReactComponent as PrivacyPolicyIcon } from '../icons/privacyPolicyIcon.svg';
import { ReactComponent as TermsAndConditionsIcon } from '../icons/termsAndConditionsIcon.svg';
import { ReactComponent as AboutUsIcon } from '../icons/aboutUsIcon.svg';
import { ReactComponent as ContactIcon } from '../icons/contactIcon.svg';
import { ReactComponent as ChatIcon } from '../icons/chatIcon.svg';
import { ReactComponent as SettingIcon } from '../icons/settingIcon.svg';
import { ReactComponent as LogoutIcon } from '../icons/logoutIcon.svg';

export const customerListItems = [
	{
		path: ROUTES.SERVICES,
		icon: null,
		text: 'navigation:services',
	},
	{
		path: ROUTES.PRICING,
		icon: null,
		text: 'navigation:price',
	},
	{
		path: ROUTES.INVOICES,
		icon: DocIcon,
		text: 'navigation:invoices',
		isAuth: true,
	},
	{
		path: ROUTES.PRIVACY_POLICY,
		icon: PrivacyPolicyIcon,
		text: 'links:privacyPolicy',
	},
	{
		path: ROUTES.TERMS_AND_CONDITIONS,
		icon: TermsAndConditionsIcon,
		text: 'links:termsConditions',
	},
	{
		path: ROUTES.ABOUT_US,
		icon: AboutUsIcon,
		text: 'navigation:aboutUs',
	},
	{
		path: ROUTES.CONTACT,
		icon: ContactIcon,
		text: 'navigation:contact',
	},
	{
		path: ROUTES.SETTINGS,
		icon: SettingIcon,
		text: 'navigation:settings',
		isAuth: true,
	},
	{
		path: ROUTES.CHAT,
		icon: ChatIcon,
		text: 'navigation:liveChat',
		isAuth: true,
	},
	{
		path: ROUTES.LOGOUT,
		icon: LogoutIcon,
		text: 'navigation:logout',
		isAuth: true,
	},
];

export const workerListItems = [
	{
		path: ROUTES.SERVICES,
		icon: null,
		text: 'navigation:services',
	},
	{
		path: ROUTES.PRICING,
		icon: null,
		text: 'navigation:price',
	},
	{
		path: ROUTES.PRIVACY_POLICY,
		icon: PrivacyPolicyIcon,
		text: 'links:privacyPolicy',
	},
	{
		path: ROUTES.TERMS_AND_CONDITIONS,
		icon: TermsAndConditionsIcon,
		text: 'links:termsConditions',
	},
	{
		path: ROUTES.ABOUT_US,
		icon: AboutUsIcon,
		text: 'navigation:aboutUs',
	},
	{
		path: ROUTES.CONTACT,
		icon: ContactIcon,
		text: 'navigation:contact',
	},
	{
		path: ROUTES.ORDER.ACTIVE,
		icon: CheckListIcon,
		text: 'navigation:activeOrders',
		isAuth: true,
	},
	{
		path: ROUTES.ORDER.CURRENT,
		icon: CheckIcon,
		text: 'navigation:currentOrder',
		isAuth: true,
	},
	{
		path: ROUTES.INVOICES,
		icon: DocIcon,
		text: 'navigation:invoices',
		isAuth: true,
	},
	{
		path: ROUTES.SETTINGS,
		icon: SettingIcon,
		text: 'navigation:settings',
		isAuth: true,
	},
	{
		path: ROUTES.LOGOUT,
		icon: LogoutIcon,
		text: 'navigation:logout',
		isAuth: true,
	},
];
