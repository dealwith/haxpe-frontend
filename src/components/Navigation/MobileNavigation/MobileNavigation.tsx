import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { NavLink } from 'react-router-dom';

import { useAuth } from 'hooks';
import { ROUTES } from 'constants/index';
import { customerListItems, workerListItems } from './mobileListItems';

import { Link } from 'components';

import './mobile-navigation.scss';

export const MobileNavigation: FunctionComponent = () => {
	const { isAuthenticated, isWorker } = useAuth();
	const { t } = useTranslation([
		'navigation',
		'button',
		'order',
	]);

	const baseClassName = 'mobile-navigation';
	const linkClassName = `${baseClassName}__link`;
	const linkLoginClassName = `${linkClassName} ${linkClassName}-login`;
	const componentClassName = {
		container: `${baseClassName}-container`,
		component: baseClassName,
		link: linkClassName,
		linkActive: `${baseClassName}__link--active`,
		linkLogin: linkLoginClassName,
		linkContainer: `${linkClassName}-container`,
		linkIcon: `${linkClassName}-icon`,
	};

	const renderNavigation = () => {
		const listToRender = isWorker
			? workerListItems
			: customerListItems;

		return listToRender
			.filter(item => {
				if (!isAuthenticated && item.isAuth)
					return false;

				return true;
			})
			.map((item, i) => {
				const Icon = item.icon ?? false;

				return <Link
					className={componentClassName.link}
					activeClassName={componentClassName.linkActive}
					href={item.path}
					key={i}
				>
					{Icon && <Icon className={componentClassName.linkIcon} />}
					{t(item.text)}
				</Link>;
			});
	};

	const navigation = renderNavigation();

	return (
		<div className={componentClassName.container}>
			{!isAuthenticated
				&& <NavLink
					className={componentClassName.linkLogin}
					activeClassName={componentClassName.linkActive}
					to={ROUTES.SIGN_IN}
				>
					{t('navigation:login')}
				</NavLink>
			}
			{!isAuthenticated
				&& <NavLink
					exact
					className={componentClassName.link}
					activeClassName={componentClassName.linkActive}
					to={ROUTES.HOME}
				>
					{t('navigation:home')}
				</NavLink>
			}
			{navigation}
		</div>
	);
};
