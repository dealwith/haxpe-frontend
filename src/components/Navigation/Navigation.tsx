import { FunctionComponent, ReactNode } from 'react';

import './navigation.scss';

type TProps = {
	children: ReactNode;
	ariaLabel?: string;
	className?: string;
}

export const Navigation: FunctionComponent<TProps> = ({ ariaLabel, children, className }) => (
	<nav className={className} aria-label={ariaLabel}>
		{children}
	</nav>
);
