import { ROUTES } from 'constants/index';

import { ReactComponent as SearchIcon } from '../../icons/searchIcon.svg';
import { ReactComponent as CheckListIcon } from '../../icons/checklistIcon.svg';
import { ReactComponent as CheckIcon } from '../../icons/checkIcon.svg';
import { ReactComponent as DocIcon } from '../../icons/docIcon.svg';
import { ReactComponent as ImprintIcon } from '../../icons/imprintIcon.svg';
import { ReactComponent as PrivacyPolicyIcon } from '../../icons/privacyPolicyIcon.svg';
import { ReactComponent as TermsAndConditionsIcon } from '../../icons/termsAndConditionsIcon.svg';
import { ReactComponent as AboutUsIcon } from '../../icons/aboutUsIcon.svg';
import { ReactComponent as ContactIcon } from '../../icons/contactIcon.svg';
import { ReactComponent as ChatIcon } from '../../icons/chatIcon.svg';
import { ReactComponent as SettingIcon } from '../../icons/settingIcon.svg';
import { ReactComponent as LogoutIcon } from '../../icons/logoutIcon.svg';

export const customerListItems = [
	{
		path: ROUTES.ORDER.CUSTOMER,
		icon: SearchIcon,
		text: 'navigation:searchCraftsmen',
	},
	{
		path: ROUTES.INVOICES,
		icon: DocIcon,
		text: 'navigation:invoices',
	},
	{
		path: ROUTES.IMPRINT,
		icon: ImprintIcon,
		text: 'links:imprint',
	},
	{
		path: ROUTES.PRIVACY_POLICY,
		icon: PrivacyPolicyIcon,
		text: 'links:privacyPolicy',
	},
	{
		path: ROUTES.TERMS_AND_CONDITIONS,
		icon: TermsAndConditionsIcon,
		text: 'links:termsConditions',
	},
	{
		path: ROUTES.ABOUT_US,
		icon: AboutUsIcon,
		text: 'navigation:aboutUs',
	},
	{
		path: ROUTES.CONTACT,
		icon: ContactIcon,
		text: 'navigation:contact',
	},
	{
		path: ROUTES.SETTINGS,
		icon: SettingIcon,
		text: 'navigation:settings',
	},
	{
		path: ROUTES.CHAT,
		icon: ChatIcon,
		text: 'navigation:liveChat',
	},
	{
		path: ROUTES.LOGOUT,
		icon: LogoutIcon,
		text: 'navigation:logout',
	},
];

export const workerListItems = [
	{
		path: ROUTES.ORDER.ACTIVE,
		icon: CheckListIcon,
		text: 'navigation:activeOrders',
	},
	{
		path: ROUTES.ORDER.CURRENT,
		icon: CheckIcon,
		text: 'navigation:currentOrder',
	},
	{
		path: ROUTES.INVOICES,
		icon: DocIcon,
		text: 'navigation:invoices',
	},
	{
		path: ROUTES.SETTINGS,
		icon: SettingIcon,
		text: 'navigation:settings',
	},
	{
		path: ROUTES.LOGOUT,
		icon: LogoutIcon,
		text: 'navigation:logout',
	},
];
