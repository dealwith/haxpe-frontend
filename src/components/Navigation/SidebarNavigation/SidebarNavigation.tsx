import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { ROUTES } from 'constants/index';

import { useAuth } from 'hooks';
import { Navigation, Container } from 'components';
import { ListItem } from './ListItem';
import { customerListItems, workerListItems } from './utils/listItems';
import { ReactComponent as UserIcon } from '../icons/userIcon.svg';

import './sidebar-navigation.scss';

export const SidebarNavigation: FunctionComponent = () => {
	const { user, isWorker } = useAuth();
	const { t } = useTranslation(['navigation', 'links']);

	const baseClassName = 'sidebar-navigation';
	const baseListClassName = 'list';
	const componentClassName = {
		list: `${baseClassName}__list ${baseListClassName}`,
		listItem: `${baseClassName}__list-item`,
		listLink: `${baseListClassName}__link`,
		listItemIcon: `${baseListClassName}__icon`,
		listLinkActive: `${baseListClassName}__link--active`,
		listDescription: `${baseListClassName}__description`,
	};

	const renderListItems = () => {
		const listToRender = isWorker
			? workerListItems
			: customerListItems;

		return listToRender.map((item, i) => {
			const Component = item.icon;

			return <ListItem
				path={item.path}
				icon={<Component />}
				className={componentClassName}
				key={i}
			>
				{t(`${item.text}`)}
			</ListItem>;
		});
	};

	const listItemsToRender = renderListItems();

	return (
		<Navigation className={baseClassName} ariaLabel='Sidebar'>
			<Container
				isInner={true}
				theme='white'
			>
				<ul className={componentClassName.list}>
					{listItemsToRender}
					<ListItem
						className={componentClassName}
						path={ROUTES.PROFILE}
						icon={<UserIcon />}
						key={ROUTES.PROFILE}
					>
						{`${user?.name} ${user?.surname}`}
					</ListItem>
				</ul>
			</Container>
		</Navigation>
	);
};
