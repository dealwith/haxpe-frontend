import { FunctionComponent, ReactNode } from 'react';
import { NavLink } from 'react-router-dom';

type TProps = {
	className: Record<string, string>;
	children: ReactNode;
	icon: ReactNode;
	path: string;
}

export const ListItem: FunctionComponent<TProps> = ({
	className: propsClassName,
	children,
	icon,
	path,
}) => (
	<li className={propsClassName.listItem}>
		<NavLink
			activeClassName={propsClassName.listLinkActive}
			className={propsClassName.listLink}
			to={path}
		>
			<span className={propsClassName.listItemIcon}>
				{icon}
			</span>
			<span className={propsClassName.listDescription}>
				{children}
			</span>
		</NavLink>
	</li>
);
