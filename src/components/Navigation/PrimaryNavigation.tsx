import { FunctionComponent } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import cn from 'classnames';

import { ROUTES } from 'constants/index';

import { useAuth } from 'hooks';
import { Button, Navigation } from 'components';

type TProps = {
	className?: string;
}

export const PrimaryNavigation: FunctionComponent<TProps> = ({
	className: propsClassName,
}) => {
	const { isAuthenticated, isWorker } = useAuth();
	const { t } = useTranslation(['navigation', 'button', 'order']);

	const baseClassName = 'navigation';
	const linkClassName = `${baseClassName}__link`;
	const linkLoginClassName = `${linkClassName} ${linkClassName}-login`;
	const baseNavClassName = `${baseClassName}__primary-nav`;
	const primaryNavClassName = cn(baseNavClassName, propsClassName);

	const componentClassName = {
		component: baseClassName,
		primaryNav: primaryNavClassName,
		link: `${baseClassName}__link`,
		linkLogin: linkLoginClassName,
		linkActive: `${baseClassName}__link--active`,
	};

	const renderHomeLink = () => {
		const link = isAuthenticated
			? isWorker
				? ROUTES.ORDER.ACTIVE
				: ROUTES.ORDER.CUSTOMER
			: ROUTES.HOME;

		const text = isAuthenticated
			? isWorker
				? t('order:activeOrders')
				: t('navigation:orderCustomer')
			: t('navigation:home');

		return (
			<NavLink
				exact
				className={componentClassName.link}
				activeClassName={componentClassName.linkActive}
				to={link}
			>
				{text}
			</NavLink>
		);
	};

	const homeLink = renderHomeLink();

	return (
		<Navigation
			className={componentClassName.primaryNav}
			ariaLabel='Primary navigation'
		>
			{!isAuthenticated
				&& <NavLink
					className={componentClassName.linkLogin}
					activeClassName={componentClassName.linkActive}
					to={ROUTES.SIGN_IN}
				>
					{t('navigation:login')}
				</NavLink>
			}
			{homeLink}
			<NavLink
				className={componentClassName.link}
				activeClassName={componentClassName.linkActive}
				to={ROUTES.ABOUT_US}
			>
				{t('navigation:aboutUs')}
			</NavLink>
			<NavLink
				className={componentClassName.link}
				activeClassName={componentClassName.linkActive}
				to={ROUTES.SERVICES}
			>
				{t('navigation:services')}
			</NavLink>
			<NavLink
				className={componentClassName.link}
				activeClassName={componentClassName.linkActive}
				to={ROUTES.PRICING}
			>
				{t('navigation:price')}
			</NavLink>
			<NavLink
				className={componentClassName.link}
				activeClassName={componentClassName.linkActive}
				to={ROUTES.CONTACT}
			>
				{t('navigation:contact')}
			</NavLink>
			<Link
				to={isAuthenticated
					? ROUTES.PROFILE
					: ROUTES.SIGN_UP.URL
				}
			>
				{isAuthenticated
					&& <Button
						isText={false}
						size='auto'
						icon='user-icon'
						theme='transparent'
					/>
				}
			</Link>
		</Navigation>
	);
};
