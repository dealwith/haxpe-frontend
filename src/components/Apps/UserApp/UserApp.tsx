import { useTranslation } from 'react-i18next';
import {
	FunctionComponent,
	lazy,
	Suspense,
	useContext,
	useEffect,
	useState,
} from 'react';
import {
	Switch,
	Route,
	Link,
} from 'react-router-dom';
import cn from 'classnames';

import { ERoles } from 'interfaces';
import { ROUTES, MEDIA_POINT } from 'constants/index';
import { useAuth, useMediaQuery } from 'hooks';

import { ModalContext } from 'context';
import {
	Main,
	Container,
	Footer,
	Header,
	Logo,
	LanguageSwitcher,
	PrivateRoute,
	Sidebar,
	PrimaryNavigation,
	OrderWorkerCurrentOrder,
	ErrorBoundary,
	CircleLoader,
	BurgerButton,
	MobileNavigation,
	Overlay,
	OrderCustomerForm,
	Modal,
} from 'components';
import {
	Home,
	Logout,
	Forbidden,
	UserSettings,
	InvoiceCustomer,
	ShowPictures,
} from 'pages';

import './user-app.scss';

const AboutUs = lazy(() => import('lazyImports/lazyAboutUs'));
const ActiveOrder = lazy(() => import('lazyImports/lazyActiveOrder'));
const Services = lazy(() => import('lazyImports/lazyServices'));
const Pricing = lazy(() => import('lazyImports/lazyPricing'));
const Contact = lazy(() => import('lazyImports/lazyContact'));
const SignIn = lazy(() => import('lazyImports/lazySignIn'));
const SignUp = lazy(() => import('lazyImports/lazySignUp'));
const SignUpCustomer = lazy(() => import('lazyImports/lazySignUpCustomer'));
const OrderCustomer = lazy(() => import('lazyImports/lazyOrderCustomer'));
const SignUpPartner =  lazy(() => import('lazyImports/lazySignUpPartner'));
const Order =  lazy(() => import('lazyImports/lazyOrder'));
const Restore =  lazy(() => import('lazyImports/lazyRestore'));
const OrderInvoicePDFForUserApp = lazy(() => import('lazyImports/lazyOrderInvoicePDFForUserApp'));

export const UserApp: FunctionComponent = () => {
	const [isMobileMenu, setMobileMenu] = useState(false);
	const { isMatches: isMaxWidth670 } = useMediaQuery({ maxWidth: MEDIA_POINT.MOBILE });
	const { isCustomer, user } = useAuth();
	const { setModal, modal } = useContext(ModalContext);
	const { t } = useTranslation(['modal']);

	useEffect(() => {
		if (user) {
			/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
			/* @ts-ignore */
			window.OneSignal = window.OneSignal || [];

			/* eslint-disable-next-line @typescript-eslint/ban-ts-comment */
			/*@ts-ignore*/
			const { OneSignal } = window;

			if (OneSignal?.SERVICE_WORKER_PATH !== '/push/onesignal/OneSignalSDKWorker.js') {
				const initConfig = {
					appId: '983a53be-4a32-4622-9418-a6c4187b3641',
					notifyButton: {
						enable: true,
					},
				};

				OneSignal.push(() => {
					OneSignal.SERVICE_WORKER_PARAM = { scope: '/push/onesignal/' };
					OneSignal.SERVICE_WORKER_PATH = '/push/onesignal/OneSignalSDKWorker.js';
					OneSignal.SERVICE_WORKER_UPDATER_PATH = '/push/onesignal/OneSignalSDKUpdaterWorker.js';
					OneSignal.init(initConfig);
				});
			}
		}
	}, [user]);

	useEffect(() => {
		if (isCustomer && !user?.phoneNumber) {
			setModal({
				isOpen: true,
				text: t('modal:addPhone'),
				isBlocking: true,
			});
		}
	}, [isCustomer, setModal, t, user?.phoneNumber]);

	const handleToggleMobileMenu = () => {
		document.body.classList.toggle('overflow-hidden');
		setMobileMenu(isMobileMenu => !isMobileMenu);
	};

	const baseClassName = 'user-app';
	const componentClassName = {
		component: cn(baseClassName, {[`${baseClassName}--overlay`]: isMobileMenu }),
		headerContainer: `${baseClassName}__header-container`,
		content: `${baseClassName}__content`,
		languageSwitcher: `${baseClassName}__language-switcher`,
	};

	return (
		<div className={componentClassName.component}>
			<Header>
				<Container centered='vertical' className={componentClassName.headerContainer}>
					{isMaxWidth670
						&& <BurgerButton
							isActive={isMobileMenu}
							onClick={handleToggleMobileMenu}
						/>
					}
					<Link to={ROUTES.HOME}>
						<Logo />
					</Link>
					{!isMaxWidth670
						&& <PrimaryNavigation/>
					}
					<LanguageSwitcher className={componentClassName.languageSwitcher}/>
				</Container>
			</Header>
			<Main>
				{isMaxWidth670 && isMobileMenu
					&& <Overlay onClick={handleToggleMobileMenu}>
						<MobileNavigation />
					</Overlay>
				}
				<ErrorBoundary>
					<Container>
						<Suspense fallback={<CircleLoader/>}>
							<Switch>
								<Route exact path={ROUTES.HOME} component={Home} />
								<Route exact path={ROUTES.ABOUT_US} component={AboutUs} />
								<Route exact path={ROUTES.SERVICES} component={Services} />
								<Route exact path={ROUTES.PRICING} component={Pricing} />
								<Route exact path={ROUTES.CONTACT} component={Contact} />
								<Route exact path={ROUTES.SIGN_IN} component={SignIn} />
								<Route exact path={ROUTES.SIGN_UP.URL} component={SignUp} />
								<Route exact path={ROUTES.RESTORE} component={Restore} />
								<Route exact path={ROUTES.GUEST} component={OrderCustomerForm} />
								<Route
									exact
									path={ROUTES.SIGN_UP.CUSTOMER}
									component={SignUpCustomer}
								/>
								<Route
									exact
									path={ROUTES.SIGN_UP.PARTNER}
									component={SignUpPartner}
								/>
								<Route exact path={ROUTES.LOGOUT} component={Logout} />
								<PrivateRoute
									path={ROUTES.USER_SETTINGS.ORIGIN}
									component={UserSettings} roles={[
										ERoles.customer,
										ERoles.partner,
										ERoles.worker,
									]}
								/>
								<Route exact path={ROUTES.FORBIDDEN} component={Forbidden} />
								<PrivateRoute
									roles={[ERoles.customer]}
									path={ROUTES.ORDER.CUSTOMER}
									component={OrderCustomer}
									exact
								/>
								<PrivateRoute
									roles={[ERoles.customer]}
									path={ROUTES.INVOICES}
									component={InvoiceCustomer}
									exact
								/>
								<PrivateRoute
									roles={[ERoles.customer]}
									path={ROUTES.INVOICE.PDF_AS_ROUTE}
									component={OrderInvoicePDFForUserApp}
									exact
								/>
								<PrivateRoute
									roles={[ERoles.customer]}
									path={ROUTES.INVOICE.SHOW_PICTURES_AS_ROUTE}
									component={ShowPictures}
									exact
								/>
								<PrivateRoute
									roles={[ERoles.customer]}
									path={ROUTES.ORDER.ORIGIN}
									component={Order}
									exact
								/>
								<PrivateRoute
									roles={[ERoles.worker]}
									path={ROUTES.ORDER.ACTIVE}
									component={ActiveOrder}
									exact
								/>
								<PrivateRoute
									roles={[ERoles.worker]}
									path={ROUTES.ORDER.CURRENT}
									component={OrderWorkerCurrentOrder}
									exact
								/>
							</Switch>
						</Suspense>
					</Container>
				</ErrorBoundary>
				<Sidebar />
			</Main>
			<Footer />
			{modal.isOpen
				&& <Modal />
			}
		</div>
	);
};
