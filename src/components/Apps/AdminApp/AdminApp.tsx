import { FunctionComponent, useMemo } from 'react';
import { Route } from 'react-router-dom';
import { Admin as ReactAdmin, Resource } from 'react-admin';
import { useTranslation } from 'react-i18next';
import { createBrowserHistory } from 'history';
import { createMuiTheme } from '@material-ui/core/styles';

import { authProvider, dataProvider, EditableFieldsProvider } from 'providers';
import { ROUTES } from 'constants/index';

import { useAuth } from 'hooks';
import {
	Layout,
	Menu,
	PrivateAdminResource,
	PrivatePartnerResource,
} from 'components';
import {
	Customers,
	CustomersShow,
	Dashboard,
	Orders,
	OrdersShow,
	Billing,
	BillingInvoicePDF,
	BillingPayoutOverviewPDF,
	Partner,
	PartnerShow,
	WorkerShow,
	Worker,
	Settings,
	Chat,
	OrderInvoicePDF,
	ShowPictures,
} from 'pages';

import { ReactComponent as BillingIcon } from './icons/billing.svg';
import { ReactComponent as ChatIcon } from './icons/chat.svg';
import { ReactComponent as WorkerIcon } from './icons/worker.svg';
import { ReactComponent as CustomersIcon } from './icons/customers.svg';
import { ReactComponent as DashboardIcon } from './icons/dashboard.svg';
import { ReactComponent as OrdersIcon } from './icons/orders.svg';
import { ReactComponent as PartnerIcon } from './icons/partner.svg';
import { ReactComponent as SettingsIcon } from './icons/settings.svg';

const customRoutes = [
	<Route
		exact
		path={ROUTES.BILLING.SHOW_INVOICE_PDF_AS_ROUTE}
		component={BillingInvoicePDF}
	/>,
	<Route
		exact
		path={ROUTES.BILLING.PAYOUT_OVERVIEW_PDF_AS_ROUTE}
		component={BillingPayoutOverviewPDF}
	/>,
	<Route
		exact
		path={ROUTES.INVOICE.PDF_AS_ROUTE}
		component={OrderInvoicePDF}
	/>,
	<Route
		exact
		path={ROUTES.INVOICE.SHOW_PICTURES_AS_ROUTE}
		component={ShowPictures}
	/>,
];

const muiTheme = createMuiTheme({
	palette: {
		type: 'dark',
		secondary: {
			main: '#4385F3',
		},
	},
	overrides: {
		MuiButton: {
			textPrimary: {
				color: '#31C963',
			},
			textSecondary: {
				color: '#C93131',
			},
		},
	},
	breakpoints: {
		values: {
			xs: 0,
			sm: 0,
			md: 0,
			lg: 0,
			xl: 0,
		},
	},
});

export const AdminApp: FunctionComponent = () => {
	const {
		isAdmin,
		isPartner,
		user,
	} = useAuth();
	const roles = user?.roles;
	const browserHistory = useMemo(() => createBrowserHistory({ basename: ROUTES.ADMIN }), []);
	const { t } = useTranslation(['adminPage', 'button']);

	return (
		<EditableFieldsProvider>
			<ReactAdmin
				authProvider={authProvider}
				dataProvider={dataProvider}
				history={browserHistory}
				theme={muiTheme}
				layout={Layout}
				customRoutes={customRoutes}
				menu={Menu}
			>
				<Resource
					options={{label: t('adminPage:dashboard')}}
					icon={() => <DashboardIcon/>}
					name='dashboard'
					list={Dashboard}
				/>
				<PrivatePartnerResource
					roles={roles}
					isAccess={isPartner || isAdmin}
					options={{label: t('adminPage:customers')}}
					icon={() => <CustomersIcon/>}
					name='customer'
					list={Customers}
					show={CustomersShow}
				/>
				<Resource
					options={{label: t('adminPage:orders')}}
					icon={() => <OrdersIcon/>}
					name='order'
					list={Orders}
					show={OrdersShow}
				/>
				<PrivatePartnerResource
					roles={roles}
					isAccess={isPartner || isAdmin}
					options={{label: t('adminPage:billing')}}
					icon={() => <BillingIcon/>}
					name='billing'
					list={Billing}
				/>
				<PrivateAdminResource
					isAccess={isAdmin}
					options={{label: t('adminPage:partner')}}
					icon={() => <PartnerIcon/>}
					name='partner'
					list={Partner}
					show={PartnerShow}
				/>
				<PrivatePartnerResource
					roles={roles}
					isAccess={isPartner || isAdmin}
					options={{label: t('adminPage:craftsmen')}}
					icon={() => <WorkerIcon/>}
					name='worker'
					list={Worker}
					show={WorkerShow}
				/>
				<Resource
					options={{label: t('adminPage:settings')}}
					icon={() => <SettingsIcon/>}
					name='settings'
					list={Settings}
				/>
				<PrivateAdminResource
					isAccess={isAdmin}
					options={{label: t('adminPage:chat')}}
					icon={() => <ChatIcon/>}
					name='chat'
					list={Chat}
				/>
			</ReactAdmin>
		</EditableFieldsProvider>
	);
};
