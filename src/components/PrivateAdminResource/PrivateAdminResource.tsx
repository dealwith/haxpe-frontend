import { FunctionComponent } from 'react';
import { Resource, ResourceProps } from 'react-admin';

type TProps = ResourceProps & {
	isAccess: boolean;
}

export const PrivateAdminResource: FunctionComponent<TProps> = props => {
	const { isAccess, ...rest } = props;

	return isAccess
		? <Resource
			{...rest}
		/>
		: null;
};
