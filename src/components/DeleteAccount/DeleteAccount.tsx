import { FunctionComponent } from 'react';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { AccountService } from 'services';
import { ROUTES } from 'constants/index';

import { H5, P, Button, Span } from 'components';

import './delete-account.scss';

export const DeleteAccount: FunctionComponent = () => {
	const { t } = useTranslation(['button', 'deleteAccount', 'coupon']);
	const { push } = useHistory();

	const handleClick = async () => {
		try {
			await AccountService.deleteAccount();
			push(ROUTES.SIGN_IN);
		} catch (err) {
			console.error(err);
		}
	};

	const baseClassName = 'delete-account';
	const componentClassName = {
		component: baseClassName,
		text: `${baseClassName}__text`,
	};

	return (
		<div className={componentClassName.component}>
			<H5 className={componentClassName.text} align='center'>
				{t('coupon:coupon').toLocaleUpperCase()}
			</H5>
			<P className={componentClassName.text} align='center'>
				{t('deleteAccount:text')}
			</P>
			<Span className={componentClassName.text} isBold={true}>
				{t('deleteAccount:continueAnyway')}
			</Span>
			<Button
				onClick={handleClick}
				size='half'
				theme='red'
				rounded='xxl'
			>
				{t('button:deleteAccount')}
			</Button>
		</div>
	);
};
