import { CSSProperties, FunctionComponent } from 'react';
import cn from 'classnames';

import { TSize } from './interfaces';

import './image.scss';

type TProps = {
	src?: string;
	alt?: string;
	className?: string;
	size?: TSize;
	width?: number,
	height?: number,
	style?: CSSProperties;
}

export const Image: FunctionComponent<TProps> = ({
	src,
	alt = '',
	className: propsClassName,
	size,
	width,
	height,
	style,
}) => {
	const baseClassName = 'image';
	const componentClassName = cn(
		baseClassName,
		propsClassName,
		{ [`${baseClassName}--size-${size}`]: size },
	);

	return <img
		width={width}
		height={height}
		src={src}
		alt={alt}
		className={componentClassName}
		style={style}
	/>;
};
