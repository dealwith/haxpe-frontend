import { FunctionComponent, useEffect, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { TTheme } from 'interfaces/container/interfaces';

import { useNotify } from 'hooks';
import { CloseButton, Container } from 'components';

import './notification.scss';

type TProps = {
	children?: undefined;
	theme?: TTheme
}

export const Notification: FunctionComponent<TProps> = ({
	theme = 'black',
}) => {
	const ref = useRef<HTMLDivElement>();
	const { t } = useTranslation('errors');
	const { notify: { isNotify, errorText }, handleSetNotify } = useNotify();

	const baseClassName = 'notification';
	const componentClassName = {
		component: baseClassName,
		container: `${baseClassName}__container`,
		active: `${baseClassName}--active`,
	};

	useEffect(() => {
		let id = null;

		if (isNotify && ref?.current) {
			id = setTimeout(() => {
				ref.current.classList.add(componentClassName.active);
			}, 0);
		}

		return () =>  clearTimeout(id);
	}, [isNotify]);

	const handleClose = () => handleSetNotify({
		errorText: '',
		isNotify: false,
	});

	if (!isNotify)
		return null;

	return <div ref={ref} className={componentClassName.component}>
		<Container
			className={componentClassName.container}
			centered='center'
			isInner={true}
			isFillContent={true}
			theme={theme}
		>
			<CloseButton handleClose={handleClose}/>
			{t(`errors:${errorText}`)}
		</Container>
	</div>;
};
