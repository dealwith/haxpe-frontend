import { FunctionComponent, useState} from 'react';
import { useTranslation } from 'react-i18next';
import {
	AllSteps,
	GetState,
	JumpFn,
	StepComponentProps,
} from 'react-step-builder';

import { ROUTES } from 'constants/index';
import { jumpToCorrectStep } from 'components/Order/Customer/utils';
import { TOrderCustomerPaymentSteps } from 'interfaces/order/customer';

import {
	Link,
	ContentContainer,
	P,
	Container,
	Button,
	PayPalButton,
} from 'components';
import applePay from '../icons/applePay.svg';
import cash from '../icons/cash.svg';
import googlePay from '../icons/googlePay.svg';
import masterCard from '../icons/masterCard.svg';
import visa from '../icons/visa.svg';

import './payment-method-general.scss';

type TProps = {
	orderCustomerGetState?: GetState,
	orderCustomerAllSteps?: AllSteps,
	orderCustomerJump?: JumpFn
} & Partial<StepComponentProps>;

const baseClassName = 'payment-method-general';
const componentClassName = {
	component: baseClassName,
	link: `${baseClassName}__link`,
	linksContainer: `${baseClassName}__links-container`,
	buttonContainer: `${baseClassName}__button-container`,
	visaImg: `${baseClassName}__visa-img`,
};

export const PaymentMethodGeneral: FunctionComponent<TProps> = ({
	next,
	prev,
	jump,
	allSteps,
	orderCustomerGetState,
	orderCustomerAllSteps,
	orderCustomerJump,
}) => {
	const { t } = useTranslation(['payment']);
	const { orderId } = orderCustomerGetState('order', '');

	const handleBackClick = () => prev();

	const handleContinueClick = () => next();

	const handleCardClick = () => {
		jumpToCorrectStep<TOrderCustomerPaymentSteps>('cardCheckout', jump, allSteps);
	};

	const handleGPayClick = () => jumpToCorrectStep<TOrderCustomerPaymentSteps>(
		'GPayApplePay',
		jump,
		allSteps,
	);

	const paymentTitle = t('payment:setUpPaymentMethod');

	return (
		<Container
			theme='white'
			maxHeight={580}
			maxWidth={380}
			isInner={true}
		>
			<ContentContainer>
				<P isBold={true} align='center'>
					{paymentTitle}
				</P>
			</ContentContainer>
			<div className={componentClassName.linksContainer}>
				<Button
					className={componentClassName.link}
					onClick={handleGPayClick}
					size='full'
					rounded='m'
					theme='light-gray'
				>
					<img src={applePay} alt='Apple pay' />
				</Button>
				<Button
					className={componentClassName.link}
					onClick={handleGPayClick}
					size='full'
					rounded='m'
					theme='light-gray'
				>
					<img src={googlePay} alt='GOOGLE PAY' />
				</Button>
				<Button
					className={componentClassName.link}
					onClick={handleGPayClick}
					size='full'
					rounded='m'
					theme='light-gray'
				>
					<img src={googlePay} alt='GOOGLE PAY' />
				</Button>
				<Button
					className={componentClassName.link}
					onClick={handleCardClick}
					size='full'
					theme='light-gray'
					rounded='m'
				>
					<img src={visa} alt='VISA card' className={componentClassName.visaImg}/>
					<img src={masterCard} alt='Mastercard' />
				</Button>
				<PayPalButton
					orderId={orderId}
					orderCustomerAllSteps={orderCustomerAllSteps}
					orderCustomerJump={orderCustomerJump}
				/>
				<Link
					className={componentClassName.link}
					href={ROUTES.PAYMENT.METHODS.CASH}
				>
					<img src={cash} alt='CASH' />
				</Link>
			</div>
			<div className={componentClassName.buttonContainer}>
				<Button
					theme='gray'
					onClick={handleBackClick}
					rounded='xxl'
				>
					{t('button:back')}
				</Button>
				<Button
					theme='green'
					onClick={handleContinueClick}
					rounded='xxl'
				>
					{t('button:continue')}
				</Button>
			</div>
		</Container>
	);
};
