import { FunctionComponent } from 'react';
import { Route, Switch } from 'react-router-dom';

import { ROUTES } from 'constants/index';

import { VisaForm } from 'components';
import { PaymentMethodGeneral } from './PaymentMethodGeneral';

import visa from './icons/visa.png';

import './payment-method.scss';

export const PaymentMethod: FunctionComponent = () => {
	const baseClassName = 'payment-method';

	return (
		<div className={baseClassName}>
			<Switch>
				<Route exact path={`*/${ROUTES.PAYMENT.ORIGIN}`}>
					<PaymentMethodGeneral />
				</Route>
				<Route exact path={`*/${ROUTES.PAYMENT.METHODS.VISA}`}>
					<VisaForm iconHref={visa}/>
				</Route>
				<Route exact path={`*/${ROUTES.PAYMENT.METHODS.MASTER_CARD}`}>
					<div>MASTER_CARD</div>
				</Route>
				<Route exact path={`*/${ROUTES.PAYMENT.METHODS.APPLE_PAY}`}>
					<div>APPLE_PAY</div>
				</Route>
				<Route exact path={`*/${ROUTES.PAYMENT.METHODS.GOOGLE_PAY}`}>
					<div>GOOGLE_PAY</div>
				</Route>
				<Route exact path={`*/${ROUTES.PAYMENT.METHODS.CASH}`}>
					<div>CASH</div>
				</Route>
				<Route exact path={`*/${ROUTES.PAYMENT.METHODS.KLARNA}`}>
					<div>KLARNA</div>
				</Route>
				<Route exact path={`*/${ROUTES.PAYMENT.METHODS.PAY_PAL}`}>
					<div>PAY_PAL</div>
				</Route>
			</Switch>
		</div>
	);
};
