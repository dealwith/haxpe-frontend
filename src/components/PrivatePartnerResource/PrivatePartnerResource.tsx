import { FunctionComponent } from 'react';
import { Resource, ResourceProps } from 'react-admin';

import { ERoles, TRoles } from 'interfaces';

import { WorkerCreate } from 'pages';

type TProps = ResourceProps & {
	isAccess: boolean;
	roles: TRoles[];
}

export const PrivatePartnerResource: FunctionComponent<TProps> = props => {
	const { isAccess, roles, ...rest } = props;

	const isPartner = isAccess && roles.includes(ERoles.partner);

	const transformProps = isPartner
		? {...rest, ...{ create: WorkerCreate }}
		: rest;

	return !!(isAccess)
		&& <Resource
			{...transformProps}
		/>;
};
