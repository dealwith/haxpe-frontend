import classNames from 'classnames';
import { FunctionComponent } from 'react';

import styles from './overlay.module.scss';

type TProps = {
	onClick: () => void;
	isCentered?: boolean;
	flow?: 'center';
}

export const Overlay: FunctionComponent<TProps> = ({
	flow,
	onClick,
	children,
}) => {
	const componentClassName = classNames(styles.overlay, {
		[styles[flow]]: flow,
	});

	return  (
		<div onClick={onClick} className={componentClassName}>
			{children}
		</div>
	);
};
