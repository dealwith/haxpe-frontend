import { FunctionComponent } from 'react';

import { Button } from 'components';

import './close-button.scss';

type TProps = {
	handleClose: () => void;
}

export const CloseButton: FunctionComponent<TProps> = ({
	handleClose,
}) => {
	const baseClassName = 'close-button';

	return <Button
		className={baseClassName}
		onClick={handleClose}
		size='auto'
		theme='transparent'
	>
		X
	</Button>;
};
