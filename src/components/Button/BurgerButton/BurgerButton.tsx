import { FunctionComponent } from 'react';
import cn from 'classnames';

import { ReactComponent as BurgerIcon } from '../icons/burger.svg';
import { Button } from '../Button';

import './burger-button.scss';

type TProps = {
	onClick: () => void;
	isActive: boolean;
}

export const BurgerButton: FunctionComponent<TProps> = ({ onClick, isActive }) => {
	const buttonClassName = 'burger-button';
	const baseClassName = cn(buttonClassName, {
		'active': isActive,
	});

	const componentClassName = {
		component: baseClassName,
		icon: `${buttonClassName}__icon`,
	};

	return (
		<Button
			className={baseClassName}
			onClick={onClick}
			size='auto'
		>
			<BurgerIcon className={componentClassName.icon}/>
		</Button>
	);
};
