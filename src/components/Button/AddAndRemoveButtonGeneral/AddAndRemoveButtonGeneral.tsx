import { FunctionComponent, SVGProps } from 'react';
import cn from 'classnames';
import { useTranslation } from 'react-i18next';

import { TTheme } from '../interfaces';

import { Button } from 'components';

import './add-and-remove-button-general.scss';

type TProps = {
	handleOnClick: () => void;
	iconComponent: FunctionComponent<SVGProps<SVGSVGElement> & {
		title?: string;
	}>;
	textForTranslate: string;
	additionalText: string;
	theme: TTheme;
	className?: string;
	textClassName?: string;
}

export const AddAndRemoveButtonGeneral: FunctionComponent<TProps> = ({
	handleOnClick,
	iconComponent: Icon,
	textForTranslate,
	additionalText,
	theme,
	className,
	textClassName,
}) => {
	const { t } = useTranslation('button');

	const baseClassName = 'add-and-remove-button-general';
	const componentClassName = {
		component: baseClassName,
		button: cn(`${baseClassName}__button`, className),
		icon: `${baseClassName}__icon`,
		text: cn(`${baseClassName}__text`, textClassName),
	};

	return (
		<div className={componentClassName.component}>
			<Button
				size='auto'
				rounded='half'
				theme={theme}
				className={componentClassName.button}
				onClick={handleOnClick}
				isText={false}
			>
				<Icon className={componentClassName.icon}/>
			</Button>
			{textForTranslate
				&& <span className={componentClassName.text}>
					{t(`button:${textForTranslate}`)} {additionalText}
				</span>
			}
		</div>
	);
};
