import { FunctionComponent } from 'react';
import { ShowButton } from 'ra-ui-materialui';
import { Record } from 'ra-core';

import { TPosition } from 'interfaces';

import { PositionContainer } from 'components';

type TProps = {
	record: Record;
	position?: TPosition;
}

export const AdminShowButton: FunctionComponent<TProps> = ({
	record,
	position = 'left',
}) => {
	return <PositionContainer position={position}>
		<ShowButton record={record}/>
	</PositionContainer>;
};
