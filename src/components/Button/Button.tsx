import { MouseEvent, FunctionComponent } from 'react';
import cn from 'classnames';

import {
	Rounded,
	TIcon,
	Theme,
	Size,
	TButtonProps,
} from './interfaces';

import { Image } from 'components';
import facebookIcon from './icons/facebookIcon.svg';
import googleIcon from './icons/googleIcon.svg';
import currentLocationIcon from './icons/currentLocationIcon.svg';
import navigationIcon from './icons/navigationIcon.svg';
import userIcon from './icons/userIcon.svg';
import arrowIcon from './icons/arrowIcon.svg';
import showPictures from './icons/showPictures.svg';

import './button.scss';

export const Button: FunctionComponent<TButtonProps> = ({
	type = 'button',
	className: propsClassName,
	isDisabled,
	onClick,
	theme = Theme.primary,
	rounded = Rounded.s,
	size = Size.s,
	children,
	icon,
	flow = 'horizontal',
	iconPosition,
	position,
	isText = true,
}) => {
	const handleClick = (event: MouseEvent): void => {
		if (onClick)
			onClick(event);
	};

	const baseClassName = 'button';
	const buttonClassName = cn(baseClassName, propsClassName, {
		[`${baseClassName}--theme-${theme}`]: theme,
		[`${baseClassName}--rounded-${rounded}`]: rounded,
		[`${baseClassName}--size-${size}`]: size,
		[`${baseClassName}--flow-${flow}`]: flow,
		[`${baseClassName}--position-${position}`]: position,
	});

	const baseIconClassName = `icon`;
	const iconClassName = cn(baseIconClassName, `${baseClassName}__icon`, {
		[`${baseIconClassName}--position-left`]: icon && !iconPosition,
		[`${baseIconClassName}--position-${iconPosition}`]: icon && iconPosition,
		[`${baseIconClassName}--${icon}`]: icon,
	});

	const componentClassName = {
		component: buttonClassName,
		icon: iconClassName,
		text: `${baseClassName}__text`,
	};

	const renderIcon = (icon: TIcon) => {
		let component;

		switch (icon) {
			case 'facebook':
				component = facebookIcon;

				break;

			case 'google':
				component = googleIcon;

				break;

			case 'current-location':
				component = currentLocationIcon;

				break;

			case 'navigation':
				component = navigationIcon;

				break;

			case 'user-icon':
				component = userIcon;

				break;

			case 'arrow':
				component = arrowIcon;

				break;

			case 'show-pictures':
				component = showPictures;

				break;

			default:
				return;
		}

		const altText = `${icon} icon`;

		return (
			isText
				? <span className={componentClassName.icon}>
					<Image src={component} alt={altText} />
				</span>
				: <Image src={component} alt={altText} />
		);
	};

	const renderChildren = () => {
		if (isText) {
			return (
				<span className={componentClassName.text}>
					{children}
				</span>
			);
		}

		return children;
	};

	return (
		<button
			className={componentClassName.component}
			onClick={handleClick}
			disabled={isDisabled as unknown as boolean}
			type={type}
		>
			{icon
				&& renderIcon(icon)
			}
			{children
				&& renderChildren()
			}
		</button>
	);
};
