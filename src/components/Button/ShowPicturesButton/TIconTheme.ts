enum EIconTheme {
	transparent = 'transparent',
	white = 'white',
}

export type TIconTheme = keyof typeof EIconTheme;
