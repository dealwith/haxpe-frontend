import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';

import { ROUTES } from 'constants/routes';
import { TIconTheme } from './TIconTheme';
import { TTheme } from '../interfaces';

import { ReactComponent as ShowPicturesIcon } from './showPictures.svg';
import { useOrderParams } from 'hooks';
import { Button, Span } from 'components';

import './show-pictures-button.scss';

type TProps = {
	iconTheme?: TIconTheme;
	theme?: TTheme;
}

export const ShowPicturesButton: FunctionComponent<TProps> = ({
	iconTheme,
	theme,
}) => {
	const { t } = useTranslation('button');
	const { orderId } = useOrderParams();
	const { push } = useHistory();

	const baseClassName = 'show-pictures-button';
	const componentClassName = {
		component: baseClassName,
		icon: `${baseClassName}__icon--theme-${iconTheme}`,
		text: `${baseClassName}__text`,
	};

	const handleClick = () => push(ROUTES.INVOICE.SHOW_PICTURES_AS_LINK(orderId));

	return <Button
		className={componentClassName.component}
		theme={theme}
		onClick={handleClick}
	>
		<ShowPicturesIcon
			className={componentClassName.icon}
		/>
		<Span className={componentClassName.text}>
			{t('showPictures')}
		</Span>
	</Button>;
};
