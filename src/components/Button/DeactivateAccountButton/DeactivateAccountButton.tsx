import { FunctionComponent } from 'react';
import { ButtonProps } from 'react-admin';

import { AccountService } from 'services';

import { AdminButton } from 'components';
import { useNotify } from 'hooks';

type TProps = {
	className?: string;
	ownerUserId?: string;
}

export const DeactivateAccountButton: FunctionComponent<TProps & ButtonProps> = ({
	ownerUserId,
	className,
	children,
	...rest
}) => {
	const { handleSetNotify } = useNotify();
	const handleDeactivate = async () => {
		try {
			await AccountService.deactivate(ownerUserId);
		} catch (err) {
			if (err instanceof Error) {
				handleSetNotify({
					errorText: 'deactivateFailed',
					isNotify: true,
				});
			} else
				console.error(err);

			console.error(err);
		}
	};

	return (
		<AdminButton
			color='secondary'
			onClick={handleDeactivate}
			className={className}
			{...rest}
		>
			{children}
		</AdminButton>
	);
};
