import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { getPreferLang } from 'utils';
import { AccountService, PartnerService } from 'services';
import { TPartnerPageInfo } from 'interfaces';

import { useEditableFields, useNotify } from 'hooks';
import { AdminButton } from 'components';

type TProps = {
	isDisabled: boolean;
	updateInfo: TPartnerPageInfo;
	changedFieldsNames: string[];
}

export const UpdatePartnerButton: FunctionComponent<TProps> = ({
	isDisabled,
	updateInfo,
	changedFieldsNames,
}) => {
	const { t } = useTranslation('button');
	const { handleClearEditableFields } = useEditableFields();
	const { handleSetNotify } = useNotify();

	const partnerChangedFieldNames = ['services', 'industries', 'partner'];
	const isPartnerInfoChanged = changedFieldsNames
		?.some(fieldName => partnerChangedFieldNames.indexOf(fieldName) !== -1);

	const handleUpdatePartner = async () => {
		const transformedIndustries = updateInfo?.industries
			?.map(({ id }) => ({ industryId: id }));
		const transformedServices = updateInfo?.services
			?.map(({ id }) => ({ serviceTypeId: id }));

		const bodyToRequest = {
			name: updateInfo.partner?.name,
			description: 'anything(unused field)',
			addressId: updateInfo.address.id,
			ownerId: updateInfo.user.id,
			numberOfWorkers: updateInfo.partner?.numberOfWorkers,
			partnerStatus: updateInfo.user.accountStatus,
			industries: transformedIndustries,
			serviceTypes: transformedServices,
		};

		const userRequest = {
			name: updateInfo.user.name,
			surname: updateInfo.user.surname,
			phoneNumber: updateInfo.user.phoneNumber,
			preferLanguage: getPreferLang(),
			email: updateInfo.user.email,
		};

		try {
			if (changedFieldsNames.includes('user'))
				await AccountService.updateAccountById('xxx', userRequest);

			if (isPartnerInfoChanged)
				await PartnerService.updatePartner(updateInfo.id, bodyToRequest);

			handleClearEditableFields();
		} catch (err) {
			if (err instanceof Error) {
				handleSetNotify({
					errorText: 'failed',
					isNotify: true,
				});
			} else
				console.error(err);

			console.error(err);
		}
	};

	return <AdminButton
		onClick={handleUpdatePartner}
		disabled={isDisabled}
		label={t('button:saveChanges')}
	/>;
};
