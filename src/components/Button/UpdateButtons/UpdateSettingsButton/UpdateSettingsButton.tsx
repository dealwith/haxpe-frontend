import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { getPreferLang } from 'utils';
import { AccountService } from 'services';
import { TSettingsPageInfo } from 'interfaces';

import { useEditableFields, useNotify } from 'hooks';
import { AdminButton } from 'components';

type TProps = {
	isDisabled: boolean;
	updateInfo: TSettingsPageInfo;
	changedFieldsNames: string[];
}

export const UpdateSettingsButton: FunctionComponent<TProps> = ({
	isDisabled,
	updateInfo,
	changedFieldsNames,
}) => {
	const { t } = useTranslation('button');
	const { handleSetNotify } = useNotify();
	const { handleClearEditableFields } = useEditableFields();

	const handleUpdateSettings = async () => {
		const userRequest = {
			name: updateInfo.account.name,
			surname: updateInfo.account.surname,
			phoneNumber: updateInfo.account.phoneNumber,
			preferLanguage: getPreferLang(),
			email: updateInfo.account.email,
		};

		try {
			if (changedFieldsNames.includes('account'))
				await AccountService.updateAccountById(updateInfo.account.id, userRequest);

			handleClearEditableFields();
		} catch (err) {
			if (err instanceof Error) {
				handleSetNotify({
					errorText: 'failed',
					isNotify: true,
				});
			} else
				console.error(err);

			console.error(err);
		}
	};

	return <AdminButton
		onClick={handleUpdateSettings}
		disabled={isDisabled}
		label={t('button:saveChanges')}
	/>;
};
