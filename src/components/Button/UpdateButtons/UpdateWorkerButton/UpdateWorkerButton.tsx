import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { useEditableFields, useNotify } from 'hooks';
import { AccountService, WorkerService } from 'services';
import { TWorkerPageInfo } from 'interfaces';

import { AdminButton } from 'components';

type TProps = {
	isDisabled: boolean;
	updateInfo: TWorkerPageInfo;
	changedFieldsNames: string[];
}

export const UpdateWorkerButton: FunctionComponent<TProps> = ({
	isDisabled,
	updateInfo,
	changedFieldsNames,
}) => {
	const { t } = useTranslation('button');
	const { handleSetNotify } = useNotify();
	const { handleClearEditableFields } = useEditableFields();

	const isWorkerInfoChanged = changedFieldsNames?.includes('worker')
	|| changedFieldsNames?.includes('services')
	|| changedFieldsNames?.includes('industries');

	const handleUpdateWorker = async () => {
		const transformedServices = updateInfo?.services
			?.map(({ id }) => ({ serviceTypeId: id }));

		const bodyToRequest = {
			partnerId: updateInfo.partnerId,
			userId: updateInfo.user.id,
			serviceTypes: transformedServices,
		};

		const userRequest = {
			name: updateInfo.user.name,
			surname: updateInfo.user.surname,
			phoneNumber: updateInfo.user.phoneNumber,
			preferLanguage: localStorage.getItem('i18nextLng'),
			email: updateInfo.user.email,
		};

		try {
			if (changedFieldsNames.includes('user')) {
				const {
					errorCode: userUpdateErrorCode,
				} = await AccountService.updateAccountById(updateInfo.user.id, userRequest);

				if (userUpdateErrorCode)
					throw new Error(userUpdateErrorCode);

			}

			if (isWorkerInfoChanged) {
				const {
					errorCode: workerUpdateErrorCode,
				} = await WorkerService.updateWorker(updateInfo.id, bodyToRequest);

				if (workerUpdateErrorCode)
					throw new Error(workerUpdateErrorCode);

			}

			handleClearEditableFields();
		} catch (err) {
			if (err instanceof Error) {
				handleSetNotify({
					errorText: 'failed',
					isNotify: true,
				});
			} else
				console.error(err);

			console.error(err);
		}
	};

	return <AdminButton
		onClick={handleUpdateWorker}
		disabled={isDisabled}
		label={t('button:saveChanges')}
	/>;
};
