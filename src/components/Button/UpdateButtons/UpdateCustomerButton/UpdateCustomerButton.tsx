import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { AccountService } from 'services';
import { TCustomerPageInfo } from 'interfaces';

import { useEditableFields, useNotify } from 'hooks';
import { AdminButton } from 'components';

type TProps = {
	isDisabled: boolean;
	updateInfo: TCustomerPageInfo;
	changedFieldsNames: string[];
}

export const UpdateCustomerButton: FunctionComponent<TProps> = ({
	isDisabled,
	updateInfo,
	changedFieldsNames,
}) => {
	const { t } = useTranslation('button');
	const { handleSetNotify } = useNotify();
	const { handleClearEditableFields } = useEditableFields();

	const handleUpdateCustomer = async () => {
		const userRequest = {
			name: updateInfo.user.name,
			surname: updateInfo.user.surname,
			phoneNumber: updateInfo.user.phoneNumber,
			preferLanguage: localStorage.getItem('i18nextLng'),
			email: updateInfo.user.email,
		};

		try {
			if (changedFieldsNames.includes('user'))
				await AccountService.updateAccountById(updateInfo.user.id, userRequest);

			handleClearEditableFields();
		} catch (err) {
			if (err instanceof Error) {
				handleSetNotify({
					errorText: 'failed',
					isNotify: true,
				});
			} else
				console.error(err);

			console.error(err);
		}
	};

	return <AdminButton
		onClick={handleUpdateCustomer}
		disabled={isDisabled}
		label={t('button:saveChanges')}
	/>;
};
