import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { TTheme } from '../interfaces';

import { Button, Span } from 'components';
import { ReactComponent as ForwardInvoiceIcon } from './letter.svg';

import './forward-invoice-button.scss';

type TProps = {
	iconTheme?: 'transparent' | 'white';
	theme?: TTheme;
}

export const ForwardInvoiceButton: FunctionComponent<TProps> = ({
	iconTheme,
	theme,
}) => {
	const { t } = useTranslation('button');

	const baseClassName = 'forward-invoice-button';
	const componentClassName = {
		component: baseClassName,
		icon: `${baseClassName}__icon--theme-${iconTheme}`,
		text: `${baseClassName}__text`,
	};

	return <Button
		theme={theme}
		className={componentClassName.component}
	>
		<ForwardInvoiceIcon className={componentClassName.icon} />
		<Span className={componentClassName.text}>
			{t('forwardInvoice')}
		</Span>
	</Button>;
};
