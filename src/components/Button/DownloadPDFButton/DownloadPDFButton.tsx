import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { TTheme } from '../interfaces';

import { usePDFDownloadHandler } from 'hooks';
import { Button } from 'components';

type TProps = {
	theme?: TTheme;
}

export const DownloadPDFButton: FunctionComponent<TProps> = ({
	theme = 'transparent',
}) => {
	const { t } = useTranslation('button');
	const { downloadHandler } = usePDFDownloadHandler();

	return <Button
		theme={theme}
		onClick={downloadHandler}
	>
		{t('download')}
	</Button>;
};
