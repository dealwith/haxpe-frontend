import { FunctionComponent } from 'react';

import { TTheme } from '../interfaces';

import { AddAndRemoveButtonGeneral } from '../AddAndRemoveButtonGeneral';
import { ReactComponent as RemoveIcon } from './minus.svg';

type TProps = {
	handleOnClick: () => void;
	className?: string;
	textClassName?: string;
	textForTranslate?: string;
	additionalText?: string;
	theme: TTheme;
}

export const RemoveButton: FunctionComponent<TProps> = ({
	handleOnClick,
	className,
	textClassName,
	textForTranslate,
	additionalText,
	theme,
}) => {
	return (
		<AddAndRemoveButtonGeneral
			handleOnClick={handleOnClick}
			iconComponent={RemoveIcon}
			className={className}
			textForTranslate={textForTranslate}
			textClassName={textClassName}
			additionalText={additionalText}
			theme={theme}
		/>
	);
};
