import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { TOrderWorkerCurrentOrderSteps } from 'interfaces';
import { THandleChangeStep } from 'hooks/interfaces';
import { TButtonProps } from 'components/Button/interfaces';

import { Button } from 'components';

type TProps = {
	buttonProps: TButtonProps;
	handleChangeStep: THandleChangeStep<TOrderWorkerCurrentOrderSteps>;
	orderId: string;
}

export const CurrentOrderCancelAndReportProblemButton: FunctionComponent<TProps> = ({
	buttonProps,
	handleChangeStep,
	orderId,
}) => {
	const { t } = useTranslation('button');

	const handleClick = async () => handleChangeStep('orderCancelReasonForm');

	return (
		<Button
			onClick={handleClick}
			{...buttonProps}
			theme='red'
		>
			{t('button:cancelAndReportProblem')}
		</Button>
	);
};
