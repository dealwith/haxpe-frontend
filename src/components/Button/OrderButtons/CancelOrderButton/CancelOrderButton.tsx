import { FunctionComponent } from 'react';
import { ButtonProps, useRefresh } from 'react-admin';

import { OrderService } from 'services';
import {
	EOrderStatus,
	TOrderStatuses,
} from 'interfaces';

import { AdminButton } from 'components';

type TProps = {
	orderId: string;
	orderStatus: TOrderStatuses;
} & ButtonProps;

export const CancelOrderButton: FunctionComponent<TProps> = ({
	label,
	color,
	orderId,
	orderStatus,
}) => {
	const refresh = useRefresh();

	const handleClick = async () => {
		try {
			await OrderService.cancelOrder('ads');
			refresh();
		} catch (err) {
			console.error(err);
		}
	};

	return (
		<AdminButton
			disabled={orderStatus === EOrderStatus.canceled}
			label={label}
			color={color}
			onClick={handleClick}
		/>
	);
};
