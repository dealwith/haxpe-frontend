import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { OrderService } from 'services';
import { TButtonProps } from 'components/Button/interfaces';

import { Button } from 'components';

	type TProps = {
		buttonProps: TButtonProps;
		orderId: string;
	}

export const PauseOrderButton: FunctionComponent<TProps> = ({
	buttonProps,
	orderId,
}) => {
	const { t } = useTranslation('button');

	const handleClick = async () => {
		try {
			await OrderService.pauseOrder(orderId);
		} catch (err) {
			console.error(err);
		}
	};

	return (
		<Button
			{...buttonProps}
			theme='light-green'
			onClick={handleClick}
		>
			{t('button:pauseOrder')}
		</Button>
	);
};
