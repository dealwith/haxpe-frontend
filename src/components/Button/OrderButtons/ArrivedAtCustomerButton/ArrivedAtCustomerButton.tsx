import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { OrderService } from 'services';
import { TOrderWorkerCurrentOrderSteps } from 'interfaces';
import { THandleChangeStep } from 'hooks/interfaces';
import { TButtonProps } from 'components/Button/interfaces';

import { Button } from 'components';

type TProps = {
	buttonProps: TButtonProps;
	handleChangeStep: THandleChangeStep<TOrderWorkerCurrentOrderSteps>;
	orderId: string;
}

export const ArrivedAtCustomerButton: FunctionComponent<TProps> = ({
	orderId,
	buttonProps,
	handleChangeStep,
}) => {
	const { t } = useTranslation('button');

	const handleClick = async () => {
		try {
			await OrderService.arrivedAtCustomer(orderId);
			handleChangeStep('arrived');
		} catch (err) {
			console.error(err);
		}
	};

	return (
		<Button
			{...buttonProps}
			onClick={handleClick}
		>
			{t('button:arrivedAtCustomer')}
		</Button>
	);
};
