import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { OrderService, InvoiceService } from 'services';
import { TOrderWorkerCurrentOrderSteps } from 'interfaces';
import { THandleChangeStep } from 'hooks/interfaces';
import { TButtonProps } from 'components/Button/interfaces';

import { Button } from 'components';

type TProps = {
	buttonProps: TButtonProps;
	handleChangeStep: THandleChangeStep<TOrderWorkerCurrentOrderSteps>;
	orderId: string;
	isDisabled?: boolean;
}

export const CompleteOrderButton: FunctionComponent<TProps> = ({
	buttonProps,
	handleChangeStep,
	orderId,
	isDisabled,
}) => {
	const { t } = useTranslation('button');

	const handleClick = async () => {
		try {
			await InvoiceService.createInvoice(orderId);
			await OrderService.completeWork(orderId);
			handleChangeStep('workCompleted');
		} catch (err) {
			console.error(err);
		}
	};

	return (
		<Button
			{...buttonProps}
			onClick={handleClick}
			isDisabled={isDisabled}
		>
			{t('button:completeOrder')}
		</Button>
	);
};
