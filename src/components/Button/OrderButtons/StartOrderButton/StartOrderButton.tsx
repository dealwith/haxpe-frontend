import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

import { OrderService } from 'services';
import { TOrderWorkerCurrentOrderSteps } from 'interfaces';
import { THandleChangeStep } from 'hooks/interfaces';
import { TButtonProps } from 'components/Button/interfaces';

import { Button } from 'components';

type TProps = {
	buttonProps: TButtonProps;
	handleChangeStep: THandleChangeStep<TOrderWorkerCurrentOrderSteps>;
	orderId: string;
}

export const StartOrderButton: FunctionComponent<TProps> = ({
	buttonProps,
	handleChangeStep,
	orderId,
}) => {
	const { t } = useTranslation('button');

	const handleClick = async () => {
		try {
			await OrderService.startOrder(orderId);
			handleChangeStep('inProgress');
		} catch (err) {
			console.error(err);
		}
	};

	return (
		<Button
			{...buttonProps}
			onClick={handleClick}
		>
			{t('button:startOrder')}
		</Button>
	);
};
