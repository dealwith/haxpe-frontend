import { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';
import { MenuItem } from '@material-ui/core';
import { PowerSettingsNew } from '@material-ui/icons';

import { useAuth } from 'hooks';

import './admin-logout-button.scss';

export const AdminLogoutButton: FunctionComponent = () => {
	const { logout } = useAuth();
	const { t } = useTranslation('button');

	const handleClick = async () => logout();

	const iconClassName = 'admin-logout-button__icon';

	return (
		<MenuItem onClick={handleClick}>
			<PowerSettingsNew className={iconClassName} /> {t('button:logout')}
		</MenuItem>
	);
};
