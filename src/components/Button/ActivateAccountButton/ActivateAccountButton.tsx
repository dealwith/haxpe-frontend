import { FunctionComponent } from 'react';
import { ButtonProps } from 'react-admin';

import { AccountService } from 'services';

import { AdminButton } from 'components';
import { useNotify } from 'hooks';

type TProps = {
	ownerUserId: string;
}

export const ActivateAccountButton: FunctionComponent<ButtonProps & TProps> = ({
	ownerUserId,
	children,
	...rest
}) => {
	const { handleSetNotify } = useNotify();
	const handleActivate = async () => {
		try {
			await AccountService.activate(ownerUserId);
		} catch (err) {
			if (err instanceof Error) {
				handleSetNotify({
					errorText: 'activateFailed',
					isNotify: true,
				});
			} else
				console.error(err);

			console.error(err);
		}
	};

	return (
		<AdminButton
			onClick={handleActivate}
			{...rest}
		>
			{children}
		</AdminButton>
	);
};
