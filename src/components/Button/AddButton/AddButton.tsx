import { FunctionComponent } from 'react';

import { TTheme } from '../interfaces';

import { AddAndRemoveButtonGeneral } from '../AddAndRemoveButtonGeneral';
import { ReactComponent as AddIcon } from './Plus.svg';

type TProps = {
	handleOnClick: () => void;
	className?: string;
	textClassName?: string;
	textForTranslate?: string;
	additionalText?: string;
	theme: TTheme;
}

export const AddButton: FunctionComponent<TProps> = ({
	handleOnClick,
	className,
	textClassName,
	textForTranslate,
	additionalText,
	theme,
}) => {
	return (
		<AddAndRemoveButtonGeneral
			className={className}
			textClassName={textClassName}
			additionalText={additionalText}
			textForTranslate={textForTranslate}
			handleOnClick={handleOnClick}
			iconComponent={AddIcon}
			theme={theme}
		/>
	);
};
