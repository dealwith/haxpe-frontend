enum EButton {
	button = 'button',
	submit = 'submit',
	reset = 'reset',
}

export type TButton = keyof typeof EButton;
