export enum Rounded {
	s = 's',
	sm = 'sm',
	m = 'm',
	l = 'l',
	xl = 'xl',
	xxl = 'xxl',
	half = 'half',
}

export type TRounded = keyof typeof Rounded;
