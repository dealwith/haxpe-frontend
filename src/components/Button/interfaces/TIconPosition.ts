export type TIconPosition = 'left'
| 'right'
| 'left-reverse';
