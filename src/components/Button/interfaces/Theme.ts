export enum Theme {
	primary = 'primary',
	secondary = 'secondary',
	green = 'green',
	gray = 'gray',
	blue = 'blue',
	transparent = 'transparent',
	red = 'red',
	yellow = 'yellow',
	'light-gray' = 'light-gray',
	'dark-blue' = 'dark-blue',
	'light-green' = 'light-green',
	'dusty-gray' = 'dusty-gray',
	'transparent-mine-shaft' = 'transparent-mine-shaft'
}

export type TTheme = keyof typeof Theme;
