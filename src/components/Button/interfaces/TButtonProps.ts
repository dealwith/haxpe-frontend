import { ReactNode, MouseEvent } from 'react';

import { TPosition } from 'interfaces';
import {
	TIcon,
	TRounded,
	TSize,
	TButton,
	TTheme,
	TFlow,
	TIconPosition,
} from '../interfaces';

export type TButtonProps = {
	className?: string;
	onClick?: (event: MouseEvent) => void;
	type?: TButton;
	children?: Element | string | ReactNode;
	isDisabled?: boolean;
	theme?: TTheme;
	rounded?: TRounded;
	size?: TSize;
	icon?: TIcon;
	flow?: TFlow;
	iconPosition?: TIconPosition;
	isText?: boolean;
	position?: TPosition;
}
