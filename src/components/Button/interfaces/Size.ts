export enum Size {
	m = 'm',
	s = 's',
	l = 'l',
	xl = 'xl',
	xxl = 'xxl',
	half = 'half',
	full = 'full',
	auto = 'auto',
	xxxl = 'xxxl',
}

export type TSize = keyof typeof Size;
