enum Icon {
	facebook = 'facebook',
	google = 'google',
	'current-location' = 'current-location',
	navigation = 'navigation',
	'user-icon' = 'user-icon',
	arrow = 'arrow',
	'show-pictures' = 'show-pictures',
}

export type TIcon = keyof typeof Icon;
