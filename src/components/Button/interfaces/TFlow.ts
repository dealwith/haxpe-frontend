import { EFlow } from './';

export type TFlow = keyof typeof EFlow;
