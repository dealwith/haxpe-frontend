export interface IResponseFacebook {
	accessToken: string;
	data_access_expiration_time: number;
	email: string;
	expiresIn: number;
	grantedScopes: string;
	graphDomain: string;
	id: string;
	name: string;
	signedRequest: string;
	userID: string;
}
