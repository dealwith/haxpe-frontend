import { FunctionComponent } from 'react';
import { GoogleLogin } from 'react-google-login';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { ROUTES } from 'constants/index';
import { CustomerService } from 'services';
import { TButtonProps } from './interfaces';

import { useAuth } from 'hooks';
import { Button } from 'components';

const GOOGLE_CLIENT_ID = process.env.REACT_APP_GOOGLE_CLIENT_ID;

type TProps = {
	isSmall?: boolean;
	size?: TButtonProps['size'];
}

export const GoogleButton: FunctionComponent<TProps> = ({ isSmall, size = 'half' }) => {
	const { googleAuth } = useAuth();
	const history = useHistory();
	const { t } = useTranslation('button');

	const responseGoogle = async googleUser => {
		try {
			const user = await googleAuth(googleUser);

			if (user?.id) {
				const customerInfoRes = await CustomerService.getCustomerInfo()
					.catch(() => null);

				if (customerInfoRes === null)
					await CustomerService.createCustomer({ userId: user.id });

				return history.push(ROUTES.ORDER.CUSTOMER);
			}
		} catch (error) {
			// TODO: Handle error from backend
			// if the customer for the account was already created
			console.error(error);
		}
	};

	const fail = data => console.error(data);

	const text = isSmall
		? t('login')
		: t('loginGoogle');

	return (
		<GoogleLogin
			render={renderProps => (
				<Button
					onClick={renderProps.onClick}
					isDisabled={renderProps.disabled}
					size={size}
					icon='google'
				>
					{text}
				</Button>
			)}
			clientId={GOOGLE_CLIENT_ID}
			onSuccess={responseGoogle}
			onFailure={fail}
		/>
	);
};
