import { useTranslation } from 'react-i18next';
import { FunctionComponent } from 'react';
import { useHistory } from 'react-router-dom';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props';

import { ROUTES } from 'constants/index';
import { IResponseFacebook, TButtonProps } from './interfaces';

import { useAuth } from 'hooks';
import { Button } from 'components';
import { CustomerService } from 'services';

const FACEBOOK_APP_ID = process.env.REACT_APP_FACEBOOK_APP_ID;

type TProps = {
	isSmall?: boolean;
	size?: TButtonProps['size'];
}

export const FacebookButton: FunctionComponent<TProps> = ({
	isSmall,
	size = 'half',
}) => {
	const { facebookAuth } = useAuth();
	const history = useHistory();
	const { t } = useTranslation('button');

	const responseFacebook = async (res: IResponseFacebook) => {
		try {
			const user = await facebookAuth(res);

			if (user?.id) {
				const customer = await CustomerService.getCustomerInfo()
					.catch(err => {
						console.error(err);
					});

				if (customer && customer.body?.id)
					await CustomerService.createCustomer({ userId: user.id });

				return history.push(ROUTES.ORDER.CUSTOMER);
			}
		} catch (error) {
			console.error(error);
		}
	};

	return (
		<FacebookLogin
			appId={FACEBOOK_APP_ID}
			callback={responseFacebook}
			fields='name,email'
			render={renderProps =>
				<Button
					theme='dark-blue'
					size={size}
					icon='facebook'
					onClick={renderProps.onClick}
				>
					{isSmall
						? t('login')
						: t('loginFacebook')}
				</Button>
			}
		/>
	);
};
