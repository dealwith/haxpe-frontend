import { FunctionComponent } from 'react';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import cn from 'classnames';

import { TPosition } from 'interfaces';
import { TSize } from './interfaces';

import { AdminButton } from 'components';
import { Button } from './Button';
import { ReactComponent as BackIcon } from './icons/backToPreviousPageIcon.svg';

import './button.scss';

type TProps = {
	className?: string;
	size?: TSize;
	position?: TPosition;
}

export const BackToPreviousPageButton: FunctionComponent<TProps> = ({
	className: propsClassName,
	size = 'full',
	position,
}) => {
	const { goBack } = useHistory();
	const { t } = useTranslation('button');

	const isInAdmin = window.location.pathname.includes('admin');

	const handleHistoryGoBack = () => goBack();

	const baseClassName = 'back-to-previous-page';
	const componentClassName = cn(baseClassName, propsClassName);

	return (
		isInAdmin
			? (
				<AdminButton
					label={t('back')}
					onClick={handleHistoryGoBack}
				>
					<BackIcon />
				</AdminButton>
			)
			: (
				<Button
					className={componentClassName}
					theme='transparent'
					size={size}
					onClick={handleHistoryGoBack}
					icon='arrow'
					iconPosition='left-reverse'
					position={position}
				>
					{t('back')}
				</Button>
			));
};
