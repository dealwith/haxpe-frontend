import { PayPalButtons, PayPalButtonsComponentProps } from '@paypal/react-paypal-js';
import {
	FunctionComponent,
	useEffect,
	useState,
} from 'react';

import { OrderService } from 'services';
import { jumpToCorrectStep } from 'components/Order/Customer/utils';
import { EOrderStatus } from 'interfaces';
import { AllSteps, JumpFn } from 'react-step-builder';

type TProps = {
	orderId: string;
	orderCustomerAllSteps?: AllSteps,
	orderCustomerJump?: JumpFn
}

export const PayPalButton: FunctionComponent<TProps> = ({
	orderId,
	orderCustomerJump,
	orderCustomerAllSteps,
}) => {
	const [isSuccess, setSuccess] = useState(false);

	const createOrder = async () => {
		const paymentCreateRes = await OrderService.paymentCreate(orderId);

		return paymentCreateRes.body.orderPaymentId;
	};

	const onApprove: PayPalButtonsComponentProps['onApprove'] = async (data, actions) => {
		const paymentCaptureRes = await OrderService.paymentCapture(orderId, data.orderID);

		if (paymentCaptureRes.status === 'success')
			return setSuccess(true);

	};

	useEffect(() => {
		if (isSuccess) {
			jumpToCorrectStep(
				EOrderStatus.completed,
				orderCustomerJump,
				orderCustomerAllSteps,
			);
		}
	}, [isSuccess]);

	return <PayPalButtons
		style={{
			color: 'silver',
			height: 45,
		}}
		createOrder={createOrder}
		onApprove={onApprove}
	/>;
};
