import { FunctionComponent } from 'react';
import { Button, ButtonProps } from 'react-admin';

export const AdminButton: FunctionComponent<ButtonProps> = ({
	children,
	...rest
}) => {
	return (
		<Button
			{...rest}
		>
			{children}
		</Button>
	);
};
