export interface IWorkerCreateInfo {
	partnerId: string;
	firstName: string;
	lastName: string;
	email: string;
	phone: string;
}
