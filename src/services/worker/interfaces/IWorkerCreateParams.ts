export interface IWorkerCreateFormParams {
	partnerId: string;
	firstName: string;
	lastName: string;
	email: string;
	phone: string;
	workerServiceTypes: number[];
}
