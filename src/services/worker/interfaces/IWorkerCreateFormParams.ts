import { IWorkerCreateInfo } from './IWorkerCreateInfo';

export interface IWorkerCreateFormParams extends IWorkerCreateInfo {
	workerServiceTypes: number[];
}
