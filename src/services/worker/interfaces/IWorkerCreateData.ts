import { IWorkerCreateInfo } from './IWorkerCreateInfo';

type TWorkerServiceTypes = {
	serviceTypeId: number;
}

export interface IWorkerCreateData extends IWorkerCreateInfo {
	serviceTypes: TWorkerServiceTypes[];
}
