import { GetListParams } from 'react-admin';

import {
	IWorker,
	IWorkerUpdateData,
	TResponse,
	TResponseMany,
	TResponsePage,
} from 'interfaces';
import { CustomFetch } from 'utils';
import { URLS } from 'constants/index';
import { createAndGetQueryParams } from 'services/utils';
import { IWorkerCreateData } from './interfaces';

export class WorkerService {
	static createWorker(data: IWorkerCreateData): TResponse<IWorker> {
		return CustomFetch.post(URLS.WORKER.CREATE, {...data});
	}

	static getWorkerPage(params: GetListParams): TResponsePage<IWorker> {
		const queryParams = createAndGetQueryParams(params);

		return CustomFetch.get(URLS.WORKER.PAGE, { queryParams });

	}

	static getWorkerById(id: string): TResponse<IWorker> {
		return CustomFetch.get(URLS.WORKER.ID(id));
	}

	static workerInfo(): TResponse<IWorker> {
		return CustomFetch.get(URLS.WORKER.INFO);
	}

	static getWorkersByPartnerId(partnerId: string): TResponseMany<IWorker> {
		return CustomFetch.get(URLS.WORKER.ORIGIN, { queryParams: { partnerId } });
	}

	static updateWorker(id: string, data: IWorkerUpdateData): TResponse<IWorker> {
		return CustomFetch.put(URLS.WORKER.ID(id), { ...data });
	}
}
