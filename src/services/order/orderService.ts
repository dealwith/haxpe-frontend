import { GetListParams } from 'react-admin';

import { CustomFetch } from 'utils';
import { URLS } from 'constants/index';
import { createAndGetQueryParams } from 'services/utils';
import {
	TResponse,
	TResponseMany,
	IOrderRes,
	TResponsePage,
	IRate,
	IPayment,
	TExtraServices,
	IImage,
} from 'interfaces';
import {
	IGetOrderParams,
	IOrderCreateData,
	ICancelOrderData,
	IAssignWorkerParams,
	IArrivalTimeParams,
	IPaymentCreate,
} from './interfaces';

/**
 * Field invoice in OrderRes interface appears
 * only after invoice creation
 */
export class OrderService {
	static getOrderPage(params: GetListParams): TResponsePage<IOrderRes> {
		const queryParams = createAndGetQueryParams(params);

		return CustomFetch.get(URLS.ORDER.PAGE, {queryParams});
	}

	static getOrderById(id: string): TResponse<IOrderRes> {
		return CustomFetch.get(URLS.ORDER.ID(id));
	}

	static createOrder(data: IOrderCreateData): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.CREATE, { ...data });
	}

	static applyCoupon(orderId: string, code: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.APPLY_COUPON(orderId), { code });
	}

	static workerStartOrder(orderId: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.START(orderId));
	}

	static customerStartOrder(orderId: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.CONFIRM(orderId));
	}

	static customerReject(orderId: string, data?: ICancelOrderData): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.CUSTOMER_REJECT(orderId), data);
	}

	static workerReject(orderId: string, data?: ICancelOrderData): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.WORKER_REJECT(orderId), data);
	}

	static cancelOrder(orderId: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.CANCEL(orderId));
	}

	static getOrder(params: IGetOrderParams): TResponseMany<IOrderRes> {
		return CustomFetch.get(URLS.ORDER.ORIGIN, { queryParams: {...params} });
	}

	static getLastOrder(params: IGetOrderParams): Promise<IOrderRes> {
		return OrderService.getOrder(params).then(( { body } ) => body[body.length - 1]);
	}

	static assignWorker({orderId, workerId}: IAssignWorkerParams): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.ASSIGN_WORKER(orderId, workerId));
	}

	static addArrivalTime({
		orderId,
		expectedArrival,
	}: IArrivalTimeParams): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.ADD_ARRIVAL_TIME(orderId), { expectedArrival });
	}

	static arrivedAtCustomer(orderId: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.ARRIVED_AT_CUSTOMER(orderId));
	}

	static completeWork(orderId: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.COMPLETE_WORK(orderId));
	}

	static startOrder(orderId: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.START(orderId));
	}

	static pauseOrder(orderId: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.PAUSE(orderId));
	}

	static resumeOrder(orderId: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.RESUME(orderId));
	}

	static completeOrder(orderId: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.COMPLETE(orderId));
	}

	static rejectByCustomer(orderId: string, data: ICancelOrderData): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.CUSTOMER_REJECT(orderId), {...data});
	}

	static paymentStart(orderId: string): TResponse<IPayment> {
		return CustomFetch.post(URLS.ORDER.PAYMENT_START(orderId));
	}

	static rate(orderId: string, data: IRate): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.RATE(orderId), data);
	}

	static sendInvoice(orderId: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.SEND_INVOICE(orderId));
	}

	static addReduction(orderId: string, data: number): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.REDUCTION_AS_QUERY(orderId, data));
	}

	static addReport(orderId: string, data: string): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.REPORT_AS_QUERY(orderId, data));
	}

	static addExtraServices(orderId: string, data: TExtraServices): TResponse<IOrderRes> {
		return CustomFetch.post(URLS.ORDER.EXTRA_SERVICES(orderId), data);
	}

	static addImages(orderId: string, data: FormData): TResponseMany<IImage> {
		return CustomFetch.postFiles(URLS.ORDER.IMAGES(orderId), data);
	}

	static getImagesByOrderId(orderId: string): TResponseMany<IImage> {
		return CustomFetch.get(URLS.ORDER.IMAGES_BY__ORDER_ID(orderId));
	}

	static getImageById(orderId: string, imageId: string): Promise<string> {
		return CustomFetch.get(URLS.ORDER.IMAGE_BY_IMAGE_ID(orderId, imageId));
	}

	static hourRate(): TResponse<number> {
		return CustomFetch.get(URLS.ORDER.HOUR_RATE);
	}

	/**
	 * Creates a paypal payment for the order with the given orderId.
	 *
	 * @param orderId Id of the active order.
	 * @returns Data from PayPal Order API.
	*/
	static paymentCreate(orderId: string): TResponse<IPaymentCreate> {
		return CustomFetch.post(URLS.ORDER.PAYMENT_CREATE(orderId));
	}

	/**
	 * Captures a payment for a PayPal order.
	 *
	 * @param orderId Id of the active order.
	 * @param orderPaymentId Id of a PayPal payment.
	*/
	static paymentCapture(orderId: string, orderPaymentId: string): TResponse<null> {
		return CustomFetch.post(URLS.ORDER.PAYMENT_CAPTURE(orderId, orderPaymentId));
	}
}
