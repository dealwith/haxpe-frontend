export interface IOrderCreateData {
	customerId: string;
	addressId: string;
	serviceTypeId: number;
	paymentMethod: 'bar';
}
