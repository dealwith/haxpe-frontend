export interface IPaymentCreate {
	amount: number;
	currency: string;
	orderPaymentId: string;
}
