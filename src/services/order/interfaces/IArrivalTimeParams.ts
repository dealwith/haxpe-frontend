export interface IArrivalTimeParams {
	orderId: string;
	expectedArrival: string;
}
