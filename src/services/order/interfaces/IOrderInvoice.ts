import { TExtraServices } from 'interfaces/order/IOrderAbstraction';

export interface IOrderInvoice {
	orderId: string;
	assignedFee: number;
	workingTime: number;
	extraServices: TExtraServices;
	tax: number;
	totalWithoutCoupon: number;
	totalWithCoupon: number;
	total: number;
}
