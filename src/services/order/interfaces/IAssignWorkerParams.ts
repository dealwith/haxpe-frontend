export interface IAssignWorkerParams {
	workerId: string;
	orderId: string;
}
