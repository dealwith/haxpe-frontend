import { TOrderStatuses } from 'interfaces';

export interface IGetOrderParams {
	WorkerId?: string;
	CustomerId?: string;
	Status?: TOrderStatuses;
	isActive?: boolean;
}
