export interface ICancelOrderData {
	reason?: string;
	comment?: string;
}
