import { GetListParams } from 'react-admin';

import { URLS } from 'constants/index';
import { createAndGetQueryParams } from 'services/utils';
import { CustomFetch } from 'utils';
import {
	TResponse,
	TResponsePage,
	ICoupon,
	ICouponCreateData,
	IResponseDetails,
} from 'interfaces';

export class CouponService {
	static getCouponPage(params: GetListParams): TResponsePage<ICoupon> {
		const queryParams = createAndGetQueryParams(params);

		return CustomFetch.get(URLS.COUPONS.PAGE, { queryParams });
	}

	static getCouponById(id: string): TResponse<ICoupon> {
		return CustomFetch.get(URLS.COUPONS.ID(id));
	}

	static createCoupon(data: ICouponCreateData): TResponse<ICoupon> {
		return CustomFetch.post<ICouponCreateData, ICoupon>(URLS.COUPONS.ORIGIN, data);
	}

	static deleteCoupon(id: string): Promise<IResponseDetails> {
		return CustomFetch.delete(URLS.COUPONS.ID(id));
	}

	static editCoupon(id: string, data: ICouponCreateData): TResponse<ICoupon> {
		return CustomFetch.put(URLS.COUPONS.ID(id), data);
	}
}
