import { IServiceType, TServicesTypeResult, TResponse } from 'interfaces';
import { URLS } from 'constants/index';
import { CustomFetch } from 'utils';

export class ServiceTypeService {
	static getServiceTypeById(id: number): TResponse<IServiceType> {
		return CustomFetch.get(URLS.SERVICE_TYPE.ID(id));
	}

	static getAll(): TResponse<TServicesTypeResult> {
		return CustomFetch.get(URLS.SERVICE_TYPE.ALL);
	}
}
