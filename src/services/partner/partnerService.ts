import { GetListParams } from 'react-admin';

import { URLS } from 'constants/index';
import { createAndGetQueryParams } from 'services/utils';
import { CustomFetch } from 'utils';
import {
	IPartner,
	IPartnerCreate,
	TResponse,
	TResponsePage,
} from 'interfaces';

export class PartnerService {
	static createPartner(data: IPartnerCreate): TResponse<IPartner> {
		return CustomFetch.post(URLS.PARTNER.ORIGIN, {...data});
	}

	//TODO: add data type
	static updatePartner(id: string, data): TResponse<IPartner> {
		return CustomFetch.put(URLS.PARTNER.ID(id), { ...data });
	}

	static getPartnerInfo(): TResponse<IPartner> {
		return CustomFetch.get(URLS.PARTNER.INFO);
	}

	static getPartnerById(id: string): TResponse<IPartner> {
		return CustomFetch.get(URLS.PARTNER.ID(id));
	}

	static getPartnerPage(params: GetListParams): TResponsePage<IPartner> {
		const queryParams = createAndGetQueryParams(params);

		return CustomFetch.get(URLS.PARTNER.PAGE, {queryParams});
	}
}
