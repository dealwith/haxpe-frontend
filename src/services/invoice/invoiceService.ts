import { GetListParams } from 'react-admin';

import { createAndGetQueryParams } from 'services/utils';
import { URLS } from 'constants/urls';
import { CustomFetch } from 'utils';
import { IInvoice, TResponse, TResponsePage } from 'interfaces';

export class InvoiceService {
	//TODO: now not used,need see is it need?
	static getInvoicePage(params: GetListParams): TResponsePage<IInvoice> {
		const queryParams = createAndGetQueryParams(params);

		return CustomFetch.get(URLS.INVOICE.PAGE, {queryParams});
	}

	static createInvoice(id: string): TResponse<IInvoice> {
		return CustomFetch.post(URLS.INVOICE.ID_AS_QUERY(id));
	}
}
