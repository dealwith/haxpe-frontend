import { URLS } from 'constants/index';
import { IIndustry, TResponse, TResponseMany } from 'interfaces';
import { CustomFetch } from 'utils';

export class IndustryService {
	static getIndustryById(id: number): TResponse<IIndustry> {
		return CustomFetch.get(URLS.INDUSTRY.ID(id));
	}

	static getAll(): TResponseMany<IIndustry> {
		return CustomFetch.get(URLS.INDUSTRY.ALL);
	}
}
