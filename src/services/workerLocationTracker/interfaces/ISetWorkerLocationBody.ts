export interface ISetWorkerLocationBody {
	updateDate?: string;
	longitude: number;
	latitude: number;
}
