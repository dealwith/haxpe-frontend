export interface ISetWorkerLocationRes {
	workerId: string;
	updateDate: string;
	longitude: number;
	latitude: number;
	id: string;
	creationDate: string;
}
