import { URLS } from 'constants/index';
import { TResponse } from 'interfaces';
import { postData } from 'utils';
import {
	ISetWorkerLocationBody,
	ISetWorkerLocationParams,
	ISetWorkerLocationRes,
} from './interfaces';

export class WorkerLocationTrackerService {
	static setWorkerLocation(
		params: ISetWorkerLocationParams,
		data: ISetWorkerLocationBody,
	): TResponse<ISetWorkerLocationRes> {
		return postData(URLS.WORKER.SET_LOCATION(params.workerId), { ...data });
	}
}
