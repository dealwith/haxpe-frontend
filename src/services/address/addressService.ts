import { CustomFetch, postData } from 'utils';
import { URLS } from 'constants/index';
import { IAddress, TResponse, TResponseMany } from 'interfaces';
import { ICreateAddressData } from './interfaces';

export class AddressService {
	static createAddress(data: ICreateAddressData): TResponse<IAddress> {
		return postData(URLS.ADDRESS.ORIGIN, {...data});
	}

	static getAddressById(id: string): TResponse<IAddress> {
		return CustomFetch.get(URLS.ADDRESS.ID(id));
	}

	static getAddresses(ids: string[]): TResponseMany<IAddress> {
		const queryParams = {
			addressIds: ids,
		};

		return CustomFetch.get(URLS.ADDRESS.ORIGIN, { queryParams });
	}

	static updateAddress(data: ICreateAddressData): TResponse<IAddress> {
		return CustomFetch.put(URLS.ADDRESS.ORIGIN, data);
	}
}
