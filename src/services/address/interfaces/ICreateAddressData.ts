export interface ICreateAddressData {
	country: string;
	city: string;
	street: string;
	buildingNum: string;
	zipCode: string;
	lon: number;
	lat: number;
	externalId: string;
}
