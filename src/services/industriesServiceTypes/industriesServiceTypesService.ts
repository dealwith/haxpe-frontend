import { TServicesTypeResult, IIndustry } from 'interfaces';
import { IndustryService, ServiceTypeService } from 'services';

type TGetResult = {
	services: TServicesTypeResult;
	industries: IIndustry[];
}

export class IndustriesServiceTypesService {
	private static industries: IIndustry[];
	private static services: TServicesTypeResult;

	static async get(): Promise<TGetResult> {
		const [
			industriesRes,
			servicesRes,
		] = await Promise.allSettled([
			IndustryService.getAll(),
			ServiceTypeService.getAll(),
		]);

		if (industriesRes.status === 'fulfilled')
			this.industries = industriesRes.value.body;

		if (servicesRes.status === 'fulfilled')
			this.services = servicesRes.value.body;

		return {
			services: this.services,
			industries: this.industries,
		};
	}
}
