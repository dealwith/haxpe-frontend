import { CustomFetch } from 'utils';
import { IAccount, TResponse, ISignIn } from 'interfaces';
import { URLS } from 'constants/index';
import { IRegisterData, IUpdateData } from './interfaces';
import { ILoginFacebookData } from './interfaces/ILoginFacebookData';

export class AccountService {
	static register(data: IRegisterData): TResponse<IAccount> {
		return CustomFetch.post(URLS.ACCOUNT.REGISTER, { ...data });
	}

	static getAccount(): TResponse<IAccount> {
		return CustomFetch.get(URLS.ACCOUNT.PROFILE);
	}

	static login(data: ISignIn): TResponse<IAccount> {
		return CustomFetch.post(URLS.ACCOUNT.LOGIN, { ...data });
	}

	static logout(): Promise<any> {
		return CustomFetch.post(URLS.ACCOUNT.LOGOUT);
	}

	static loginGoogle(accessToken: string): TResponse<IAccount> {
		return CustomFetch.post(URLS.ACCOUNT.LOGIN_GOOGLE, { accessToken });
	}

	static loginFacebook(data: ILoginFacebookData): TResponse<IAccount> {
		return CustomFetch.post(URLS.ACCOUNT.LOGIN_FACEBOOK, { ...data });
	}

	static getWSToken(): TResponse<string> {
		return CustomFetch.get(URLS.ACCOUNT.WS_TOKEN);
	}

	static updateAccount(data: IUpdateData): TResponse<IAccount> {
		return CustomFetch.put(URLS.ACCOUNT.PROFILE, data);
	}

	static updateAccountById(id: string, data: IUpdateData): TResponse<IAccount> {
		return CustomFetch.put(URLS.ACCOUNT.ID(id), { ...data });
	}

	static activate(id: string): TResponse<void> {
		return CustomFetch.put(URLS.ACCOUNT.ACTIVATE(id));
	}

	static deactivate(id: string): TResponse<void> {
		return CustomFetch.put(URLS.ACCOUNT.DEACTIVATE(id));
	}

	static resetPassword(password: string): TResponse<void> {
		return CustomFetch.post(URLS.ACCOUNT.SET_PASSWORD, { NewPassword: password });
	}

	static deleteAccount(): TResponse<void> {
		return CustomFetch.delete(URLS.ACCOUNT.ORIGIN);
	}

	static updateLanguage(data: { preferLanguage: string }): TResponse<IAccount> {
		return CustomFetch.put(URLS.ACCOUNT.PROFILE, data);
	}

	static setNewPassword(email: string): TResponse<IAccount> {
		return CustomFetch.post(URLS.ACCOUNT.SET_NEW_PASSWORD(email));
	}
}
