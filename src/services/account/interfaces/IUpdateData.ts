export interface IUpdateData {
	email?: string;
	name?: string;
	surname?: string;
	phoneNumber?: string;
	preferLanguage?: string;
}
