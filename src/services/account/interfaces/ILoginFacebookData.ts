export interface ILoginFacebookData {
	accessToken: string;
	userId: string;
}
