export interface IRegisterData {
	email: string;
	password: string;
	firstName: string;
	lastName: string;
	phone: string;
	preferLanguage: string;
}
