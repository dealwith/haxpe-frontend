import { GetListParams } from 'react-admin';

import { camelizeFirstLetter } from 'utils';

type TPageQueryParams = {
	pageNumber: number;
	pageSize: number;
	sortedBy: string;
	sortDirection: string;
	searchValue?: string;
}

export const createAndGetQueryParams = ({
	pagination: {
		page,
		perPage,
	},
	sort: {
		field,
		order,
	},
	filter: {
		q,
	},
}: GetListParams): TPageQueryParams  => ({
	pageNumber: page - 1,
	pageSize: perPage,
	sortedBy: camelizeFirstLetter(field),
	sortDirection: order,
	...(q && { searchValue: q }),
});
