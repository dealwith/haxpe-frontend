import { URLS } from 'constants/index';
import { EResponseStatus, IUser, TResponse, TResponseMany } from 'interfaces';
import { WorkerService } from 'services';
import { CustomFetch } from 'utils';

export class UserService {
	static getUserById(id: string): TResponse<IUser> {
		return CustomFetch.get(URLS.USER.ID(id));
	}

	static getUsers(ids: string[]): TResponseMany<IUser> {
		return CustomFetch.get(URLS.USER.ORIGIN, {queryParams: { userIds: ids }});
	}

	static async getUserByWorkerId(workerId: string): TResponse<IUser> {
		const { body: workerRes, status: workerStatus } = await WorkerService.getWorkerById(workerId);

		if (workerStatus === EResponseStatus.success)
			return this.getUserById(workerRes.userId);
	}
}
