import { GetListParams } from 'react-admin';

import { URLS } from 'constants/index';
import { createAndGetQueryParams } from 'services/utils';
import { CustomFetch } from 'utils';
import { TResponse, TResponsePage, ICustomer, TResponseMany } from 'interfaces';
import { ICreateCustomerData } from './interfaces';

export class CustomerService {
	static getCustomers(ids: string[]): TResponseMany<ICustomer> {
		return CustomFetch.get(URLS.CUSTOMER.ORIGIN, { queryParams: { CustomerIds: ids } });
	}
	static createCustomer(data: ICreateCustomerData): TResponse<ICustomer> {
		return CustomFetch.post(URLS.CUSTOMER.ORIGIN, { ...data });
	}

	static getCustomerInfo(): TResponse<ICustomer> {
		return CustomFetch.get(URLS.CUSTOMER.INFO);
	}

	static getCustomerPage(params: GetListParams): TResponsePage<ICustomer> {
		const queryParams = createAndGetQueryParams(params);

		return CustomFetch.get(URLS.CUSTOMER.PAGE, {queryParams});
	}

	static getCustomerById(id: string): TResponse<ICustomer> {
		return CustomFetch.get(URLS.CUSTOMER.ID(id));
	}
}
