import { CustomFetch, ModifiedDate } from 'utils';
import { URLS } from 'constants/index';
import { TRange, TResponseMany, IStatistic } from 'interfaces';

const { createDateToShow } = new ModifiedDate();

const getQueryParams = ({startDate, endDate}: TRange) => {
	return {
		startDate: createDateToShow({
			dateArg: startDate,
			splitArg: '/',
			format: 'y:m:d',
		}),
		endDate: createDateToShow({
			dateArg: endDate,
			splitArg: '/',
			format: 'y:m:d',
		}),
	};
};

export class StatisticService {

	static getCustomerChart(date: TRange): TResponseMany<IStatistic> {
		const queryParams = getQueryParams(date);

		return CustomFetch.get(URLS.STATISTICS.CUSTOMER, { queryParams });
	}

	static getOrderChart(date: TRange): TResponseMany<IStatistic> {
		const queryParams = getQueryParams(date);

		return CustomFetch.get(URLS.STATISTICS.ORDER, { queryParams });
	}

	static getPartnerChart(date: TRange): TResponseMany<IStatistic> {
		const queryParams = getQueryParams(date);

		return CustomFetch.get(URLS.STATISTICS.PARTNER, { queryParams });
	}

	static getWorkerChart(date: TRange): TResponseMany<IStatistic> {
		const queryParams = getQueryParams(date);

		return CustomFetch.get(URLS.STATISTICS.WORKER, { queryParams });
	}
}
