import { TUnit } from './TUnit';

export interface ICouponCreateData {
	code: string;
	expirationDate: string;
	value: number;
	unit: TUnit;
}
