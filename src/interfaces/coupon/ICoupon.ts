import { TUnit } from './TUnit';

export interface ICoupon {
	code: string;
	expirationDate: string;
	createdDate: string;
	isDeleted: boolean;
	value: number;
	unit: TUnit;
	id: string;
	creationDate: string;
}
