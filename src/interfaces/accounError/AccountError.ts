enum ErrorCode {
	accountInvalidUserNameOrPassword = 'accountInvalidUserNameOrPassword',
}

export type TErrorCodes = keyof typeof ErrorCode;

export interface AccountError {
	status: 'error';
	message: TErrorCodes;
}
