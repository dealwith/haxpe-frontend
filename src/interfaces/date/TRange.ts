import { TDate } from './TDate';

export type TRange = {
	startDate: TDate;
	endDate: TDate;
}
