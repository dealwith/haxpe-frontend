export type TDateFormat = 'y:m:d'
| 'y:d:m'
| 'm:y:d'
| 'm:d:y'
| 'd:m:y'
| 'd:y:m';
