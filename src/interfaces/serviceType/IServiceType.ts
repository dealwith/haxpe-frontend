export interface IServiceType {
	id: number;
	industryId: number;
	key: string;
	creationDate: string;
}

export type TServicesTypeResult = IServiceType[];
