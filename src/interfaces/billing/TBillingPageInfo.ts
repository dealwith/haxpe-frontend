import { IAddress, IUser } from 'interfaces';
import { IBilling } from './IBilling';

export type TBillingPageInfo = IBilling
& {
	user?: IUser;
	address?: IAddress;
};
