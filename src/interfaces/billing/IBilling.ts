export interface IBilling {
	creationDate: string;
	id: string;
	partnerId: string;
	income: number;
	commission: number;
	amount: number;
	status: string;
}
