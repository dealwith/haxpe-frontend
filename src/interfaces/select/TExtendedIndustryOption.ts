import { TSelectOption } from 'interfaces';

export type TExtendedIndustryOption = TSelectOption & { id?: number };
