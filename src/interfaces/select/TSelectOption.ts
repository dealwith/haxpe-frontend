export type TSelectOption = {
	label: string;
	value: number;
}
