import { TSelectOption } from './TSelectOption';

export type TExtendedServiceOption = TSelectOption & {
	id?: number;
	industryId?: number;
};
