import { ReactNode } from 'react';

import { TPosition } from 'interfaces';
import { TCentered, TFlow, TTheme, TFlexDirection } from './interfaces';

export interface IContainer {
	children?: ReactNode;
	centered?: TCentered;
	className?: string;
	isInner?: boolean;
	maxWidth?: string | number;
	theme?: TTheme;
	isFitContent?: boolean;
	flow?: TFlow;
	height?: number;
	maxHeight?: string | number;
	minHeight?: number;
	position?: TPosition;
	isWrap?: boolean;
	isDefault?: boolean;
	flexDirection?: TFlexDirection;
	isFillContent?: boolean;
}
