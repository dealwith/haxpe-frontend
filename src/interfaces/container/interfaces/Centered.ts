export enum ECentered {
	vertical = 'vertical',
	horizontal = 'horizontal',
	center = 'center',
	'center-column' = 'center-column',
	'space-between' = 'space-between',
	'flex-start' = 'flex-start',
}

export type TCentered = keyof typeof ECentered;
