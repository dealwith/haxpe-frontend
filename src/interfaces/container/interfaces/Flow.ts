export enum EFlow {
	vertical = 'vertical',
	horizontal = 'horizontal',
}

export type TFlow = keyof typeof EFlow;
