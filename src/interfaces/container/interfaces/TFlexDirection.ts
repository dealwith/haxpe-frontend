enum EFlexDirection {
	column = 'column',
	row = 'row',
}

export type TFlexDirection = keyof typeof EFlexDirection;
