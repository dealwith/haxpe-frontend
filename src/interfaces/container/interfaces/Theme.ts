export enum Theme {
	white = 'white',
	gray = 'gray',
	green = 'green',
	'gray-green' = 'gray-green',
	warn = 'warn',
	yellow = 'yellow',
	transparent = 'transparent',
	blue = 'blue',
	black = 'black',
}

export type TTheme = keyof typeof Theme;
