import { ReactNode } from 'react';

import { IContainer, TPosition } from 'interfaces';
import { TCentered, TTheme } from './interfaces';

export interface IContentContainer {
	children?: ReactNode;
	centered?: TCentered;
	className?: string;
	maxWidth?: string | number;
	theme?: TTheme;
	position?: TPosition;
	height?: number | 'auto';
	isWrap?: boolean;
	minHeight?: number;
	flow?: IContainer['flow'];
	isInner?: boolean;
	isDefault?: boolean;
}
