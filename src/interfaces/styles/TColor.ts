export enum EColor {
	white = 'white',
	gray = 'gray',
	black = 'black',
	'light-green' = 'light-green',
	'dark-gray' = 'dark-gray',
}

export type TColor = keyof typeof EColor;
