export type TOrderInvoiceTableInputWidth = {
	small: number;
	normal: number;
	large: number;
}
