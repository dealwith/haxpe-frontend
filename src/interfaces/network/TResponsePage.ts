import { IResponseDetails } from './IResponseDetails';

interface IResponsePageBody<T> {
	result: T[];
	totalCount: number;
}

interface IResponsePage<T> extends IResponseDetails {
	body: IResponsePageBody<T>;
}

export type TResponsePage<T> = Promise<IResponsePage<T>>;
