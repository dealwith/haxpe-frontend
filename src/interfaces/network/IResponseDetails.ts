import { EResponseStatus } from './';

export enum EErrorCodes {
	accountDuplicateUserName = 'accountDuplicateUserName',
	accountExternalUserPasswordChange = 'accountExternalUserPasswordChange',
	accountInvalidUserNameOrPassword = 'accountInvalidUserNameOrPassword',
	accountNotAllowed = 'accountNotAllowed',
	accountLockedOut = 'accountLockedOut',
	accountRequiresTwoFactor = 'accountRequiresTwoFactor',
	accountInvalidEmailAddress = 'accountInvalidEmailAddress',
	accountFacebookNoEmail = 'accountFacebookNoEmail',
	accountGoogleNoEmail = 'accountGoogleNoEmail',
	customerNotFound = 'customerNotFound',
	orderNotFound = 'orderNotFound',
	orderAssignWorkerFromOtherPartner = 'orderAssignWorkerFromOtherPartner',
	orderWorkflowViolation = 'orderWorkflowViolation',
	tooManyObjectsToReturn = 'tooManyObjectsToReturn',
}

export interface IResponseDetails {
	status: keyof typeof EResponseStatus;
	errorCode: keyof typeof EErrorCodes;
}
