import { IResponseDetails } from './IResponseDetails';

interface IResponse<T> extends IResponseDetails {
	body: T;
}

export type TResponse<T> = Promise<IResponse<T>>;
// eslint-disable-next-line @typescript-eslint/no-redeclare
export const TResponse = Promise;
