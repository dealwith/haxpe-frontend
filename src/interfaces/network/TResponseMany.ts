import { EErrorCodes, EResponseStatus } from './';

export interface IResponseMany<T> {
	body: T[];
	status: keyof typeof EResponseStatus;
	errorCode: keyof typeof EErrorCodes;
}

export type TResponseMany<T> = Promise<IResponseMany<T>>;
