import {
	IUser,
	IIndustry,
	TServicesTypeResult,
	IAddress,
	IWorker,
	IServiceTypesId,
	IIndustryId,
} from 'interfaces';

export type TWorkerPageInfo = IWorker
& {
	user?: IUser,
	services?: TServicesTypeResult,
	industry?: IIndustry,
	address?: IAddress,
	partnerServiceTypes?: IServiceTypesId[],
	partnerIndustries?: IIndustryId[],
}
