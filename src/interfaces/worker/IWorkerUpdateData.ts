import { IServiceTypesId } from 'interfaces';

export type IWorkerUpdateData = {
	partnerId: string;
	userId: string;
	serviceTypes: IServiceTypesId[];
}
