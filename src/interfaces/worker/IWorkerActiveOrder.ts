import { TAvailableStatus } from 'components/Order/Worker/ActiveOrder/interfaces';

export interface IWorkerActiveOrder {
	id: string;
	serviceTypeId: number;
	orderStatus: TAvailableStatus;
}
