import { IMultiServiceState } from 'interfaces';

export interface IWorkerCreateForm {
	setMultiServiceState: (arg: Record<string, unknown>) => void;
	multiServiceState: IMultiServiceState;
}
