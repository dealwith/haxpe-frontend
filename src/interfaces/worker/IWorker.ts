import { IServiceTypesId } from 'interfaces';

export interface IWorker {
	partnerId: string;
	userId: string;
	serviceTypes: IServiceTypesId[];
	id: string;
	creationDate: string;
}
