export type TBeforeWorkCompletedReturnType = {
	orderId: string;
	fullName: string;
	address: string;
	phoneNumber: string;
	service: string;
	industry: string;
}
