import {
	TAddService,
	TEditService,
	TExtraServices,
	TRemoveService,
} from 'interfaces';

export type TAfterWorkCompletedReturnType = {
	additionalServices: TExtraServices;
	reduction: number;
	report: string;
	editService: TEditService;
	addService: TAddService;
	removeService: TRemoveService;
	handleAddReduction: (reduction: number) => void;
	handleAddReport: (coupon: string) => void;
	handleClearReport: () => void;
}
