import { TIndustryResult } from './industry';
import { TServicesTypeResult } from './serviceType';

type TExtendedIndustryOption = {
	value?: number;
	name?: string;
}

type TExtendedServiceOption = {
	value?: number;
	name?: string;
}

export interface IMultiServiceState {
	industries: TExtendedIndustryOption[];
	services: TExtendedServiceOption[];
	responseServices: TServicesTypeResult;
	responseIndustries: TIndustryResult;
	selectedIndustry: TExtendedIndustryOption;
	selectedService: TExtendedServiceOption;
}
