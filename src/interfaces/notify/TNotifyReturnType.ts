
export type TNotify = {
	isNotify: boolean;
	errorText: string;
}

export type TNotifyReturnType = {
	notify: TNotify;
	handleSetNotify: (notify: TNotify) => void;
}
