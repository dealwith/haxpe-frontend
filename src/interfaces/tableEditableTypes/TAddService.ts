import { TExtraService } from 'interfaces';

export type TAddService = (data: TExtraService) => void;
