import { TExtraService } from 'interfaces';

export type TEditService = (index: number, data: TExtraService) => void;
