import { TLanguages, TRoles } from 'interfaces';
export interface IAccount {
	id: string;
	email: string;
	hasPassword: boolean;
	isExternal: boolean;
	name: string;
	partnerId: null | string;
	phoneNumber: null | string;
	preferLanguage: TLanguages;
	roles: TRoles[];
	surname: string;
	userName: string;
}
