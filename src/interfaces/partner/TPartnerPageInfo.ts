import {
	IAddress,
	IPartner,
	IUser,
	TIndustryResult,
	TServicesTypeResult,
} from 'interfaces';

export type TPartnerPageInfo = {
	id: string;
	partner?: IPartner;
	address?: IAddress;
	industries?: TIndustryResult;
	services?: TServicesTypeResult;
	user?: IUser;
	partnerWorkers?: IUser[];
}
