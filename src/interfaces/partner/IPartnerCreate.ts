import { IIndustryId } from 'interfaces';

export interface IPartnerCreate {
	name: string;
	description: string;
	addressId: string;
	ownerUserId: string;
	numberOfWorkers: number;
	industries: IIndustryId[];
}
