import { IPartnerCreate } from './IPartnerCreate';
import { IServiceTypesId } from 'interfaces';

export interface IPartner extends IPartnerCreate {
	id: string;
	creationDate: string;
	serviceTypes: IServiceTypesId[];
}
