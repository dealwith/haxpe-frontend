import {
	IAddress,
	ICustomer,
	IOrder,
	IUser,
} from 'interfaces';

export type TCustomerPageInfo = ICustomer
& { user?: IUser }
& { address?: IAddress }
& { order?: IOrder & { latestOrderCreationDate: 'string' } }
