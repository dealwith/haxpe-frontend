export interface ICustomer {
	id: string;
	userId: string;
	addressId: string;
	creationDate: string;
	latestOrderDate: string;
}
