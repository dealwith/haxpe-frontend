export interface IGeometry {
	bounds: {
		northeast: {
			lat: number,
			lng: number
		},
		southwest: {
			lat: number,
			lng: number
		}
	},
	location: {
		lat: number,
		lng: number
	},
	location_type: 'GEOMETRIC_CENTER' | string,
	viewport: {
		northeast: {
			lat: number
			lng: number
		},
		southwest: {
			lat: number,
			lng: number
		}
	}
}
