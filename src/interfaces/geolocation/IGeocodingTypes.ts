import { IGeometry } from './IGeometry';

interface IAddressComponent {
	long_name: string;
	short_name: string;
	types: string[];
}

export interface IGeocodeResult {
	address_components: IAddressComponent[];
	formatted_address: string;
	geometry: IGeometry;
	place_id: string;
	types: string[];
}
