import { MouseEvent } from 'react';

export interface IOnClick {
	onClick: <T extends HTMLElement>(e: MouseEvent<T>) => void;
}
