import { IOrderRes } from './IOrderRes';

export type TSelectedOrderReturnType = {
	selectedOrder: IOrderRes;
	changeSelectedOrder: (orderId: string) => Promise<void>;
	clearSelectedOrder: () => void;
}
