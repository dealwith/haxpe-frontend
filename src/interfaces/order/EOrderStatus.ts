export enum EOrderStatus {
	draft = 'draft',
	created = 'created',
	reserved = 'reserved',
	inProgress = 'inProgress',
	completed = 'completed',
	systemError = 'systemError',
	rejectByCustomer = 'rejectByCustomer',
	rejectByWorker = 'rejectByWorker',
	canceled = 'canceled',
	paused = 'paused',
	workerFounded = 'workerFounded',
	workerOnWay = 'workerOnWay',
	workerArrived = 'workerArrived',
	workCompleted = 'workCompleted',
	payment = 'payment',
}

export type TOrderStatuses = keyof typeof EOrderStatus;
