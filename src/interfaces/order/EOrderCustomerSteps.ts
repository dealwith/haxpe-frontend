export enum ECustomerOrderSteps {
	orderCreation = 'orderCreation',
	orderDetails = 'orderDetails',
	searchWorker = 'searchWorker',
	canceledOrder = 'canceledOrder',
	cancelReason = 'cancelReason',
	workerFounded = 'workerFounded',
	thanksForOrderWorkerInOnHisWayToYou = 'thanksForOrderWorkerInOnHisWayToYou',
	yourCraftsmanIsOnTheWay = 'yourCraftsmanIsOnTheWay',
	paymentData = 'paymentData',
	thanksForReview = 'thanksForReview',
	reserved = 'reserved',
}

export type TCustomerOrderSteps = keyof typeof ECustomerOrderSteps;
