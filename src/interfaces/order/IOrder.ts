import { IOrderAbstraction } from './IOrderAbstraction';

export interface IOrder extends IOrderAbstraction {
	industry: string;
	serviceType: string;
}
