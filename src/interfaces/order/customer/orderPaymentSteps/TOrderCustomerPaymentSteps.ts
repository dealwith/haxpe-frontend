import { EOrderCustomerPaymentSteps } from '.';

export type TOrderCustomerPaymentSteps = keyof typeof EOrderCustomerPaymentSteps;
