export enum EOrderCustomerPaymentSteps {
	invoice = 'invoice',
	paymentData = 'paymentData',
	paymentMethods = 'paymentMethods',
	cardCheckout = 'cardCheckout',
	GPayApplePay = 'GPayApplePay',
}
