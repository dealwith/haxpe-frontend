import {
	IAddress,
	IIndustry,
	IOrderRes,
	IServiceType,
	IUser,
} from 'interfaces';

export type TOrderPageInfo = IOrderRes
& { user?: IUser }
& { address?: IAddress }
& { industry?: IIndustry }
& { service?: IServiceType }
