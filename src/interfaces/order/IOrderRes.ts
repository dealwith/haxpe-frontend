import { IOrderAbstraction } from './IOrderAbstraction';

export interface IOrderRes extends IOrderAbstraction {
	industryId: number;
	serviceTypeId: number;
}
