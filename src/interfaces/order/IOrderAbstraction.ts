import { IInvoice } from 'interfaces';
import { TOrderStatuses } from './EOrderStatus';

type net = number;
type tax = number;
type total = number;

export type TExtraService = {
	name: string;
	value: [net, tax, total];
}

export type TWorkerDistance = {
	workerId: string;
	distance: string;
}

export type TExtraServices = TExtraService[];

export type TWorkerDistances = TWorkerDistance[];

export interface IOrderAbstraction {
	id: string;
	partnerId: string;
	workerId: string;
	customerId: string;
	addressId: string;
	orderDate: string;
	duration: string;
	reduction: number;
	paymentMethod: 'bar';
	rating: number;
	comment: string;
	report: string;
	orderStatus: TOrderStatuses;
	couponId: string;
	couponCode: string;
	cancelReason: string;
	expectedArrival: string;
	extraServices: TExtraServices;
	workerDistances: TWorkerDistances;
	invoice: IInvoice;
	creationDate: string;
}
