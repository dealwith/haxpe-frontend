export interface IRate {
	comment?: string;
	rating?: number;
}
