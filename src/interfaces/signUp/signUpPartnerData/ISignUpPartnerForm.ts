import { IFile, TSelectOption, IIndustryId } from 'interfaces';

export interface ISignUpPartnerForm {
	buildingNum: string;
	city: string;
	companyName: string;
	confirmPassword: string;
	craftsmanAmount: number;
	email: string;
	firstName: string;
	lastName: string;
	password: string;
	phone: string;
	street: string;
	zipCode: string;
	files: IFile[];
	industries: TSelectOption & IIndustryId[];
}
