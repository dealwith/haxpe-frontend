export type IImage = {
	fileName: string;
	fileType: string;
	creationDate: string;
	id: string;
  }
