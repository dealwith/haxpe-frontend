export interface IPayment {
	amount: number;
	currency: string;
	stripeKey: string;
}
