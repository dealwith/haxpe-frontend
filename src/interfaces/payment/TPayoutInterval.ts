export enum EPayoutInterval {
	monthly = 'monthly',
	weekly = 'weekly',
}

export type TPayoutInterval = keyof typeof EPayoutInterval;
