export interface IAddress {
	id: string;
	country: string,
	city: string,
	street: string,
	buildingNum: string,
	zipCode: string,
	lon: number;
	lat: number;
	externalId: string,
	creationDate: string;
}
