export interface IStatistic {
	creationDate: string;
	count: number;
}
