export interface IIndustry {
	key: string;
	id: number;
	creationDate: string;
}

export type TIndustryResult = IIndustry[];
