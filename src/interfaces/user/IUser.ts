//TODO: isLocked will been replaced into API and add
export enum EAccountStatus {
	activated = 'activated',
	deactivated = 'deactivated',
	requested = 'requested',
}

type TAccountStatus = keyof typeof EAccountStatus;

export interface IUser {
	id: string;
	name: string;
	surname: string;
	fullName: string;
	partnerId: string;
	preferLanguage: string;
	isExternal: boolean;
	creationDate: string;
	email: string;
	phoneNumber: string;
	isLocked: boolean;
	accountStatus?: TAccountStatus;
}
