import { IOrderRes } from 'interfaces';

enum EType {
	'order.offer' = 'order.offer',
	'order.changed' = 'order.changed',
}

interface IPayload {
	order: IOrderRes;
	creationDate: string;
	orderId: string;
}
export interface IWorkerSocket {
	data: {
		type: keyof typeof EType;
		payload: IPayload;
	};
	gen: undefined;
	offset: undefined;
	seq: undefined;
}

export type TSocketReturnType = {
	orderOffer: IOrderRes;
	orderChanged: IOrderRes;
	clearOrderOffer: () => void;
	clearOrderChanged: () => void;
}
