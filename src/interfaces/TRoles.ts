export enum ERoles {
	admin = 'admin',
	partner = 'partner',
	worker = 'worker',
	customer = 'customer',
}

export type TRoles = keyof typeof ERoles;
