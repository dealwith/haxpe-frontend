import { TExtraServices } from 'interfaces';

type net = number;
type tax = number;
type total = number;
type amount = [net, tax, total];

export interface IInvoice {
	orderId: string;
	assignedFee: amount;
	workingTime: amount;
	workingTimeQuantity: number;
	serviceStart: string;
	serviceEnd: string;
	extraServices: TExtraServices;
	net: number;
	tax: number;
	totalWithoutCoupon: number;
	totalWithCoupon: number;
	total: number;
	id: number;
	creationDate: string;
}
