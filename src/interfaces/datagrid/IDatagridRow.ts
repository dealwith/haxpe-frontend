import { ReactElement } from 'react';

import { TDatagridRowSection } from './TDatagridRowSection';
import { Dictionary } from '../Dictionary';

export interface IDatagridRow {
	title?: Dictionary<string | string[]>;
	body?: TDatagridRowSection[][];
	footerButtonsGroup?: HTMLElement[] | ReactElement[];
}
