export interface IStarRating {
	name: string;
	value: number;
	starCount?: number;
	onStarClick?: (nextValue: number, prevValue: number, name: string) => void;
	onStarHover?: (nextValue: number, prevValue: number, name: string) => void;
	onStarHoverOut?: (nextValue: number, prevValue: number, name: string) => void;
	renderStarIcon?: (
		nextValue: number,
		prevValue: number,
		name: string
	) => React.ReactNode | string;
	renderStarIconHalf?: (
		nextValue: number,
		prevValue: number,
		name: string
	) => React.ReactNode | string;
	starColor?: string;
	emptyStarColor?: string;
	editing?: boolean;
}
