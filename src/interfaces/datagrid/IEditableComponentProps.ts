import { TDatagridRowSectionBody } from './TDatagridRowSectionBody';

export interface IEditableComponentProps<T = void> {
	handleClose?: () => void;
	body?: TDatagridRowSectionBody;
	pageData?: T;
	record?: T;
	textArray?: string[];
	textArrayTitle?: string;
	editedFieldName?: string;
}
