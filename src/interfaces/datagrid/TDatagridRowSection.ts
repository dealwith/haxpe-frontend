import { FunctionComponent } from 'react';

import { TColor, TEither } from 'interfaces';
import { IEditableComponentProps } from './IEditableComponentProps';
import { TDatagridRowSectionBody } from './TDatagridRowSectionBody';
import { IStarRating } from './IStarRating';

export type TDatagridRowSection = {
	title?: string;
	body?: TDatagridRowSectionBody;
	textArray?: string[];
	color?: TColor;
	rate?: IStarRating;
	isEditable?: boolean;
} & TEither<{
	editedFieldName?: string;
}, {
	component?: FunctionComponent<IEditableComponentProps> ;
}>
