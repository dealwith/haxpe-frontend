import { Dictionary } from 'interfaces';
import { IStarRating } from './IStarRating';

export type TDatagridRowSectionBody = Dictionary<string | IStarRating>[];
