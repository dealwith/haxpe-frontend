import { IAccount, IAddress } from 'interfaces';

export type TSettingsPageInfo = {
	address?: IAddress;
	account?: IAccount;
};
