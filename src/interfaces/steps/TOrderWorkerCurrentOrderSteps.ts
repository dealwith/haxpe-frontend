import { ReactElement } from 'react';

export type TOrderWorkerCurrentOrderSteps = {
	arrived: 'workerArrived' | ReactElement;
	reserved: ['reserved', 'workerFounded', 'workerOnWay', 'created'] | ReactElement;
	inProgress: ['inProgress', 'paused'] | ReactElement;
	workCompleted: 'workCompleted' | ReactElement;
	sendInvoice: 'sendInvoice' | ReactElement;
	orderCancelReasonForm: 'orderCancelReasonForm' | ReactElement;
}
