import { ReactElement } from 'react';

export type TActiveOrderSteps = {
	orderList: 'orderList' | ReactElement;
	incomingRequest: 'incomingRequest' | ReactElement;
}
