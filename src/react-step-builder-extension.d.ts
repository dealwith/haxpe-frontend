import 'react-step-builder';

declare module 'react-step-builder' {
    type JumpFn = (step: number) => void;
    type AllSteps = {
        order: number;
        title: string;
    }[];
    type GetState = (key: keyof State, defaultValue: State[keyof State]) => any;
    type SetState = (key: keyof State, value: State[keyof State]) => void;
}
