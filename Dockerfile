FROM nikolaik/python-nodejs:latest as build
ENV NODE_ENV=production
WORKDIR /app
COPY package*.json /app
COPY yarn.lock /app
RUN yarn install
COPY . /app
RUN yarn build

FROM nginx:alpine
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]